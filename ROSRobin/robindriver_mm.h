// --------x----------------------------x-----------------------------------x--
/**
 ** \file   robindriver.h
 **
 ** \author Markus Joos, CERN-PH/ESS
 **
 ** \date   4. Feb. 05
 **
 */
// --------x----------------------------x-----------------------------------x--


/**
 * Description:
 * This is the private header file for ROBIN driver
 */

#ifndef ROSROBIN_ROBINDRIVER_H
#define ROSROBIN_ROBINDRIVER_H

#include <linux/types.h>
/* #include <robin_ppc/robin_vhdl_map_ver4.hxx> */
/*************/
/* Constants */
/*************/
#define PCI_VENDOR_ID_ROBIN2 0x10b5
#define PCI_DEVICE_ID_ROBIN2 0x9656

#define PCI_VENDOR_ID_ROBIN3 0x10dc
#define PCI_DEVICE_ID_ROBIN3 0x0144

#define FPGA_REG_OFFSET      0
#define MESSAGE_RAM_OFFSET   0x10000
#define MESSAGE_RAM_SIZE     0x2000
#define MESSAGE_FIFO_OFFSET  0x14000
#define MESSAGE_FIFO_SIZE    4
#define MESSAGE_FIFO_DEPTH   32

/*
#define FPGA_REG_OFFSET      0
#define MESSAGE_RAM_OFFSET   (PCI_DPM_RANGE << (PCI_RANGE_ADDRESS_SIZE + 2))
#define MESSAGE_RAM_SIZE     (1 << (PCI_DPM_SIZE + 2))
#define MESSAGE_FIFO_OFFSET  0x14000
#define MESSAGE_FIFO_SIZE    4
#define MESSAGE_FIFO_DEPTH   32
*/

#define PLX_REG_SIZE         512

// User modifyable constants
#define MAXCARDS             10   // Max. number of ROBIN cards
#define MAXCHANNELS          3    // Max. number of channes per ROBIN card
#define MAXROLS              (MAXCARDS * MAXCHANNELS)

//H/W constants
#define MESSAGE_RAM_SIZE     0x2000
#define MESSAGE_FIFO_SIZE    4
#define MESSAGE_FIFO_DEPTH   32

//For the memory management
#define MAX_BLOCKLETS        10
#define BLOCKLET_UNUSED      0  
#define BLOCKLET_FREE        1
#define BLOCKLET_USED        2
#define BLOCK_TYPES          2


/***************/
/*PLX registers*/
/***************/
enum 
{
  PLX_LAS0RR = 0,
  PLX_LAS0BA = 1,
  PLX_DMRR   = 7,
  PLX_DMLBAM = 8,
  PLX_DMPBAM = 10,
  PLX_INTCSR = 26,
  PLX_CNTRL  = 27
};

/****************/
/*FPGA registers*/
/****************/
enum 
{
  FPGA_FW_VERSION = 0
};



/********/
/*Macros*/
/********/
#ifdef ROBIN_DEBUG
  #define kdebug(x) {if (debug) printk x;}
#else
  #define kdebug(x)
#endif

#ifdef ROBIN_ERROR_DEBUG
  #define kerror(x) {if (errorlog) printk x;}
#else
  #define kerror(x)
#endif


/******************************/
/*Standard function prototypes*/
/******************************/
static int robindriver_init(void);
static void robindriver_exit(void);
static int robin_open(struct inode * ino, struct file * file);
static int robin_release(struct inode * ino, struct file * file);
static int robin_ioctl(struct inode * inode, struct file * file, u_int cmd, unsigned long arg);
static int robin_mmap(struct file * file, struct vm_area_struct * vma);
static void robin_vclose(struct vm_area_struct * vma);


/*****************************/
/*Special function prototypes*/
/*****************************/
//static int init_robins(void);
//static int cleanup_robins(void);
static int initmem(u_int card, u_int size, u_int base);
static int getmem(u_int card, u_int type, u_int size, u_int *start_add, u_int *handle);
static int retmem(u_int card, u_int type, u_int handle);


/****************************/
/* Exports                  */
/****************************/
static int readFpga(int card, unsigned int addressOffset);
static void writeFpga(int card, unsigned int addressOffset, int data);
static int fpgaLoaded(int card);
static int getCardNum(void);

#endif

/************ C 2005 - The software with that certain something **************/


/* $Id$
 * $Revision$
 * $Log$
 * Revision 1.1  2005/08/04 16:24:53  mmueller
 * Added robintty driver
 *
 * Revision 1.11  2005/06/22 16:13:58  joos
 * bug fixes and other changes
 *
 * Revision 1.10  2005/06/10 13:06:54  joos
 * globally: bug fixes and other modifications. The code contains still stuff for debugging and has to be cleaned up
 *
 * Revision 1.9  2005/05/30 08:55:18  joos
 * MAXCARDS set to 10
 *
 * Revision 1.8  2005/05/23 12:46:16  joos
 * lots of small changes and bug fixes
 *
 * Revision 1.7  2005/05/10 12:54:16  joos
 * modifications and bug fixes
 *
 * Revision 1.6  2005/04/27 13:34:57  joos
 * Lots of new code. Rol.cpp does not compile
 *
 * Revision 1.5  2005/04/20 14:16:58  joos
 * lots of new code. Not yet tested
 *
 * Revision 1.4  2005/04/14 15:00:59  joos
 * lotsof modification and new code. Some first tests done
 *
 * Revision 1.3  2005/04/14 07:06:00  joos
 * lots of new untested code
 *
 * Revision 1.2  2005/04/11 11:24:15  joos
 * Modifications and new files of Matthias
 *
 * Revision 1.1.1.1  2005/04/07 11:34:38  joos
 * added ROSRobin
 *
 *
 */

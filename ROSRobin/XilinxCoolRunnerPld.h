// --------x----------------------------x-----------------------------------x--
/**
 ** \file   XilinxCoolRunnerPld.h
 **
 ** \author
 **
 ** \date
 **
 */
// --------x----------------------------x-----------------------------------x--


/* $Id$
 * $Revision$
 * $Log$
 * Revision 1.4  2005/05/30 14:06:06  mmueller
 * Added proper autodetection of the FPGA and CPLD Jtag chain
 *
 * Revision 1.3  2005/04/14 07:06:00  joos
 * lots of new untested code
 *
 * Revision 1.2  2005/04/11 11:24:15  joos
 * Modifications and new files of Matthias
 *
 * Revision 1.1.1.1  2005/04/07 11:34:38  joos
 * added ROSRobin
 *
 *
 */

/**
 * Description:
 *
 */

#ifndef ROSROBIN_XILINXCOOLRUNNERPLD_H
#define ROSROBIN_XILINXCOOLRUNNERPLD_H
#include "ROSRobin/BitStream.h"
#include "ROSRobin/JTagDevice.h"

namespace ROS {

  class JTagController;
  
  class XilinxCoolRunnerPld : public JTagDevice {
  public:
    
    XilinxCoolRunnerPld(JTagController & jtagController);

    ~XilinxCoolRunnerPld(void);

    void program(std::string firmwareFile);
    
    bool isProgrammed(void);
    
    ROS::BitStream getDeviceId();

    void clear(void);
 };

}
#endif //XILINXCOOLRUNNERPLD_H

// --------x----------------------------x-----------------------------------x--
/**
 ** \file   robindriver_common.h
 **
 ** \author Markus Joos, CERN-PH/ESS
 **
 ** \date   11. Apr. 05
 **
 */
// --------x----------------------------x-----------------------------------x--
/************ C 2005 - The software with that certain something **************/


/**
 * Description:
 * This is the common (driver and library) header file for ROBIN driver
 */
 
#ifndef ROSROBIN_ROBINDRIVERCOMMON_H 
#define ROSROBIN_ROBINDRIVERCOMMON_H

#ifndef COMPILE_DRIVER
  #include <sys/types.h>
#endif

/*************/
/* Constants */
/*************/
#define MAXCARDS                 10   // Max. number of ROBIN cards

//For the proc file interface
#define PROC_MAX_CHARS           0x10000 //The max. length of the output of /proc/robin

//For the IRQ handling
#define NO_EVENT                 0
#define GUEST_EVENT              1
#define INMATE_EVENT             2


/******************/
/*Type definitions*/
/******************/
typedef struct
{
  u_int slot;
  u_int plx_base;  
  u_int plx_size;
  u_int fpga_base;
  u_int fpga_size;
  u_int nrols;
} T_link_params;

typedef struct
{
  u_int slot;
  u_int mem_base;
  u_int mem_size;
} T_init_dma_params;

typedef struct
{
  u_int slot;
  u_int n_fifo;
  u_int dpm_size;
  u_int dpm_base;
  u_int dpm_offset;
  u_int mem_size;
  u_int mem_base;
  u_int mem_offset;
} T_msg_req_params;

typedef struct
{
  u_int used;
  u_long start;
  u_int size;
} T_blocklet;

typedef struct
{
  u_int ncards;
  u_int ch0_id[MAXCARDS];
  u_int ch0_type[MAXCARDS];
  u_int ch1_id[MAXCARDS];
  u_int ch1_type[MAXCARDS];
  u_int ch2_id[MAXCARDS];
  u_int ch2_type[MAXCARDS];
} T_irq_info;


/*************/
/*ioctl codes*/
/*************/
#define ROBIN_MAGIC 'x'

#define LINK     _IOW(ROBIN_MAGIC, 44, T_link_params)
#define INIT_DMA _IOW(ROBIN_MAGIC, 45, T_init_dma_params)
#define MSG_REG  _IOW(ROBIN_MAGIC, 46, T_msg_req_params)
#define MSG_FREE _IOW(ROBIN_MAGIC, 47, int)
#define GETPROC  _IO(ROBIN_MAGIC, 48)
#define WAIT_IRQ _IOR(ROBIN_MAGIC, 49, T_irq_info)


/*
enum 
{
  LINK = 1,
  INIT_DMA,
  MSG_REG,
  MSG_FREE,
  LINK_IRQ,
  GETPROC,
  WAIT_IRQ
};
*/

/********************/
/*driver error codes*/
/********************/
enum
{
  RD_OK = 0,
  RD_KMALLOC,        // 1
  RD_CTU,            // 2
  RD_CFU,            // 3
  RD_ALREADYDONE,    // 4
  RD_FIFOEMPTY,      // 5
  RD_EIO,            // 6
  RD_EIO2,           // 7
  RD_PROC,           // 8
  RD_NOMEMORY,       // 9
  RD_NOSLOT,         // 10
  RD_ILLHANDLE,      // 11
  RD_TOOMANYCARDS,   // 12
  RD_REMAP,          // 13
  RD_BAR,            // 14
  RD_NOCARD,         // 15
  RD_MMAP,           // 16
  RD_NOSHARE,        // 17
  RD_RPR,            // 18
  RD_REQIRQ,         // 19
  RD_INTBYSIGNAL     // 20
};

static char ioctl_strings[21][200] = {
"robin driver(RD_OK): No error",
"robin driver(RD_KMALLOC): Failed to execute the kmalloc() function",
"robin driver(RD_CTU): Failed to execute the copy_to_user() function",
"robin driver(RD_CFU): Failed to execute the copy_from_user() function",
"robin driver(RD_ALREADYDONE): Robin::reserveMsgResources has already been called one",
"robin driver(RD_FIFOEMPTY): You have requested more entries from the message passing FIFO of a Robin than are currently available",
"robin driver(RD_EIO): Failed to execute the pci_register_driver() function",
"robin driver(RD_EIO2): Failed to execute the register_chrdev() function",
"robin driver(RD_PROC): Failed to execute the create_proc_entry() function",
"robin driver(RD_NOMEMORY): You have requested more DPM or DMA memory from a Robin than is currently available (Hint: check if robinconfig was run sccessfully)",
"robin driver(RD_NOSLOT): The bookkeeping tables for the DPM or DMA memory are full",
"robin driver(RD_ILLHANDLE): You are trying to return resources (DPM or DMA memor, FIFO entries) but the handle is invalid",
"robin driver(RD_TOOMANYCARDS): There are more Robincards installed than the driver can handle",
"robin driver(RD_REMAP): Failed to execute the ioremap() function",
"robin driver(RD_BAR): The BAR0 of something that looks like a Robin is not a PCI-MEM resource",
"robin driver(RD_NOCARD): You are trying to execute an ioctl function on a non-existing Robin. Check your physicalAddress parameter",
"robin driver(RD_MMAP): It is not possible to mmap an unaligned address",
"robin driver(RD_NOSHARE): mmap failed because shared flag is missing",
"robin driver(RD_RPR): The robin driver failed to execute the remap_page_range() function",
"robin driver(RD_REQIRQ): The robin driver failed to register the interrupt handler",
"robin driver(RD_INTBYSIGNAL): When waiting for an interrupt the driver got a signal"
};
#endif

/// robin_bufman.h buffer manager functions
/** \addtogroup RobInBMan
robin_bufman.h buffer manager functions

$Header: /local/reps/atlastdaq/DAQ/DataFlow/robin_ppc/robin_ppc/robin_bufman.h,v 1.58 2008/10/17 10:33:54 akugel Exp $
$Author$
$Date$
$Revision$

\section Description Description
Buffer manager functionality<br>
Receive fragments from UsedPageFifo. Store/retrieve/remove fragment information.

*@{
*/

#ifndef BUFFERMAN_FUNCTIONS_H
#define BUFFERMAN_FUNCTIONS_H

// -------------------------------------------------------------------------------------------------
// we are using some MACROs to speed things up
/// get fpf full status
#define GET_FPF_ITEMS(X) ((*(upfStatPtr[X]) >> enum_fpfFillShift) & enum_fpfFillMask)
/// get number of items in FPF
#define GET_FPF_FULL(X) ((fpfSize - 1) == GET_FPF_ITEMS(X)? 1:0)
/// pop free list
#define POP_FREE(X) {if (0 != freeTos[X]) freeTos[X]--;}
/// push free list
#define PUSH_FREE(X) {if ((cfgPtr[enum_cfgNumPages].value - 1) != freeTos[X]) freeTos[X]++;}


// -------------------------------------------------------------------------------
// Fragment structure
/** \struct PageInfo
\brief ENUM for pageinfo array in FragInfo
Used FIFO 0

bits 23 downto 0 accumulated errors during page
bits 30 downto 24 last page mark 0x8A = last page of event (otherwise = 0x55)
bit 31             Used FIFO not empty

Used FIFO 1
23 down to 0 L1ID
31 down to 24 ECR

Used FIFO 2
23 downto 0 address of end of data in used page

Used FIFO 3
Unused

*/


/// UPF DMA status
typedef enum {
	enum_fragDmaInactive,
	enum_fragDmaStarted,
	enum_fragDmaCompleted
} FragDmaStatus;

/// varous values related to UPF
typedef enum {
	enum_upfRegCount = 4,		// number of UPF elements per fragment
    // UPF 0: status
    enum_upfValid     = 0x80000000, // valid flag: bit 31
    enum_upfEndMask   = 0x7F000000, // last page marker: bits 24 to 30
    enum_upfEndMarker = 0x2A000000, // last page marker. otherwise value is 0x55000000
    enum_upfMoreMarker = 0x55000000, // non-last page marker

	// UPF error flags (Februar 2007)
	// !!! Don't forget to update bm_error_types after changes to this list !!!
	// also, don't forget to update twiki page for error flags
	enum_upfFlagCount			= 16,		// number of flags
	enum_upfEventIdFlag			= (1 << 15),
	enum_upfTrigTypeFlag		= (1 << 14),
	enum_upfRunNumberFlag		= (1 << 13),
	enum_upfFormatErrorFlag		= (1 << 12),
	enum_upfHdrMarkerErrorFlag	= (1 << 11),
	enum_upfMissingBofErrorFlag = (1 << 10),
	enum_upfMissingEofErrorFlag = (1 << 9),
	enum_upfShortHdrErrorFlag	= (1 << 8),
	enum_upfNoHeaderErrorFlag	= (1 << 7),
	enum_upfControlErrorFlag	= (1 << 6),
	enum_upfDataErrorFlag		= (1 << 5),
	enum_upfBofFlag				= (1 << 4),
	enum_upfEofFlag				= (1 << 3),
	enum_upfControlWordFlag		= (1 << 2),
	enum_upfFragSizeErrorFlag	= (1 << 1),
	enum_upfUnused				= (1 << 0),  	// TTsync no longer used

	// error combinations
	// enum_upfErrorMask = 0x1fe2,  // real error bits: 1 and 5 .. 12
	enum_upfErrorMask = enum_upfFragSizeErrorFlag | enum_upfDataErrorFlag | enum_upfControlErrorFlag | \
						enum_upfNoHeaderErrorFlag | enum_upfShortHdrErrorFlag | enum_upfMissingEofErrorFlag | \
						enum_upfMissingBofErrorFlag | enum_upfHdrMarkerErrorFlag | enum_upfFormatErrorFlag,
    // enum_upfHardErrorMask = ((1 << 12) | (1 << 11) | (1 << 7) | (1 << 8)),  // Unrecoverable errors are 12, 11, 8 and 7
    enum_upfHardErrorMask = enum_upfNoHeaderErrorFlag | enum_upfShortHdrErrorFlag,
    enum_upfBofMask = enum_upfControlWordFlag | enum_upfBofFlag,  // CTL + BOF
    enum_upfEofMask = enum_upfControlWordFlag | enum_upfEofFlag,  // CTL + EOF
    enum_upfCtlMask = enum_upfControlWordFlag | enum_upfBofFlag | enum_upfEofFlag,  // CTL, BOF, EOF

	// UPF 1: event id
    enum_upfEcrMask   = 0xff000000, // ECR mask
    enum_upfEcrShift  = 24,			// ECR right shift

	// UPF 2: trigger type and address
    // TT INFORMATION is defined in https://edms.cern.ch/file/573726/1/TimingEvents.pdf
    /*
    Preceding each Event Counter Reset (ECR) signal a calibration trigger of a
    new type L1ASync is sent. This calibration event is identified by its Trigger
Type, which is defined as follows:
    bit 0-2 are the three least significant bits of the current L1ID in the CTP
    bit 3-6 is 14 (one of the as yet unused sub-detector IDs)
    bit 7 is 0 (calibration trigger)
    */
    enum_upfAddrMask  = 0x00ffffff, // lower 24 bits used for buffer address
	enum_upfTtMask	  = 0xff000000,	// upper 8 bits used for TT
	enum_upfTtSyncType = 0x70,	// trigger types 0x70 thru 0x7f are used to check L1ID
	enum_upfTtSyncTypeMask = 0xf8,	// trigger types 0x70 thru 0x7f are used to check L1ID
	enum_upfTtSyncIdMask = 0x07,	// lower 3 bits used only
	enum_upfTtShift	  = 24			// TT right shift

	// UPF 3: run number

} UpfFlags;

/*
Free Page Fifo status in BufMgr Status Register (same as UPF status)

bit 25 down to 16   Used FIFO fill
bit 11 down to 0    Free FIFO fill
*/

typedef enum {
    enum_upfFillShift = 16,         // shift by this value, then apply mask
    enum_upfFillMask = 0x3ff,
    enum_fpfFillShift = 0,         // shift by this value, then apply mask
    enum_fpfFillMask = 0xfff
} BufMgrStatus;


/** buffer manager control bits<br>
    bit 31 is XOFF<br>
    Bit 3 downto 0 control the page size. The default value is
    8192 long word (32k bytes) <br>

    0x0   256 long words<br>
    0x1   512 long words<br>
    0x2  1024 long words<br>
    0x3  2048 long words<br>
    0x4  4096 long words<br>
    0x5  8192 long words<br>
    0x6 16384 long words<br>
    0x7 32768 long words<br>
    <br>

*/
typedef enum {
    enum_bmCtlXoff      = (1 << 31),     // xoff bit
    enum_bmCtlKeepCtl   = (1 << 30),      // control word stripping
    enum_bmCtlReset     = (1 << 8),      // BufMan and UPF/FPF fifo reset
	// finish conditions
	enum_finish_on_format_ver    = (1 << 13),
	enum_finish_on_frag_size     = (1 << 14),
	enum_finish_on_hdr_mark      = (1 << 15),
	enum_finish_on_CNTR          = (1 << 16),
	enum_finish_on_BOF           = (1 << 17),
	enum_finish_on_EOF           = (1 << 18),
	enum_finish_on_EventId       = (1 << 19),
	enum_finish_on_TriggerType   = (1 << 20),
	enum_finish_on_RunNumber     = (1 << 21),
	// enum_finish_on_Double_BOF    = (1 << 22),
	// enum_finish_on_Double_EOF    = (1 << 23),
	enum_finish_on_Missing_BOF   = (1 << 24),
	enum_finish_on_Missing_EOF   = (1 << 25),
	enum_finish_on_IncomplHeader = (1 << 26),
	enum_finish_on_NoHeader      = (1 << 27),
	enum_finish_on_CTL_Error     = (1 << 28),
	enum_finish_on_Data_Error    = (1 << 29),
	//
    enum_bmCtlSizeMask  = 0x0f,
    enum_bmCtlSize1k    = 0,
    enum_bmCtlSize2k    = 1,
    enum_bmCtlSize4k    = 2,
    enum_bmCtlSize8k    = 3,
    enum_bmCtlSize16k   = 4,
    enum_bmCtlSize32k   = 5,
    enum_bmCtlSize64k   = 6,
    enum_bmCtlSize128k  = 7,
	enum_bmCtlInitVal = enum_bmCtlXoff | enum_bmCtlKeepCtl | enum_finish_on_EOF | enum_finish_on_Missing_EOF

} BufMgrCtl;

/** Buffer Manager fragment definitions
Max number of pages per fragment,
max group size for processing
*/
typedef enum {
    enum_bmMaxFragPages = 512,	// allow 1MB fragments at 2kB typical page size
	enum_bmMaxFragGroup = 32	// use multiples of 8 here (DMA buffer size)
} BufMgrFragDefs;

/** ROL registers
RobIn ROL-Handler: Register description:

Reg_0 :	BOF pattern:
bit 31-0:	read/write	BOF pattern.
	Default:	#B0F00000

Reg_1 :	EOF pattern:
bit 31-0:	read/write	EOF pattern.
	Default:	#E0F00000

Reg_2 :	Mask bits:
bit 31-0:	read/write	If a bit is set, the corresponding bit in the BOF / EOF pattern
		must match. If not set, the corresponding bit in the BOF / EOF pattern
		is a don't care.
	Default:	#FFFF0000

Reg_3 :	General Control:
bit 0:	 read/write	BOF control bit: if set, BOF must be an S-LIINK control word.
		If not set, BOF must be a data word.
	Default:	'1'

bit 1:	 read/write	EOF control bit: if set, EOF must be an S-LIINK control word.
		If not set, EOF must be a data word.
	Default:	'1'

bit 2:	 read/write	BOF /EOF mask control bit: if set, the BOF / EOF control bit
		must match with the control bit of the received data word
		to set the BOF or EOF output. If not set, the BOF / EOF control bit
		is a don�t care.
	Default:	'1'

bit 7-3:	 read/write	Contain a number indicating how many words after a BOF word,
		the HeaderWordA signal will be active for one data word.
	Default:	"00101"

bit 8:	 read/write	If set, the HeaderWordA signal is enabled.
		If cleared, the HeaderWordA signal is disabled.
	Default:	'1'

bit 13-9:	 read/write	Contain a number indicating how many words after a BOF word
		the triggertype data word is expected in the data stream. The
		HeaderWordB signal will be active for one word if the
		TriggerType pattern combined with the TriggerType mask matches this
		TriggerType data word.
	Default:	"00111"

bit 14:	 read/write	If set, the HeaderWordB signal is enabled.
		If cleared, the HeaderWordB signal is disabled.
	Default:	'0'

bit 19-15:	 read/write	Contain a number indicating how many words after a BOF word,
		the HeaderWordC signal will be active for one data word.
	Default:	"00000"

bit 20:	 read/write	If set, the HeaderWordC signal is enabled.
		If cleared, the HeaderWordC signal is disabled.
	Default:	'0'

bit 25-21:	read/write	A number that defines the length of the header of a fragment.
		If the number of words in a fragment is less than this number,
		the IncomplHeader signal will be set.
	Default:	"01000"

bit 26:	read/write	On a rising edge the URESET# signal will be set and
		the S-LINK Input FIFO will be reset. URESET# will stay active
		until the LDOWN# signal is de-asserted.
	Default:	'0'

bit 27:	read/write	When set, data from the Test-Input FIFO will be used instead
		of data from the S-LINK Hola Core.
	Default:	'0'

bit 28:	read/write	TriggerType control bit: if set, TriggerType must be an S-LIINK control
		word. 	If not set, TriggerType must be a data word.
	Default:	'0'

bit 29:	read/write	TriggerType mask control bit: if set, the TriggerType control bit
		must match with the control bit of the received data word to
		set the TriggerType output. If not set, the TriggerType control bit
		is a don't care.
	Default:	'0'

bit 30:	read only	Status of the Full Flag of the Test-Input FIFO.

bit 31:	read only	Status of the Empty Flag of the Test-Input FIFO.

Default for bit 29 - 0:	#01000F2F

Reg_4 :	LDOWN & XOFF Status and Counter:
bits 13-0:	read only	The number of times that the XOFF# signal
		has gone active.
bit 14:	read only	If set, the counter has overflowed one or more times.
bit 15:	read only	The actual status of the XOFF# signal.
 bits 29-16:	read only	The number of times that the LDOWN# signal
		has gone active.
bit 30:	read only	If set, the counter has overflowed one or more times.
bit 31:	read only	The actual (synchronized) status of the LDOWN# signal.
bit 0:	write	If bit 0 = �1� clear XOFF Counter (D2 � D31 are don't care).
bit 1:	write	If bit 1 = �1� clear LDOWN Counter (D2 � D31 are don't care).

Reg_5 :	Write Test-Input Port:
bits 31-0	write only	Data to the Test-Input FIFO with CNTR-bit = '0' and DERR-bit = '0'.

Reg_6 :	Write Test-Input Port:
bits 31-0	write only	Data to the Test-Input FIFO with CNTR-bit = '1' and DERR-bit = '0'.

Reg_7 :	Write Test-Input Port:
bits 31-0	write only	Data to the Test-Input FIFO with CNTR-bit = '0' and DERR-bit = '1'.

Reg_8 :	Write Test-Input Port:
bits 31-0	write only	Data to the Test-Input FIFO with CNTR-bit = '1' and DERR-bit = '1'.

Reg_9 :	Trigger Type Mask/Pattern:
bits  7 - 0	R/W Trigger Type pattern
bits 15 - 8	R/W Trigger Type mask. Set bit => match required, clear bit => don't care
bits 24 -16	RO: don't care
bits 25	R/W: Enable S-Link data generator
bits 31 -28	R/W: Hola URL  lines
bits 27 -26	R/W: Hola UDW  lines


*/
/*
typedef enum {
    enum_rolRegBofPattern       = 0,
    enum_rolRegBofPatternInit   = 0xb0f00000,
    enum_rolRegEofPattern       = 1,
    enum_rolRegEofPatternInit   = 0xe0f00000,
    enum_rolRegBofEofMask       = 2,
    enum_rolRegBofEofMaskInit   = 0xffff0000,
    enum_rolRegControl          = 3,
	//
	// enum_rolRegControlInit      = 0x01200f37,   // L1ID at pos 6, Hdr size 9
	// set bits 0..2. L1ID position 3, hdr size position 21
    enum_rolRegControlInit      = ((6 << 21) | (1 << 8) | (6 << 3) | (1 << 2) | (1 << 1) | (1 << 0)),
    enum_rolRegCounters         = 4,
    enum_rolRegTestData         = 5,
    enum_rolRegTestCtl          = 6,
    enum_rolRegTestDataErr      = 7,
    enum_rolRegTestCtlErr       = 8,
    enum_rolRegTriggerType      = 9,
    enum_rolRegTtInit           = 0x0000ff00,
    enum_rolNumRegs             = 10
} RolRegs;
*/

// new version (feb 2007)

/*
Reg_0 :	BOF / EOF pattern:
bit 31-16:	read/write	16-bit BOF pattern.
bit 15-0:	read/write	16-bit EOF pattern.
	Default:	#B0F0E0F0


Reg_1 :	BOF / EOF Mask:
bit 31-16:	read/write	If a bit is set, the corresponding bit in the BOF pattern
		must match with the corresponding bit in the upper 16-bits of the
		received data word to assert the BOF output. If not set, the
		corresponding bit in the BOF pattern is a don't care.
bit 15-0:	read/write	If a bit is set, the corresponding bit in the EOF pattern
		must match with the corresponding bit in the upper 16-bits of the
		received data word to assert the EOF output. If not set, the
		corresponding bit in the EOF pattern is a don't care.
	Default:	#FFFFFFFF


Reg_2 :	FormatVersion pattern:
bit 31-0:	read/write	FormatVersion pattern.
	Default:	#03000000

Reg_3 :	General Control:
bit 0:	 read/write	BOF control bit: if set, BOF must be an S-LIINK control word.
		If not set, BOF must be a data word.
	Default:	'1'
bit 1:	read/write	EOF control bit: if set, EOF must be an S-LIINK control word.
		If not set, EOF must be a data word.
	Default:	'1'
bit 2:	read/write	BOF /EOF mask control bit: if set, the BOF / EOF control bit
		must match with the control bit of the received data word
		to set the BOF or EOF output. If not set, the BOF / EOF control bit
		is a don�t care.
	Default:	'1'
bit 7-3:	read/write	Contain a number indicating how many words after a BOF word,
		the EventID will be active during one data word.
	Default:	"00110"
bit 8:	read/write	If set, the EventID output is enabled.
		If cleared, the EventID output is disabled.
	Default:	'1'
bit 13-9:	read/write	Contain a number indicating how many words after a BOF word
		the TriggerType output will be active during one data word.
	Default:	"01000"
bit 14:	read/write	If set, the TriggerType output is enabled.
		If cleared, the TriggerType output is disabled.
	Default:	'1'
bit 19-15:	read/write	Contain a number indicating how many words after a BOF word,
		the RunNumber output will be active during one data word.
	Default:	"00101" (Run number position)
bit 20:	read/write	If set, the RunNumber output is enabled.
		If cleared, the RunNumber output is disabled.
	Default:	'1'
bit 25-21:	read/write	A number that defines the minimum length of the header of a ROD
		fragment. If the number of words in a received ROD fragment is less than
		this number, the IncomplHeader output will be set.
	Default:	"00110"
bit 26:	read/write	On a rising edge the URESET# output will be set and
		the S-LINK Input FIFO will be reset. URESET# will stay active
		until the LDOWN# signal is de-asserted.
	Default:	'0'
bit 27:	read/write	When set, data from the Test-Input FIFO will be used instead
		of data from the S-LINK Hola Core.
	Default:	'0'
bit 28:	read/write	BOFNoWait. When '1' the Robin will NOT wait for the first BOF or EOF but immediately accept all data
							   When '0' the Robin waits for the first BOF or EOF received before data is accepted.
	Default:	'0'
bit 29:	read/write	Not used.
	Default:	'0'
bit 30:	read only	Status of the Full Flag of the Test-Input FIFO.
bit 31:	read only	Status of the Empty Flag of the Test-Input FIFO.
Default(31:0):	#00D2D137


Reg_4 :	LDOWN & XOFF Status and Counter:
bit 0:	write	If set clear XOFF Counter	(D2 � D31 are don't care).
bit 1:	write	If set clear LDOWN Counter	(D2 � D31 are don't care).
bits 13-0:	read only	The number of times that the XOFF# signal
		has gone active.
bit 14:	read only	If set, the counter has overflowed one or more times.
bit 15:	read only	The actual status of the XOFF# signal.
bits 29-16:	read only	The number of times that the LDOWN# signal
		has gone active.
bit 30:	read only	If set, the counter has overflowed one or more times.
bit 31:	read only	The actual (synchronized) status of the LDOWN# signal.
	Default:	#00000000


Reg_5 :	Write Test-Input Port:
bits 31-0	write only	Data to the Test-Input FIFO with CNTR-bit = '0' and DERR-bit = '0'.


Reg_6 :	Write Test-Input Port:
bits 31-0	write only	Data to the Test-Input FIFO with CNTR-bit = '1' and DERR-bit = '0'.


Reg_7 :	Write Test-Input Port:
bits 31-0	write only	Data to the Test-Input FIFO with CNTR-bit = '0' and DERR-bit = '1'.


Reg_8 :	Write Test-Input Port:
bits 31-0	write only	Data to the Test-Input FIFO with CNTR-bit = '1' and DERR-bit = '1'.


Reg_9 :	S_Link control:
bit 26-0:	read/write	Not used.
bit 27:	read/write	Selects between real S_Link data or emulated data.
		If set, emulated data is used instead of real S_Link data.
		If not set, real S_Link data via HOLA LDC is used.
bit 31-28:	read/write	The value on these bits is passed on to the S-Link URL lines.
		When bit 27 is set, the value on these bits control the behavior of
		the data emulator:
		bits 29-28:	�00�	fragment size is:	100 Bytes.
		bits 29-28:	�10�	fragment size is: 	1000 Bytes.
		bits 29-28:	�01�	fragment size is: 	2000 Bytes.
		bits 29-28:	�11�	fragment size is: 	2000 Bytes.
		bits 31-30:	�00�	value on data word(31-16) is: #1100.
		bits 31-30:	�01�	value on data word(31-16) is: #2200.
		bits 31-30:	�10�	value on data word(31-16) is: #3300.
		bits 31-30:	�11�	value on data word(31-16) is: #4400.
	Default:	#00000000


*/
typedef enum {
    enum_rolRegCtlPattern		= 0,
    enum_rolRegCtlPatternInit   = 0xb0f0e0f0,
    enum_rolRegCtlmask			= 1,
    enum_rolRegCtlMaskInit		= 0xffffffff,
    enum_rolRegCtlMaskEofDontCare	= 0xffff0000,		// any control word is accepted as EOF
    enum_rolRegFormatPattern    = 2,
    enum_rolRegFormatInit		= 0x03010000,		// Event format 4.0 allows for ROD format 3.1
    enum_rolRegControl          = 3,
	//
	// enum_rolRegControlInit      = 0x01200f37,   // L1ID at pos 6, Hdr size 9
	// set bits 0..2. L1ID position 3, hdr size position 21
	enum_rolCtlRegBofCtlBit		= (1 << 0),
	enum_rolCtlRegBofCtlInit	= enum_rolCtlRegBofCtlBit,
	enum_rolCtlRegEofCtlBit		= (1 << 1),
	enum_rolCtlRegEofCtlInit	= enum_rolCtlRegEofCtlBit,
	enum_rolCtlRegCtlMaskBit	= (1 << 2),
	enum_rolCtlRegCtlMaskInit	= enum_rolCtlRegCtlMaskBit,

	enum_rolCtlRegL1IdMask		= 0x1f,
	enum_rolCtlRegL1IdInit		= 6,
	enum_rolCtlRegL1IdLshift	= 3,
	enum_rolCtlRegL1IdEnBit		= (1 << 8),

	enum_rolCtlRegTTMask		= 0x1f,
	enum_rolCtlRegTTInit		= 8,
	enum_rolCtlRegTTLshift		= 9,
	enum_rolCtlRegTTEnBit		= (1 << 14),

	enum_rolCtlRegRunMask		= 0x1f,
	enum_rolCtlRegRunInit		= 5,
	enum_rolCtlRegRunLshift		= 15,
	enum_rolCtlRegRunEnBit		= (1 << 20),

	enum_rolCtlRegSizeMask		= 0x1f,
	enum_rolCtlRegSizeInit		= 6,
	enum_rolCtlRegSizeLshift	= 21,

	enum_rolCtlRegUresetBit		= (1 << 26),
	enum_rolCtlRegUresetInit	= 0,
	enum_rolCtlRegTestEnBit		= (1 << 27),
	enum_rolCtlRegTestEnInit	= 0,
	enum_rolCtlRegTestFFBit		= (1 << 30),	// Read only
	enum_rolCtlRegTestEFBit		= (1 << 31),	// read only
    // enum_rolRegControlInit      = ((6 << 21) | (1 << 8) | (6 << 3) | (1 << 2) | (1 << 1) | (1 << 0)),
    enum_rolRegControlInit      = (enum_rolCtlRegBofCtlInit | enum_rolCtlRegEofCtlBit | enum_rolCtlRegCtlMaskInit | \
								  (enum_rolCtlRegL1IdInit << enum_rolCtlRegL1IdLshift) | enum_rolCtlRegL1IdEnBit | \
								  (enum_rolCtlRegTTInit << enum_rolCtlRegTTLshift) | enum_rolCtlRegTTEnBit | \
								  (enum_rolCtlRegRunInit << enum_rolCtlRegRunLshift) | enum_rolCtlRegRunEnBit | \
								  (enum_rolCtlRegSizeInit << enum_rolCtlRegSizeLshift) | \
								   enum_rolCtlRegUresetInit | enum_rolCtlRegTestEnInit ),
	//
    enum_rolRegCounters         = 4,
    enum_rolRegTestData         = 5,
    enum_rolRegTestCtl          = 6,
    enum_rolRegTestDataErr      = 7,
    enum_rolRegTestCtlErr       = 8,
    enum_rolRegSlink			= 9,
    enum_rolRegSlinkInit        = 0
} RolRegs;


typedef enum {
    // register 9
    enum_rolBitSlinkRolDatGen    = 27,
    enum_rolBitSlinkUrlMask      = 0xf,    // after shift applied
    enum_rolBitSlinkUrlShift     = 28,
    // enum_rolBitTtUdwMask      = 0x3,    // after shift applied
    // enum_rolBitTtUdwShift     = 26,

    // data generator control via URL signals
    enum_rolDataGenChannelMask = 0x0c,
    enum_rolDataGenChannelShift = 2,
    enum_rolDataGenEtMisMask = 	 0x03,
    enum_rolDataGenEtMisNone = 	 0x00, // standard data format
    enum_rolDataGenEtMis1 = 	 0x01, // ET mis format 1
    enum_rolDataGenEtMis2 = 	 0x02, // ET mis format 2
    enum_rolDataGenTile = 	 	 0x03, // Tile format


    // register 4
    enum_rolBitXoffShift      = 0,
    enum_rolBitXoffStatMask   = 0x8000,
    enum_rolBitXoffCntMask    = 0x7fff,
    enum_rolBitXoffCntClrBit  = 0,       // write to reg 4
    enum_rolBitLdownShift     = 16,
    enum_rolBitLdownStatMask  = 0x8000,
    enum_rolBitLdownCntMask   = 0x7fff,
    enum_rolBitLdownCntClrBit = 1,       // write to reg 4
    // register 3
    // there are more bits now in reg3. Need to be completed
    enum_rolBitHdrPositionMask   = 0x1f,    // to be shifted
    enum_rolBitHdrAPositionShift = 3,       // HDRA (= L1ID) starts at bit# 3
    enum_rolBitHdrAEnableBit     = 8,       // Enables HDRA (= L1ID) if 1
    enum_rolBitHdrBPositionShift = 9,       // starts at bit# 9
    enum_rolBitHdrBEnableBit     = 14,      // Enables HDRB (= TT) if 1
    enum_rolBitHdrCPositionShift = 15,      // starts at bit# 15
    enum_rolBitHdrCEnableBit     = 20,      // Enables HDRC if 1
    enum_rolBitHdrSizeMask       = 0x1f,    // to be shifted
    enum_rolBitHdrSizeShift      = 21,      // size of header
    enum_rolBitUresetBit      = 26,       // write to reg 3
    enum_rolBitTestInputEnBit = 27,       // write to reg 3

	enum_rolBitBofNoWait = 28,           // No wait for BOF after RESET
    // enum_rolBitTtTypeMaskBit = 29,       // readwrite, reg 3 NOT USED

	enum_rolBitTestInputFFBit = 30,       // read, reg 3
    enum_rolBitTestInputEFBit = 31       // read, reg 3
} RolBits;

typedef enum {
    enum_BypassCtlBitEnable        = 0,
    enum_BypassCtlBitTLKEnable     = 1,
    enum_BypassCtlBitLoopback      = 2,
    enum_BypassCtlBitPRBS          = 3,
    enum_BypassCtlBitOptDisable    = 4,
    enum_BypassCtlBitLUPLED        = 5,
    enum_BypassCtlBitLDERRLED      = 6,
    enum_BypassCtlBitFLOWCTLLED    = 7,
    enum_BypassCtlBitACTIVITYLED   = 8,
    enum_BypassCtlBitShiftPerRol   = 10,
    enum_BypassCtlBitShiftRolId    = 30 //bits 31-30 define RolID: 01: ROL0 (Final Robin only); 10: ROL1; 11: ROL2
} BypassCtlBits;

typedef enum {
    enum_BypassStatBitEnable        = 0,
    enum_BypassStatBitTLKEnable     = 1,
    enum_BypassStatBitLoopback      = 2,
    enum_BypassStatBitPRBS          = 3,
    enum_BypassStatBitOptDisable    = 4,
    enum_BypassStatBitLUPLED        = 5,
    enum_BypassStatBitLDERRLED      = 6,
    enum_BypassStatBitFLOWCTLLED    = 7,
    enum_BypassStatBitACTIVITYLED   = 8,
    enum_BypassStatCtlMask          = ((1 << (enum_BypassStatBitACTIVITYLED + 1)) - 1),
    enum_BypassStatBitTXfull        = 12,
    enum_BypassStatBitTXdata        = 13,
    enum_BypassStatBitRXempty       = 14,
    enum_BypassStatBitRXdata        = 15,
    enum_BypassStatBitRolIdMask     = 0xC00 //bits 11-10 show ROLID: 01: ROL0 (Final Robin only); 10: ROL1; 11: ROL2
} BypassStatBits;

typedef enum {
    enum_BypassTXenable             = 0x10000,
    enum_BypassTXerror              = 0x20000,
    enum_BypassRXdatavalid          = 0x10000,
    enum_BypassRXerror              = 0x20000,
    enum_BypassRXfifoDepth          = 16 // 1024		// we are now using Blockrams. 16
} BypassTXRX;

// prototypes
// -------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------
/// test availability of new fragment from page level buffer manager. Read UPF from PPC, in single cycles
FragInfo *checkFragmentPpc(int rolId);

// -------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------
/// test availability of new fragment from page level buffer manager. Read UPF via DMA
FragInfo *checkFragmentDma(int rolId);

// -------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------
/// Process fragment
void processFragment(FragInfo *theFragment, int rolId);

// ---------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------
/// find the first entry in the item list for a event id return 0 if not present
// MgmtPtr findId(Dword fragId, int rolId);
MgmtPtr findId(EventTag* eventTag, int rolId);

// -------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------
/// add the entry to the hash list after the given position.
void addItem(FragInfo *fragment, MgmtPtr position, int rolId);

// -------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------
/// find and remove all entries with fragId
// int deleteId(Dword fragId, int rolId);
int deleteId(EventTag *eventTag, int rolId);

// -------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------
/// remove all entries with same id from the list, starting at position.
void deletePos(MgmtPtr position, int rolId);

// -------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------
///get next available entry from freeList
int popFreeList(int rolId);

// -------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------
/// put page back to freeList
int pushFreeList(int rolId);

// -------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------
/// take next free page from freeList and write to FPF
void writeFpf(int rolId);

// -------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------
/// return status of FPF
int fpfFull(int rolId);

// -------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------
/// write free entry to fpf
void fpfPut(Word item, int rolId);

// -------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------
/// read numer of entries in FPF
int fpfNumItems(int rolId);

// -------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------
/// check eventID sequence. Return 0 if OK, else 1
int checkSeqError(Dword oldId, Dword newId);

// -------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------
/// add suppressed page to separate storage
int addRejectedPage(FragInfo *theFragment, int type, Dword mostRecentId, int rolId);

// -------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------

/*@}*/

#endif  //BUFFERMAN_FUNCTIONS_H


// ROBIN Global Types Header File
#ifndef ROBIN_VHDL_MAP_VER4
#define ROBIN_VHDL_MAP_VER4
// This is a generated file, do not edit!
// Design identification parms
#define DESIGN_ID 84094977
#define DESIGN_VERSION_BITS 8
#define DESIGN_VERSION_SHIFT 24
#define DESIGN_MAJOR_REVISION_BITS 8
#define DESIGN_MAJOR_REVISION_SHIFT 16
#define DESIGN_MINOR_REVISION_BITS 11
#define DESIGN_MINOR_REVISION_SHIFT 0
#define DESIGN_AUTHOR_BITS 4
#define DESIGN_AUTHOR_SHIFT 12
#define DESIGN_TYPE_BITS 1
#define DESIGN_TYPE_SHIFT 11
// Author identification
#define AUTHOR_CODE 3
// Author map: Unknown Green Kieft Kugel Mueller Krause Schroer 
#define AUTHOR_UNKNOWN 0
#define AUTHOR_BGREEN 1
#define AUTHOR_GKIEFT 2
#define AUTHOR_AKUGEL 3
#define AUTHOR_MMUELLER 4
#define AUTHOR_EKRAUSE 5
#define AUTHOR_NSCHROER 6
// PCI address map
// Addresses refer to 32 bit entities. Shift left by 2 to get virtual address  
#define PCI_RANGE_ADDRESS_SIZE 14
#define PCI_DPM_MAX_SIZE 12
#define PCI_DMP_RANGE_SIZE 2
// Actual DPM might be smaller than max size
#define PCI_DPM_SIZE 12
// MAC TX buffer size in number of DPMs, standard Ethernet frames (default), 4k jumbos, 6k jumbos
#define VHDL_NET_6K_JUMBOS 
#define VHDL_NET_HDR_FIFO_SIZE (3*512) 

// Testinput FIFO size, either small distributed (16x34) or in BRAM (512x34), if 6k Jumbos are used the small distributed FIFO has to be used
#define TESTINPUT_FIFO_SIZE 16 

// Address ranges must be shifted by address_range_size to get real address  
#define PCI_DESIGN_ID_RANGE 0
#define PCI_DPM_RANGE 1
#define PCI_REGISTER_RANGE 2
#define PCI_UNUSED_RANGE 3
// PCI DPM area subdivided into multiple smaller ranges using the top most PCI_DMP_RANGE_SIZE bits of PCI_RANGE_ADDRESS_SIZE
#define PCI_DPM_REQUEST_RANGE 0
#define PCI_DPM_FIFO_RANGE 1
#define PCI_DPM_REGISTER_RANGE 2
// Register area
#define PCI_UART_STATUS_REG_ADDRESS 0
#define PCI_UART_READ_DATA_ADDRESS 1
#define PCI_UART_WRITE_DATA_ADDRESS 1
#define PCI_QUICK_STATUS_REG_ADDRESS 16
// register block sizes
#define AMAP_UPF_REGS 4
#define AMAP_ROL_REGS 10
// buffer address
#define  AMAP_Buffer_mem_address  67108864
// DMP mux bit
#define AMAP_MAC_PCI_DPM_mux_bit 22
// All register addresses must be shifted by AMAP_ADDR_RANGE_LSHIFT
#define  AMAP_ADDR_RANGE_LSHIFT  19
// register map
#define  AMAP_ROL_handler_2_reg_9_address  17
#define  AMAP_ROL_handler_2_reg_4_address  16
#define  AMAP_ROL_handler_2_reg_3_address  15
#define  AMAP_ROL_handler_2_reg_2_address  14
#define  AMAP_ROL_handler_2_reg_1_address  13
#define  AMAP_ROL_handler_2_reg_0_address  12
#define  AMAP_ROL_handler_1_reg_9_address  11
#define  AMAP_ROL_handler_1_reg_4_address  10
#define  AMAP_ROL_handler_1_reg_3_address  9
#define  AMAP_ROL_handler_1_reg_2_address  8
#define  AMAP_ROL_handler_1_reg_1_address  7
#define  AMAP_ROL_handler_1_reg_0_address  6
#define  AMAP_ROL_handler_0_reg_9_address  5
#define  AMAP_ROL_handler_0_reg_4_address  4
#define  AMAP_ROL_handler_0_reg_3_address  3
#define  AMAP_ROL_handler_0_reg_2_address  2
#define  AMAP_ROL_handler_0_reg_1_address  1
#define  AMAP_ROL_handler_0_reg_0_address  0
#define AMAP_ROL_handler_2_reg_8_address 62
#define AMAP_ROL_handler_2_reg_7_address 61
#define  AMAP_ROL_handler_2_reg_6_address  60
#define  AMAP_ROL_handler_2_reg_5_address  59
#define  AMAP_ROL_handler_1_reg_8_address  58
#define  AMAP_ROL_handler_1_reg_7_address  57
#define  AMAP_ROL_handler_1_reg_6_address  56
#define  AMAP_ROL_handler_1_reg_5_address  55
#define  AMAP_ROL_handler_0_reg_8_address  54
#define  AMAP_ROL_handler_0_reg_7_address  53
#define  AMAP_ROL_handler_0_reg_6_address  52
#define  AMAP_ROL_handler_0_reg_5_address  51
#define  AMAP_Buffer_mngr_2_Free_FIFO_address  50
#define  AMAP_Buffer_mngr_1_Free_FIFO_address  49
#define  AMAP_Buffer_mngr_0_Free_FIFO_address  48
#define  AMAP_ROL_Hndlr_2_LDCemu_Write_address  47
#define  AMAP_ROL_Hndlr_1_LDCemu_Write_address  46
#define  AMAP_ROL_Hndlr_0_LDCemu_Write_address  45
#define  AMAP_Hola_Bypass_Cntl_Write_address  44
#define  AMAP_Hola_Bypass_Tx_Write_address  43
#define  AMAP_Design_Id_address  42
#define  AMAP_Buffer_mngr_2_Used_FIFO_address  41
#define  AMAP_Buffer_mngr_1_Used_FIFO_address  40
#define  AMAP_Buffer_mngr_0_Used_FIFO_address  39
#define  AMAP_mac_rx_descriptor_address  38
#define  AMAP_Hola_Bypass_Rx_address  37
#define  AMAP_PCI_DPM_status_reg_address  36
#define  AMAP_Hola_Bypass_Status_address  35
#define  AMAP_MAC_Hdr_FIFO_address  34
#define  AMAP_MAC_Hdr_FIFO_status_reg_address  33
#define  AMAP_MAC_Config_address  32
#define  AMAP_PCI_DMA_status_reg_address  31
#define  AMAP_PCI_DMA_control_reg_address  30
#define  AMAP_PCI_Hdr_FIFO_status_reg_address  29
#define  AMAP_PCI_status_reg_address  28
#define  AMAP_PCI_control_reg_address  27
#define  AMAP_PCI_Hdr_FIFO_address  26
#define  AMAP_common_control_reg_address  25
#define  AMAP_common_Status_reg_address  24
#define  AMAP_Buffer_mngr_2_status_reg_address  23
#define  AMAP_Buffer_mngr_2_control_reg_address  22
#define  AMAP_Buffer_mngr_1_status_reg_address  21
#define  AMAP_Buffer_mngr_1_control_reg_address  20
#define  AMAP_Buffer_mngr_0_status_reg_address  19
#define  AMAP_Buffer_mngr_0_control_reg_address  18
// control register bits
#define  CTL_BIT_RESET_ROL0  12
#define  CTL_BIT_RESET_ROL1  13
#define  CTL_BIT_RESET_ROL2  14
#define  CTL_BIT_RESET_PCI_LBC  15
#define  CTL_BIT_RESET_PCI_DMA  16
#define  CTL_BIT_RESET_ALL  17
#define  CTL_BIT_RESET_COUNTER  23
#define  CTL_BIT_LBUS_INTEN  20
#define  CTL_BIT_LED0  24
#define  CTL_BIT_BUFPAGE_0  21
#define  CTL_BIT_BUFPAGE_1  22
#endif //ROBIN_VHDL_MAP_VER4

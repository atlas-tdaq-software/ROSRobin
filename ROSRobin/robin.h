/// robin.h defines basic data types and structures
/** \mainpage
\addtogroup robin

$Header: /local/reps/atlastdaq/DAQ/DataFlow/robin_ppc/robin_ppc/robin.h,v 1.233 2009/04/29 17:07:43 akugel Exp $
$Author$
$Date$
$Revision$

\section Description Description
Main ROBIN header file. To be included from host application as well<br>
Defines main data structures, like message format, status and configuration.



*@{
*/

#ifndef ROBIN_H
#define ROBIN_H

// no ROS_HDR from event format version 4
//#define ROS_HDR


/*
For compatibility with Win32, GCC supports a set of #pragma directives which change the
maximum alignment of members of structures (other than zero-width bitfields), unions,
and classes subsequently defined. The n value below always is required to be a
small power of two and specifies the new alignment in bytes.

Alternatively:
-fpack-struct[=n]
    Without a value specified, pack all structure members together without holes.
	When a value is specified (which must be a small power of two),
	pack structure members according to this value, representing the
	maximum alignment (that is, objects with default alignment requirements larger
	than this will be output potentially unaligned at the next fitting location.
*/

// check if compilation if for the PPC target
#ifndef HOST
#ifdef WIN32
#define HOST
#endif
#ifdef LINUX
#define HOST
#endif
#endif

// make sure strctures are packed
// at least for HOST, maybe also for PPC
#ifdef HOST
#pragma pack(1)
#endif
// test network / structure packing problem
//#define PACK_TEST

// -------------------------------------------------------------------------------
// define number of channels
// first check if we are using one of the rol channels as LSC
#ifdef VHDL_VER3
#define NUM_ROLS 2
#define MAX_ROLS 2	// Must be power of 2. never use this software for more than MAX_ROLS links
#endif

#ifdef VHDL_VER4
#define NUM_ROLS 3
#define MAX_ROLS 3	// Must be power of 2. never use this software for more than MAX_ROLS links
#endif

// -------------------------------------------------------------------------------

// define OLD_UBOOZ in the makefile
//#define OLD_UBOOT	// old style or new one (RobinXpress)

// include PPC include files
// depending on uboot version
#ifndef HOST
// #include "uboot_100_string.h"
// #include "uboot_100_types.h"
#ifdef OLD_UBOOT
#include "uboot_100_processor.h"
#include "uboot_100_exports.h"	// local file
#else
#include "processor.h"	// new local file
#include "exports.h"	// new local file
#endif
#else
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#ifdef WIN32
# include <conio.h>
# include <windows.h>
# define TSTC _kbhit
#else
// linux
// #define TSTC kb_getc		// linux. to be improved
#define TSTC kbhit
#endif
#endif

#define xmin(x,y) (x) < (y) ? (x):(y)


// DC wants only 1 fragment status
// #define DC_COMPATIBLE

/// for tests we want to have the event log enabled
#define EVENT_LOG
#define EVENT_LOG_SIZE 25   // arbitrary value

// set a fixed runber in absence of real run number
#define FIXED_RUN_NUMBER

// debug pci dma
// #define LOG_PCI_DMA


// -------------------------------------------------------------------------------
// #define PRINTF mon_printf
#ifndef HOST
  #define GETC  getc
  #define PUTC  putc
  #define TSTC tstc
  #define EXIT(x) return(x)
  #define PRINTF printf
  #ifdef NO_PRINT
    #define PRINT(...)
  #else
    #define PRINT debugPrintf       // enable dyamic on/off
  #endif
#else
  #include <stdio.h>
  #define PRINT  printf
  #define PRINTF printf
  #define EXIT(x) exit(x)
  #define GETC  getchar
  #define PUTC  putc
  // #define udelay(x) Sleep(((x)/1000)?((x)/1000 + 1):1)
  #define udelay(x)
#endif

// basic BOOL values
#ifndef FALSE
#define FALSE 0
#define TRUE !FALSE
#endif

#ifndef NULL
#define NULL 0
#endif

// -------------------------------------------------------------------------------
// -------------------------------------------------------------------------------
int robin(int argc, char* argv[]);


// --------------------------------------------------------------------------------
/** Shared section \defgroup RobInShared Exported items */
/*@{*/
/// Put all items, which are needed by external software (e.g. ROS) into the shared group
// --------------------------------------------------------------------------------

// -------------------------------------------------------------------------------
// -------------------------------------------------------------------------------
// define basic data types
typedef unsigned char Byte;
typedef unsigned short Word;
typedef unsigned int Dword;
typedef unsigned long long Qword;

// run control state definition
/// Valid run control states are: START, STOP. States are defined PER ROL
typedef enum {
    enum_runCtlStop = 0,
    enum_runCtlStart = 1
} RunCtlStates;

// -------------------------------------------------------------------------------
/// struct cpuTypes
/**
CPU internal and external identification
*/
typedef enum {
	enum_cpuIdMask 	= 0xffff0000,
	enum_cpuIdRegCR = 0x40110000,
	enum_cpuIdRegGP = 0x40120000,
	enum_cpuIdRegGX = 0x51b20000,
	enum_cpuTypeCR	= 1,
	enum_cpuTypeGP	= 2,
	enum_cpuTypeGX	= 3,
	enum_cpuTypeOther	= 0
} CpuTypes;


// -------------------------------------------------------------------------------
/// struct ReqMsg defines generic RobIn request
/**
Response codes are defined in #ServCode <Br>
<table>
<tr><td>
Offset </td><td> Byte 3 </td><td> Byte 2 </td><td> Byte 1 </td><td> Byte 0 </td>
<tr><td>
0: Request field </td><td> ID </td><td> -> </td><td> SEQ  </td><td> SRV </td>
<tr><td>
1: Address field </td><td> Address </td><td> ->  </td><td> ->  </td><td> ->  </td>
<tr><td>
2: Ext. Address field </td><td> Extended Address </td><td> ->  </td><td> ->  </td><td> ->  </td>
<tr><td>
3..n: Data field (s) </td><td> Data </td><td> ->  </td><td> ->  </td><td> -> </td>
</table>
ReqMsg is immediately followed by a block of data Dwords, if data are required
for the service <br>
*/
typedef struct
{
	Dword request;	// 3 fields: Word ROL-ID, Byte Sequence number, Byte ServCode
	Dword destAddr;
	Dword destAddrExt;
} ReqMsg;

/// bit fields in request message
typedef enum {
    reqModIdShift = 16,			    // shift value for module id field in request message
    reqModIdSize  = 16,			    // sused bits for module id field in request message
    reqSeqShift = 8,				// shift value for sequence number field in request message
    reqSeqSize  = 8				    // used bits for sequence number field in request message
} ReqFields;

// -------------------------------------------------------------------------------
// Service request
/// RobIn Request Codes (Byte) used by ReqMsg
/**
RobIn Service codes (Byte) used by generic request ReqMsg<br>
*/
typedef enum
{
	enum_reqFragment = 0x01,            // fragment data request
	enum_reqEtSum = 0x08,            	// request energy sums. Provides base address(es)
	enum_reqClear = 0x10,               // delete particular entries from specific ROL
	enum_reqClearAck = 0x18,            // delete particular entries from specific ROL. Send response acknowledgement !
	enum_reqGarbCollect = 0x19,         // delete all entries older than <value> from specific ROL. Responde ACK if any events removed, else NACK
	enum_reqClearAllRols = 0x11,        // delete particular entries from all ROLs
	enum_reqStatusMonitorData = 0x20,   // request for status, statisitcs and monitoring data, ROL specific
	enum_reqTempVal = 0x21,				// request for temperatur sensor data
	enum_reqKeptPage = 0x22,            // request to send one of the kept rejected pages
	enum_reqClearKeptPages = 0x23,      // request to release all kept pages. Responds with ACK
	enum_reqEventList = 0x24,           // request to build event list and copy to scratch area
	enum_reqEventLog = 0x28,            // request to transfer event log. Warning: this is a LARGE data set
	enum_reqClearMonitorData = 0x2f,    // clear statisitcs and monitoring data, ROL specific
	enum_reqGetConfig = 0x30,           // get name and value of particular configuration item. Returns CFG parms
	enum_reqSetConfig = 0x38,           // set name and value of particular configuration item. Returns CFG parms
	enum_reqSetSecSi = 0x3a,            // set OTP SecSi sector data
	enum_reqLoadFragment = 0x40,        // load fragment with L1ID into buffer, via buffer manager. Emulated ROD data
	enum_reqLoadRodFragment = 0x41,     // load fragment into buffer, via buffer manager. Provided ROD data via scratch mem. Can generate errors
	enum_reqFormatBuffer = 0x42,		// format entire buffer with emulated fragments, from L1ID = 0
	enum_reqLoadMem = 0x50,             // write data to cpu scratch memory at specific offset
	enum_reqReadMem = 0x58,             // read data from cpu memory. if MSB set, read from FLASH, else read from scratch area
	enum_reqDumpBuf = 0x5f,             // dump entire buffer
	enum_reqReProgramFpga = 0x60,       // reprogram FPGA from scratch area (no parms)
	enum_reqReFlashFpga = 0x70,         // reprogram FLASH for FPGA from scratch area (needs size)
	enum_reqReFlashUboot = 0x74,          // reprogram FLASH for CPU from scratch area (needs size)
	enum_reqReFlashCpu = 0x78,          // reprogram FLASH for Uboot from scratch area (needs size)
	enum_reqRestart = 0x80,             // restart channel
	enum_reqTest = 0x81,             	// test operation, one word command code
	enum_reqHardReset = 0x8f,           // hard reset
	enum_reqEnterDiscardMode = 0x90,	// enter mode where input is dropped
	enum_reqLeaveDiscardMode = 0x91,	// leave mode where input is dropped
	enum_reqExit = 0xee,                // Exit application: experts only
	enum_reqPing = 0xff                 // Returns ACK
} ServCode;


// -------------------------------------------------------------------------------
/// struct RespMsg defines generic RobIn response, other than ROB fragment.
/**
Response codes are defined in #RespCode <Br>
<table>
<tr><td>
Offset </td><td> Byte 3 </td><td> Byte 2 </td><td> Byte 1 </td><td> Byte 0 </td>
<tr><td>
0: Response Marker </td><td> Start-of-header marker, e.g. 0x33fedc33 (inverted ROB marker)  </td><td> -> </td><td> ->  </td><td> -> </td>
<tr><td>
1: Response size </td><td> Total response size in words  </td><td> ->  </td><td> -> </td><td> -> </td>
<tr><td>
2: Format version </td><td> Major format number (e.g. 0x0001)  </td><td> -> </td><td> Minor format number (e.g. 0x0001)  </td><td> -> </td>
<tr><td>
3: Response field	 </td><td> ID </td><td> -> </td><td> SEQ </td><td> RSP </td>
<tr><td>
4..n: Data field (s)	Data </td><td>  </td><td>  </td><td>  </td>
</table>
RespMsg is immediately followed by a block of data Dwords, if data are required <br>
for the response <br>
*/
typedef struct {
	Dword marker;	// default marker value = inverted ROB hdr marker (= 0x33fedc33);
	Dword size;
	Dword version;	// Has 2 fields: major (Word), minor (Word)
	Dword response;	// Has 3 fields: ROL-ID (Word), Sequence number (Byte), RespCode (Byte)
} RespMsg;

/// bit fields inf response message
typedef enum {
    respModIdShift = 16,			// shift value for module id field in response message
    respModIdSize  = 16,			// used bits for module id field in response message
    respSeqShift = 8,				// shift value for sequence number field in response message
    respSeqSize  = 8,				// used bits for sequence number field in response message
	respCodeMask = 0xff				// response code mask
} RespFields;

// -------------------------------------------------------------------------------
/// RobIn Response Codes used by response message
/**
Response	RSP	Data field(s) in RespMsg<br>
<table>
<tr><td>
Clear Acknowledge </td><td> #enum_respClear </td><td> Status: Zero = OK, non-zero = error </td>
<tr><td>
Status and Monitoring Data </td><td> #enum_respStatusMonitorData </td><td> Status and Statistics Block </td>
<tr><td>
Configuration Item </td><td> #enum_respCfgItem </td><td> Config Item </td>
<tr><td>
Dump </td><td> #enum_respDumpBuffer </td><td> Size of block (1 word = N), buffer data (N words) </td>
</table>
*/
typedef enum
{
	enum_respClearAck = 0x18,               /// ClearAck acknoledge
	enum_respClearAll = 0x19,               /// ClearAll acknoledge
	enum_respStatusMonitorData = 0x20,		/// status and monitoring data
	enum_respKeptPage = 0x22,               /// Kept rejected page
	enum_respEventLog = 0x28,               /// Event log. Warning: this is a LARGE data set
	enum_respCfgItem = 0x30,                /// Get configuration item
	enum_respReadMem = 0x58,                /// data from cpu memory
	enum_respDumpBuf = 0x5f,                /// data from buffer
	enum_respAck = 0x00,                    /// general positive acknowledge
	enum_respNack = 0x08                    /// general negative acknowledge
} RespCode;

// -------------------------------------------------------------------------------
/// Status values for RobFragment (in ROB header)
/**
Status field:<br>
ï¿½	Zero: Fragment OK <br>
ï¿½	Non-Zero: Fragment corrupted, further details application specific. <br>
*/
typedef enum {
    enum_fragStatusOk = 0,                  // fragment OK
    enum_fragStatusRolError		= 0x00000003,  // ROL Error mask, if TX error set
	// specific errors
	// lower 8 bits mapped from UPF error bits
    // TTsync removed, bit unused
    enum_fragStatusSizeError	= 0x00020000,  // UPF: fragment size error
    enum_fragStatusDataError	= 0x00040000,  // UPF: data block error
    enum_fragStatusCtlError		= 0x00080000,  // UPF: ctl word error
    enum_fragStatusBofError		= 0x00100000,  // UPF: missing BOF
    enum_fragStatusEofError		= 0x00200000,  // UPF: missing EOF
    enum_fragStatusMarkerError	= 0x00400000,  // UPF: invalid header marker
    enum_fragStatusFormatError	= 0x00800000,  // UPF: Major format version mismatch
	// upper 8 bits: higher-level indicators
    enum_fragStatusDuplicate	= 0x01000000,  // duplicate fragment. BC-ID of this fragment needs to be verified
    enum_fragStatusSeqError		= 0x02000000,  // out-of-sequence fragment
    enum_fragStatusTxError		= 0x04000000,  // transmission error. Set on any S-Link or fragment format error condition
    enum_fragStatusTrunc		= 0x08000000,  // Fragment truncation
    // enum_fragStatusErrMask   = (enum_fragStatusTrunc | enum_fragStatusTxError),
    // enum_fragStatusErrMask   = (enum_fragStatusTxError),  // don't consider truncation an error
	enum_fragStatusShort		= 0x10000000,  // ROD size smaller than header + trailer
    enum_fragStatusLost			= 0x20000000,  // fragment lost. No data is available for the requested L1ID
    enum_fragStatusPending		= 0x40000000,  // fragment pending. No data available yet, but might still arrive
    enum_fragStatusDiscard		= 0x80000000,  // Indicates discard mode
	// generic errors
    enum_fragGenStatusTime		= 0x00000004,  // timeout error: fragment lost or pending
    enum_fragGenStatusData		= 0x00000008,  // data error: corrupted fragment
    enum_fragGenStatusBuffer	= 0x00000010,  // buffer error: truncation
	//
    enum_fragMinTheshold		= 0x02000000,  // min threshold for pending range
    enum_fragMaxTheshold		= 0xfe000000   // max threshold for pending range
} FragStatusIndicator;

// -------------------------------------------------------------------------------
/// Status values for PLX mailbox register
/**
*/
typedef enum {
    enum_mboxStatusReset	= 0x0,			// not initialised / reset
    enum_mboxStatusOk		= 0x0055aa00,	// application running with no BIST errors
    enum_mboxStatusCfgProg	= 0x0155cc88,	// FPGA being (re-) configured by application
    enum_mboxStatusCfgDone	= 0x0055cc88,	// FPGA (re-)configuration completed
    enum_mboxStatusCfgFail	= 0x0055ccff,	// FPGA (re-)configuration failed
    enum_mboxStatusTerm		= 0x00aa5500,	// application terminated due to user request
    enum_mboxStatusWarn		= 0x0055aa88,	// application running with BIST warnings
    enum_mboxStatusErr		= 0x0055aaff,	// application running with BIST errors
    enum_mboxStatusFail		= 0x00aa55ff	// application terminated due to BIST or INIT error
} MboxStatusIndicator;

// -------------------------------------------------------------------------------
/// Emulated event control word pattern
/**
*/
typedef enum {
    enum_emuTypeNone		= 0x0,			// No emulation
    enum_emuTypeInternal	= 0x1,			// Internally formatted, with L1ID masking
    enum_emuTypeUpload		= 0x2,			// Event upload via test input
    enum_emuEventPattern0	= 0x0,			// internal channel pattern 0: counting up with channel marker
    enum_emuEventPattern1	= 0x1,			// internal channel pattern 1: counting up
    enum_emuEventPattern2	= 0x2,			// Data generator ETmis pattern 1. counting up with channel marker
    enum_emuEventPattern3	= 0x3,			// Data generator ETmis pattern 1. counting up
    enum_emuEventPattern4	= 0x4,			// Data generator ETmis pattern 2. counting up with channel marker
    enum_emuEventPattern5	= 0x5,			// Data generator ETmis pattern 2. counting up
    enum_emuEventPattern6	= 0x6,			// Data generator Tile pattern. counting up
    enum_emuEventPattern7	= 0x7,			// Data generator Tile pattern. counting up with channel marker
    enum_emuEventPatternMax	= enum_emuEventPattern7,	// Max value

    enum_emuEventDataError	= 0x1,			// data error bit
    enum_emuEventCtlError	= 0x2,			// ctl error bit
    enum_emuEventCtlDataError = (enum_emuEventDataError | enum_emuEventCtlError), // any error
    enum_emuEventMisBofError = 0x100,		// missing bof
    enum_emuEventMisEofError = 0x200,		// missing eof
    enum_emuEventTerm		= 0x400,		// terminate event

    enum_emuEventMemoryTag	= 0x80000000,	// must be set to use data from memory
    enum_emuEventBofCtl		= 0x1,			// first word is control word
    enum_emuEventEofCtl		= 0x2,			// last word is control word
    enum_emuEventHasData	= 0x4,			// data words present
    enum_emuEventNoError	= (enum_emuEventMemoryTag + enum_emuEventBofCtl + enum_emuEventEofCtl + enum_emuEventHasData)
} EmuEventFlags;

// -------------------------------------------------------------------------------
// Monitoring and error structures

/// Error types
typedef enum {
	enum_errHard = 0,			// any hardware error
	enum_errSoft,				// hard software error (memory failure, illegal operation,...
	enum_errWarn,				// warning
	enum_errBuff,				// buffer full
	enum_errDown,				// link transition to down state
	enum_errXoff,				// transition to xoff
	enum_errMgmtIn,    		    // buffer managment event receive error
	enum_errMgmtOut,    		// buffer managment event retrieve error
	enum_errHeat,    			// temperature error
	enum_errPciDmaRst,			// resetting the dma engine
	enum_errPciDmaAbort,		// Aborting DMA transfer
	enum_errIrq,				// interrupt issued
	enum_LAST_ERRCNT
} ErrorType;

/// Fragment statistics
typedef enum {
	enum_fragsReceived = 0,
	enum_fragsAvail,
	enum_fragsNotAvail,
	enum_fragsPending,
	enum_fragsAdded,
	enum_fragsDeleted,
	enum_fragsTruncated,
	enum_fragsCorrupted,
	enum_fragsRejected,
	enum_fragsReplaced,
	enum_fragsOutOfSync,
	enum_fragsMissOnClear,
	enum_LAST_FRAGSTAT
} FragStat;

/// Page statistics
typedef enum {
	enum_pagesReceived = 0,
	enum_pagesAdded,
	enum_pagesDeleted,
	enum_pagesProvided,				// written to free page fifo
	enum_pagesSupressed,			// truncation
	enum_pagesInvalid,
	enum_pagesDmaRx,
	enum_LAST_PAGESTAT
} PageStat;

/// Message statistics
typedef enum {
	enum_msgsReceived = 0,      // counted at ROL0 only
	enum_msgsAccepted,
	enum_msgsRejected,
	enum_msgsDataReq,
	enum_msgsDataResp,
	enum_msgsClrReq,
	enum_msgsBroadCast,
	enum_msgsPciQueueFull,
	enum_msgsPciTestedEmpty,
	enum_msgsPciTestedOK,
	enum_LAST_MSGSTAT
} MsgStat;

// -------------------------------------------------------------------------------



/// histogramming statistics
typedef enum {
    enum_histGranularity = 4,   // fraction of page size to be used (fragment histogram)
    enum_histEntries = 16       // number of entries for histogramms (all)
} HistStat;

/// Event logging
typedef struct {
    int logEnabled;     // if we keep a history
    int logSize;        // number of log entries
    int current;        // points to current index
    int overflow;       // set after first roll-over
#ifdef __x86_64__
  int eventArray; //dummy variable, not meaningful in 64bit
#else
  Dword **eventArray;  // pointer to log entries
#endif
    } EventLog;

// -------------------------------------------------------------------------------
// -------------------------------------------------------------------------------
// ATLAS part id
typedef struct {
	Byte project[2];	// 20	Atlas
	Byte system[1];		// 0	DAQ
	Byte subSystem[1];	// 3	Readout
	Byte institute[3];	// 082 (UK) or 084 (D)
	Byte prefix[1];		// 1	ROBIN
	Byte sernum[6];		// 1 .. 1023
} AtlasPartId;
// SecSi Sector structure
// don forget to update secsi version if changes made
typedef struct {
	Dword magic;		// should be 0x12344321
	Word secSiVersion;	// format version
	AtlasPartId id;		// part id, MSB first
	Byte prodDate[8];	// Build date, YYYY/MM/DD, Y-MSB at offset 18
	Byte schemDate[8];	// Date of schematics
	Dword schemId;		// Identifier (e.g. version) of schematics.
	Byte pcbName[16];	// 0-terminated string of PCB manufacturer
	Byte pcbId[16];		// 0-terminated production code from PCB manufacturer
	Dword gerberId;		// Identifier (e.g. version) of Germber files.
	Byte asmName[16];	// 0-terminated string of assembly company
	Dword bomVersion;	// Version number of BOM
	Dword asmId;		// Identification number from asm company
} SecSiInfo;

typedef enum {  enum_secSiVersion = 4,
				enum_secSiMagic = 0x12344321
} SecSiFormat;

/// Monitoring and error structure
/**
Provides counters for errors and fragment management<br>
See definitions in #ErrorType, #PageStat, #FragStat, #MsgStat, #HistStat
*/
typedef struct
{
	// Variable commented as ROB has ROB-scope, all others are channel specific
  int          statVersion;                  /// ROB: status/statistic block version. Position of this value must not change
  unsigned int endianFlag;                   /// ROB: endian flag = 0x12345678. Position of this value must not change
  int          statByteSize;                 /// ROB: size of status block.  Position of this value must not change
  int          serNum;                       /// ROB: board serial number
  int          swVersion;                    /// ROB: software version number
  int          cfgVersion;					 /// ROB: configuration version
  int          cfgNumItems;                  /// ROB: Numbe rof configuration items
  int          designVersion;	             /// ROB: FPGA design version
  unsigned int fragFormat;                   /// ROB: ROD format
  unsigned int bufByteSize;                  /// ROB: buffer size. Used to set up host kernel memory structures
  unsigned int numChannels;                  /// ROB: number of channels
  unsigned int cpuSpeed;                     /// ROB: cpu speed
  int          robinModel;                   /// ROB: robin model
  int          appType;                      /// ROB: application type
  int          switches;                     /// ROB: switch settings
  char         cDate[4*sizeof(Dword)];       /// ROB: software compile date (size must be multiple of int)
  char         cTime[4*sizeof(Dword)];       /// ROB: software compile time (size must be multiple of int)
  SecSiInfo    secSi;                        /// ROB: copy of the OPT information. Will be emulated for prototypes
  int	       pagesFree;                    /// number of free pages (true value can be larger by a few pages)
  int	       pagesInUse;					 /// number of used pages (see comment above)
  int    idleCount;                    /// ROB: current value of idle counter (reset upon monitoring command)
  int          errCntSize;					 /// ROB: number of error counter, see #ErrorType
  int          fragStatSize;	             /// ROB: number of fragment statistic entries, see #FragStat
  int          pageStatSize;	             /// ROB: number of page statistic entries, see #PageStat
  int          msgStatSize;					 /// ROB: number of msg statistic entries, see #msgStat
  int          histoSize;					 /// ROB: number of histogram entries
  int          bistResult;                   /// BIST result
  unsigned int mostRecentId;                 /// most recent L1ID
  int		   savedRejectedPages;			 /// number of saved rejected pages
  EventLog     eventLog;                     /// event log
  int          bufferFull;                   /// Current status of buffer
  int          rolXoffStat;                  /// Current status of XOFF signal
  int          rolDownStat;                  /// Current status of LDOWN
  int          tempOk;                       /// ROB: temperature sensor flag
  unsigned int errors[enum_LAST_ERRCNT];     /// error counters, see #ErrorType
  unsigned int fragStat[enum_LAST_FRAGSTAT]; /// fragment statistics, see #FragStat
  unsigned int pageStat[enum_LAST_PAGESTAT]; /// page statistics, see #PageStat
  unsigned int msgStat[enum_LAST_MSGSTAT];   /// message statistics, see #MsgStat
  unsigned int fragHisto[enum_histEntries];  /// fragment histogram, see #HistStat
  unsigned int bufHisto[enum_histEntries];   /// buffer histogram, see #HistStat
} StatisticsBlock;
// -------------------------------------------------------------------------------

// configuration stuff
/// string length of configuration item name
#define CFG_PARM_NAME_LEN 15

/// list of configuration items. Integer/bool values only
typedef enum {
    enum_cfgSerNum = 0,     // serial number
    enum_cfgSubDet,         // subdetector id
    enum_cfgPageSize,       // buffer manager page size
    enum_cfgNumPages,       // pages to be used. MUST come after pageSize !!!
    enum_cfgHashBits,       // number of hash bits. small value less memory, but increased search time
    enum_cfgKeepFrags,      // fake deletion of fragments
    enum_cfgRolEmu,         // use rol emulation
    enum_cfgTestSize,       // default emulation fragment size
    enum_cfgDebug,          // enable debugging
    enum_cfgDumpRol,        // dump ROL data
    enum_cfgInterActive,    // enable user interface for tests
    enum_cfgKeepCtlWords,   // don't strip off S-Link ctl words
    enum_cfgRolDataGen,     // use data generator for S-Link input
    enum_cfgRolEnabled,     // if 0, channel is disabled
    enum_cfgExtendedBist,   // if 1, run extended BIST
    enum_cfgEmuPattern,     // set pattern of data generator (maybe sw-emu as well)
    enum_cfgContinousBist,  // do ebist only
    enum_cfgIgnoreBist,	    // don't care about result
    enum_cfgRolExtLoop,	    // ROL BIST loopback with external fibre
    enum_cfgDmaFifoCheck,   // check filling state of DMA fifos prior to sending data
    enum_cfgUseFragDma,	    // use fragment (UPF) dma
    enum_cfgUseHdrDma,	    // use header dma
    enum_cfgPrescaleFrag,   // loop prescaler values
    enum_cfgPrescaleMsg,    // "
    enum_cfgPrescaleFpf,    // "
    enum_cfgRolSrcId,	    // channel source id
    enum_cfgSecSiEmu,	    // secsi sector emulation
    enum_cfgGcPages,	    // Garbage collection threshold for free pages
    enum_cfgMaxRxPages,	    // truncate incoming fragments after so many pages
    enum_cfgTempAlarmValue, // upper threshold for MAX1618
	enum_cfgRolBofNoWait,	// Don't wait for BOF after reset
	enum_cfgIrqEnable,		// Enable host interrupts
	enum_cfgDiscardMode,	// Discard input fragments
    enum_cfgNumItems
} CfgItems;

/// Layout of various configuration parameters, see #CfgItems. Values only integer/bool
typedef struct {
    char name[CFG_PARM_NAME_LEN + 1];
    /// If 0, value cannot be changed at run-time. FLASH-update and reset required
	/*
	int dynamic;
    /// if 1, modification requires re-initialisation of management
    int init;
    /// bool or integer
    int isBool;
    // global (affecting all ROLs)
    int global;
    // If 1, cannot be changed via configure command
    int fixed;
	*/
    // Flags:
	// Dynamic: if 0, requires reset to be updated
	// Init: if 1, modification requires re-initialisation of management
	// isBool: boolean value (else integer)
	// global: if 1: affects all channels
	// fixed: if 1, cannot be changed via message passing
	// expert: only relevant for advanced testing
	// unsigned int unusedFlags: 32 - 24, expert: 4, dynamic:4, init:4, isBool:4, global:4, fixed:4;
#ifndef HOST
	char expert;
	char dynamic;
	char init;
	char isBool;
	char global;
	char fixed;
	char unusedFlags[2];	// to make up to 32 bit boundary
#else
	char isBool;
	char init;
	char dynamic;
	char expert;
	char unusedFlags[2];	// to make up to 32 bit boundary
	char fixed;
	char global;
#endif

    unsigned int value; // this is the variable
    unsigned int defVal;
    unsigned int minVal;
    unsigned int maxVal;
} CfgParm;

/// BIST result codes
typedef enum {
    enum_bistDesIdWarning   = (1 <<  0),
    enum_bistDesIdWrong     = (1 <<  1),
    enum_bistResetError     = (1 <<  2),

    enum_bistBufEnableError = (1 <<  3),
    enum_bistBufDataError   = (1 <<  4),
    enum_bistBufAddrError   = (1 <<  5),
    enum_bistBufExtError    = (1 <<  6),

    enum_bistRolEnableError = (1 << 7),
    enum_bistRolTlkPrbs     = (1 << 8),
    enum_bistRolTlkLoop     = (1 << 9),
    enum_bistRolTlkExtLoop  = (1 << 10),

    enum_bistParmError      = (1 << 11),  // wrong call paramter

	enum_bistLoadCfgError      = (1 << 12),  // Initialisation error
	enum_bistInitPpcError      = (1 << 13),  // Initialisation error
	enum_bistInitRolError      = (1 << 14),  // Initialisation error
	enum_bistRclkSpeedError	   = (1 << 15),	 // wrong RCLK speed
	enum_bistSecSiInvalid	   = (1 << 16),	 // SecSi sector not valid

    enum_bistErrorTypes     = 17,        // number of errors
    enum_bistError          = (1 << 31)   // highest bit indicated hard error. else warning only
} BistCodes;
// --------------------------------------------------------------------------------
/** End of shared section */
/*@}*/
// --------------------------------------------------------------------------------

// --------------------------------------------------------------------------------
/** Buffer manager section \defgroup RobInBMan Buffer management items */
/*@{*/
// -------------------------------------------------------------------------------
// management paramters, values in words

#define SMALL_MGMT		// stay with 16 bit values where possible

typedef enum {
    enum_mgmtMinHashBits = 3,
#ifdef SMALL_FOOTPRINT
    enum_mgmtMaxHashBits = 6,
#else
    enum_mgmtMaxHashBits = 16,
#endif
    enum_mgmtMinPageSize = 256, // MIN_PAGE_BYTE_SIZE/sizeof(Dword),
    enum_mgmtMaxPageSize = 2048, // MAX_PAGE_BYTE_SIZE/sizeof(Dword),
#ifdef SMALL_MGMT
	enum_mgmtMaxBufSizePpc = (16*1024*1024),	// real max size
#else
	enum_mgmtMaxBufSizePpc = (256*1024*1024),	// real max size: 1GB
#endif
#ifndef HOST
    enum_mgmtMaxBufSize  = enum_mgmtMaxBufSizePpc, // MAX_BUFFER_BYTE_SIZE/sizeof(Dword),
#else
    enum_mgmtMaxBufSize  = (1024*1024), // limit buffer size on host
#endif

	// rejected page storage
#ifndef HOST
	enum_mgmtRejectedSize = 100,
#else
	enum_mgmtRejectedSize = 10,
#endif

#ifdef SMALL_FOOTPRINT
    enum_mgmtMaxNumPages = (1*32) // 1024 // 512
#else
#ifdef HOST
    enum_mgmtMaxNumPages = 128
#else
#ifdef SMALL_MGMT
    enum_mgmtMaxNumPages = 0x10000         // this is due to the size of the management pointer, which is 16 bit
#else
    enum_mgmtMaxNumPages = 0x100000
#endif
#endif
#endif

} MgmtParms;

/// Fragment info received from FPGA buffer manager
// make sure that a 128 byte is a multiple of this size for UPFDMA
typedef struct {
    Dword status;		// has 2 entries: Byte lastPage, fragmentStatus (24 Bit)
    Dword eventId;		// hase 2 entries: Byte ECR, L1ID (24 Bit)
#ifndef HOST
    Word pageNum;		// embedded trigger type in upper byte
    Word pageLen;
#else
    Word pageLen;		// in Dwords
    Word pageNum;
#endif
    Dword runNum;		// run number
} UpfInfo;

/*
/// Fragment info received from FPGA buffer manager
typedef struct {
    Dword status;		// has 2 entries: Byte lastPage, fragmentStatus (24 Bit)
    Dword eventId;		// hase 2 entries: Byte ECR, L1ID (24 Bit)
#ifndef HOST
    Word pageNum;
    Word pageLen;
#else
    Word pageLen;		// in Dwords
    Word pageNum;
#endif
    Dword runNum;		// run number
	Dword TrigType;
	Dword dummy[3];		// 3 dummy words to increas size to 8 dwords for DMA
} FragInfo;
*/

/// Fragment info received from FPGA buffer manager
typedef struct {
	UpfInfo upfInfo;
	Dword trigType;
	Dword prevId;
} FragInfo;

/// event identification via l1id and run number
typedef struct {
	Dword eventId;
	Dword runNumber;
} EventTag;

// -------------------------------------------------------------------------------
/// Management pointer needs to hold indices of elements
/** size must be sufficiently large, e.g. Word for 16 bit hash key and 64k pages
*/
#ifdef SMALL_MGMT
	typedef Word MgmtPtr;
#else
	typedef Dword MgmtPtr;
#endif

/// Management elements for fragment lists
/**
A management entry is composed of two word pointers (previous, next),
the FragInfo field, a status field and a fragment size field (total size in words)<br>
In addition, there is a flag to indicate completion (i.e. all pages of a multi-page
fragment received.<br>
*/
typedef struct
{
	MgmtPtr prev;
	MgmtPtr next;
	FragInfo info;
	Dword status;
	// Dword size;
	Word size;
    Word isComplete;
} MgmtEntry;

/// Management elements for rejected pages
/**
Keeps UPF info block and MRI
*/
typedef struct
{
	Dword mri;		// most recent (valid) L1ID
	FragInfo info;
} RejectedEntry;

// --------------------------------------------------------------------------------
/** End of buffer manager section */
/*@}*/
// --------------------------------------------------------------------------------

// --------------------------------------------------------------------------------
/** Messaging section \defgroup RobInMsg Message handling items */
/*@{*/
// -------------------------------------------------------------------------------

// -------------------------------------------------------------------------------
/// Header markers and IDs
/** \struct HdrMarkers
Define markers and fixed ids for ROB and ROS header<br>
*/
typedef enum
{
	enum_hdrMarkerRod = 0xee1234ee,
	enum_hdrMarkerRob = 0xdd1234dd,
	enum_hdrMarkerRos = 0xcc1234cc,
	enum_hdrMarkerPciTx = 1,        // indicate ongoing transfer
	enum_hdrMarkRobMod = 1 << 16,
	enum_hdrMarkRosMod = 2 << 16
} HdrMarker;

// -------------------------------------------------------------------------------
/// struct RobMsg defines generic ROB Fragment message
/**
The format of the ROB-fragment is defined in ATL-DAQ-98-129. <br>
*/
typedef enum {
	enum_robHdrStatusWords = 2,
	enum_robHdrCrcType = 0x01,		// Robin used 16 bit CRC
	enum_robHdrCrcTypeNone = 0x00,		// No CRC on truncated fragments
	enum_robHdrSpecificWords = 0
} RobHdrDefs;

typedef struct {
	// generic
	Dword marker;
	Dword totalSize;
	Dword hdrSize;
	Dword version;
	Dword srcId;
    Dword statCnt;			// 2 status words here
	Dword fragStat;
    Dword mrEvntId;         // most recent event id from S-Link
   	Dword crc;				// crc added in event format version 4
	// specific: none
	// Dword specCnt;			// spec count removed in event format version 4
} RobMsgHdr;


#ifdef ROS_HDR
// -------------------------------------------------------------------------------
/// struct RosMsg defines generic ROS Fragment message
/**
The format of the ROB-fragment is defined in ATL-DAQ-98-129. <br>

*/
typedef struct {
	// generic
	Dword marker;
	Dword totalSize;
	Dword hdrSize;
	Dword version;
	Dword srcId;
	Dword statCnt;
	Dword fragStat;
	// specific
	Dword specCnt;      // the 3 items: run number, bxId, i1Id
    Dword runNum;
	Dword l1Id;
	Dword bxId;
} RosMsgHdr;
#endif

// -------------------------------------------------------------------------------
/// struct RodInfo defines header and trailer of ROD fragment
/**
The format of the ROD-fragment is defined in ATL-DAQ-98-129. <br>
Header:<br>
0	Start of header marker
1	Header size
2	Format version number
3	Source identifier
4	Run number
5	Extended Level 1 ID
6	Bunch crossing ID
7	Level 1 trigger type
8	Detector event type
Trailer:<br>
0	Status word
1	Number of status words
2	Number of data elements
3	Status block position (1: status after data)
*/
typedef struct {
	Dword hdrMarker;
	Dword hdrSize;
	Dword eventFormat;
	Dword srcId;
	Dword runId;
	Dword eventId;
	Dword bxId;
	Dword trigType;
	Dword eventType;
} RodHeader;

typedef struct {
	Dword status;
	Dword numStatusWords;
	Dword numDataWords;
	Dword statusPosition;
} RodTrailer;

typedef struct {
	RodHeader header;
    // the data section is in between here
    RodTrailer trailer;
} RodFragment;

typedef struct {
	Dword header[9];
    // the data section is in between here
    Dword trailer[4];
} RodInfo;


// -------------------------------------------------------------------------------
/// DMA command structures
/**
Responses are sent by the PowerPC to either network (= MAC) or PCI. In both cases the generic
format is identical and consists of the DMA descriptor, a media dependent field and header data.<br>
DMA descriptor gives the size of the header (lower 15 bit in hdrInfo), size of user data
and starting address of user data in the SD-RAM buffer. <br>
For a regular fragment response (= ROB Fragment) the user data is
copied by the DMA engine from the buffer. The upper 4 bits of the field dataAddr are used to
encode the ROL number to identify the buffer in a multi-link RobIn.<br>
The DMA engine will read the descriptor, then copy the header from the fifo followed by the buffer
data, if any.<br>
For other message types user data will be part of the header and included in the header length.<br>
IP handling is different for RobinPrototype and Robin3L.<br>
Prototype (aka. VHDL_VER3):<br>
For IP (hence also UDP) style messages an incomplete header may be sent, using the incomplete header flag. This will
discard the last 16 bits of the last header word.<br>
For network messages the hdrInfo field contains a VLAN-enableFlag (bit 15) and a VLAN word (bits 16..31). <br>
Final Robin (aka. VHDL_VER4):<br>
The VLAN control has to be embedded into the byte stream (tag and value). This has to be indicated by
the bit VLAN-PACKET (bit 15)<br>
For IP style messages, a 16 bit dummy value can be appended to the ethernet header which is the
stripped of in hardware, to produce the correct bytestream. This behaviour is controlled by the
bit IP_TX_ALIGNEMENT (bit 14).<br>


1st 32 bit element: hdrInfo  - VLAN word (31..16), VLAN EnableFLag (15), Incomplete header flag (14), HeaderLength (13..0) <br>
2nd 32 bit element: dataSize - size of data block in 32 bit words <br>
3nd 32 bit element: dataAddr - BufferNumber (31..28), start address (in 32 bit words) (27..0) <br>
... 32 bit elements: optional header words, if header length in first word > 0

For PCI oriented messages the VLAN word and the VLAN enable flag must be ZERO.<br>

The media dependent field is different for PCI and network messages:<br>
For PCI oriented messages the destination address on the PCI bus is provided. This address
is initially copied from the "destination address" fields in the request and maintained by the processor.<br>
Note: the destination address is a 64 bit value, event though to date only 32 bit PCI addresses are used.
The two address word are NOT accounted for in the header length field but rather the PCI dma engines reads
the 2 additional words every time (effectively increases the size of the dmadescriptor).<br>
<b>Note that the PCI protocoll requires that the first word of the response message has to be written AFTER
the rest of the message. To indicate processing the first response word should be set to 1 in the intials
transfer and then set to the Marker value after transfer completion.<br>
</b>
*/
typedef struct
{
	int hdrInfo;		// consist of VLAN-tag (if enable), VLAN Enable Flag, Incomplete-header falg, size of header in 32 bit words
	int dataSize;		// size of data block in 32 bit words, if to be taken from buffer. Else 0
	int dataAddr;		// start address of data block in buffer. Upper 4 bits used for ROL number
} DmaDesc;

/// For PCI DMA 2 words in addition to DmaDesc are required, to indicate PCI destination address
typedef struct
{
    DmaDesc dma;        // this is the normal DMA descriptor
    Dword destAddr;     // PCI (better: local bus) destination address
    Dword destAddrExt;  // PCI (better: local bus) destination address
} DmaDescPci;

// PCI request tag
typedef struct {
	Dword mediaHeader;	// must be at position 0
	Dword destAddr;
	Dword destAddrExt;
} PciReqTag;

// net request tag
typedef struct {
	Dword mediaHeader;	// must be at position 0
	Dword vlan;
	Dword dummy;
} NetReqTag;

typedef union {
	PciReqTag pci;
	NetReqTag net;
} ReqTag;


/// Media descriptor
/**
ENUM for media type and media header size
*/
typedef enum {
	enum_mediaTypeMask = 0xffff0000,
	enum_mediaSizeMask = 0x1fff,		// only bits 0..12 can be used, bit 13 is used to mark "partial" packets
	enum_mediaDataBlockStartAddrMask = 0xffffff,
	// enum_mediaPciSize = 3,
	enum_mediaPciSize = sizeof(ReqTag)/sizeof(Dword),
	enum_mediaPciTypeMarker = 0x0,
	// enum_mediaNetSize = 3,			// entries for media descriptor and vlan info in NetHdr. must match NetHdrOffset
	enum_mediaNetSize = sizeof(ReqTag)/sizeof(Dword),			// entries for media descriptor and vlan info in NetHdr. must match NetHdrOffset
	enum_mediaNetTypeMarker = enum_mediaTypeMask,
	enum_mediaNetVlanEnableShift = 15,	// bit to indicate VLAN tagged frame
	enum_mediaNetShortHdrShift = 14,	// bit to indicate last header word is 16 bit wide only on VHDL3
						// or ip-alignement on VHDL4
    enum_mediaBufIdShift   = 28,            // bits to shift buffer id in buffer address field
	enum_mediaTypeField = 0,
	enum_mediaPciAddrField = 1,
	enum_mediaPciXAddrField = 2,
	enum_mediaVlanShift = 16,
	enum_mediaVlanEnBit = 0x8000,            //Vlan enable bit in DmaDesc.hdrInfo
	enum_mediaIncLstWord = 0x4000,           //Incomplete last header word bit in DmaDesc.hdrInfo
	enum_mediaPartialPacketEnBit = 0x2000 	 //Ethernet packet is not complete yet, thus the "partial" packet bit controls the filling up
} MediaDesc;

// --------------------------------------------------------------------------------
/** End of buffer manager section */
/*@}*/
// --------------------------------------------------------------------------------

// --------------------------------------------------------------------------------
/** General section \defgroup RobIn General items */
/*@{*/
// -------------------------------------------------------------------------------

// -------------------------------------------------------------------------------
// -------------------------------------------------------------------------------
/// Common control register
#ifdef VHDL_VER3
/**
Reset and related bits<br>
---------------

  -- assert buffer manager reset while buffer clock system not operational
  Reset_Buff_Man_0			<= control_bit(27) or PPC_Rst; --  or not Bclk_locked;
  Reset_Buff_Man_1			<= control_bit(2) or PPC_Rst; --  or not Bclk_locked;
  Reset_Buff_Man_2			<= control_bit(3) or PPC_Rst; -- or not Bclk_locked;
  Reset_Buffer_Ctl_0			<= control_bit(28) or PPC_Rst;
  Reset_Buffer_Ctl_1			<= control_bit(4) or PPC_Rst;
  Reset_Buffer_Ctl_2			<= control_bit(5) or PPC_Rst;

  Reset_PCI_Header_Fifo		<= control_bit(6) or PPC_Rst;
  Reset_PCI_Receive_Interface	<= control_bit(7) or PPC_Rst;
  Reset_PCI_Local_Bus_Client	<= control_bit(8) or PPC_Rst;
  Reset_PCI_DMA_Engine		<= control_bit(9) or PPC_Rst;

  Reset_MAC_Header_Fifo		<= control_bit(10) or PPC_Rst;
  Reset_MAC_Interface			<= control_bit(11) or PPC_Rst;
  Reset_MAC_DMA_Engine		<= control_bit(12) or PPC_Rst;
  Reset_TLK_Bypass	    	<= control_bit(29) or PPC_Rst;
  FPGA_LED0					<= control_bit(13);
  -- FPGA_LED1					<= control_bit(14);
  BclkSlowSelect				<= control_bit(16);		-- switch between 33 (slow) and 66Mhz source frequency
  resetCnt					<= control_bit(17);		-- reset clock counters
  Single_Cycle				<= control_bit(18);		-- enable single cycle read access for 33MHz

  -- local bus interrupt
  lbusIntEn	 				<= control_bit(19);		-- enable interrupt on local bus

  buffer_page(0)              <= control_bit(24);
  buffer_page(1)              <= control_bit(25);

  page_select : process (buffer_page) is
  begin
    case buffer_page is
      when "00" =>
      buffer_mem_page_select <= Buffer_Page_0_select;

      when "01" =>
      buffer_mem_page_select <= Buffer_Page_1_select;

      when "10" =>
      buffer_mem_page_select <= Buffer_Page_2_select;

      when others =>
      buffer_mem_page_select <= Buffer_Page_deselect;

    end case;
  end process page_select;

---------------

Reset_Rol_Handler_1         <= control_bit(0) or PPC_Rst;<br><br>
Reset_Rol_Handler_2			<= control_bit(1) or PPC_Rst;<br>
-- assert buffer manager reset while buffer clock system not operational
Reset_Buff_Man_1			<= control_bit(2) or PPC_Rst or not Bclk_locked;<br>
Reset_Buff_Man_2			<= control_bit(3) or PPC_Rst or not Bclk_locked;<br>
Reset_Buffer_Ctl_1			<= control_bit(4) or PPC_Rst;<br>
Reset_Buffer_Ctl_2			<= control_bit(5) or PPC_Rst;<br>
Reset_PCI_Header_Fifo		<= control_bit(6) or PPC_Rst;<br>
Reset_PCI_Input_Fifo		<= control_bit(7) or PPC_Rst;<br>
Reset_PCI_Local_Bus_Client	<= control_bit(8) or PPC_Rst;<br>
Reset_PCI_DMA_Engine		<= control_bit(9) or PPC_Rst;<br>

Reset_PCI_Header_Fifo		<= control_bit(6) or PPC_Rst;<br>
Reset_PCI_Input_Fifo		<= control_bit(7) or PPC_Rst;<br>
Reset_PCI_Local_Bus_Client	<= control_bit(8) or PPC_Rst;<br>
Reset_PCI_DMA_Engine		<= control_bit(9) or PPC_Rst;<br>

Reset_MAC_Header_Fifo		<= control_bit(10) or PPC_Rst;<br>
Reset_MAC_Interface			<= control_bit(11) or PPC_Rst;<br>
Reset_MAC_DMA_Engine		<= control_bit(12) or PPC_Rst;<br>
FPGA_LED0						<= control_bit(13);<br>
FPGA_LED1						<= control_bit(14);<br>
FPGA_LED2						<= control_bit(15);<br>
BclkSlowSelect				<= control_bit(16);-- switch between 33 (slow) and 66Mhz source frequency<br>
resetCnt					<= control_bit(17);-- reset clock counters<br>
Single_Cycle				<= control_bit(18);-- enable single cycle read access for 33MHz<br>

lbusIntEn                   <= control_bit(19);

buffer_page(0)              <= control_bit(24);
buffer_page(1)              <= control_bit(25);
	case buffer_page is
	when "00" =>
		buffer_mem_page_select <= Buffer_Page_0_select;

	when "01" =>
		buffer_mem_page_select <= Buffer_Page_1_select;

	when "10" =>
		buffer_mem_page_select <= Buffer_Page_2_select;

	when others =>
       buffer_mem_page_select <= Buffer_Page_deselect;



*/
typedef enum {
    enum_comCtlResetRolHandler1 = 1 << 0,
    enum_comCtlResetRolHandler2 = 1 << 1,
    enum_comCtlResetBufMgr1     = 1 << 2,
    enum_comCtlResetBufMgr2     = 1 << 3,
    enum_comCtlResetBufCtl1     = 1 << 4,
    enum_comCtlResetBufCtl2     = 1 << 5,
    enum_comCtlResetPciHdrFifo  = 1 << 6,
    enum_comCtlResetPciMsgFifo  = 1 << 7,
    enum_comCtlResetPciLbus     = 1 << 8,
    enum_comCtlResetPciDma      = 1 << 9,
    enum_comCtlResetMacHdrFifo  = 1 << 10,
    enum_comCtlResetMacMsgFifo  = 1 << 11,
    enum_comCtlResetMacDma      = 1 << 12,
    enum_comCtlFpgaLed0         = 1 << 13,
    enum_comCtlFpgaLed1         = 1 << 14,
    enum_comCtlFpgaLed2         = 1 << 15,
    enum_comCtlBclkSlowSelect   = 1 << 16,
    enum_comCtlResetCnt         = 1 << 17,
    enum_comCtlSingleCycle      = 1 << 18,
    enum_comCtlLbusIntEn        = 1 << 19,
    enum_comCtlClkMuxBclkRol1   = 1 << 20,
    enum_comCtlGlobalReset      = 1 << 21,
    enum_comCtlBufPage0         = 1 << 24,
    enum_comCtlBufPage1         = 1 << 25,
    enum_comCtlResetBypass      = 1 << 26,
    enum_comCtlGlobalResetMask  = (((1 << 13) - 1) | enum_comCtlResetBypass | enum_comCtlGlobalReset)    // all resets below up to bit 12
} ComCtlBits;
#endif

#ifdef VHDL_VER4
/*

  Reset_Rol_Handler_0         <= control_bit(0) or PPC_Rst;
  Reset_Rol_Handler_1         <= control_bit(1) or PPC_Rst;
  Reset_Rol_Handler_2		<= control_bit(2) or PPC_Rst;
  -- assert buffer manager reset while buffer clock system not operational
  Reset_Buff_Man_0			<= control_bit(3) or PPC_Rst; --  or not Bclk_locked;
  Reset_Buff_Man_1			<= control_bit(4) or PPC_Rst; --  or not Bclk_locked;
  Reset_Buff_Man_2			<= control_bit(5) or PPC_Rst; -- or not Bclk_locked;
  Reset_Buffer_Ctl_0		<= control_bit(6) or PPC_Rst;
  Reset_Buffer_Ctl_1		<= control_bit(7) or PPC_Rst;
  Reset_Buffer_Ctl_2		<= control_bit(8) or PPC_Rst;

  Reset_PCI_Header_Fifo		<= control_bit(9) or PPC_Rst;
  Reset_PCI_Receive_Interface	<= control_bit(10) or PPC_Rst;
  Reset_PCI_Local_Bus_Client	<= control_bit(11) or PPC_Rst;
  Reset_PCI_DMA_Engine		<= control_bit(12) or PPC_Rst;

  Reset_MAC_Header_Fifo		<= control_bit(13) or PPC_Rst;
  Reset_MAC_Interface		<= control_bit(14) or PPC_Rst;
  Reset_MAC_DMA_Engine		<= control_bit(15) or PPC_Rst;
  Reset_TLK_Bypass	    		<= control_bit(16) or PPC_Rst;

  lbusIntEn	 			<= control_bit(20);		-- enable interrupt on local bus

  buffer_page(0)              <= control_bit(21);
  buffer_page(1)              <= control_bit(22);

  resetCnt				<= control_bit(23);		-- reset clock counters

  FPGA_LED0				<= control_bit(24);
  -- FPGA_LED1				<= control_bit(25);

  -- local bus interrupt

  page_select : process (buffer_page) is
  begin
    case buffer_page is
      when "00" =>
      buffer_mem_page_select <= Buffer_Page_0_select;

      when "01" =>
      buffer_mem_page_select <= Buffer_Page_1_select;

      when "10" =>
      buffer_mem_page_select <= Buffer_Page_2_select;

      when others =>
      buffer_mem_page_select <= Buffer_Page_deselect;

    end case;
  end process page_select;

*/
typedef enum {
    enum_comCtlResetPciLbc      = 1 << 15,	// additional reset condition for PCI local bus controller
    enum_comCtlResetPciDma      = 1 << 16,	// additional reset condition for PCI DMA
    enum_comCtlGlobalReset      = 1 << 17,	// activates all resets
    enum_comCtlLbusIntEn        = 1 << 20,
    enum_comCtlBufPage0         = 1 << 21,
    enum_comCtlBufPage1         = 1 << 22,
    enum_comCtlResetCnt         = 1 << 23,
    enum_comCtlFpgaLed0         = 1 << 24,
    enum_comCtlFpgaLed1         = 1 << 25,
    enum_comCtlGlobalResetMask  = enum_comCtlGlobalReset
} ComCtlBits;
#endif

// -------------------------------------------------------------------------------
// -------------------------------------------------------------------------------
/// Common status register
#ifdef VHDL_VER3
/**
Common_Status_Output_Data(31 downto 16)   <= std_logic_vector(bclkCnt(15 downto 0));
Common_Status_Output_Data(15 downto 5)    <= (others => '0'); -- x"0FF"  ;
Common_Status_Output_Data(4)              <= cntDone;	-- pclk reached X"ff"
Common_Status_Output_Data(3)              <= useInvertedPciReset;
Common_Status_Output_Data(2)              <= Buffer_Mem_2_Initialized;
Common_Status_Output_Data(1)              <= Buffer_Mem_1_Initialized;
Common_Status_Output_Data(0)              <= Bclk_locked;
*/
typedef enum {
    enum_comStatBufInit1        = 1 << 1,
    enum_comStatBufInit2        = 1 << 2,
    enum_comStatHasGlobalReset  = 1 << 3,
    enum_comStatBclkCntDone     = 1 << 4,
    enum_comStatBclkCntMask     = 0xffff0000
} ComStatusBits;
#endif

#ifdef VHDL_VER4
/**
  Common_Status_Output_Data(31 downto 16)   <= std_logic_vector(bclkCnt(15 downto 0));  -- x"FACE" ;
  Common_Status_Output_Data(15 downto 5)    <= (others => '0'); -- x"0FF"  ;
  Common_Status_Output_Data(4)              <= cntDone;	-- pclk reached X"ff"
  Common_Status_Output_Data(3)              <= Buffer_Mem_0_Initialized;
  Common_Status_Output_Data(2)              <= Buffer_Mem_2_Initialized;
  Common_Status_Output_Data(1)              <= Buffer_Mem_1_Initialized;
  Common_Status_Output_Data(0)              <= Bclk_locked;
*/
typedef enum {
    enum_comStatBufInit0        = 1 << 0,
    enum_comStatBufInit1        = 1 << 1,
    enum_comStatBufInit2        = 1 << 2,
    enum_comStatRclkCntDone     = 1 << 3,
    enum_comStatRclkCntShift    = 8,			// shift right
    enum_comStatRclkCntMask     = 0x000fffff	// mask after shift
} ComStatusBits;
#endif

// -------------------------------------------------------------------------------
// -------------------------------------------------------------------------------
#ifdef VHDL_VER4
/**
   PCI Quick Status
   -- Map status SLINK Leds to SLINK status bus
   SLINK_STATUS(0)   <= ROL0_LDERRLED_n;		-- link data error (data or control word)
   SLINK_STATUS(1)   <= ROL0_LUPLED_n;			-- link up
   SLINK_STATUS(2)   <= ROL0_FLOWCTLLED_n;		-- XOFF
   SLINK_STATUS(3)   <= ROL0_ACTIVITYLED_n;		-- Data transmission
   SLINK_STATUS(4)   <= ROL1_LDERRLED_n;
   SLINK_STATUS(5)   <= ROL1_LUPLED_n;
   SLINK_STATUS(6)   <= ROL1_FLOWCTLLED_n;
   SLINK_STATUS(7)   <= ROL1_ACTIVITYLED_n;
   SLINK_STATUS(8)   <= ROL2_LDERRLED_n;
   SLINK_STATUS(9)   <= ROL2_LUPLED_n;
   SLINK_STATUS(10)  <= ROL2_FLOWCTLLED_n;
   SLINK_STATUS(11)  <= ROL2_ACTIVITYLED_n;
   SLINK_STATUS(12)  <= RH0_EnableLdcEmu;		-- Data generator
   SLINK_STATUS(13)  <= RH1_EnableLdcEmu;
   SLINK_STATUS(14)  <= RH2_EnableLdcEmu;
*/
typedef enum {
	enum_quickNotLinkError0		= (1 << 0),
	enum_quickNotLinkUp0		= (1 << 1),
	enum_quickNotLinkXoff0		= (1 << 2),
	enum_quickNotLinkActive0	= (1 << 3),

	enum_quickNotLinkError1		= (1 << 4),
	enum_quickNotLinkUp1		= (1 << 5),
	enum_quickNotLinkXoff1		= (1 << 6),
	enum_quickNotLinkActive1	= (1 << 7),

	enum_quickNotLinkError2		= (1 << 8),
	enum_quickNotLinkUp2		= (1 << 9),
	enum_quickNotLinkXoff2		= (1 << 10),
	enum_quickNotLinkActive2	= (1 << 11),

	enum_quickDataGen0			= (1 << 12),
	enum_quickDataGen1			= (1 << 13),
	enum_quickDataGen2			= (1 << 14),

	enum_quickAppActive			= (1 << 31)
} PciQuickStatus;
#endif
// -------------------------------------------------------------------------------
// -------------------------------------------------------------------------------
// include some specific PPC definitions
#include "robin_ppc.h"

// -------------------------------------------------------------------------------
// -------------------------------------------------------------------------------
// Some Application specific values which need to be available to the ROS applications as well
#define ROBIN_MAIN_APP_BINARY_ADDRESS   0xff800000
#define ROBIN_BKP_APP_BINARY_ADDRESS	0xffA00000
#define ROBIN_MAIN_FPGA_BINARY_ADDRESS  0xff900000
#define ROBIN_BKP_FPGA_BINARY_ADDRESS   0xffB00000

// -------------------------------------------------------------------------------
/// Important software version numbers. Modify number if functionality (or size) changes
typedef enum {
    enum_swVersion = 0xb0001,   // general SW number !!! don't use 0xa002b - used for mmu fix on older version
    enum_cfgVersion = 0x2d,       // configuration data structure
	enum_basicStatVersion = 0x0e,	// version of statistics structure
	enum_statVersion = (enum_basicStatVersion << 16) + enum_secSiVersion  // status/statistic data structure
} VersionCodes;

// --------------------------------------------------------------------------------
/** End of general RobIn section */
/*@}*/
// --------------------------------------------------------------------------------

// reset packing
#ifdef HOST
#pragma pack()
#endif


#endif // ROBIN_H

// --------x----------------------------x-----------------------------------x--
/**
 ** \file   PlxEepromFile.h
 **
 ** \author Matthias Mueller
 **
 ** \date
 **
 */
// --------x----------------------------x-----------------------------------x--


#ifndef ROSROBIN_PLXEEPROMFILE_H
#define ROSROBIN_PLXEEPROMFILE_H

#include <string>
#include <vector>
#include <iosfwd>

namespace ROS 
{
  class Robin;

  class PlxEepromFile 
  {
  public:
    // This constructor opens a file and reads eeprom contents from there.
    PlxEepromFile(std::string fileName);

    // This constructor assigns the object a black eeprom. If this would be
    // writte, the PLX would not load the eeprom any more an assign
    // default values.
    PlxEepromFile(void);

    // Reads the data from a file           
    void read(std::string fileName);

    // Writes the data to a file
    void write(std::string fileName);

    std::vector<unsigned short> getRawData(void);
    void setFromRawData(std::vector<unsigned short> & rawData);
    std::string getSerialNumber(void);
    std::string getBoardType(void);
    unsigned int getRevisionNumber(void);

  private:
    void writeText(std::ofstream & outputStream);
    void writeIntelHex(std::ofstream & outputStream);
    void readText(std::ifstream & inputStream);
    unsigned int readTextItem(std::ifstream & inputStream, std::string key);
    void readIntelHex(std::ifstream & inputStream);
    void wordSwap(unsigned int & dword);

    struct EepromDataStruct 
    {
      unsigned short m_deviceId;
      unsigned short m_vendorId;
      unsigned int   m_classCode;
      unsigned char  m_maxLatency;
      unsigned char  m_minGrant;
      unsigned char  m_intPin;
      unsigned char  m_intLineRouting;
      unsigned int   m_mailbox0;
      unsigned int   m_mailbox1;
      unsigned int   m_pci2LocalRange;
      unsigned int   m_pci2LocalBase;
      unsigned int   m_localArbiterReg;
      unsigned int   m_endianDescriptor;
      unsigned int   m_pci2LocalExpRomRange;
      unsigned int   m_pci2LocalExpRomBase;
      unsigned int   m_busRegionDescr0;
      unsigned int   m_dm2PciRange;
      unsigned int   m_dm2PciMemoryBase;
      unsigned int   m_dm2PciIoCfgBus;
      unsigned int   m_dm2PciBase;
      unsigned int   m_dm2PciIoCfgAddr;
      unsigned short m_subsystemDeviceId;
      unsigned short m_subsystemVendorId;
      unsigned int   m_pci2Local1Range;
      unsigned int   m_pci2Local1Base;
      unsigned int   m_busRegionDescr1;
      unsigned int   m_hotSwapControl;
      unsigned short m_pciArbiterControl;
      EepromDataStruct(void);
    } __attribute__ ((packed));

    EepromDataStruct m_eepromContent;

    std::string    m_vpdBoardName;
    std::string    m_vpdBoardSerial;
    unsigned int   m_vpdBoardRev;
  };
}
#endif //ROSROBIN_JTAGDEVICE_H

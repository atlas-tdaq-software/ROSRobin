// --------x----------------------------x-----------------------------------x--
/**
 ** \file   Firmware.h
 **
 ** \author
 **
 ** \date
 **
 */
// --------x----------------------------x-----------------------------------x--


/* $Id$
 * $Revision$
 * $Log$
 * Revision 1.4  2005/04/26 15:50:51  mmueller
 * Inplemented a bit file parser. Needs to be tested
 *
 * Revision 1.3  2005/04/14 07:06:00  joos
 * lots of new untested code
 *
 * Revision 1.2  2005/04/11 11:24:15  joos
 * Modifications and new files of Matthias
 *
 * Revision 1.1.1.1  2005/04/07 11:34:38  joos
 * added ROSRobin
 *
 *
 */

/**
 * Description:
 *
 */

#ifndef ROSROBIN_FIRMWARE_H
#define ROSROBIN_FIRMWARE_H

#include <string>
#include "ROSRobin/BitStream.h"

namespace ROS {
  
  class Firmware {
  public:
    
    Firmware(void);

    Firmware(std::string fileName);

    ~Firmware(void);
    
    void readFromFile(std::string fileName);
    
    ROS::BitStream getBitstream(void);
    
  private:
    /**@link dependency*/
    /*# BitStream lnkBitStream; */
    
    enum FileFormat {XSVF, RBT, BIT, UNKNOWN };

    void readFromBitFile(std::istream &  is);
    
    void readFromXsvfFile(std::istream &  is);

    void readFromRbtFile(std::istream &  is);
    
    enum FileFormat m_format;

    std::string m_designName;
    std::string m_chipType;
    std::string m_date;
    std::string m_time;
    ROS::BitStream m_firmwareData;

    static const unsigned int c_maxBitFileFieldLength;
    

  };

}

#endif //FIRMWARE_H

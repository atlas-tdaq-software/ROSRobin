// robin_statistics_map.h: statistic counter descriptions
// Created on Mar 21 2016 06:02:23
// Warning: this is a generated file, do not edit

#ifndef ROBIN_STATISTICS_MAP_H
#define ROBIN_STATISTICS_MAP_H

// Error counters
#define ROBIN_ERROR_STRING_COUNT  12
char *robinErrorStrings[ROBIN_ERROR_STRING_COUNT] = { \
    "Hardware errors", \
    "Software errors", \
    "Software warning", \
    "Buffer full occurencies", \
    "ROL DOWN occurencies", \
    "ROL XOFF occurencies", \
    "Buffer manager receive errors", \
    "Buffer manager request errors", \
    "Temperature warning", \
    "PCI DMA reset", \
    "PCI DMA abort", \
    "Interrupts" };

// Fragment statistics
#define ROBIN_FRAGSTAT_STRING_COUNT  12
char *robinFragStatStrings[ROBIN_FRAGSTAT_STRING_COUNT] = { \
    "Frags received", \
    "Frags available", \
    "Frags not available", \
    "Frags pending", \
    "Frags added", \
    "Frags deleted", \
    "Frags truncated", \
    "Frags corrupted", \
    "Frags rejected", \
    "Frags replaced", \
    "Frags out of sync", \
    "Frags missing on delete" };

// Page statistics
#define ROBIN_PAGESTAT_STRING_COUNT  7
char *robinPageStatStrings[ROBIN_PAGESTAT_STRING_COUNT] = { \
    "Pages received", \
    "Pages added", \
    "Pages deleted", \
    "Pages provided", \
    "Pages supressed", \
    "Pages invalid", \
    "Pages dma'ed" };

// Message statistics
#define ROBIN_MSGSTAT_STRING_COUNT  10
char *robinMsgStatStrings[ROBIN_MSGSTAT_STRING_COUNT] = { \
    "Messages received (counted at ROL 0 only)", \
    "Messages accepted", \
    "Messages rejected", \
    "Messages data request", \
    "Messages data response", \
    "Messages clear request", \
    "Messages broadcast", \
    "Messages PCI TX queue full", \
    "Messages PCI Tested Empty (counted at ROL 0 only)", \
    "Messages PCI Tested OK (counted at ROL 0 only)" };

#endif // ROBIN_STATISTICS_MAP_H

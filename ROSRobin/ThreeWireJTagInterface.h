// --------x----------------------------x-----------------------------------x--
/** 
 *  \file   ThreeWireJTagInterface.h
 *
 *  \author M.Mueller 
 *  
 *  \date   06.12.2004
 *
 *
 *
 */
// --------x----------------------------x-----------------------------------x--

/*
 * $Id$
 * $Revision$
 * $Log$
 * Revision 1.1  2005/05/09 07:25:38  mmueller
 * Added a class for ROBIN Jtag communication (low level).
 *
 *
*/

/**
 * Description:
 *    This file defines the class ThreeWireJTagInterface. This class provides
 *    access to the Robin PLD and FPGA JTAG port via the PLX registers. Three
 *    signals are used between the PLX and PLD to run a compressed JTAG 
 *    protocol which is un-compressed inside the ROBIN's PLD. The methods within
 *    this class drive the compressed JTAG interface and provide a "normal" one
 *    to software clients.
 */


#ifndef THREEWIREJTAGINTERFACE_H
#define THREEWIREJTAGINTERFACE_H

namespace ROS {
  /** @interface */
  
  class Robin;

  class ThreeWireJTagInterface {
  public:

    /** The constructor which initialises the object with a reference to the
	dedicated Robin object
    */
    ThreeWireJTagInterface(Robin &  robinBoard);
    
    /** Set the signal level of the TDI pin. The signal is only
	commited of setTck has been toggled!
    */
    void setTdi(int value);

    /** Set the signal level of the TMS pin. The signal is only
	commited of setTck has been toggled!
    */
    void setTms(int value);

    /** Set the signal level of the TCK pin. On the rising edge
	TMS is written to the PLD, on the falling edge TDI.
    */
    void setTck(int value);

    /** Gets the signal level of TDO.
    */
    int getTdo();

    /** Sets the wait time between two TCK clock cycles. This is required
	for older ROBIN prototypes which have no pullup resistor
	on the LINTo line. (serial # 1-20) 
    */
    void setWaitTime(double timeInSeconds);
    
  protected:

    /** Asserts or de-asserts the LINTo line to signal a change of
	TDI or TMS. The signal is only recognised by the PLD on edges
	of TCK.
    */
    void setThreeWireTdiTms(int value);

    /** Toggles TCK and thus generates the JTAG clock.
    */
    void setThreeWireTck(int value);

    /** Gets the TDO level 
     */
    int getThreeWireTdo();

    /** Basic access method to assert or de-assert LINTo
     */
    void setLINToInt(unsigned int value);
    
    /** Enables or disables the MBox register as source of interrupt
     */
    void setMBoxInt(unsigned int value);

    /// The interface wait time.
    unsigned int m_waitTime;   

    /// The TDI signal value which is written with falling edge of TCK 
    unsigned int m_tdi;

    /// The TMS signal value which is written with raising edge of TCK 
    unsigned int m_tms;

    /// The next TCK signal level
    unsigned int m_tck;

    /// The last TDO signal level
    unsigned int m_tdo;

    /// Reference to the ROBIN hardware
    Robin &  m_robinBoard;

  };

}
#endif //JTAGINTERFACE_H

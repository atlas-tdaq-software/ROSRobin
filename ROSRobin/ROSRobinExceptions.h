

#ifndef ROSROBIN_ROSROBINEXCEPTIONS_H
#define ROSROBIN_ROSROBINEXCEPTIONS_H

#include <string>
#include <iostream>
#include "DFExceptions/ROSException.h"

namespace ROS
{
  class ROSRobinExceptions : public ROSException 
  {
    public:
      enum ErrorCode 
      {
	ALREADY_DONE, 
	NOTOPEN,
	MUNMAP,
	MMAP,
	ALREADY_DONE2,
	CMEM,
	OPEN,
	CLOSE,
	IOCTL,
	ALIGN,
	ALIGN2,
	ILL_ORDER,
	NROLS,
	ROBSTAT,
	TIMEOUT,
	BITSTREAM,
	FIRMWARE,
	JTAG,
	MISCHANDLE,
	WRONGREPLY,
	WRONGVERSION,
	TSTAMPE,
	NOPCI,
	ILLSIZE,
	BADCRC,
	BADDELETE,
	MALLOC
      };
      
      ROSRobinExceptions(ErrorCode errorCode);
      ROSRobinExceptions(ErrorCode errorCode, std::string description);
      ROSRobinExceptions(ErrorCode errorCode, const ers::Context& context);
      ROSRobinExceptions(ErrorCode errorCode, std::string description, const ers::Context& context);
      ROSRobinExceptions(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context);

      virtual ~ROSRobinExceptions(void) throw() { }
      
    protected:
      virtual std::string getErrorString(unsigned int errorId) const;
      virtual ers::Issue * clone() const { return new ROSRobinExceptions( *this ); }
      static const char * get_uid() {return "ROS::ROSRobinExceptions";}
      virtual const char* get_class_name() const {return get_uid();}

  };
}
#endif //ROSROBIN_ROSROBINEXCEPTIONS_H

// --------x----------------------------x-----------------------------------x--
/**
 ** \file   JTagController.h
 **
 ** \author
 **
 ** \date
 **
 */
// --------x----------------------------x-----------------------------------x--


/* $Id$
 * $Revision$
 * $Log$
 * Revision 1.5  2005/05/30 14:05:54  mmueller
 * Added proper autodetection of the FPGA and CPLD Jtag chain
 *
 * Revision 1.4  2005/05/04 09:25:38  mmueller
 * Initial implementation which supports JTag chains
 *
 * Revision 1.3  2005/04/14 07:06:00  joos
 * lots of new untested code
 *
 * Revision 1.2  2005/04/11 11:24:15  joos
 * Modifications and new files of Matthias
 *
 * Revision 1.1.1.1  2005/04/07 11:34:38  joos
 * added ROSRobin
 *
 *
 */

/**
 * Description:
 *
 */

#ifndef ROSROBIN_JTAGCONTROLLER_H
#define ROSROBIN_JTAGCONTROLLER_H

#include "ROSRobin/Robin.h"
#include "ROSRobin/ThreeWireJTagInterface.h"

#include <vector>

namespace ROS {

  class Robin;
  class BitStream;
  class Bit;
  class JTagDevice;
  
  class JTagController {
    
  public:
    
    enum TAPStates {
      TAPReset = 0, TAPIdle = 1,
      
      TAPSelectDrScan = 2, TAPCaptureDr = 3, TAPShiftDr = 4, TAPExit1Dr = 5, TAPPauseDr = 6, TAPExit2Dr = 7, TAPUpdateDr = 8,
      
      TAPSelectIrScan = 9, TAPCaptureIr = 10, TAPShiftIr = 11, TAPExit1Ir = 12, TAPPauseIr = 13, TAPExit2Ir =
      14, TAPUpdateIr = 15
    };
        
    

    // Constructs a JTagController with a Robin object
    JTagController(Robin & robinBoard);

    // Deletes the JTagController and marks all JTagDevices which are still registered
    // in the chain to be without controller now
    ~JTagController(void);
    
    // Resets the controller
    void reset(void);

    // Loads all devices of the jtag chain with the bitstreams stored in their
    // nextIrBitstream attribute
    void loadIrAllDevices(void);

    // Loads all devices of the jtag chain with the bitstreams stored in their
    // nextDrBitstream attribute
    void loadDrAllDevices(void);

    // Clocks the JTag FSM to a specific state
    void gotoState(enum TAPStates targetState);

    // Appends a device to a chain
    void appendToChain(JTagDevice * device);

    // Removes a device from a chain
    void removeFromChain(JTagDevice * device);

    // Returns the jtag chain
    std::vector<JTagDevice * > getChain(void);

    // Returns the number of JTag Devices in the hardware chain
    unsigned int getNumberOfDevices(void);

    // Returns the size of the IR register of all devices together
    unsigned int getIRRegistersSize(void);

  private:

    struct TAPTransitions {
      enum TAPStates m_stateTms0;
      enum TAPStates m_stateTms1;
    };

    // This helper method detects basic configuration parameters of the 
    // JTag chain. This comprises the overall size of the IR register 
    // for all devices together and the number of the devices in the chain.
    // This method should be executed in the constructor. It may throw 
    // an exception if the Robin board is not yet open!
    void detectChainParameters(unsigned int &  nrOfDevices, unsigned int &  irSize);

    // The generic jtag access interface. All public methods of the 
    // are implemented with the next three methods

    // Pulses one TMS signal into the JTag interface
    void pulseTms(int value);

    // Sets the TDI signal. pulseTms must be called to generate
    // a clock edge which commits the TDI value to the devices
    void setTdi(const ROS::Bit value);

    // Gets the current TDO value at the end of the chain
    ROS::Bit getTdo();

    // This is the real JTag hardware interface which knows how to 
    // toggle the bits on the PLX device.
    ThreeWireJTagInterface m_jtagPort;

    enum TAPStates m_currentState;

    static const struct TAPTransitions m_tapFSM[16];

    // The JTag chain which is assigned to this controller
    std::vector<JTagDevice *  > m_jtagChain;

    // The number of devices in the hardware jtag chain (not the number of
    // registered devices!)
    unsigned int m_nrOfDevices;
    
    // The size of the IR register (the IR registers of all devices alltogether)
    unsigned int m_irSize;


  };

}
#endif //JTAGCONTROLLER_H

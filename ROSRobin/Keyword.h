// --------x----------------------------x-----------------------------------x--
/**
 ** \file   Keyword.h
 **
 ** \author
 **
 ** \date
 **
 */
// --------x----------------------------x-----------------------------------x--


/* $Id$
 * $Revision$
 * $Log$
 * Revision 1.1  2005/05/17 09:12:16  mmueller
 * Added Files with code for handling PLX Eeprom contents
 *
 *
 *
 */

/**
 * Description:
 *
 */

#ifndef KEYWORD_H
#define KEYWORD_H

#include <string>
#include <iosfwd>

/** This class enables to search for text keywords in a stream
 */
class Keyword {
  
 public:
  Keyword(std::string keyword);
  std::string getKeyword(void);
  
 private:
  std::string m_keyword;
};

// Overloaded operator which appears as a manipulator
std::istream &  operator >> (std::istream & is, Keyword  key);

// Converter functions
unsigned int decStringToUInt(std::string str);
unsigned int hexStringToUInt(std::string str);

#endif

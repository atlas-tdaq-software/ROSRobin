/*******************************************************/
/*                                                     */
/* File Robin.h                                        */
/*                                                     */ 
/* This is the header file for the Robin class         */ 
/*                                                     */
/*** C - 2007 - The S/W with that certain something ****/


//Description:
// This is the definition of the class Robin, the top level interface for 
// ROBIN access.


#ifndef ROSROBIN_ROBIN_H
#define ROSROBIN_ROBIN_H

#include <sys/types.h>
#include "DFThreads/DFFastMutex.h"
#include "robindriver_common.h"

namespace ROS 
{
  class Rol;

  class Robin 
  {
    public:  
      /*************************************************************************/
      /*          Construction / Destruction                                   */
      /*************************************************************************/

      // This is the main constructor. Assigns the object to one ROBIN board
      Robin(u_int slotNumber, float timeout);

      // This is the descrutor. Cleans the Robin object and releases resources if necessaryy
      ~Robin();

      /*************************************************************************/
      /*                         Board initialisation stuff                    */
      /*************************************************************************/

      // Opens the driver and does some basic checks checks */
      void open(void);

      // Closes the board
      void close(void);

      // Resets the ROBIN board
      void reset(void);
      
      /*************************************************************************/
      /*                         Message passing init (with DMA)               */
      /*************************************************************************/

      // Allocates DMA memory for message passing (reply messages) and anounces it to the driver. To be done only once after power-up!!
      void setDmaRamParams(u_int order);

      // Reserves the resources required for message passing
      void reserveMsgResources(u_int numberOfOutstandingReq, u_int msgInputMemorySize, u_int miscSize, u_int eventSize = 0);
      
      // Frees the resources previously reserved with reserveMsgResources (ALL of them)
      void freeMsgResources(void);

      // Gets the base address of the reserved DMA memory for misc. messages (statistics, ...)
      u_long getVirtMiscBase(void);
      u_int getPhysMiscBase(void);

      // Gets the base address of the reserved DMA memory for event fragment transfers      
      u_long getVirtEventBase(void);
      u_int getPhysEventBase(void);

      // Functions to allocate and return memory in the misc. buffer      
      void getMiscMem(u_int size, u_long *start_add, u_int *handle);
      void retMiscMem(u_int handle);
      void getProcInfo(void); 

      /*************************************************************************/
      /*                         Message passing                               */
      /*************************************************************************/

      // Sends a message with no parameters
      virtual void sendMsg(u_int rolId, u_int service, u_long replyAddress);

      // Sends a message with only one parameter
      virtual void sendMsg(u_int rolId, u_int service, u_long replyAddress, u_int parameter);

      // Sends a message with two parameters
      virtual void sendMsg(u_int rolId, u_int service, u_long replyAddress, u_int parameter1, u_int parameter2);

      // Sends a message with a whole data block as a parameter
      virtual void sendMsg(u_int rolId, u_int service, u_long replyAddress, u_int size, u_int *data);

      // Polls for a reply message
      virtual bool receiveMsg(u_long replyAddress, bool blocking);


      /*************************************************************************/
      /*                         ROLs                                          */
      /*************************************************************************/

      Rol & getRol(u_int rolId);
      u_int getNrOfRols(void);

      /*************************************************************************/
      /*                         Interrupts                                    */
      /*************************************************************************/
      
      u_int waitForInterrupt(T_irq_info *irq_info);

      /*************************************************************************/
      /*                         Basic board I/O                               */
      /*************************************************************************/

      // Writes data to the ROBIN FPGA
      void writeFpga(u_int address, u_int data);

      // Reads data from the ROBIN FPGA
      u_int readFpga(u_int address);

      // Defines the register set of the PLX 9656
      enum Plx9656LocalConfigRegs 
      {
	// Local Configuration Registers offsets :    
	LAS0RR  = 0x00,	LAS0BA  = 0x04,	MARBR   = 0x08,	BIGEND  = 0x0c,
	EROMRR  = 0x10,	EROMBA  = 0x14,	LBRD0   = 0x18,	DMRR    = 0x1c,
	DMLBAM  = 0x20,	DMLBAI  = 0x24,	DMPBAM  = 0x28,	DMCFGA  = 0x2c,
	LAS1RR  = 0xf0,	LAS1BA  = 0xf4,	LBRD1   = 0xf8,
	// Runtime Registers offsets :
	MBOX0   = 0x40,	MBOX1   = 0x44,	MBOX2   = 0x48,	MBOX3   = 0x4c,
	MBOX4   = 0x50,	MBOX5   = 0x54,	MBOX6   = 0x58,	MBOX7   = 0x5c,
	P2LBELL = 0x60,	L2PBELL = 0x64,	INTCSR  = 0x68,	CNTRL   = 0x6c,
	PCIHDR  = 0x70,	PCIHREV = 0x74,
	// DMA registers offsets :
	DMAMODE0= 0x80,	DMAPADR0= 0x84,	DMALADR0= 0x88,	DMASIZ0 = 0x8c,
	DMADPR0 = 0x90,	DMAMODE1= 0x94,	DMAPADR1= 0x98,	DMALADR1= 0x9c,
	DMASIZ1 = 0xa0,	DMADPR1 = 0xa4,	DMACSR0 = 0xa8,	DMACSR1 = 0xa9,
	DMAARB  = 0xac,	DMATHR  = 0xb0,
	// Messaging Queue Registers offsets:
	OPLFIS  = 0x30,	OPLFIM  = 0x34,	IQP     = 0x40,	OQP     = 0x44,
	MQCR    = 0xc0,	QBAR    = 0xc4,	IFHPR   = 0xc8,	IFTPR   = 0xcc,
	IPHPR   = 0xd0,	IPTPR   = 0xd4,	OFHPR   = 0xd8,	OFTPR   = 0xdc,
	OPHPR   = 0xe0,	OPTPR   = 0xe4,	QSR     = 0xe8
      };

      // Writes to a PLX register
      void writePlx(u_int address, u_int data);

      // Reads from a PLX register
      u_int readPlx(u_int address);

      // Writes data to the PLX EEPROM
      void writeEeprom(u_int address, u_short data);

      // Reads data from the PLX EEPROM
      u_short readEeprom(u_int address);
      
      /*************************************************************************/
      /*                        Misc                                           */
      /*************************************************************************/
      
      u_int getIdentity(void);
      
    protected:
      /*************/
      /* Constants */
      /*************/
      static const u_int s_max_msgs          = 150;  //Should be larger than the MSG FIFO (was 40)
      static const u_int s_max_order         = 10;  //2^10 * 64 kB = 64 MB
      static const u_int s_64k               = 0x10000;
      static const u_int s_direct_dma_offset = 0xa0000000;
      static const u_int s_fifo_index        = 0x14000; 

      enum bloc_states
      {
        BLOCKLET_UNUSED = 0,  
        BLOCKLET_FREE,
        BLOCKLET_USED
      };

      typedef struct T_mmap
      {
	u_int *user_virt_base;
	u_int user_phys_base;
	u_int user_size;
	void *virt_base;
	u_int phys_base;
	u_int offset;
	u_int size;
	u_int index;
	u_int misc_size;
      } T_mmap;

      
      // The tree ROLs
      Rol * m_rolVector[3];      
      
      u_int m_is_open;
      u_int m_slot_number;
      int m_cmem_handle;
      u_int m_nFifoSlots;
      u_int m_setmemok;
      u_int m_reservememok;
      int m_dev_handle;
      float m_timeout;
      u_int m_dpm_oldest_index;
      u_int m_dpm_current_index;
      u_int m_mem_offset;
      u_int m_dpm_offset;
      u_int m_pending;

      u_int m_dpm_start[s_max_msgs];
      DFFastMutex *m_messageLock;
      DFFastMutex *m_miscmemLock;
      DFFastMutex *m_sendmutex;
      T_mmap msg_dpm;
      T_mmap reply_mem;
      T_mmap plx_win;
      T_mmap fpga_win;
      T_blocklet blocklets[s_max_msgs];
      u_int m_n_rols;


      /*******************/
      /* Private Methods */
      /*******************/
      virtual Rol* newRol(u_int number);

      void unmapmem(T_mmap *param);
      void mapmem(T_mmap *param);
      void isOpen(void);
      u_int wait_dpm_and_fifo(u_int size);
      void msg_done(void);

      // Low level method for PLX eeprom access: Set DI
      void setEepromDI(u_int value);
      
      // Low level method for PLX eeprom access: Get DO
      u_int getEepromDO(void);
      
      // Low level method for PLX eeprom access: Generate one clock
      void pulseEepromClk(void);
      
      // Enable or disable the PLX EEPROM chip select signal
      void setEepromCS(u_int value);
  };
} 
#endif //ROSROBIN_ROBIN_H


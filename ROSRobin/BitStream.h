/************************/
/*			*/
/* Author:  M. Mueller	*/
/*			*/
/************************/

#ifndef ROSROBIN_BITSTREAM_H
#define ROSROBIN_BITSTREAM_H

#include <iostream>
#include "ROSRobin/Bit.h"

namespace ROS 
{
  class BitStream 
  {
  public:
    // enum Endian
    // Names which represents the order of the BitStream.
    // begin{itemize}
    // item #LOWEND#: Bit at index 0 has lowest order.
    // item #HIGHEND#: Bit at index n has lowest order.
    // end{itemize}*/
    typedef enum 
    {
      LOWEND, HIGHEND
    }
    Endian;

    // Constructors
    // Standard constructor
    BitStream();

    // Copy constructor
    BitStream(const BitStream & bs);

    // Constructor initializing the BitStream from a string (char *) of 0 and 1.
    BitStream(const char * bits);

    ~BitStream(void);
    
    // Set and get methodes 
    // Set the BitStream with a string (char*) of 1 and 0
    void set(const char * bitString);

    // Set the BitStream with the value of another BitStream
    void set(const BitStream & bs);

    // Set the bitstream from an array of raw data (32 bit) in low endian.
    void set(const unsigned int * data, unsigned int size);

    // Get the length of the BitStream
    unsigned int getLength() const;

    // Calculate the binary value of 8 bit in the BitStream starting at a specified index.
    // Note: less performant than GetUIn32().
    // return a 8 bit integer value
    // param index starting index for conversion to a 8 bit number
    // param e order of the bits
    unsigned char getUInt8(unsigned int index, Endian e = LOWEND) const;

    // Calculate the binary value of 16 bit in the BitStream starting at a specified index.
    // Note: less performant than GetUIn32().
    // return a 32 bit integer value
    // param index starting index for conversion to a 16 bit number
    // param e order of the bits
    unsigned short getUInt16(unsigned int index, Endian e = LOWEND) const;

    //Calculate the binary value of 32 bit in the BitStream starting at a specified index.
    //return A 32 bit integer value
    //param index starting index for conversion to a 32 bit number
    //param e order of the bits
    unsigned int getUInt32(unsigned int index, Endian e = LOWEND) const;

    // Get the Bit at position index
    const Bit getAt(unsigned int index) const;

    // Methodes to modify a BitStream 
    //Assign the value bit to the Bit at position index
    void setAt(unsigned int index, const Bit bit);

    // Append a Bit to the BitStream
    void append(const Bit & bit);

    // Append a BitStream to the BitStream
    void append(const BitStream & bs);

    //Extract a part out of the BitStream
    //return A BitStream
    //param position The starting position of the extraction
    //param length The number of Bits to extract */
    BitStream extract(unsigned int position, unsigned int length) const;

    // Shift the BitStream 'count' times left
    void shl(unsigned short count);

    // Shift the BitStream 'count' times right
    void shr(unsigned short count);

    // Reverse the BitStream
    void reverse();
    
    // Operators
    // Assignment operator
    BitStream & operator = (const BitStream & bs);

    // Equal operator
    bool operator == (const BitStream & bs) const;
    bool operator != (const BitStream & bs) const;
//    friend ROS::BitStream ROS::Not(const ROS::BitStream & bs);
//    friend ROS::BitStream ROS::Xor(const ROS::BitStream & bs1, const ROS::BitStream & bs2);

  private:
    void setBit(unsigned int position, bool value);
    void resize(unsigned int newLength);
    bool getBit(unsigned int position) const;

    unsigned int *m_bits;
    unsigned int  m_length;
    unsigned int  m_nrOfWords;

    static const unsigned int c_bytesPerWord;
  };
  
//  BitStream Not(const BitStream & bs);
//  BitStream Xor(const BitStream & bs1, const BitStream & bs2);
}

// Output via ostream
std::istream & operator >> (std::istream & is, ROS::BitStream & bitstream);
// Input via istream
std::ostream & operator << (std::ostream & os, const ROS::BitStream & bitstream);

#endif //BITSTREAM1_H

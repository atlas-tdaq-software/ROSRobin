// --------x----------------------------x-----------------------------------x--
/**
 ** \file   robindriver.h
 **
 ** \author Markus Joos, CERN-PH/ESS
 **
 ** \date   4. Feb. 05
 **
 */
// --------x----------------------------x-----------------------------------x--
/************ C 2005 - The software with that certain something **************/


/**
 * Description:
 * This is the private header file for ROBIN driver
 */

#ifndef ROSROBIN_ROBINDRIVER_H
#define ROSROBIN_ROBINDRIVER_H

/*************/
/* Constants */
/*************/
#define PCI_VENDOR_ID_ROBIN2       0x10b5
#define PCI_DEVICE_ID_ROBIN2       0x9656

#define PCI_VENDOR_ID_ROBIN3       0x10dc
#define PCI_DEVICE_ID_ROBIN3       0x0144
#define PCI_DEVICE_ID_ROBIN3_bad   0x0324

#define PCIExUC_DEVICE_ID_ROBIN3   0x9056
#define PCIExUC_VENDOR_ID_ROBIN3   0x10b5
#define PCIEx_DEVICE_ID_ROBIN3     0x0188
#define PCIEx_VENDOR_ID_ROBIN3     0x10dc

//H/W constants
#define MESSAGE_RAM_OFFSET         0x10000
#define MESSAGE_RAM_SIZE           0x4000
#define MESSAGE_FIFO_DEPTH         128
#define MESSAGE_FIFO_OFFSET        0x14000
#define SLINK_REG_OFFSET           0x20040
#define PLX_BAR                    0
#define FPGA_BAR                   2
#define DESIGN_ID_S                0x05033801
#define DESIGN_ID_P                0x05033001

//User modifyable constants
#define MAXCHANNELS                3    // Max. number of channes per ROBIN card
#define MAXROLS                    (MAXCARDS * MAXCHANNELS)

//For the memory management
#define MAX_BLOCKLETS              10
#define BLOCKLET_UNUSED            0  
#define BLOCKLET_FREE              1
#define BLOCKLET_USED              2
#define BLOCK_TYPES                2

//Other macros
#define ROBIN_UNKNOWN              0
#define ROBIN_CLASSIC              1
#define ROBIN_EXPRESS              2

/********/
/*Macros*/
/********/
#ifdef DRIVER_DEBUG
  #define kdebug(x) {if (debug) printk x;}
#else
  #define kdebug(x)
#endif

#ifdef DRIVER_ERROR
  #define kerror(x) {if (errorlog) printk x;}
#else
  #define kerror(x)
#endif

/******************/
/*Type definitions*/
/******************/
struct robin_proc_data_t 
{
  char name[10];
  char value[100];
};

typedef struct
{
  struct semaphore   thread_sema;
  int                dpm_handle[MAXCARDS];
  int                mem_handle[MAXCARDS];
  int                fifo_handle[MAXCARDS]; //MJ: We do not relly need arays here as one pdata has to be generated per card
  int                slot;
  T_link_params      link_params;
  T_init_dma_params  init_dma_params;
  T_msg_req_params   msg_req_params;
} T_private_data;

typedef struct
{
  struct pci_dev *pciDevice;
  u_int pci_memaddr_plx;
  u_int pci_memsize_plx;
  u_int *plx_regs;      //pointer to PLX registers
  u_int pci_memaddr_bus;
  u_int pci_memsize_bus;
  u_int *fpga_regs;     //pointer to registers inside the FPGA
  u_int mem_base;
  u_int mem_size;
  u_int nrols;
  u_int bar2;
  u_int irqnumber;
  u_int card_type;
  int fpgaConfigured;
} T_card;


/***************/
/*PLX registers*/
/***************/
enum 
{
  PLX_LAS0RR   = 0,
  PLX_LAS0BA   = 1,
  PLX_DMRR     = 7,
  PLX_DMLBAM   = 8,
  PLX_DMPBAM   = 10,
  PLX_MBOX2    = 18,
  PLX_MBOX4    = 20,
  PLX_MBOX5    = 21,
  PLX_MBOX6    = 22,
  PLX_MBOX7    = 23,
  PLX_L2PDBELL = 25,
  PLX_INTCSR   = 26,
  PLX_CNTRL    = 27
};


/****************/
/*FPGA registers*/
/****************/
enum 
{
  FPGA_FW_VERSION = 0
};


/******************************/
/*Standard function prototypes*/
/******************************/
static int robindriver_init(void);
static void robindriver_exit(void);
static int robin_open(struct inode * ino, struct file * file);
static int robin_release(struct inode * ino, struct file * file);
static long robin_ioctl(struct file * file, u_int cmd, u_long arg);
static int robin_mmap(struct file * file, struct vm_area_struct * vma);
static void robin_vclose(struct vm_area_struct * vma);


/*****************************/
/*Special function prototypes*/
/*****************************/
static int initmem(u_int card, u_int size, u_int base);
static int getmem(u_int card, u_int type, u_int size, u_int *start_add, u_int *handle);
static int retmem(u_int card, u_int type, u_int handle);

/*********/
/*Exports*/
/*********/
#if LINUX_VERSION_CODE < KERNEL_VERSION(5,6,0)
static int readFpga(u_int card, u_int addressOffset);
static void writeFpga(u_int card, u_int addressOffset, int data);
static int getCardNum(void);
#endif

static int fpgaLoaded(u_int card, u_int *fpgadesign);  

#endif


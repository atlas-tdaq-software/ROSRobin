// --------x----------------------------x-----------------------------------x--
/**
 ** \file
 **
 ** \author
 **
 ** \date
 **
 */
// --------x----------------------------x-----------------------------------x--


/* $Id$
 * $Revision$
 * $Log$
 * Revision 1.3  2005/04/26 15:46:34  mmueller
 * Adapted Bit.h
 *
 * Revision 1.2  2005/04/14 07:06:00  joos
 * lots of new untested code
 *
 * Revision 1.1  2005/04/11 11:24:15  joos
 * Modifications and new files of Matthias
 *
 *
 */

/**
 * Description:
 *
 */

#ifndef ROSROBIN_BIT_H
#define ROSROBIN_BIT_H

#include <iostream>

/** The class Bit.
    This object represents a Bit which can be of Low,High,Undefined or Invalid
    value. */

namespace ROS {
  
  class Bit {
    
  public:
    /**@name enum BitType
       Definition of Names which stands for the values of a Bit
       \begin{itemize}
       \item #HIGH,HI#: Bit has high value (1)
       \item #LOW,LO#: Bit has low value (0)
       \item #UNDEFINED#: Bit has undefined value
       \item #INVALID#: Bit value is invalid
       \end{itemize}
    */
    typedef enum {
        HIGH, HI = HIGH, LOW, LO = LOW, UNDEFINED, INVALID
    }
    BitType;

    /**@name Constructors
     */
    //@{
    /** Standard Constructor
     */
    Bit() {
    };

    /** Initialize Bit with a boolean
     */
    Bit(bool b);

    /** Initialize Bit with a char. This can be 1 or 0.
     */
    Bit(const char c);

    /** Initialize Bit with a value of the enum BitType
     */
    Bit(BitType v) {
        value = v;
    }

    /** Copy constructor
     */
    Bit(const Bit & bit) {
        value = bit.value;
    }
    //@}

    /**@name Standard set and get methodes
     */
    //@{

    /** Set with a char (can be 0 or 1).
     */
    void set(const char c);

    /** Set with a BitType value.
     */
    void set(BitType v) {
        value = v;
    }

    /** Returns the Bit value.
     */
    BitType get(void)const {
        return value;
    }
    //@}

    /**@name Operators*/
    //@{

    /**Assignment operator.
     */
    Bit & operator = (const Bit & bit);

    operator bool() const;
    /** Equal operator.
     */

    bool operator == (const Bit & bit) const;
    /** Not equal operator.
     */
    bool operator != (const Bit & bit) const;
    //@}
private:
    BitType value;
    friend class BitStream;

  };
  
}


/** @name IO Operators for Bit*/
//@{
/// Output via ostream
std::ostream & operator << (std::ostream & os, const ROS::Bit & bit);
/// Input via istream
std::istream & operator >> (std::istream & is, ROS::Bit & bit);
//@}


#endif //BIT_H

/****************************************************************/
/* file   Rol.cpp      						*/
/*                     						*/
/* author: Markus Joos 						*/
/*                     						*/
/*****C 2007 - The software with that certain something**********/

#ifndef ROSROBIN_ROL_H
#define ROSROBIN_ROL_H

#include <vector>
#include <string>

#ifndef HOST
#define HOST
#endif
#include <ROSRobin/robin.h>

#define MAX_L1IDS 100

namespace ROS 
{
  class Robin;
  class Rol 
  {
    public:

      typedef struct ECRStatisticsBlock
      {      
        u_int mostRecentId;
	u_int overflow;
	u_int necrs;
	u_int ecr[25];
      } ECRStatisticsBlock;
      
      typedef struct GCBlock
      {      
        u_int done;
	u_int deleted;
	u_int free;
      } GCBlock;
         
      typedef struct KeptPage
      {
	RejectedEntry rentry;
        u_int pagedata_size;
        u_int pagedata[enum_mgmtMaxPageSize];
      } KeptPage;
	 
      typedef struct L1idRange
      {
        u_int first;
        u_int last;
      } L1idRange;
      
      	    
      Rol(Robin & robinBoard, u_int rolId);
      virtual ~Rol();
      u_long requestFragment(u_int eventId, u_long replyAddress);
      u_int *getFragment(u_long ticket);
      u_int releaseFragment(const std::vector <u_int> *eventIds);
      u_int releaseFragmentNoack(const std::vector <u_int> *eventIds);
      u_int releaseFragmentAll(const std::vector <u_int> *eventIds);
      u_int getTemperature(void);
      void runTest(u_int ttype);
      StatisticsBlock getStatistics(void);
      ECRStatisticsBlock getECR(void);
      GCBlock collectGarbage(int eventId);
      std::vector <CfgParm> getConfig(void);
      KeptPage getRejectedPage(u_int pageno);
      std::vector <L1idRange> getL1idList(void); 
 
      void loadFragment(u_int size, u_int *data, u_int mode);
      void setConfig(u_int itemNumber, u_int value);
      void reset(void);
      void clearStatistics(void);
      void ping(void);
      void dumpRol(u_int *data, u_int blocklet); 
      void clearRejectedPages(void); 
      void formatBuffer(void); 
      void configureDiscardMode(u_int mode); 
      void setCRCInterval(int crc_interval);
      u_short crctablefast16(u_short* p, u_long len); 

  protected:
      Robin & m_robin;
      u_int m_rolId;
  private:
      u_int m_crc_interval;
      static const u_int c_DataChunkSize;
      
      //CRC stuff
      u_long crcinit_direct;
      u_long crctab16[65536];   
      u_long crcmask;
      static const u_int c_order;  
  };
}
#endif //ROL_H

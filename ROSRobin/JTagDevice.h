// --------x----------------------------x-----------------------------------x--
/**
 ** \file   JTagDevice.h
 **
 ** \author
 **
 ** \date
 **
 */
// --------x----------------------------x-----------------------------------x--


/* $Id$
 * $Revision$
 * $Log$
 * Revision 1.1  2005/05/04 09:26:25  mmueller
 * A base class for all JTag devices
 *
 *
 */

/**
 * Description:
 *
 */

#ifndef ROSROBIN_JTAGDEVICE_H
#define ROSROBIN_JTAGDEVICE_H

#include <string>
#include <map>

#include "ROSRobin/BitStream.h"

namespace ROS {

  class JTagController;

  class JTagDevice {

  public:

    /** Sets the command bitstream for the next Jtag operation.
	The  command bitstream is identified by its name which makes it
	easier to keep the bitstreams for a device.
	The data is not shifted into JTag by this method
    */
    void setIR(std::string name);

    /** Sets the command bitStream for the next Jtag operation.
	The data is not shifted into JTag by this method
    */
    //    void setIR(BitStream cmdBits);

    /** Gets the content of the IR obtained from the device during
	the last access.
    */
    BitStream getIR(void);

    /** Sets the data bitStream for the next Jtag operation.
	The data is not shifted into JTag by this method
    */
    void setDR(BitStream dataBits);

    /** Gets the content of the DR obtained from the device during
	the last access.
    */
    BitStream getDR(void);

  protected:
    
    // All these things need to be used by sub-classes, which implement the
    // real JTag device out of this basclass.

    /** The constructor which initialises the object with a JTagController
	and the length of the instruction register. This constructor
	also generates the default Jtag instructions (BYPASS, ...).
    */
    JTagDevice(JTagController & jtagController, unsigned int IRLength);
    
    /** The destructor which deletes the device and removes it from the
	JTag chain
    */
    ~JTagDevice();

    /** Adds a instruction to the device.
     */
    void addInstruction(std::string name, BitStream cmdBits);

    /** Reads a instruction from the collection
     */
    BitStream getInstruction(std::string name);

    JTagController &  getJTagController(void);


  private:

    friend class JTagController;
    
    ROS::BitStream getNextIr(void);
    ROS::BitStream getNextDr(void);

    void setIrReturnContent(ROS::BitStream &  value);
    void setDrReturnContent(ROS::BitStream &  value);

    // A reference to the JTagController object
    JTagController &  m_jtagController;

    // A variable which shows if the devices has been added to a chain.
    bool m_isInChain;
    
    // The Instruction pool
    std::map<std::string, ROS::BitStream> m_instructionPool;

    // The IR and DR bitstreams which will be sent to the device
    // during the next operation
    ROS::BitStream m_nextIrContent;
    ROS::BitStream m_nextDrContent;    

    // The IR and DR bitstreams which have been obtained from the device
    ROS::BitStream m_irContent;
    ROS::BitStream m_drContent;    
  };

}


#endif //ROSROBIN_JTAGDEVICE_H

/// robin_ppc.h defines some ppc related stuff, like DMA structures
/** \addtogroup RobInPpc
robin_ppc.h PPC related definitions

$Header: /local/reps/atlastdaq/DAQ/DataFlow/robin_ppc/robin_ppc/robin_ppc.h,v 1.10 2005/04/22 14:29:05 akugel Exp $
$Author$
$Date$
$Revision$

\section Description Description
PowerPC related definitions, like DMA<br>

*@{
*/

#ifndef ROBIN_PPC_H
#define ROBIN_PPC_H


/// DMA S-G descriptor
// must be 128bit (16 bytes) aligned
typedef struct {
	Dword control;
	Dword count;
	Dword srcHi;
	Dword srcLo;
	Dword destHi;
	Dword destLo;
	Dword nextDescHi;
	Dword nextDescLo;
} PpcDmaDesc;

// control word:
/*
0 CE
Channel Enable
0 Channel is disabled
1 Channel is enabled
This field is automatically cleared when the
transfer completes or an error occurs.

1 CIE
Channel Interrupt Enable
0 Disable interrupts from this channel
1 Enable interrupts from this channel
When enabled, interrupts can be generated for
terminal count, end of transfer, and errors conditions.
Each of these interrupt types must be individually
enabled in DMA0_CTn. See �Interrupts.�

2 TD
In peripheral mode:
0 Transfers are from memory-to-peripheral
1 Transfers are from peripheral-to-memory
In device-paced memory-to-memory mode:
0 Peripheral is at the destination address
1 Peripheral is at the source address
TD is not used (don�t care) for software-initiated
memory-to-memory transfers.

3 PL
Peripheral Location/Destination Memory Location
For memory-to-memory transfers:
0 Destination address located in PLB memory space
1 Destination address located in OPB memory space
For peripheral/DPM-to-memory or memory-to-
DPM/peripheral transfers:
0 Device located on external bus
1 Device located on the OPB.

4:6 PW
Peripheral Width/Transfer Width
000 Byte (8 bits)
001 Halfword (16 bits)
010 Word (32 bits)
011 Doubleword (64 bits)
100 Quadword (128 bits)
Transfers involving peripheral, device-pacedmemory,
and OPB FIFO devices can only be
byte, halfword, or word. This limitation also
applies to transfers involving EBC memory when
SAI=0 or DAI=0. PLB FIFO devices can only be
quadword size.

7 DAI
Destination Address Increment
0 Do not increment destination address
1 After each data transfer increment the destination
address by:
� 1, for byte (8-bit) transfers
� 2, for halfword (16-bit) transfers
� 4, for word (32-bit) transfers
� 8, for doubleword (64-bit) transfers
� 16, for quadword (128-bit) transfers
Valid only when DEC=0. Is required that DAI=1
for peripheral-to-memory and device-paced
memory-to-memory transfers since peripheralto-
FIFO type devices or DPM-to-FIFO type
devices are not supported.

8 SAI
Source Address Increment
0 Do not increment source address
1 After each data transfer increment the source address
by:
� 1, for byte (8-bit) transfers
� 2, for halfword (16-bit) transfers
� 4, for word (32-bit) transfers
� 8, for doubleword (64-bit) transfers
� 16, for quadword (128-bit) transfers
Valid only when DEC=0. Is required that SAI=1
for memory-to-peripheral and memory-to-devicepaced
memory transfers since FIFO-to-peripheral/
DPM transfers are not supported.

9 BEN
Buffer Enable
0 Disable DMA 128-byte buffer
1 Enable DMA 128-byte buffer
If BEN=0 discrete read and write operations
occur for each data transfer.

10:11 TM
Transfer mode
00 Peripheral
01 Reserved
10 Software-initiated memory-to-memory
11 Device-paced memory-to-memory
12:13 PSC
Peripheral Setup Cycles
0-3
Number of PerClk cycles that the peripheral bus
is idle from the last peripheral bus transaction to
DMAAckn becoming active. Used only for the
peripheral side of peripheral mode transfers.

14:19 PWC
Peripheral Wait Cycles
0-63
DMAAckn remains active for PWC+1 PerClk
cycles. Used only for the peripheral side of
peripheral mode transfers.

20:22 PHC
Peripheral Hold Cycles
0-7
The number of PerClk cycles between the time
that DMAAckn becomes inactive until the peripheral
bus is available for the next bus access.
Used only during the peripheral side of peripheral
mode transfers.

23 ETD
End-of-Transfer/Terminal Count (EOTn[TCn])
Pin Direction
0 EOTn[TCn] is an EOT input
1 EOTn[TCn] is a TC output
ETD must be set to 1 if the channel is configured
for software-initiated memory-to-memory transfers.

24 TCE
Terminal Count (TC) Enable
0 Channel does not stop when TC is reached
1 Channel stops when TC is reached
If TCE=1, it is required that ETD=1.

25:26 CP
Channel Priority
00 Low priority
01 Medium low priority
10 Medium high priority
11 High priority
Actively requesting channels of the same priority
are ranked in order by channel number, channel
0 having the highest priority. For more information,
see �DMA Arbitration Transfer Priorities.�

27:28 PF
Memory Read Prefetch Transfer
00 Prefetch 1 quadword (128-bits)
01 Prefetch 2 quadwords
10 Prefetch 4 quadwords
11 Prefetch 8 quadwords
Used only during memory-to-peripheral and
memory to deviced-paced memory transfers. To
enable prefetching it is required that BEN=1.

29 DEC
Address Decrement
0 SAI and DAI fields control memory address
incrementing.
1 After each data transfer the memory address is
decremented by the transfer width.
If DEC=1, it is required that BEN=0. This field is
valid only for peripheral mode transfers (TM=00).

30 SL
Source Address Location/Memory Location
0 PLB
1 OPB
For memory-to-memory transfers (TM=1x) SL
indicates whether the source memory controller
is on the PLB or OPB. For peripheral mode
transfers, this field denotes the location of the
memory controller.

31 Reserved

*/
#define PPC_DMACTL_CE  (0x80000000 >> 0)	// 1 to enable channel
#define PPC_DMACTL_CIE (0x80000000 >> 1)	// normally 0
#define PPC_DMACTL_TD  (0x80000000 >> 2)	// normally 0
#define PPC_DMACTL_PL  (0x80000000 >> 3)	// 0 for FPGA to CPU transfers
#define PPC_DMACTL_PW4  (0x80000000 >> 5)	// 0-1-0 for 32 bit (4 bytes) transfers
#define PPC_DMACTL_PW16  (0x80000000 >> 4)	// 0-1-0 for 128 bit (16 bytes)transfers
#define PPC_DMACTL_DAI (0x80000000 >> 7)	// 
#define PPC_DMACTL_SAI (0x80000000 >> 8)	// 
#define PPC_DMACTL_BEN (0x80000000 >> 9)	// 
#define PPC_DMACTL_TMM2M  (0x80000000 >> 10)	// 1-0 for m2m, 11 for dev.paced mem to mem
#define PPC_DMACTL_TMDPM  ((0x80000000 >> 10)|(0x80000000 >> 11))	// 1-0 for m2m, 11 for dev.paced mem to mem
#define PPC_DMACTL_ETD (0x80000000 >> 23)	// 1 for m2m
#define PPC_DMACTL_TCE (0x80000000 >> 24)	// normally 1
#define PPC_DMACTL_CP_L (0)					// 0-0 for low priority
#define PPC_DMACTL_CP_M (0x80000000 >> 25)	// 1-0 for medium priority
#define PPC_DMACTL_SL   (0x80000000 >> 30)	// 1 for FPGA to CPU transfers

// count word: 
// Bits 0 and 1 are used only for Scatter-Gathher transfers
//   MSB is link control bit. 1 => next descriptor available. 0 => last descriptor
//   next bit is descriptot location (PLB vs. OPB). always 0 for PLB
// 
//   other upper bits are 0 in our case, for memory2memory
//   lower bits are transfer count (in units of transfer size (32bit))
#define PPC_DMACNT_CNTMASK ((1 << 20) - 1)
#define PPC_SGCNT_LINK	0x80000000
// default value of CNT register for normal memory 2 memory DMA (count = 0)
#define PPC_DMACNT_M2M_DEFVAL 0	
// default value of CNT register for UPF device paced memory DMA (count = 0)
// using burst size of 4 (4 upf words in one go)
#define PPC_DMACNT_DPM_DEFVAL ((0x80000000 >> 8) | (0x80000000 >> 10))


// SG control
/*
19.3.7 DMA Scatter/Gather Command Register (DMA0_SGC)
The DMA Scatter/Gather Command Register (DMA0_SGC) is a 32-bit register, of which 12-bits are implemented.
Bits 0:3 are the Start Scatter/Gather Enable bits for channels 0 to 3, bits 4:7 determine whether a
given channel�s Scatter/Gather Descriptor Table resides in PLB or OPB address space, and bits 16:19 are
the corresponding Enable Mask bits for the Start Scatter/Gather Enable bits. Setting a Start Scatter/Gather
Enable bit causes the selected channel to begin a scatter/gather operation, while writing a 0 stops the
Scatter/Gather operation. To start or stop a specific Scatter/Gather channel, the corresponding Enable Mask
bit must be set to 1; otherwise, the register holds the previous value. Note that halting a scatter/gather
transfer prevents the channel from continuing to the next descriptor, but does not stop the transfer currently in
progress.
Upon completion of a scatter/gather sequence of transfers the DMA controller clears DMA0_SGC[SSGn]. If
an error occurs when the DMA controller is reading the Scatter/Gather descriptor table, DMA0_SGC[SSGn] is
cleared for the affected channel, and the channel�s error status bit (DMA0_SR[RIn]) is set. For additional
details see Scatter/Gather Transfers on page 630.

0:3 SSG[0:3]
Start Scatter/Gather for channels 0-3
0 Scatter/gather support is disabled
1 Scatter/gather support is enabled
To start a scatter/gather operation for channel
EM[n] must also be set.

4:7 SGL[0:3]
Scatter/Gather Descriptor Table Location for
channels 0-3
0 PLB memory space
1 OPB memory space
A channel�s descriptor table may not cross
between PLB and OPB address space. See
Scatter/Gather Transfers on page 630.

8:15 Reserved

16:19 EM[0:3]
Enable Mask for channels 0-3
0 Writes to SSG[n] and SGL[n] are ignored
1 Allow writing to SSG[n] and SGL[n]
To write SSG[n] or SGL[n], EM[n] must be set.
Otherwise, writing SSG[n] or SGL[n] has no
effect.
20:31 Reserved
*/
#define PPC_SGCTL_SSG0	(0x80000000 >> 0)
#define PPC_SGCTL_SSG1	(0x80000000 >> 1)
#define PPC_SGCTL_SSG2	(0x80000000 >> 2)
#define PPC_SGCTL_SSG3	(0x80000000 >> 3)

#define PPC_SGCTL_SSL0	(0x80000000 >> 4)
#define PPC_SGCTL_SSL1	(0x80000000 >> 5)
#define PPC_SGCTL_SSL2	(0x80000000 >> 6)
#define PPC_SGCTL_SSL3	(0x80000000 >> 7)

#define PPC_SGCTL_EM0	(0x80000000 >> 16)	// em bit must set to 1 for changes of ssg or ssl bit
#define PPC_SGCTL_EM1	(0x80000000 >> 17)
#define PPC_SGCTL_EM2	(0x80000000 >> 18)
#define PPC_SGCTL_EM3	(0x80000000 >> 19)

/*
Figure 19-11. DMA Status Register (DMA0_SR)
0:3 CS[0:3]
Channel 0-3 Terminal Count Status
0 Terminal count has not occurred
1 Terminal count has been reached
Set when the transfer count reaches 0 and the
DMA0_CRn(TCE) bit is set.

4:7 TS[0:3]
Channel 0-3 End of Transfer Status
0 End of transfer has not been requested
1 End of transfer has been requested
Only valid for channels with DMA0_CRn[ETD]=0
(no memory-to-memory transfers).

8:11 RI[0:3]
Channel 0-3 Error Status
0 No error
1 Error occurred
See Errors section for more information.

12:15 IR[0:3]
Internal DMA Request
0 No internal DMA request pending
1 Internal DMA request is pending

16:19 ER[0:3]
External DMA Request
0 No external DMA request pending
1 External DMA request is pending

20:23 CB[0:3]
Channel Busy
0 Channel is idle
1 Channel currently active
During catter/gather fetches, these bits are active.

24:27 SG[0:3]
Scatter/Gather Status
0 No scatter/gather operation in progress
1 Scatter/gather operation in progress
For multiple scatter/gather links, the scatter/gather
status bit is set when the first link is loaded and is
kept adctive until the last link is completed.

28:31 Reserved
*/
#define PPC_DMASTAT_CS0		(0x80000000 >>  0)
#define PPC_DMASTAT_CS1		(0x80000000 >>  1)
#define PPC_DMASTAT_CS2		(0x80000000 >>  2)
#define PPC_DMASTAT_CS3		(0x80000000 >>  3)

#define PPC_DMASTAT_TS0		(0x80000000 >>  4)
#define PPC_DMASTAT_TS1		(0x80000000 >>  5)
#define PPC_DMASTAT_TS2		(0x80000000 >>  6)
#define PPC_DMASTAT_TS3		(0x80000000 >>  7)

#define PPC_DMASTAT_RI0		(0x80000000 >>  8)
#define PPC_DMASTAT_RI1		(0x80000000 >>  9)
#define PPC_DMASTAT_RI2		(0x80000000 >>  10)
#define PPC_DMASTAT_RI3		(0x80000000 >>  11)

#define PPC_DMASTAT_IR0		(0x80000000 >>  12)
#define PPC_DMASTAT_IR1		(0x80000000 >>  13)
#define PPC_DMASTAT_IR2		(0x80000000 >>  14)
#define PPC_DMASTAT_IR3		(0x80000000 >>  15)

#define PPC_DMASTAT_ER0		(0x80000000 >>  16)
#define PPC_DMASTAT_ER1		(0x80000000 >>  17)
#define PPC_DMASTAT_ER2		(0x80000000 >>  18)
#define PPC_DMASTAT_ER3		(0x80000000 >>  19)

#define PPC_DMASTAT_CB0		(0x80000000 >>  20)
#define PPC_DMASTAT_CB1		(0x80000000 >>  21)
#define PPC_DMASTAT_CB2		(0x80000000 >>  22)
#define PPC_DMASTAT_CB3		(0x80000000 >>  23)

#define PPC_DMASTAT_SG0		(0x80000000 >>  24)
#define PPC_DMASTAT_SG1		(0x80000000 >>  25)
#define PPC_DMASTAT_SG2		(0x80000000 >>  26)
#define PPC_DMASTAT_SG3		(0x80000000 >>  27)


// polarity register: bit = 0 => active high !
#define PPC_DMAPOL_REQ0		(0x80000000 >>  0)
#define PPC_DMAPOL_REQ1		(0x80000000 >>  3)
#define PPC_DMAPOL_REQ2		(0x80000000 >>  6)
#define PPC_DMAPOL_REQ3		(0x80000000 >>  9)

#define PPC_DMAPOL_ACK0		(0x80000000 >>  1)
#define PPC_DMAPOL_ACK1		(0x80000000 >>  4)
#define PPC_DMAPOL_ACK2		(0x80000000 >>  7)
#define PPC_DMAPOL_ACK3		(0x80000000 >> 10)

#define PPC_DMAPOL_EOT0		(0x80000000 >>  2)
#define PPC_DMAPOL_EOT1		(0x80000000 >>  5)
#define PPC_DMAPOL_EOT2		(0x80000000 >>  8)
#define PPC_DMAPOL_EOT3		(0x80000000 >> 11)

// ---------- ROBIN3L I2C Register Map --------------
#define I2C_CONTROL_ADDRESS 0x30	// ControlPLD address on I2C bus
#define I2C_CONTROL_BYTES 1			// 1 byte, only 3 bits used, plus enable mask
#define I2C_CTL_LEDGRN (1 << 0)		// Pos side of bi-color LED: GREEN
#define I2C_CTL_LEDRED (1 << 1)		// Neg side of bi-color led: RED
#define I2C_CTL_PROG   (1 << 2)		// FPGA PROG  signal
#define I2C_CTL_MASK   ((1 << 3) - 1)	// mask for control bits
#define I2C_CTL_ENABLE 0xa0			// value to enable writes
#define I2C_STATUS_BYTES  2
// byte 1 (Lower!)
#define I2C_STAT_LEDPOS  (1 << 0)
#define I2C_STAT_LEDNEG  (1 << 1)
#define I2C_STAT_PROG    (1 << 2)
#define I2C_STAT_LED0    (1 << 3)	// PLD led 0: global reset. High when reset active
#define I2C_STAT_LED1    (1 << 4)	// PLD led 1: FPGA done. Low when FPGA done
#define I2C_STAT_PWR1_5	 (1 << 5)	// power good on 1.5V
#define I2C_STAT_PWR2_5	 (1 << 6)	// power good on 2.5V
#define I2C_STAT_MUBINT	 (1 << 7)	// Alarm on FPGA temperatur supervisor
// byte 0 (Higher!)
#define I2C_STAT_SWMASK	 0xff		// upper 8 bits are switch value

#define I2C_EEPROM_ADDRESS 0x50	// EEPROM address on I2C bus

// --------------------- GPIO Registers ------------------------

#define GPIO_MASK(x) (0x80000000 >> x) // make bit value from GPIO number

#ifdef VHDL_VER4
#define GPIO5_CR_MASK  (0x80000000 >>  5)
#define GPIO6_CR_MASK  (0x80000000 >>  6)
#define GPIO7_CR_MASK  (0x80000000 >>  7)
#define GPIO8_CR_MASK  (0x80000000 >>  8)
#define GPIO9_CR_MASK  (0x80000000 >>  9)
#define GPIO10_CR_MASK (0x80000000 >>  10)
// soft addresses 
#define GPIO0_OR_ADDR	(ppcRegs + gpioOutputReg/sizeof(Dword))
#define GPIO0_TCR_ADDR	(ppcRegs + gpioTriStateReg/sizeof(Dword))
#define GPIO0_ODR_ADDR	(ppcRegs + gpioOpenDrainReg/sizeof(Dword))
#define GPIO0_IR_ADDR	(ppcRegs + gpioInputReg/sizeof(Dword))

// define some GPIO related bits
#define GPIO_TCK_BIT		0
#define GPIO_TMSTDI_BIT		1
#define GPIO_TDO_BIT		2
#define GPIO_JTAG_STAT_BIT	3
#define GPIO_JTAG_CTL_BIT	4
#define GPIO_MUBCLK_BIT		6
#define GPIO_MUBDATA_BIT	7
#define GPIO_MUBINT_BIT		11
#define GPIO_LED0_BIT		14
#define GPIO_LED1_BIT		15
#define GPIO_LED2_BIT		16

#endif

#ifdef VHDL_VER3
// GPIO control. PC405CR has 23 GPIOs: GPIO1 .. GPIO23 (doesn't start with 0!!)

// #define CPCO_CR0	0x00700022      // Control register to enable/disable pin usage as GPIO
#define GPIO13_CR_MASK (0x80000000 >>  8)    // enable bit in CPC_CR0
#define GPIO14_CR_MASK (0x80000000 >>  9)    // enable bit in CPC_CR0
#define GPIO15_CR_MASK (0x80000000 >> 10)    // enable bit in CPC_CR0
#define GPIO16_CR_MASK (0x80000000 >> 11)    // enable bit in CPC_CR0
// hard coded register addresses
#define GPIO0_OR_ADDR	(Dword*)0xEF600700  // output register. GPIO bits map from 1 .. 23
#define GPIO0_TCR_ADDR	(Dword*)0xEF600704  // three-state control register. 0 => high-z 
#define GPIO0_IR_ADDR	(Dword*)0xEF60071C  // input register
#endif

#endif // ROBIN_PPC_H

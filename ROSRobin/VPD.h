// --------x----------------------------x-----------------------------------x--
/** 
 *  \file   VPD.h
 *
 *  \author M.Mueller 
 *  
 *  \date   13.08.2001
 *
 *
 *
 */
// --------x----------------------------x-----------------------------------x--
 



/* $Id$
 * $Revision$
 * $Log$
 * Revision 1.1  2005/05/17 09:12:16  mmueller
 * Added Files with code for handling PLX Eeprom contents
 *
 *
 */

#ifndef VPD_H
#define VPD_H

#include <string>
#include <vector>

/** A class to handle a Vital Product Data (VPD) key - data pair.
    The key is a two characters value conform to the VPD PCI specs.
    Data is intended to be a alphanumerical value of variable
    length.
    This class is a data primitive for the next class VPD. Refer to
    VPD for more details
*/

class VPDEntry {

 public:

  // Constructors

  /// Standart constructor (Empty in the moment?!)
  VPDEntry() { }


  /** Initialize the object with a key data pair out of a string and
      a char vector.
      @param key The data key. A two char value.
      @param data The data to be stored using that key.
  */
  VPDEntry(std::string key, std::vector<char> data);


  /** Initialize the object with a key data pair out of a string and
      a char vector.
      @param key The data key. A two char value.
      @param data The data to be stored using that key.
  */
  VPDEntry(char  *key, std::vector<char> data);


  /** Initialize the object with a key data pair out of a string and
      a charvector.
      @param key The data key. A two char value.
      @param data The data to be stored using that key.
      @param nrOfData The length of the data array.
  */
  VPDEntry(std::string key, const char *data, unsigned int nrOfData);


  /** Initialize the object with a key data pair out of a string and
      a char vector.
      @param key The data key. A two char value.
      @param data The data to be stored using that key.
      @param nrOfData The length of the data array.
  */
  VPDEntry(char  *key, const char *data, unsigned int nrOfData);


  /// Standard destructor
  ~VPDEntry();


  /** Set the key value
      @param key The data key. A two char value.
  */
  void setKey(std::string key);


  /** Set the key value
      @param key The data key. A two char value.
  */
  void setKey(char *key);


  /** Get the key value
      @returns The data key. A two char value.
  */
  std::string &  getKey(void);


  /** Set the data of this object.
      @param data The to be stored inside this object.
      @param nrOfData The length of the data array.
  */
  void setData(const char *data, size_t nrOfData);


  /** Set the data of this object.
      @param data The to be stored inside this object.
  */
  void setData(std::vector<char> data);


  /** Get the data of this object.
      @returns The to stored inside this object.
  */
  std::vector<char> getData(void);

 
  /** Gets the size of the data inside this object.
      @returns The size.
  */
  size_t getSize(void);


  /** A subscripting operator to access the data of this object.
      (Not the key!)
  */
  char &operator[](unsigned int index);


 protected:

  /// The key
  std::string m_key;


  /// Its data
  std::vector<char> m_data;

};



/** A class to handle Vital Product Data. This class can collect all 
    vital product data described in the PCI spec Appendix X. It is possible
    to add a product ID string, keys for the Read Only Field and keys for 
    Read Write Field. The class can search keys and return the dedicated data.
    The VPD information can be read and written to a byte array for EEPROM 
    initialisation.
*/

class VPD {

 public:

  /// The standard key to store the serial number.
  static const std::string SerialNumKey;


  /// The standard key to store the revision number.
  static const std::string RevisionKey;


  /** The standard key to store the remaining space in the 
      Read Only Field and the checksum.
  */
  static const std::string ChecksumTag;//    = "RV";


  /** The standard key to store the remaining space in the 
      Read Write Field.
  */
  static const std::string RemainingRWTag;// = "RW";


  /// Standard constructor
  VPD();


  /** Constructor which initialises the object with data inside a byte array
      @param dataField The byte array with the VPD inside (may be read out of an Eeprom)
  */
  VPD(char *dataField);


  /// The destuctor
  ~VPD();


  /** Returns the Product ID string.
      @returns The product ID string.
  */
  std::string getIdString(void);


  /** Sets the Product ID string.
      @param id The ID string.
  */
  void setIdString(std::string id);


  /** Get a VPD key - data pair by its number inside the VPD Read Only Field.
      @param index The index to identify the key - data pair.
  */
  VPDEntry &  roGetByNumber(unsigned int index);


  /** Get a VPD key - data pair by its key name inside the VPD Read Only Field.
      @param key The two char key name.
      @param index The index to identify the key - data pair if multiple pairs have been found.
  */
  VPDEntry &  roGetByKey(std::string key, unsigned int index = 0);


  /** The total number of key - data pairs in the Read Only Field.
      @returns The total number
  */
  size_t roNumberOfEntries(void);


  /** The number of key - data pairs of one two char key in the Read Only Field.
      q param The key
      @returns Its number
  */
  size_t roNumberOfEntries(std::string key);


  /** Writes a new key - data pair to the position index inside the Read Only
      Field.
      @param index The key - data position.
      @param key The two char key value.
      @param data The byte data array.
      @param size The number of data inside the array.
  */
  void roWriteEntry(unsigned int index, 
		    std::string key, 
		    char *data, 
		    unsigned int size);


  /** Writes a new key - data pair to the position index inside the Read Only
      Field.
      @param index The key - data position.
      @param entry A reference to a VPDEntry object which contains key and data.
  */
  void roWriteEntry(unsigned int index, VPDEntry &entry);


  /** Adds a new key - data pair to the end of the Read Only Field.
      @param key The two char key value.
      @param data The byte data array.
      @param size The number of data inside the array.
  */
  void roAddEntry(std::string key, 
		  const char *data, 
		  unsigned int size);


  /** Adds a new key - data pair to the end of the Read Only Field.
      @param entry A reference to a VPDEntry object which contains key and data.
  */
  void roAddEntry(VPDEntry &entry);


  /** Get a VPD key - data pair by its number inside the VPD Read Write Field.
      @param index The index to identify the key - data pair.
  */
  VPDEntry &  rwGetByNumber(unsigned int index);


  /** Get a VPD key - data pair by its key name inside the VPD Read Write Field.
      @param key The two char key name.
      @param index The index to identify the key - data pair if multiple pairs have been found.
  */
  VPDEntry &  rwGetByKey(std::string key, unsigned int index = 0);


  /** The total number of key - data pairs in the Read Write Field.
      @returns The total number
  */
  size_t rwNumberOfEntries(void);


  /** The number of key - data pairs of one two char key in the Read Write Field.
      q param The key
      @returns Its number
  */
  size_t rwNumberOfEntries(std::string key);


  /** Writes a new key - data pair to the position index inside the Read Write
      Field.
      @param index The key - data position.
      @param key The two char key value.
      @param data The byte data array.
      @param size The number of data inside the array.
  */
  void rwWriteEntry(unsigned int index, 
		    std::string key, 
		    char *data, 
		    unsigned int size);


  /** Writes a new key - data pair to the position index inside the Read Write
      Field.
      @param index The key - data position.
      @param entry A reference to a VPDEntry object which contains key and data.
  */
  void rwWriteEntry(unsigned int index, VPDEntry &entry);


  /** Adds a new key - data pair to the end of the Read Write Field.
      @param key The two char key value.
      @param data The byte data array.
      @param size The number of data inside the array.
  */
  void rwAddEntry(std::string key, 
		  char *data, 
		  unsigned int size);


  /** Adds a new key - data pair to the end of the Read Write Field.
      @param entry A reference to a VPDEntry object which contains key and data.
  */
  void rwAddEntry(VPDEntry &entry);


  /** Parses a char array and reads the VPD information inside the object.
      @param dataField the byte field which contains VPD data (maybe from a Eeprom)
  */
  void readFromDataField(char *dataField);


  /** Writes the VPD data to a byte array conform to PCI spec.
      @param dataField The byte field  for the VPD (maybe for a Eeprom).
      @param avROSpace The available space for the Read Only Field.
      @param avRWSpace The available space for the Read Write Field.
  */
  unsigned int writeToDataField(char *dataField, size_t avROSpace, unsigned int avRWSpace);


 protected:

  /// The ID string
  std::string m_typeIdString;


  /// The key - data pairs inside the RO Field
  std::vector<VPDEntry> m_readOnlyField;


  /// The key - data pairs inside the RW Field
  std::vector<VPDEntry> m_readWriteField;


  /// The Product ID tag
  static const char LargeResIDTag;


  /// The Read Only Field Tag
  static const char LargeResROTag;


  /// The Read Write Field Tag
  static const char LargeResRWTag;


  /// The End Tag
  static const char EndTag;

};

#endif



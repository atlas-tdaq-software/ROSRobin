// --------x----------------------------x-----------------------------------x--
/**
 ** \file   XilinxVirtexIIFpga.h
 **
 ** \author
 **
 ** \date
 **
 */
// --------x----------------------------x-----------------------------------x--


/* $Id$
 * $Revision$
 * $Log$
 * Revision 1.6  2005/07/28 21:19:32  akugel
 * file format corrected (duplicated linefeeds)
 *
 * Revision 1.5  2005/07/28 16:09:21  akugel
 * JTAG configuration proceedure modified by adding a JPROG_B command
 * prior to sending the bitfile. After termination of JPROG_B we wait
 * for completion of FPGA initialsation. This ensures proper re-configuration
 * of the FPGA
 *
 * Revision 1.4  2005/05/04 09:27:14  mmueller
 * Initial implementation which can read the FPGA config status and program it.
 *
 * Revision 1.3  2005/04/14 07:06:00  joos
 * lots of new untested code
 *
 * Revision 1.2  2005/04/11 11:24:15  joos
 * Modifications and new files of Matthias
 *
 * Revision 1.1.1.1  2005/04/07 11:34:38  joos
 * added ROSRobin
 *
 *
 */

/**
 * Description:
 *
 */

#ifndef ROSROBIN_XILINXVIRTEXIIFPGA_H
#define ROSROBIN_XILINXVIRTEXIIFPGA_H

#include "ROSRobin/JTagDevice.h"

namespace ROS {

  class BitStream;
  
  class XilinxVirtexIIFpga : public JTagDevice {

  public:
    
    XilinxVirtexIIFpga(JTagController & jtagController);
    
    ~XilinxVirtexIIFpga();
    
    void program(std::string firmwareFile);
    
    bool isProgrammed(void);

    bool isInitialised(void);
    
    ROS::BitStream getDeviceId(void);

    void clear();
  };

}
#endif //XILINXVIRTEXIIFPGA_H


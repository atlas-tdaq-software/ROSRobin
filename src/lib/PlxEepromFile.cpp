// --------x----------------------------x-----------------------------------x--
/**
 ** \file   PlxEepromFile.cpp
 **
 ** \author Matthias Mueller
 **
 ** \date
 **
 */
// --------x----------------------------x-----------------------------------x--

#include <iostream>
#include <fstream>
#include <string>
#include <sys/types.h>
#include "DFDebug/DFDebug.h"
#include "ROSRobin/PlxEepromFile.h"
#include "ROSRobin/Keyword.h"
#include "ROSRobin/ROSRobinExceptions.h"
#include "ROSRobin/VPD.h"


/************************************************************/
ROS::PlxEepromFile::EepromDataStruct::EepromDataStruct(void) :
/************************************************************/
  m_deviceId(0xFFFF),
  m_vendorId(0xFFFF),
  m_classCode(0xFFFFFFFF),
  m_maxLatency(0xFF),
  m_minGrant(0xFF),
  m_intPin(0xFF),
  m_intLineRouting(0xFF),
  m_mailbox0(0xFFFFFFFF),
  m_mailbox1(0xFFFFFFFF),
  m_pci2LocalRange(0xFFFFFFFF),
  m_pci2LocalBase(0xFFFFFFFF),
  m_localArbiterReg(0xFFFFFFFF),
  m_endianDescriptor(0xFFFFFFFF),
  m_pci2LocalExpRomRange(0xFFFFFFFF),
  m_pci2LocalExpRomBase(0xFFFFFFFF),
  m_busRegionDescr0(0xFFFFFFFF),
  m_dm2PciRange(0xFFFFFFFF),
  m_dm2PciMemoryBase(0xFFFFFFFF),
  m_dm2PciIoCfgBus(0xFFFFFFFF),
  m_dm2PciBase(0xFFFFFFFF),
  m_dm2PciIoCfgAddr(0xFFFFFFFF),
  m_subsystemDeviceId(0xFFFF),
  m_subsystemVendorId(0xFFFF),
  m_pci2Local1Range(0xFFFFFFFF),
  m_pci2Local1Base(0xFFFFFFFF),
  m_busRegionDescr1(0xFFFFFFFF),
  m_hotSwapControl (0xFFFFFFFF),
  m_pciArbiterControl(0xFFFF)
{
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"PlxEepromFile::EepromDataStruct::constructor: Called");
}


// This constructor opens a file and reads eeprom contents from there.
/*******************************************************/
ROS::PlxEepromFile::PlxEepromFile(std::string fileName) :
/*******************************************************/
  m_eepromContent(),
  m_vpdBoardName("Unknown"),
  m_vpdBoardSerial("0"),
  m_vpdBoardRev(0)
{
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"PlxEepromFile::PlxEepromFile::constructor 1: Called");
  // Read from the file
  read(fileName);
}


// Writes a firmware file to the EEPROM
/***************************************/
ROS::PlxEepromFile::PlxEepromFile(void) :
/***************************************/
  m_eepromContent(),
  m_vpdBoardName(""),
  m_vpdBoardSerial("0"),
  m_vpdBoardRev(0)
{
   DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"PlxEepromFile::PlxEepromFile::constructor 2: Called");
}


// Reads from the EEPROM and writes the content to a firmware file
/*************************************************/
void ROS::PlxEepromFile::read(std::string fileName)
/*************************************************/
{
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"PlxEepromFile::read: Called");

  // First check the file format
  // Search for the file name suffix
  u_int dotPosition = fileName.rfind(".");
  std::string suffix = fileName.substr(dotPosition + 1, fileName.size() - dotPosition - 1);
  std::ifstream file;
    
  // Check the suffix and throw an exception if it is not known (file format not supported)
  if (suffix == "etx") 
  {
    file.open(fileName.c_str());
    if (!file) 
      throw ROS::ROSRobinExceptions(ROS::ROSRobinExceptions::FIRMWARE, "Could not open file!");
    readText(file);
  }
  else if (suffix == "ihx") 
  {
    file.open(fileName.c_str());
    if (!file) 
      throw ROS::ROSRobinExceptions(ROS::ROSRobinExceptions::FIRMWARE, "Could not open file!");
    readIntelHex(file);
  }
  else 
    throw ROSRobinExceptions(ROS::ROSRobinExceptions::FIRMWARE, "Unknown file format");

  file.close();
}


// Writes the eeprom contents to a file
/**************************************************/
void ROS::PlxEepromFile::write(std::string fileName)
/**************************************************/
{
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"PlxEepromFile::read: Called");

  // First check the file format
  // Search for the file name suffix
  u_int dotPosition = fileName.rfind(".");
  std::string suffix = fileName.substr(dotPosition + 1, fileName.size() - dotPosition - 1);

  std::ofstream file(fileName.c_str());

  if (!file) 
    throw ROS::ROSRobinExceptions(ROS::ROSRobinExceptions::FIRMWARE,  "Could not open file!");
  
  // Check the suffix and throw an exception if it is not known (file format not supported)
  if (suffix == "etx")
    writeText(file);
  else if (suffix == "ihx") 
    writeIntelHex(file);
  else 
  {
    file.close();
    throw ROSRobinExceptions(ROS::ROSRobinExceptions::FIRMWARE, "Unknown file format");
  }
  
  file.close();
}


// Gets the raw data for a eeprom content
/*******************************************************/
std::vector<u_short> ROS::PlxEepromFile::getRawData(void)
/*******************************************************/
{
  unsigned int tempi;  //fix for gcc344 
  
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"PlxEepromFile::getRawData: Called");

  struct EepromDataStruct tempEepromStruct = m_eepromContent;
  tempi = tempEepromStruct.m_classCode;            wordSwap(tempi); tempEepromStruct.m_classCode = tempi;
  tempi = tempEepromStruct.m_mailbox0;             wordSwap(tempi); tempEepromStruct.m_mailbox0 = tempi;
  tempi = tempEepromStruct.m_mailbox1;             wordSwap(tempi); tempEepromStruct.m_mailbox1 = tempi;
  tempi = tempEepromStruct.m_pci2LocalRange;       wordSwap(tempi); tempEepromStruct.m_pci2LocalRange = tempi;
  tempi = tempEepromStruct.m_pci2LocalBase;        wordSwap(tempi); tempEepromStruct.m_pci2LocalBase = tempi;
  tempi = tempEepromStruct.m_localArbiterReg;      wordSwap(tempi); tempEepromStruct.m_localArbiterReg = tempi;
  tempi = tempEepromStruct.m_endianDescriptor;     wordSwap(tempi); tempEepromStruct.m_endianDescriptor = tempi;
  tempi = tempEepromStruct.m_pci2LocalExpRomRange; wordSwap(tempi); tempEepromStruct.m_pci2LocalExpRomRange = tempi;
  tempi = tempEepromStruct.m_pci2LocalExpRomBase;  wordSwap(tempi); tempEepromStruct.m_pci2LocalExpRomBase = tempi;
  tempi = tempEepromStruct.m_busRegionDescr0;      wordSwap(tempi); tempEepromStruct.m_busRegionDescr0 = tempi;
  tempi = tempEepromStruct.m_dm2PciRange;          wordSwap(tempi); tempEepromStruct.m_dm2PciRange = tempi;
  tempi = tempEepromStruct.m_dm2PciMemoryBase;     wordSwap(tempi); tempEepromStruct.m_dm2PciMemoryBase = tempi;
  tempi = tempEepromStruct.m_dm2PciIoCfgBus;       wordSwap(tempi); tempEepromStruct.m_dm2PciIoCfgBus = tempi;
  tempi = tempEepromStruct.m_dm2PciBase;           wordSwap(tempi); tempEepromStruct.m_dm2PciBase = tempi;
  tempi = tempEepromStruct.m_dm2PciIoCfgAddr;      wordSwap(tempi); tempEepromStruct.m_dm2PciIoCfgAddr = tempi;
  tempi = tempEepromStruct.m_pci2Local1Range;      wordSwap(tempi); tempEepromStruct.m_pci2Local1Range = tempi;
  tempi = tempEepromStruct.m_pci2Local1Base;       wordSwap(tempi); tempEepromStruct.m_pci2Local1Base = tempi;
  tempi = tempEepromStruct.m_busRegionDescr1;      wordSwap(tempi); tempEepromStruct.m_busRegionDescr1 = tempi;
  tempi = tempEepromStruct.m_hotSwapControl;       wordSwap(tempi); tempEepromStruct.m_hotSwapControl = tempi;
  
  // Make a pointer to the raw memory of the eepromContent struct
  u_short *rawContent = reinterpret_cast<u_short *>(& tempEepromStruct);

  // Allocate some memory for the return value
  std::vector<u_short> retValue(0xFF);

  // Init the return value with 0xF
  u_int i;
  for (i = 0; i < 0xFF; i++) 
    retValue[i] = 0x0;

  // Copy the eeprom data
  for (i = 0; i < sizeof(EepromDataStruct) / 2; i++) 
    retValue[i] = rawContent[i];

  // Now create the VPD contents
  VPD vpdContent;

  // First set the board name
  vpdContent.setIdString(m_vpdBoardName);

  // Now the serial number
  vpdContent.roAddEntry(VPD::SerialNumKey, m_vpdBoardSerial.c_str(), m_vpdBoardSerial.length());

  // And finally the revision
  std::stringstream conv;
  conv << m_vpdBoardRev;
  std::string revisionStr = conv.str();
  vpdContent.roAddEntry(VPD::RevisionKey, revisionStr.c_str(), revisionStr.length());

  // Get the VPD information in raw format
  u_short dataField[0x20];

  for (i = 0; i < 0x20; i++) 
    dataField[i] = 0;

  u_int length = vpdContent.writeToDataField( reinterpret_cast<char *>(dataField), 0x25, 0x30 - 0x25);

  // and copy it
  u_int vpdIdx;
  for (vpdIdx = 0; vpdIdx < length / 2; vpdIdx++) 
    retValue[0x30 + vpdIdx] = dataField[vpdIdx];
  
  return retValue;
}

// Sets the eeprom content from a raw data array
/*********************************************************************/
void ROS::PlxEepromFile::setFromRawData(std::vector<u_short> & rawData)
/*********************************************************************/
{
  unsigned int tempi;  //fix for gcc344 

  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"PlxEepromFile::setFromRawData: Called");
  // Make a pointer to the raw memory of the eepromContent struct
  u_short *rawContent = reinterpret_cast<u_short *>(& m_eepromContent);

  // Copy the eeprom information 
  u_int i;
  for (i = 0; i < sizeof(EepromDataStruct) / 2; i++) 
    rawContent[i] = rawData[i];

  tempi = m_eepromContent.m_classCode;            wordSwap(tempi); m_eepromContent.m_classCode = tempi;
  tempi = m_eepromContent.m_mailbox0;             wordSwap(tempi); m_eepromContent.m_mailbox0 = tempi;
  tempi = m_eepromContent.m_mailbox1;             wordSwap(tempi); m_eepromContent.m_mailbox1 = tempi;
  tempi = m_eepromContent.m_pci2LocalRange;       wordSwap(tempi); m_eepromContent.m_pci2LocalRange = tempi;
  tempi = m_eepromContent.m_pci2LocalBase;        wordSwap(tempi); m_eepromContent.m_pci2LocalBase = tempi;
  tempi = m_eepromContent.m_localArbiterReg;      wordSwap(tempi); m_eepromContent.m_localArbiterReg = tempi;
  tempi = m_eepromContent.m_endianDescriptor;     wordSwap(tempi); m_eepromContent.m_endianDescriptor = tempi;
  tempi = m_eepromContent.m_pci2LocalExpRomRange; wordSwap(tempi); m_eepromContent.m_pci2LocalExpRomRange = tempi;
  tempi = m_eepromContent.m_pci2LocalExpRomBase;  wordSwap(tempi); m_eepromContent.m_pci2LocalExpRomBase = tempi;
  tempi = m_eepromContent.m_busRegionDescr0;      wordSwap(tempi); m_eepromContent.m_busRegionDescr0 = tempi;
  tempi = m_eepromContent.m_dm2PciRange;          wordSwap(tempi); m_eepromContent.m_dm2PciRange = tempi;
  tempi = m_eepromContent.m_dm2PciMemoryBase;     wordSwap(tempi); m_eepromContent.m_dm2PciMemoryBase = tempi;
  tempi = m_eepromContent.m_dm2PciIoCfgBus;       wordSwap(tempi); m_eepromContent.m_dm2PciIoCfgBus = tempi;
  tempi = m_eepromContent.m_dm2PciBase;           wordSwap(tempi); m_eepromContent.m_dm2PciBase = tempi;
  tempi = m_eepromContent.m_dm2PciIoCfgAddr;      wordSwap(tempi); m_eepromContent.m_dm2PciIoCfgAddr = tempi;
  tempi = m_eepromContent.m_pci2Local1Range;      wordSwap(tempi); m_eepromContent.m_pci2Local1Range = tempi;
  tempi = m_eepromContent.m_pci2Local1Base;       wordSwap(tempi); m_eepromContent.m_pci2Local1Base = tempi;
  tempi = m_eepromContent.m_busRegionDescr1;      wordSwap(tempi); m_eepromContent.m_busRegionDescr1 = tempi;
  tempi = m_eepromContent.m_hotSwapControl;       wordSwap(tempi); m_eepromContent.m_hotSwapControl = tempi;

  unsigned char dataField[0x40];

  // Old ROBIN boards may have a slightly different format of
  // the VPD information. It is written in big endian format
  // and starts at 0x2d. We try to be compatible with this.

  // Look if there is the VPD large resource tag at 0x2d
  if (rawData[0x2d] == 0x8200) 
  {
    // Copy the VPD information    
    u_int vpdIdx;
    for (vpdIdx = 0; vpdIdx < 0x20; vpdIdx++) 
    {
      dataField[vpdIdx * 2] = rawData[0x2d + vpdIdx] >> 8;
      dataField[vpdIdx * 2 + 1] = rawData[0x2d + vpdIdx] & 0xFF;
    }
  }
  // otherwise we start at 0x30 with the VPD information.
  else 
  {
    u_int vpdIdx;
    for (vpdIdx = 0; vpdIdx < 0x20; vpdIdx++)
    {
      dataField[vpdIdx * 2] = rawData[0x30 + vpdIdx] & 0xFF;
      dataField[vpdIdx * 2 + 1] = rawData[0x30 + vpdIdx] >> 8;
    }
  }

  VPD vpdContent(reinterpret_cast<char *>(dataField));

  m_vpdBoardName   = vpdContent.getIdString();
  
  std::vector<char> convArray = vpdContent.roGetByKey(VPD::SerialNumKey).getData();
  std::string convString;

  std::vector<char>::iterator iter;
  for (iter = convArray.begin(); iter != convArray.end(); iter++) 
    convString.append(1, *iter);

  m_vpdBoardSerial = convString;
  convArray = vpdContent.roGetByKey(VPD::RevisionKey).getData();
  convString = "";
  for (iter = convArray.begin(); iter != convArray.end(); iter++) 
    convString.append(1, *iter);

  m_vpdBoardRev = decStringToUInt(convString);
}



// Writes the eeprom contents to a text file
/**************************************************************/
void ROS::PlxEepromFile::writeText(std::ofstream & outputStream)
/**************************************************************/
{
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"PlxEepromFile::writeText: Called");

  outputStream << "DeviceId:               " << std::hex <<  m_eepromContent.m_deviceId << std::endl;
  outputStream << "VendorId:               " <<  m_eepromContent.m_vendorId << std::endl;
  outputStream << "ClassCode:              " <<  m_eepromContent.m_classCode << std::endl;
  outputStream << "MaxLatency:             " <<  static_cast<u_int> (m_eepromContent.m_maxLatency) << std::endl;
  outputStream << "MinGrant:               " <<  static_cast<u_int> (m_eepromContent.m_minGrant) << std::endl;
  outputStream << "IntPin:                 " <<  static_cast<u_int> (m_eepromContent.m_intPin) << std::endl;
  outputStream << "IntLineRouting:         " <<  static_cast<u_int> (m_eepromContent.m_intLineRouting) << std::endl;
  outputStream << "Mailbox0:               " <<  m_eepromContent.m_mailbox0 << std::endl;
  outputStream << "Mailbox1:               " <<  m_eepromContent.m_mailbox1 << std::endl;
  outputStream << "Pci2LocalRange:         " <<  m_eepromContent.m_pci2LocalRange << std::endl;
  outputStream << "Pci2LocalBase:          " <<  m_eepromContent.m_pci2LocalBase << std::endl;
  outputStream << "LocalArbiterReg:        " <<  m_eepromContent.m_localArbiterReg << std::endl;
  outputStream << "EndianDescriptor:       " <<  m_eepromContent.m_endianDescriptor << std::endl;
  outputStream << "Pci2LocalExpRomRange:   " <<  m_eepromContent.m_pci2LocalExpRomRange << std::endl;
  outputStream << "Pci2LocalExpRomRange:   " <<  m_eepromContent.m_pci2LocalExpRomBase << std::endl;
  outputStream << "BusRegionDescr0:        " <<  m_eepromContent.m_busRegionDescr0 << std::endl;
  outputStream << "Dm2PciRange:            " <<  m_eepromContent.m_dm2PciRange << std::endl;
  outputStream << "Dm2pciMemoryBase:       " <<  m_eepromContent.m_dm2PciMemoryBase << std::endl;
  outputStream << "Dm2PciIoCfgBus:         " <<  m_eepromContent.m_dm2PciIoCfgBus << std::endl;
  outputStream << "Dm2PciBase:             " <<  m_eepromContent.m_dm2PciBase << std::endl;
  outputStream << "Dm2PciIoCfgAddr:        " <<  m_eepromContent.m_dm2PciIoCfgAddr << std::endl;
  outputStream << "SubsystemDeviceId:      " <<  static_cast<u_int>(m_eepromContent.m_subsystemDeviceId) << std::endl;
  outputStream << "SubsystemVendorId:      " <<  static_cast<u_int>(m_eepromContent.m_subsystemVendorId) << std::endl;
  outputStream << "Pci2Local1Range:        " <<  m_eepromContent.m_pci2Local1Range << std::endl;
  outputStream << "Pci2Local1Base:         " <<  m_eepromContent.m_pci2Local1Base << std::endl;
  outputStream << "BusRegionDescr1:        " <<  m_eepromContent.m_busRegionDescr1 << std::endl;
  outputStream << "HotSwapControl:         " <<  m_eepromContent.m_hotSwapControl << std::endl;
  outputStream << "PciArbiterControl:      " <<  static_cast<u_int>(m_eepromContent.m_pciArbiterControl) << std::dec << std::endl;
  outputStream << "VPDBoardName:           " <<  m_vpdBoardName << std::endl;
  outputStream << "VPDBoardSerial:         " <<  m_vpdBoardSerial << std::endl;
  outputStream << "VPDBoardRev:            " <<  m_vpdBoardRev << std::endl;
}


// Writes the eeprom contents to a hex file (intel hex format)
/******************************************************************/
void ROS::PlxEepromFile::writeIntelHex(std::ofstream & /*outputStream*/)
/******************************************************************/
{
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"PlxEepromFile::writeIntelHex: Called (empty function)");
}


// Reads the eeprom file from a text file
/************************************************************/
void ROS::PlxEepromFile::readText(std::ifstream & inputStream)
/************************************************************/
{
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"PlxEepromFile::readText: Called");

  m_eepromContent.m_deviceId             = readTextItem(inputStream, "DeviceId:");
  m_eepromContent.m_vendorId             = readTextItem(inputStream, "VendorId:");
  m_eepromContent.m_classCode            = readTextItem(inputStream, "ClassCode:");
  m_eepromContent.m_maxLatency           = readTextItem(inputStream, "MaxLatency:");
  m_eepromContent.m_minGrant             = readTextItem(inputStream, "MinGrant:");
  m_eepromContent.m_intPin               = readTextItem(inputStream, "IntPin:");
  m_eepromContent.m_intLineRouting       = readTextItem(inputStream, "IntLineRouting:");
  m_eepromContent.m_mailbox0             = readTextItem(inputStream, "Mailbox0:");
  m_eepromContent.m_mailbox1             = readTextItem(inputStream, "Mailbox1:");
  m_eepromContent.m_pci2LocalRange       = readTextItem(inputStream, "Pci2LocalRange:");
  m_eepromContent.m_pci2LocalBase        = readTextItem(inputStream, "Pci2LocalBase:");
  m_eepromContent.m_localArbiterReg      = readTextItem(inputStream, "LocalArbiterReg:");
  m_eepromContent.m_endianDescriptor     = readTextItem(inputStream, "EndianDescriptor:");
  m_eepromContent.m_pci2LocalExpRomRange = readTextItem(inputStream, "Pci2LocalExpRomRange:");
  m_eepromContent.m_pci2LocalExpRomBase  = readTextItem(inputStream, "Pci2LocalExpRomRange:");
  m_eepromContent.m_busRegionDescr0      = readTextItem(inputStream, "BusRegionDescr0:");
  m_eepromContent.m_dm2PciRange          = readTextItem(inputStream, "Dm2PciRange:");
  m_eepromContent.m_dm2PciMemoryBase     = readTextItem(inputStream, "Dm2pciMemoryBase:");
  m_eepromContent.m_dm2PciIoCfgBus       = readTextItem(inputStream, "Dm2PciIoCfgBus:");
  m_eepromContent.m_dm2PciBase           = readTextItem(inputStream, "Dm2PciBase:");
  m_eepromContent.m_dm2PciIoCfgAddr      = readTextItem(inputStream, "Dm2PciIoCfgAddr:");
  m_eepromContent.m_subsystemDeviceId    = readTextItem(inputStream, "SubsystemDeviceId:");
  m_eepromContent.m_subsystemVendorId    = readTextItem(inputStream, "SubsystemVendorId:");
  m_eepromContent.m_pci2Local1Range      = readTextItem(inputStream, "Pci2Local1Range:");
  m_eepromContent.m_pci2Local1Base       = readTextItem(inputStream, "Pci2Local1Base:");
  m_eepromContent.m_busRegionDescr1      = readTextItem(inputStream, "BusRegionDescr1:");
  m_eepromContent.m_hotSwapControl       = readTextItem(inputStream, "HotSwapControl:");
  m_eepromContent.m_pciArbiterControl    = readTextItem(inputStream, "PciArbiterControl:");

  std::string value;
  inputStream >> Keyword("VPDBoardName:") >> value;
  m_vpdBoardName         = value;

  value = "";
  inputStream >> Keyword("VPDBoardSerial:") >> value;
  m_vpdBoardSerial       = value;

  value = "";
  inputStream >> Keyword("VPDBoardRev:") >> value;
  m_vpdBoardRev          = decStringToUInt(value);
}


/***************************************************/
std::string ROS::PlxEepromFile::getSerialNumber(void) 
/***************************************************/
{
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"PlxEepromFile::getSerialNumber: Called");
  return m_vpdBoardSerial;
}


/************************************************/
std::string ROS::PlxEepromFile::getBoardType(void)
/************************************************/
{
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"PlxEepromFile::getBoardType: Called");
  return m_vpdBoardName;
}


/***********************************************/
u_int ROS::PlxEepromFile::getRevisionNumber(void)
/***********************************************/
{
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"PlxEepromFile::getRevisionNumber: Called");
  return m_vpdBoardRev;
}


// Reads a single text item from a text file
/**********************************************************************************/
u_int ROS::PlxEepromFile::readTextItem(std::ifstream & inputStream, std::string key)
/**********************************************************************************/
{
  std::string value;
  
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"PlxEepromFile::readTextItem: Called");
  inputStream >> Keyword(key) >> value;
  return hexStringToUInt(value);
}


// Reads the eeprom file from a hex file (Intel hex format)
/****************************************************************/
void ROS::PlxEepromFile::readIntelHex(std::ifstream & inputStream)
/****************************************************************/
{  
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"PlxEepromFile::readIntelHex: Called (Empty function)");
}


/**********************************************/
void ROS::PlxEepromFile::wordSwap(u_int & dword)
/**********************************************/
{
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"PlxEepromFile::wordSwap: Called");

  u_int temp = (dword & 0xFFFF) << 16;
  temp |= (dword >> 16) & 0xFFFF;
  dword = temp;
}


/* $Id$
 * $Revision$
 * $Log$
 * Revision 1.8  2008/12/03 10:16:29  joos
 * (some) compiler warnings fixed
 *
 * Revision 1.7  2006/01/17 09:53:59  joos
 * modifications for gcc344 and cosmetic changes
 *
 * Revision 1.6  2005/08/18 14:57:15  joos
 * globally: some bugs fixed. some features added. Some files re-formatted
 *
 * Revision 1.5  2005/07/22 11:31:57  mmueller
 * Added get methods for three eeprom contents
 *
 * Revision 1.4  2005/07/21 14:59:10  mmueller
 * robin_firmware_update bugfixes + PPC environment support
 *
 * Revision 1.3  2005/07/12 13:36:14  mmueller
 * Changed the type of the VPD serial number variable
 *
 * Revision 1.2  2005/06/07 13:46:16  mmueller
 * Changed the size of some EEPROM contents. Tested and removed some bugs.
 *
 * Revision 1.1  2005/05/17 09:11:11  mmueller
 * Added Files with code for handling PLX Eeprom contents
 *
 *
 *
 */


#include "ROSRobin/ROSRobinExceptions.h"

using namespace ROS;

/*****************************************************************************/
ROSRobinExceptions::ROSRobinExceptions(ROSRobinExceptions::ErrorCode errorCode)
/*****************************************************************************/
: ROSException("ROSRobin", errorCode, getErrorString(errorCode))
{
}


/******************************************************************************************************/
ROSRobinExceptions::ROSRobinExceptions(ROSRobinExceptions::ErrorCode errorCode, std::string description)
/******************************************************************************************************/
: ROSException("ROSRobin", errorCode, getErrorString(errorCode), description)
{ 
}

/*****************************************************************************/
ROSRobinExceptions::ROSRobinExceptions(ROSRobinExceptions::ErrorCode errorCode, const ers::Context& context)
/*****************************************************************************/
: ROSException("ROSRobin", errorCode, getErrorString(errorCode), context)
{
}


/******************************************************************************************************/
ROSRobinExceptions::ROSRobinExceptions(ROSRobinExceptions::ErrorCode errorCode, std::string description, const ers::Context& context)
/******************************************************************************************************/
: ROSException("ROSRobin", errorCode, getErrorString(errorCode), description, context) 
{ 
}

ROSRobinExceptions::ROSRobinExceptions(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context) 
     : ROSException(cause, "ROSRobin", error, getErrorString(error), description, context)
{
}


/************************************************************************/
std::string ROSRobinExceptions::getErrorString(unsigned int errorId) const 
/************************************************************************/
{
  std::string rc;
  switch (errorId) 
  {
  /*******************************/
  /*Exceptions of the Robin class*/
  /*******************************/
  case ALREADY_DONE:
    rc = "You have called the function reserveDmaMemory twice. This function must only be called once in each application";
    break;
  case IOCTL:
    rc = "Error from call to ROBIN driver";
    break;
  case NOTOPEN:
    rc = "The library has not yet been opened";
    break;
  case MUNMAP:
    rc = "Error from call to munmap()";
    break;
  case MMAP:
    rc = "Error from call to mmap()";
    break;
  case ALREADY_DONE2:
    rc = "You have called the function setDmaRamParams twice. This function must only be called once in each application";
    break;
  case CMEM:
    rc = "Error from a function of the CMEM_RCC package";
    break;
  case OPEN:
    rc = "Failed to open /dev/robin. Check if the file exists and if the read and write flags for user have been set";
    break;
  case CLOSE:
    rc = "Failed to close /dev/robin";
    break;
  case ALIGN:
    rc = "The parameter msgInputMemorySize is not 32 byte aligned";
    break;
  case ALIGN2:
    rc = "The parameter address is not 4 byte aligned";
    break;
  case ILL_ORDER:
    rc = "The parameter order is too large";
    break;
  case NROLS:    
    rc = "Real and requested number of ROLs does not match";
    break;
  case BITSTREAM:
    rc = "Exception while handling a BitStream object";
    break;
  case FIRMWARE:
    rc = "Exception while reading a firmware file";
    break;
  case TIMEOUT:
    rc = "Timeout when waiting for the RobIn";
    break;
  case JTAG:
    rc = "Error in the JTAG handling";
    break;
  case MISCHANDLE:
    rc = "Invalid handle passed to retMiscMem()";
    break;
  case TSTAMPE:
    rc = "Failed to open the rcc_time_stamp package";
    break;
  case NOPCI:
    rc = "The driver did not return valiad PCI addresses for the PLX and FPGA. Maybe you are trying to link to a non existing Robin";
    break;
  case ILLSIZE:
    rc = "The value of <size> is too big for the available DPM window";
    break;    
  case BADCRC:
    rc = "The CRC of the ROD fragment does not match";
    break;
  
  /*****************************/
  /*Exceptions of the Rol class*/
  /*****************************/
  case ROBSTAT:
    rc = "Status word of ROB header contains error flag";
    break;
  case WRONGREPLY: 
    rc = "The reply message from the ROBIN contains invalid data";
    break;
  case WRONGVERSION:
    rc = "The Robin cannot deal with data of that format. Your application and the PPC F/W of the ROBIN are not compatible. Most likely you are mixing code from different releases.";
    break;
  default:
    rc = "";
    break;
  }
  return rc;
}

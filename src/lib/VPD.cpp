// --------x----------------------------x-----------------------------------x--
/**
 *  \file   VPD.cpp
 *
 *  \author M.Mueller
 *
 *  \date   13.08.2001
 *
 *
 *
 */
// --------x----------------------------x-----------------------------------x--


/* $Id$
 * $Log$
 * Revision 1.1  2005/05/17 09:11:11  mmueller
 * Added Files with code for handling PLX Eeprom contents
 *
 *
 */

#include <string>
#include <vector>

#include "ROSRobin/ROSRobinExceptions.h"
#include "ROSRobin/VPD.h"

const std::string VPD::SerialNumKey = "SN";
const std::string VPD::RevisionKey = "EC";

const std::string VPD::ChecksumTag    = "RV";
const std::string VPD::RemainingRWTag = "RW";


VPDEntry::VPDEntry(std::string key, std::vector<char> data) {
  setKey(key);
  setData(data);
}

VPDEntry::VPDEntry(char *key, std::vector<char> data) {
  setKey(key);
  setData(data);
}

VPDEntry::VPDEntry(std::string key, const char *data, unsigned int nrOfData) {
  setKey(key);
  setData(data, nrOfData);
}

VPDEntry::VPDEntry(char *key, const char *data, unsigned int nrOfData) {
  setKey(key);
  setData(data, nrOfData);
}

VPDEntry::~VPDEntry() {

}

inline void VPDEntry::setKey(std::string key) {
  m_key = key;
}

inline void VPDEntry::setKey(char *key) {
  m_key = key;
}

inline std::string &  VPDEntry::getKey(void) {
  return m_key;
}

inline void VPDEntry::setData(const char *data, size_t nrOfData) {

  size_t counter;

  if (m_data.size() != 0)
    m_data.clear();

  for (counter = 0; counter < nrOfData; counter++) {
    m_data.push_back(data[counter]);
  }

}

inline void VPDEntry::setData(std::vector<char> data) {
  m_data = data;
}

std::vector<char> VPDEntry::getData(void) {
  return m_data;
}

size_t VPDEntry::getSize(void) {
  return m_data.size();
}

char&  VPDEntry::operator[](unsigned int index) {
  return m_data[index];
}


VPD::VPD() {
}

VPD::VPD(char*dataField) {
  readFromDataField(dataField);
}

VPD::~VPD() {
}

std::string VPD::getIdString(void) {
  return m_typeIdString;
}

void VPD::setIdString(std::string id) {
  m_typeIdString = id;
}

VPDEntry &  VPD::roGetByNumber(unsigned int index) {
  return m_readOnlyField[index];
}

VPDEntry &  VPD::roGetByKey(std::string key, unsigned int index/* = 0*/) {
  unsigned int counter = 0;

  for (std::vector<VPDEntry>::iterator iter = m_readOnlyField.begin();
       iter < m_readOnlyField.end();
       iter++) {
    if (iter->getKey() == key) {
      if (counter == index)
	return *iter;
      else
	counter++;
    }
  }
  throw ROS::ROSRobinExceptions(ROS::ROSRobinExceptions::FIRMWARE, "Class VPD: Key not found");
}

size_t VPD::roNumberOfEntries(void){
  return m_readOnlyField.size();
}

size_t VPD::roNumberOfEntries(std::string key) {
  unsigned int counter = 0;

  for (std::vector<VPDEntry>::iterator iter = m_readOnlyField.begin();
       iter < m_readOnlyField.end();
       iter++) {
    if (iter->getKey() == key) {
      counter++;
    }
  }
  return counter;
}

void VPD::roWriteEntry(unsigned int index,
		       std::string key,
		       char*data,
		       unsigned int size) {
  VPDEntry temp(key, data, size);
  m_readOnlyField[index] = temp;
}

void VPD::roWriteEntry(unsigned int index, VPDEntry &entry) {
  m_readOnlyField[index] = entry;
}

void VPD::roAddEntry(std::string key,
		     const char *data,
		     unsigned int size) {
  VPDEntry temp(key, data, size);
  m_readOnlyField.push_back(temp);
}

void VPD::roAddEntry(VPDEntry &entry) {
  m_readOnlyField.push_back(entry);
}


VPDEntry &  VPD::rwGetByNumber(unsigned int index) {
  return m_readWriteField[index];
}

VPDEntry &  VPD::rwGetByKey(std::string key, unsigned int index /*= 0*/) {
  unsigned int counter = 0;

  for (std::vector<VPDEntry>::iterator iter = m_readWriteField.begin();
       iter < m_readWriteField.end();
       iter++) {
    if (iter->getKey() == key) {
      if (counter == index)
	return *iter;
      else
	counter++;
    }
  }
  throw ROS::ROSRobinExceptions(ROS::ROSRobinExceptions::FIRMWARE, "Class VPD: Key not found");
}

size_t VPD::rwNumberOfEntries(void) {
  return m_readWriteField.size();
}

size_t VPD::rwNumberOfEntries(std::string key) {
  unsigned int counter = 0;

  for (std::vector<VPDEntry>::iterator iter = m_readWriteField.begin();
       iter < m_readWriteField.end();
       iter++) {
    if (iter->getKey() == key) {
      counter++;
    }
  }
  return counter;
}

void VPD::rwWriteEntry(unsigned int index,
		       std::string key,
		       char*data,
		       unsigned int size) {

  VPDEntry temp(key, data, size);
  m_readWriteField[index] = temp;
}

void VPD::rwWriteEntry(unsigned int index, VPDEntry &entry) {
  m_readWriteField[index] = entry;
}

void VPD::rwAddEntry(std::string key,
		     char*data,
		     unsigned int size) {
  VPDEntry temp(key, data, size);
  m_readWriteField.push_back(temp);
}

void VPD::rwAddEntry(VPDEntry &entry) {
  m_readWriteField.push_back(entry);
}

//FIXME: crashes on gcc3.2/ia64.
void VPD::readFromDataField(char*dataField) {

  unsigned int length, counter, markRO, markRW;
  bool lastTag;

  if (dataField[0] != LargeResIDTag)
    throw ROS::ROSRobinExceptions(ROS::ROSRobinExceptions::FIRMWARE, "Class VPD: "
                                                                     "Could not find VPD Large Resource ID String Tag");

  length = dataField[2] | (dataField[1] << 8);

  char *tempStr;

  tempStr = new char[length + 1];

  for (counter = 0; counter < length; counter++) {
    tempStr[counter] = (char ) dataField[counter + 3] ;
  }

  tempStr[counter] = 0;

  m_typeIdString = tempStr;

  delete[] tempStr;

  markRO = length + 3;

  if (dataField[markRO] != LargeResROTag)
    throw ROS::ROSRobinExceptions(ROS::ROSRobinExceptions::FIRMWARE, "Class VPD: No VPD RO Field");

  length = dataField[markRO + 2] | (dataField[markRO + 1] << 8);

  lastTag = false;
  counter = markRO + 3;
  while (!lastTag) {
    std::string key;
    unsigned int entryLength;
    char *tempStr;

    tempStr = new char[3];

    tempStr[2] = 0;
    tempStr[0] = dataField[counter++];
    tempStr[1] = dataField[counter++];
    entryLength = dataField[counter++];

    key = tempStr;

    delete[] tempStr;

    if (key == ChecksumTag) {
      lastTag = true;
      break;
    }

    VPDEntry entry(key, &dataField[counter], entryLength);

    m_readOnlyField.push_back(entry);
    counter += entryLength;
  }

  markRW = markRO + length + 3;

  if (dataField[markRW] != LargeResRWTag)
    throw ROS::ROSRobinExceptions(ROS::ROSRobinExceptions::FIRMWARE, "Class VPD: No VPD RW Field");

  length = dataField[markRW + 2] | (dataField[markRW + 1] << 8);

  lastTag = false;
  counter = markRW + 3;
  while (!lastTag) {
    std::string key;
    unsigned int entryLength;
    char *tempStr;

    tempStr = new char[3];

    tempStr[2] = 0;
    tempStr[0] = dataField[counter++];
    tempStr[1] = dataField[counter++];
    entryLength = dataField[counter++];

    key = tempStr;

    delete[] tempStr;

    if (key == RemainingRWTag) {
      lastTag = true;
      break;
    }

    VPDEntry entry(key, &dataField[counter], entryLength);

    m_readWriteField.push_back(entry);
    counter += entryLength;
  }

  if (dataField[markRW + length + 3] != EndTag)
    throw ROS::ROSRobinExceptions(ROS::ROSRobinExceptions::FIRMWARE, "Class VPD: Invalid or none VPD End Tag");

} // end: void VPD::ReadFromDataField(char*dataField)



unsigned int VPD::writeToDataField(char*dataField, size_t avROSpace, unsigned int avRWSpace) {
  size_t counter, length, markRO, markRW;
  int checksum = 0;

  // The first word is the Large Resource ID Tag.
  // This introduces the ID string field.
  dataField[0] = LargeResIDTag;

  checksum += dataField[0];

  //First write the string length as the resource field length
  length = m_typeIdString.size();
  dataField[1] = (char) ((length >> 8) & 0xFF);
  dataField[2] = (char) length & 0xFF;
  checksum += dataField[1] + dataField[2];

  // Now write the string characters
  counter = 3;
  for (std::string::iterator iter = m_typeIdString.begin();
       iter < m_typeIdString.end();
       iter++) {

    dataField[counter] = (char) *iter;
    checksum += dataField[counter++];
  }

  // Mark where we are in the data array
  markRO = length + 3;

  // Next is the Readonly field
  dataField[markRO] = LargeResROTag;
  checksum += dataField[markRO];

  // Now we know how many data has been written into the RO field
  // So we can update the length entry at the beginning of the RO field
  size_t roLength = avROSpace;
  dataField[markRO + 1] = (char) ((roLength >> 8) & 0xFF);
  dataField[markRO + 2] = (char) (roLength & 0xFF);
  checksum += dataField[markRO + 1] + dataField[markRO + 2];

  // Each key in the RO field is written to the data array by this loop
  counter = markRO + 3;
  std::vector<VPDEntry>::iterator entryIter;
  for (entryIter = m_readOnlyField.begin();
       entryIter < m_readOnlyField.end();
       entryIter++) {

    // Pick up the key name
    std::string key = entryIter->getKey();
    // and the data
    std::vector<char> data = entryIter->getData();
    size_t keyLength = data.size();
    if (key == "")
      throw ROS::ROSRobinExceptions(ROS::ROSRobinExceptions::FIRMWARE, "Class VPD: VPD::WriteToDataField: "
                                                                       "Key field empty in RO");

    // Now write the two key characters and the data length
    dataField[counter] = (char) key[0];
    checksum += dataField[counter++];
    dataField[counter] = (char) key[1];
    checksum += dataField[counter++];
    dataField[counter] = (char) keyLength;
    checksum += dataField[counter++];

    // Finally write the data
    for (std::vector<char>::iterator dataIter = data.begin();
	 dataIter < data.end();
	 dataIter++) {

      dataField[counter] = *dataIter;
      checksum += dataField[counter++];
    }

    if ((counter - (markRO +3)) > avROSpace)
      throw ROS::ROSRobinExceptions(ROS::ROSRobinExceptions::FIRMWARE, "Class VPD: VPD::WriteToDataField: "
				                                       "Not enough space in RW field");

  }

  // Finalize the RO field with the Checksum and Reserved Key "RV"
  // Write the two key letters
  dataField[counter] = (char) ChecksumTag[0];
  checksum += dataField[counter++];
  dataField[counter] = (char) ChecksumTag[1];
  checksum += dataField[counter++];
  // The length of this key comprises the checksum and the remaining
  // space in the RO field
  dataField[counter] = (char)(avROSpace - (counter - (markRO + 3)) - 1);
  checksum += dataField[counter++];

  // The checksum is a number which lead to a sum of zero when
  // added to the sum of all data from the start of VPD up to this
  // position
  checksum = 0 - checksum;
  dataField[counter++] = checksum;

  // After the RO The Readwrite field starts. So we mark it
  counter = markRW = markRO + avROSpace + (size_t)3;

  // Write the RW Field tag
  dataField[counter++] = LargeResRWTag;

  // The length is calculated out of the available RW space
  length = avRWSpace;
  dataField[counter++] = (char) (length & 0xFF) >> 8;
  dataField[counter++] = (char) length & 0xFF;

  counter = markRW + 3;

  // Now write the keys and data as done with the RO field before
  for (entryIter = m_readWriteField.begin();
       entryIter < m_readWriteField.end();
       entryIter++) {

    std::string key = entryIter->getKey();
    std::vector<char> data = entryIter->getData();
    size_t keyLength = data.size();
    if (key == "")
      throw ROS::ROSRobinExceptions(ROS::ROSRobinExceptions::FIRMWARE, "Class VPD: VPD::WriteToDataField: "
                                                                       "Key field empty in RW");

    dataField[counter++] = (char) key[0];
    dataField[counter++] = (char) key[1];
    dataField[counter++] = (char) keyLength;

    for (std::vector<char>::iterator dataIter = data.begin();
	 dataIter < data.end();
	 dataIter++) {

      dataField[counter++] = *dataIter;
    }

    if ((counter - (markRW +3)) > avRWSpace)
      throw ROS::ROSRobinExceptions(ROS::ROSRobinExceptions::FIRMWARE, "Class VPD: VPD::WriteToDataField: "
                                                                       "Not enough space in RW field");

  }

  // The RW field finalizes with the Remaining RW Tag which contains
  // the remainig space int this field
  dataField[counter++] = (char) RemainingRWTag[0];
  dataField[counter++] = (char) RemainingRWTag[1];
  dataField[counter++] = (char)(avRWSpace - (counter - (markRW + 3)) - 1);

  counter = markRW + avRWSpace + 3;

  // VPD ends with an end tag
  dataField[counter] = EndTag;

  return counter + 1;

}

/// The Product ID tag
const char VPD::LargeResIDTag = 0x82;
/// The Read Only Field Tag
const char VPD::LargeResROTag = 0x90;
/// The Read Write Field Tag
const char VPD::LargeResRWTag = 0x91;
/// The End Tag
const char VPD::EndTag        = 0x78;

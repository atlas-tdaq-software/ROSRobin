/****************************************************************/
/* file: Rodin_JNI.cpp						*/
/* author: Markus Joos, CERN-PH/ESE				*/
/****** C 2010 - The software with that certain something *******/

#include <jni.h>
#include <stdio.h>
#include <sstream>
#include <string>

#include "DFDebug/DFDebug.h"
#include "ROSEventFragment/ROBFragment.h"
#include "ROSRobin/ROSRobinExceptions.h"
#include "ROSRobin/robindriver_common.h"
#include "ROSMemoryPool/WrapperMemoryPool.h"
#include "ROSRobin/Rodin_JNI.h"
#include "ROSRobin/Robin.h"
#include "ROSRobin/Rol.h"
#include "ROSRobin/robin.h"
#include "ROSRobin/robin_statistics_map.h"

using namespace ROS;

//Constants
#define MAX_ROBIN_CARDS   5
#define MAX_ROLS          15
#define IS_PRESENT        1
#define IS_ABSENT         0

//Macros
#define CONVERT_ENDIAN_16(X) ((Word)(((X) >> 8) + ((X) << 8)))
#define CONVERT_ENDIAN_32(X) ((Dword)( (((X) & 0xff000000) >> 24) | (((X) & 0x00ff0000) >> 8) | (((X) & 0x0000ff00) << 8) | (((X) & 0x000000ff) << 24)))


//Global variables
int robin_nr[MAX_ROBIN_CARDS], rol_nr[MAX_ROLS];
Robin *rorobin[MAX_ROBIN_CARDS];
Rol *prol[MAX_ROLS];
WrapperMemoryPool *m_memoryPool[MAX_ROBIN_CARDS];


/*****************************************************************************************/
JNIEXPORT void JNICALL Java_rodin_RobinJNI_Robin_1open(JNIEnv* env, jclass, jintArray rols)
/*****************************************************************************************/
{
  int card, rol, thiscard, thisrol;
  float timeout = 100.0;
  u_int numberOfOutstandingReq = 1, msgInputMemorySize = 0x20, miscSize = 0x2000, eventSize = 0x1000;
      
  DF::GlobalDebugSettings::setup(5, 14);  
  
  //Initialize the arrays
  for (card = 0; card < MAX_ROBIN_CARDS; card++)
    robin_nr[card] = IS_ABSENT;
  for (rol = 0; rol < MAX_ROLS; rol++)
    rol_nr[rol] = IS_ABSENT;
      
  jsize rollen = env->GetArrayLength(rols);
  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Java_rodin_RobinJNI_Robin_1open: rollen = " << rollen);

  jint *rolbody = env->GetIntArrayElements(rols, 0);
  for (rol = 0; rol < rollen; rol++)
  {
    thisrol = rol;
    thiscard = rol / 3;
    DEBUG_TEXT(DFDB_ROSROBIN, 15, "Java_rodin_RobinJNI_Robin_1open: Card number = " << thiscard << ", Rol number = " << thisrol);

    if(rolbody[rol])
    {
      DEBUG_TEXT(DFDB_ROSROBIN, 15, "Java_rodin_RobinJNI_Robin_1open: This Rol is selected");
      rol_nr[thisrol] = IS_PRESENT;
      robin_nr[thiscard] = IS_PRESENT;
    } 
  }
  env->ReleaseIntArrayElements(rols, rolbody, 0);

  //Open access to the cards
  for (card = 0; card < MAX_ROBIN_CARDS; card++)
  {
    if(robin_nr[card] == IS_PRESENT)
    {
      DEBUG_TEXT(DFDB_ROSROBIN, 15, "Java_rodin_RobinJNI_Robin_1open: Creating ROBIN object for card = " << card);
      try
      {
        rorobin[card] = new Robin(card, timeout);
        rorobin[card]->open();
        rorobin[card]->reserveMsgResources(numberOfOutstandingReq, msgInputMemorySize, miscSize, eventSize); 
        // Create a memoryPool structure around the event buffer
        u_long eventBase       = rorobin[card]->getVirtEventBase();
        u_int physicalAddress = rorobin[card]->getPhysEventBase();
        u_int npages = 1, pageSize = 0x100;
        m_memoryPool[card] = new WrapperMemoryPool(npages, pageSize, eventBase, physicalAddress);
      }
      catch (ROSRobinExceptions& e)
      {
        std::cout << "Exception caught in Java_rodin_RobinJNI_Robin_1open" << std::endl;
        std::cout << e << std::endl;
        return;
      }            
    }
  }
    
  for (rol = 0; rol < MAX_ROLS; rol++)
  {
    if(rol_nr[rol] == IS_PRESENT)
    {
      try
      {
        thiscard = rol / 3;
        thisrol = rol % 3;
        DEBUG_TEXT(DFDB_ROSROBIN, 15, "Java_rodin_RobinJNI_Robin_1open: Creating ROL object for card = " << thiscard << ", rol = " << thisrol);
        prol[rol] = new Rol(*rorobin[thiscard], thisrol);
      }
      catch (ROSRobinExceptions& e)
      {
        std::cout << "Exception caught in Java_rodin_RobinJNI_Robin_1open" << std::endl;
        std::cout << e << std::endl;
        return;
      }            
    }
  }   
}



/***********************************************************************/
JNIEXPORT void JNICALL Java_rodin_RobinJNI_Robin_1close(JNIEnv *, jclass)
/***********************************************************************/
{ 
  int rol, card;

  for (rol = 0; rol < MAX_ROLS; rol++)
  {
    if(rol_nr[rol] == IS_PRESENT)
    {
       DEBUG_TEXT(DFDB_ROSROBIN, 15, "Java_rodin_RobinJNI_Robin_1close: Deleting ROL object for rol = " << rol);
       delete prol[rol];
     }
  }   

  for (card = 0; card < MAX_ROBIN_CARDS; card++)
  {
    if(robin_nr[card] == IS_PRESENT)
    {
      DEBUG_TEXT(DFDB_ROSROBIN, 15, "Java_rodin_RobinJNI_Robin_1close: Deleting ROBIN object for card = " << card);
      rorobin[card]->freeMsgResources(); 
      rorobin[card]->close();
      delete rorobin[card];  
    }
  }
}

  
/**************************************************************************************/
JNIEXPORT jstring JNICALL Java_rodin_RobinJNI_Robin_1ping(JNIEnv *env, jclass, jint rol)
/**************************************************************************************/
{
  char buf[128];

  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Java_rodin_RobinJNI_Robin_1ping: pinging rol = " << rol);
  try
  {
    prol[rol]->ping();
  }
  catch (ROSRobinExceptions& e)
  {
    std::cout << "Exception caught in Java_rodin_RobinJNI_Robin_1ping" << std::endl;
    std::cout << e << std::endl;
    sprintf(buf, "The ROBIN is not alive");
  }
  sprintf(buf, "The ROBIN is alive");
  return env->NewStringUTF(buf);
}       



/****************************************************************************************/
JNIEXPORT jstring JNICALL Java_rodin_RobinJNI_Robin_1status(JNIEnv *env, jclass, jint rol)
/****************************************************************************************/
{
  char buf[100000];
  StatisticsBlock statblock;
  u_int nchars = 0, loop;

  try
  {
    statblock = prol[rol]->getStatistics();
  }
  catch (ROSRobinExceptions& e)
  {
    std::cout << "Exception caught in Java_rodin_RobinJNI_Robin_1status" << std::endl;
    std::cout << e << std::endl;
    sprintf(buf, "Exception caught in Java_rodin_RobinJNI_Robin_1status");
    return env->NewStringUTF(buf);
  }

  if (statblock.statVersion != enum_statVersion)
  {
    nchars += sprintf(&buf[nchars], "statVersion = 0x%08x\n", statblock.statVersion);
    nchars += sprintf(&buf[nchars], "Sorry. I was expecting 0x%08x\n", enum_statVersion);
    nchars += sprintf(&buf[nchars], "serNum                = %d\n", statblock.serNum);
  }
  else
  {      
    nchars += sprintf(&buf[nchars], "statVersion           = 0x%08x\n", statblock.statVersion);
    nchars += sprintf(&buf[nchars], "endianFlag            = 0x%08x\n", statblock.endianFlag);
    nchars += sprintf(&buf[nchars], "statByteSize          = 0x%08x\n", statblock.statByteSize);
    nchars += sprintf(&buf[nchars], "serNum                = %d\n", statblock.serNum);
    nchars += sprintf(&buf[nchars], "swVersion             = 0x%08x\n", statblock.swVersion);
    nchars += sprintf(&buf[nchars], "cfgVersion            = 0x%08x\n", statblock.cfgVersion);
    nchars += sprintf(&buf[nchars], "cfgNumItems           = 0x%08x\n", statblock.cfgNumItems);
    nchars += sprintf(&buf[nchars], "designVersion         = 0x%08x\n", statblock.designVersion);
    nchars += sprintf(&buf[nchars], "fragFormat            = 0x%08x\n", statblock.fragFormat);
    nchars += sprintf(&buf[nchars], "bufByteSize           = 0x%08x\n", statblock.bufByteSize);
    nchars += sprintf(&buf[nchars], "numChannels           = 0x%08x\n", statblock.numChannels);
    nchars += sprintf(&buf[nchars], "cpuSpeed              = %d\n", statblock.cpuSpeed);
    nchars += sprintf(&buf[nchars], "robinModel            = 0x%08x\n", statblock.robinModel);
    nchars += sprintf(&buf[nchars], "appType               = 0x%08x\n", statblock.appType);
    nchars += sprintf(&buf[nchars], "switches              = 0x%08x\n", statblock.switches);
    nchars += sprintf(&buf[nchars], "cDate                 = %s\n", statblock.cDate);           
    nchars += sprintf(&buf[nchars], "cTime                 = %s\n", statblock.cTime); 
    
    // check, eventually convert endianness
    SecSiInfo newSecSi, *secSiPtr;
    Dword *secSiSrc = (Dword*)&statblock.secSi, *secSiDest = (Dword*)&newSecSi;

    for (loop = 0; loop < sizeof(SecSiInfo) / sizeof(Dword);loop++)	
      secSiDest[loop] = CONVERT_ENDIAN_32(secSiSrc[loop]);
    secSiPtr = &newSecSi;
    nchars += sprintf(&buf[nchars], "SECSI magic word      = 0x%08x\n", CONVERT_ENDIAN_32(secSiPtr->magic));
    nchars += sprintf(&buf[nchars], "SECSI version         = %d\n", CONVERT_ENDIAN_16(secSiPtr->secSiVersion));
 
    nchars += sprintf(&buf[nchars], "SECSI ATLAS Part ID   = ");
    nchars += sprintf(&buf[nchars], "%d", static_cast<u_int>(secSiPtr->id.project[0])); 
    nchars += sprintf(&buf[nchars], "%d-", static_cast<u_int>(secSiPtr->id.project[1])); 
    nchars += sprintf(&buf[nchars], "%d-", static_cast<u_int>(secSiPtr->id.system[0])); 
    nchars += sprintf(&buf[nchars], "%d-", static_cast<u_int>(secSiPtr->id.subSystem[0])); 
    nchars += sprintf(&buf[nchars], "%d", static_cast<u_int>(secSiPtr->id.institute[0])); 
    nchars += sprintf(&buf[nchars], "%d", static_cast<u_int>(secSiPtr->id.institute[1])); 
    nchars += sprintf(&buf[nchars], "%d-", static_cast<u_int>(secSiPtr->id.institute[2])); 
    nchars += sprintf(&buf[nchars], "%d-", static_cast<u_int>(secSiPtr->id.prefix[0])); 
    nchars += sprintf(&buf[nchars], "%d", static_cast<u_int>(secSiPtr->id.sernum[0])); 
    nchars += sprintf(&buf[nchars], "%d", static_cast<u_int>(secSiPtr->id.sernum[1])); 
    nchars += sprintf(&buf[nchars], "%d", static_cast<u_int>(secSiPtr->id.sernum[2])); 
    nchars += sprintf(&buf[nchars], "%d", static_cast<u_int>(secSiPtr->id.sernum[3])); 
    nchars += sprintf(&buf[nchars], "%d", static_cast<u_int>(secSiPtr->id.sernum[4])); 
    nchars += sprintf(&buf[nchars], "%d\n", static_cast<u_int>(secSiPtr->id.sernum[5])); 

    nchars += sprintf(&buf[nchars], "SECSI prodDate        = "); 
    nchars += sprintf(&buf[nchars], "%d%d%d%d/%d%d/%d%d\n", static_cast<u_int>(secSiPtr->prodDate[0]),static_cast<u_int>(secSiPtr->prodDate[1]),static_cast<u_int>(secSiPtr->prodDate[2]),
      static_cast<u_int>(secSiPtr->prodDate[3]),static_cast<u_int>(secSiPtr->prodDate[4]),static_cast<u_int>(secSiPtr->prodDate[5]),
      static_cast<u_int>(secSiPtr->prodDate[6]),static_cast<u_int>(secSiPtr->prodDate[7])); 

    nchars += sprintf(&buf[nchars], "SECSI prodDate        = "); 
    nchars += sprintf(&buf[nchars], "%d%d%d%d/%d%d/%d%d\n", static_cast<u_int>(secSiPtr->schemDate[0]),static_cast<u_int>(secSiPtr->schemDate[1]),static_cast<u_int>(secSiPtr->schemDate[2]),
      static_cast<u_int>(secSiPtr->schemDate[3]),static_cast<u_int>(secSiPtr->schemDate[4]),static_cast<u_int>(secSiPtr->schemDate[5]),
      static_cast<u_int>(secSiPtr->schemDate[6]),static_cast<u_int>(secSiPtr->schemDate[7])); 

    nchars += sprintf(&buf[nchars], "SECSI schemId         = %d\n", CONVERT_ENDIAN_32(secSiPtr->schemId));

    nchars += sprintf(&buf[nchars], "SECSI pcbName         = ");
    for(loop = 0; loop < 16; loop++)
      if (secSiPtr->pcbName[loop] != 0)
        nchars += sprintf(&buf[nchars], "%c", secSiPtr->pcbName[loop]);
    nchars += sprintf(&buf[nchars], "\n");

    nchars += sprintf(&buf[nchars], "SECSI pcbId           = ");
    for(loop = 0; loop < 16; loop++)
      if (secSiPtr->pcbId[loop] != 0)
        nchars += sprintf(&buf[nchars], "%c", secSiPtr->pcbId[loop]);
    nchars += sprintf(&buf[nchars], "\n");

    nchars += sprintf(&buf[nchars], "SECSI gerberId        = %d\n", CONVERT_ENDIAN_32(secSiPtr->gerberId));

    nchars += sprintf(&buf[nchars], "SECSI asmName         = ");
    for(loop = 0; loop < 16; loop++)
      if (secSiPtr->asmName[loop] != 0)
        nchars += sprintf(&buf[nchars], "%c", secSiPtr->asmName[loop]);
    nchars += sprintf(&buf[nchars], "\n");

    nchars += sprintf(&buf[nchars], "SECSI asmId           = %d\n", CONVERT_ENDIAN_32(secSiPtr->asmId));
    nchars += sprintf(&buf[nchars], "SECSI bomVersion      = %d\n", CONVERT_ENDIAN_32(secSiPtr->bomVersion));
    nchars += sprintf(&buf[nchars], "pagesFree             = 0x%08x\n", statblock.pagesFree);		
    nchars += sprintf(&buf[nchars], "pagesInUse            = 0x%08x\n", statblock.pagesInUse);	
    nchars += sprintf(&buf[nchars], "idleCount             = 0x%08x\n", statblock.idleCount);        
    nchars += sprintf(&buf[nchars], "errCntSize            = 0x%08x\n", statblock.errCntSize);			
    nchars += sprintf(&buf[nchars], "fragStatSize          = 0x%08x\n", statblock.fragStatSize);		
    nchars += sprintf(&buf[nchars], "pageStatSize          = 0x%08x\n", statblock.pageStatSize);	
    nchars += sprintf(&buf[nchars], "msgStatSize           = 0x%08x\n", statblock.msgStatSize);		
    nchars += sprintf(&buf[nchars], "histoSize             = 0x%08x\n", statblock.histoSize);		
    nchars += sprintf(&buf[nchars], "bistResult            = 0x%08x\n", statblock.bistResult);
    nchars += sprintf(&buf[nchars], "mostRecentId          = 0x%08x\n", statblock.mostRecentId);
    nchars += sprintf(&buf[nchars], "savedRejectedPages    = %d\n", statblock.savedRejectedPages);
    nchars += sprintf(&buf[nchars], "eventLog.logEnabled   = 0x%08x\n", statblock.eventLog.logEnabled);
    nchars += sprintf(&buf[nchars], "eventLog.logSize      = 0x%08x\n", statblock.eventLog.logSize);
    nchars += sprintf(&buf[nchars], "eventLog.current      = 0x%08x\n", statblock.eventLog.current);
    nchars += sprintf(&buf[nchars], "eventLog.overflow     = 0x%08x\n", statblock.eventLog.overflow);
    nchars += sprintf(&buf[nchars], "eventLog.eventArray   = 0x%016lx\n", (u_long)statblock.eventLog.eventArray);
    nchars += sprintf(&buf[nchars], "bufferFull            = 0x%08x\n", statblock.bufferFull);    
    nchars += sprintf(&buf[nchars], "rolXoffStat           = 0x%08x\n", statblock.rolXoffStat);    
    nchars += sprintf(&buf[nchars], "rolDownStat           = 0x%08x\n", statblock.rolDownStat);   
    nchars += sprintf(&buf[nchars], "tempOk                = %d\n", statblock.tempOk);
    
    for(loop = 0; loop < enum_LAST_ERRCNT; loop++)
      nchars += sprintf(&buf[nchars], "errors[%3d](%52s) = 0x%08x\n",loop, robinErrorStrings[loop], statblock.errors[loop]);

    for(loop = 0; loop < enum_LAST_FRAGSTAT; loop++)
      nchars += sprintf(&buf[nchars], "fragStat[%3d](%52s) = 0x%08x\n", loop, robinFragStatStrings[loop], statblock.fragStat[loop]);

    for(loop = 0; loop < enum_LAST_PAGESTAT; loop++)
      nchars += sprintf(&buf[nchars], "pageStat[%3d](%52s) = 0x%08x\n", loop, robinPageStatStrings[loop], statblock.pageStat[loop]);

    for(loop = 0; loop < enum_LAST_MSGSTAT; loop++)
      nchars += sprintf(&buf[nchars], "msgStat[%3d](%52s) = 0x%08x\n", loop, robinMsgStatStrings[loop], statblock.msgStat[loop]);

    for(loop = 0; loop < enum_histEntries; loop++)
      nchars += sprintf(&buf[nchars], "fragHisto[%3d] = 0x%08x\n", loop, statblock.fragHisto[loop]);

    for(loop = 0; loop < enum_histEntries; loop++)
      nchars += sprintf(&buf[nchars], "bufHisto[%3d]  = 0x%08x\n", loop, statblock.bufHisto[loop]);
  }
    
  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Java_rodin_RobinJNI_Robin_1status: nchars = " << nchars);

  return env->NewStringUTF(buf);
}


/****************************************************************************************/
JNIEXPORT jstring JNICALL Java_rodin_RobinJNI_Robin_1config(JNIEnv *env, jclass, jint rol)
/****************************************************************************************/
{
  char buf[10000];
  u_int nchars = 0;
  std::vector <CfgParm> cfg;

  try
  {
    cfg = prol[rol]->getConfig();
  }
  catch (ROSRobinExceptions& e)
  {
    std::cout << "Exception caught in Java_rodin_RobinJNI_Robin_1config" << std::endl;
    std::cout << e << std::endl;
    sprintf(buf, "Exception caught in Java_rodin_RobinJNI_Robin_1config");
    return env->NewStringUTF(buf);
  }
  
  u_int somebad = 0;

  nchars += sprintf(&buf[nchars], "Abbreviations:\n");
  nchars += sprintf(&buf[nchars], "#  = Number of the configuration parameter\n");
  nchars += sprintf(&buf[nchars], "Ex = 1: Expert parameter       0: Standard parameter\n");
  nchars += sprintf(&buf[nchars], "Dy = 1: Dynamic parameter      0: Static parameter\n");
  nchars += sprintf(&buf[nchars], "Bo = 1: Boolean parameter      0: Integer parameter\n");
  nchars += sprintf(&buf[nchars], "Gl = 1: ROBIN-wide parameter   0: ROL-specific parameter\n");
  nchars += sprintf(&buf[nchars], "Fi = 1: Fixed value parameter  0: Modifyable parameter\n");
  nchars += sprintf(&buf[nchars], "In = 1: Requires reinitialisation of buffer management (erase data)\n");
  nchars += sprintf(&buf[nchars], "\n");
  nchars += sprintf(&buf[nchars], "           Name|  #| Ex| Dy| Bo| Gl| Fi| In|      Value|    Default|        Min|        Max|\n");
  nchars += sprintf(&buf[nchars], "===============|===|===|===|===|===|===|===|===========|===========|===========|===========|\n");

  for (u_int loop = 0; loop < enum_cfgNumItems; loop++)
  {
    nchars += sprintf(&buf[nchars], "%15s|", cfg[loop].name);
    nchars += sprintf(&buf[nchars], "%3d|", loop);
    nchars += sprintf(&buf[nchars], "%3d|", static_cast<int>(cfg[loop].expert));
    nchars += sprintf(&buf[nchars], "%3d|", static_cast<int>(cfg[loop].dynamic));
    nchars += sprintf(&buf[nchars], "%3d|", static_cast<int>(cfg[loop].isBool));
    nchars += sprintf(&buf[nchars], "%3d|", static_cast<int>(cfg[loop].global));
    nchars += sprintf(&buf[nchars], "%3d|", static_cast<int>(cfg[loop].fixed));
    nchars += sprintf(&buf[nchars], "%3d|", static_cast<int>(cfg[loop].init));
    nchars += sprintf(&buf[nchars], "%11d|", cfg[loop].value);  
    nchars += sprintf(&buf[nchars], "%11d|", cfg[loop].defVal); 
    nchars += sprintf(&buf[nchars], "%11d|", cfg[loop].minVal);
    nchars += sprintf(&buf[nchars], "%11d|\n", cfg[loop].maxVal);

    u_int dw = cfg[loop].defVal;
    u_int cw = cfg[loop].value;
    u_int ex = static_cast<int>(cfg[loop].expert);
    u_int fi = static_cast<int>(cfg[loop].fixed);

    if((loop != enum_cfgTempAlarmValue) && (cw != dw) && ex && !fi)
      somebad = 1;
  }

  if (somebad)
  {
    nchars += sprintf(&buf[nchars], "\n");
    nchars += sprintf(&buf[nchars], "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
    nchars += sprintf(&buf[nchars], "Some Expert parameters do not have the default value.\n");
    nchars += sprintf(&buf[nchars], "You may want to check this!\n");

    for (u_int loop = 0; loop < enum_cfgNumItems; loop++)
    {
      u_int dw = cfg[loop].defVal;
      u_int cw = cfg[loop].value;
      u_int ex = static_cast<int>(cfg[loop].expert);
      u_int fi = static_cast<int>(cfg[loop].fixed);
    
      if ((loop != enum_cfgTempAlarmValue) && !fi && (cw != dw) && ex)
        nchars += sprintf(&buf[nchars], "Parameter %s is %d instead of %d\n", cfg[loop].name, cfg[loop].value, cfg[loop].defVal);
    }
    nchars += sprintf(&buf[nchars], "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
  }
  return env->NewStringUTF(buf);
}


/****************************************************************************************/
JNIEXPORT jstring JNICALL Java_rodin_RobinJNI_Robin_1events(JNIEnv *env, jclass, jint rol)
/****************************************************************************************/
{
  char buf[10000];
  std::vector<Rol::L1idRange> idlist;
  u_int nchars = 0;

  try
  {
    idlist = prol[rol]->getL1idList();
  }
  catch (ROSRobinExceptions& e)
  {
    sprintf(buf, "Exception thrown by getL1idList\n");
    return env->NewStringUTF(buf);
  }

  u_int totnum = 0;
  for (std::vector<Rol::L1idRange>::const_iterator it2 = idlist.begin(); it2 != idlist.end(); it2++)
  {
    Rol::L1idRange pair = *it2;
    if (pair.first == pair.last)
    {
      nchars += sprintf(&buf[nchars], "L1ID = 0x%08x\n", pair.first);
      if (pair.first != 0xffffffff)
	totnum++;
    }
    else
    {
      nchars += sprintf(&buf[nchars], "First L1ID = 0x%08x,  Last L1ID = 0x%08x  (%d events)\n",pair.first,pair.last, pair.last - pair.first + 1);
      totnum += pair.last - pair.first + 1;
    }
  }
  nchars += sprintf(&buf[nchars], "There are currently %d events in memory\n", totnum);

  return env->NewStringUTF(buf);
}

/*******************************************************************************************************************/
JNIEXPORT jstring JNICALL Java_rodin_RobinJNI_Robin_1setconfig(JNIEnv *env, jclass, jint rol, jint index, jint value)
/*******************************************************************************************************************/
{
  char buf[100];

  try
  {
    prol[rol]->setConfig(index, value);
  } 
  catch(std::exception& e)
  {
    sprintf(buf, "Failed to set parameter\n");
    return env->NewStringUTF(buf);
  }
  sprintf(buf, "Parameter set OK\n");
  return env->NewStringUTF(buf);
}


/********************************************************************************************************/
JNIEXPORT jstring JNICALL Java_rodin_RobinJNI_Robin_1getfragment(JNIEnv *env, jclass, jint rol, jint l1id)
/********************************************************************************************************/
{
  char *bptr, buf[10000];
  int loop, thiscard = rol / 3;
  MemoryPage *mem_page = NULL;
  u_int ticket, *fptr;
  ROBFragment* robFragment;

  DEBUG_TEXT(DFDB_ROSROBIN, 6, "Java_rodin_RobinJNI_Robin_1getfragment: rol = " << rol << ", l1id = " << l1id);

  try
  {
    mem_page = m_memoryPool[thiscard]->getPage();
  } 
  catch(std::exception& e)
  {
    sprintf(buf, "Failed to get mem_page\n");
    return env->NewStringUTF(buf);
  } 

  // Check if the allocated page is OK
  if (mem_page == 0) 
  {						
    sprintf(buf, "get mem_page is NULL\n");
    return env->NewStringUTF(buf);
  }

  u_int *fragBufferAddress = static_cast<u_int *> (mem_page->reserve(mem_page->capacity()));
  try
  {
    ticket = prol[rol]->requestFragment(l1id, reinterpret_cast<u_long> (fragBufferAddress));
    fptr = prol[rol]->getFragment(ticket);      
    DEBUG_TEXT(DFDB_ROSROBIN, 6, "Java_rodin_RobinJNI_Robin_1getfragment: fragment received");
  }
  catch (ROSRobinExceptions& e)
  {
    sprintf(buf, "ROSRobinException caught\n");
    return env->NewStringUTF(buf);
  }
  
  robFragment = new ROBFragment(mem_page);
  std::ostringstream output;
  robFragment->print(output);

  //output.str() is a C++ string and output.str().c_str() gives a C style string.  
  DEBUG_TEXT(DFDB_ROSROBIN, 6, "Printing ROB fragment: " << output.str().c_str());

  std::string s_str = output.str();
  bptr = (char *)s_str.c_str(); 
  
  for(loop = 0; loop < 10000; loop++)
  {
    buf[loop] = bptr[loop];
    if (loop < 10)
      DEBUG_TEXT(DFDB_ROSROBIN, 6, "char = " << bptr[loop] << ", hex=" << HEX((int)bptr[loop]));
    if (bptr[loop] == 0)
      break;
  }
  
  delete robFragment;
  mem_page->free();
  return env->NewStringUTF(buf);
}


/******************************************************************************************/
JNIEXPORT jstring JNICALL Java_rodin_RobinJNI_Robin_1resetrol(JNIEnv *env, jclass, jint rol)
/******************************************************************************************/
{
  char buf[100];      
  
  try
  {
    prol[rol]->reset();
  } 
  catch(std::exception& e)
  {
    sprintf(buf, "Failed to reset ROL\n");
    return env->NewStringUTF(buf);
  }
  sprintf(buf, "ROL reset\n");

  return env->NewStringUTF(buf);
}


/************************************************************************************************/
JNIEXPORT jstring JNICALL Java_rodin_RobinJNI_Robin_1gettemperature(JNIEnv *env, jclass, jint rol)
/************************************************************************************************/
{
  char buf[100];      
  u_int temp;
  
  try
  {
    temp = prol[rol]->getTemperature();
  } 
  catch(std::exception& e)
  {
    sprintf(buf, "Failed to read temperature\n");
    return env->NewStringUTF(buf);
  }
  sprintf(buf, "The temperature of the Robin is about %d deg. C\n", temp);

  return env->NewStringUTF(buf);
}


/*******************************************************************************************/
JNIEXPORT jstring JNICALL Java_rodin_RobinJNI_Robin_1clearstat(JNIEnv *env, jclass, jint rol)
/*******************************************************************************************/
{
  char buf[100];      
  
  try
  {
    prol[rol]->clearStatistics();
  } 
  catch(std::exception& e)
  {
    sprintf(buf, "Failed to clear statistics\n");
    return env->NewStringUTF(buf);
  }
  sprintf(buf, "Statistics data cleared\n");

  return env->NewStringUTF(buf);
}










// --------x----------------------------x-----------------------------------x--
/**
 ** \file   XilinxVirtexIIFpga.cpp
 **
 ** \author
 **
 ** \date
 **
 */
// --------x----------------------------x-----------------------------------x--


/* $Id$
 * $Revision$
 * $Log$
 * Revision 1.9  2005/12/13 16:28:55  akugel
 * FPGA clear corrected
 *
 * Revision 1.8  2005/09/12 13:33:29  akugel
 * FPGA erase (clear) function improved by adding check for DONE signal after PROG pulse
 *
 * Revision 1.7  2005/07/28 21:19:32  akugel
 * file format corrected (duplicated linefeeds)
 *
 * Revision 1.6  2005/07/28 16:09:21  akugel
 * JTAG configuration proceedure modified by adding a JPROG_B command
 * prior to sending the bitfile. After termination of JPROG_B we wait
 * for completion of FPGA initialsation. This ensures proper re-configuration
 * of the FPGA
 *
 * Revision 1.5  2005/06/28 14:38:16  mmueller
 * Fixed a bug in the FPGA firmware bitstream reading algorithm.
 *
 * Revision 1.4  2005/06/22 16:13:59  joos
 * bug fixes and other changes
 *
 * Revision 1.3  2005/05/04 09:23:13  mmueller
 * Initial implementation. Supports to test if the chip has been programmed
 * and is able to program a configuration.
 *
 * Revision 1.2  2005/04/11 11:24:16  joos
 * Modifications and new files of Matthias
 *
 * Revision 1.1.1.1  2005/04/07 11:34:38  joos
 * added ROSRobin
 *
 *
 */

/**
 * Description:
 *
 */

#include "ROSRobin/XilinxVirtexIIFpga.h"
#include "ROSRobin/Firmware.h"
#include "ROSRobin/JTagDevice.h"
#include "ROSRobin/JTagController.h"
#include "ROSRobin/BitStream.h"
#include "ROSRobin/ROSRobinExceptions.h"
#include <time.h>

//***************************************************************************
ROS::XilinxVirtexIIFpga::XilinxVirtexIIFpga(JTagController & jtagController) : 
//***************************************************************************
  JTagDevice(jtagController, 6)

{

  addInstruction("IDCODE", ROS::BitStream("100100"));
  addInstruction("JSTART", ROS::BitStream("001100"));
  addInstruction("JSHUTDOWN", ROS::BitStream("101100"));
  addInstruction("CFG_IN", ROS::BitStream("101000"));
  addInstruction("JPROG_B", ROS::BitStream("110100"));

  
}

//*********************************************
ROS::XilinxVirtexIIFpga::~XilinxVirtexIIFpga() 
//*********************************************
{

}


//**************************************
void ROS::XilinxVirtexIIFpga::program(std::string firmwareFile) 
//**************************************
{

  ROS::Firmware firmware(firmwareFile);

  // ---------------------------------
  // Issue JPROG_B command first, to get rid of previous configuration
  clear();
  std::cout << "FPGA erased" << std::endl;

  // --------------------------------
  setIR("CFG_IN");
  
  ROS::BitStream resetCRC("1111111111111111111111111111111110101010100110010101010101100110"
			  "0011000000000000100000000000000100000000000000000000000000000111"
			  "0000000000000000000000000000000000000000000000000000000000000000");

  setDR(resetCRC);

  getJTagController().loadIrAllDevices();
  getJTagController().loadDrAllDevices();

  setIR("JSHUTDOWN");
  getJTagController().loadIrAllDevices();
  unsigned int i;
  for (i = 0; i < 13; i++) {
    getJTagController().gotoState(ROS::JTagController::TAPIdle);
  }
 
  getJTagController().gotoState(ROS::JTagController::TAPReset);

  ROS::BitStream extendedBitstream("00110000000000001000000000000001"
				   "00000000000000000000000000001000");

  extendedBitstream.append(firmware.getBitstream());

//  std::cout << firmware.getBitstream() << std::endl;
      
  setIR("CFG_IN");      
  setDR(extendedBitstream);

  getJTagController().loadIrAllDevices();
  getJTagController().loadDrAllDevices();

  setIR("JSTART");
  getJTagController().loadIrAllDevices();

  for (i = 0; i < 20; i++) {
    getJTagController().gotoState(ROS::JTagController::TAPIdle);
  }

  setIR("BYPASS");
  getJTagController().loadIrAllDevices();

  setIR("JSTART");
  getJTagController().loadIrAllDevices();

  for (i = 0; i < 20; i++) {
    getJTagController().gotoState(ROS::JTagController::TAPIdle);
  }

  setIR("BYPASS");
  getJTagController().loadIrAllDevices();

  getJTagController().gotoState(ROS::JTagController::TAPReset);

}

//*******************************************
bool ROS::XilinxVirtexIIFpga::isProgrammed() 
//*******************************************

{

  setIR("BYPASS");
  getJTagController().loadIrAllDevices();

  ROS::BitStream returnIr = getIR();

  if (returnIr.getAt(5) == Bit('0'))
    return false;

  return true;
}

//*******************************************
bool ROS::XilinxVirtexIIFpga::isInitialised() 
//*******************************************

{

  setIR("BYPASS");
  getJTagController().loadIrAllDevices();

  ROS::BitStream returnIr = getIR();

  if (returnIr.getAt(4) == Bit('0'))
    return false;

  return true;
}


//***********************************************
ROS::BitStream ROS::XilinxVirtexIIFpga::getDeviceId() 
//***********************************************
{

  setIR("IDCODE");
  setDR(ROS::BitStream("1111111111111111"
		  "1111111111111111"));
  
  getJTagController().loadIrAllDevices();
  getJTagController().loadDrAllDevices();

  return getDR();

}


//*******************************
void ROS::XilinxVirtexIIFpga::clear() 
//*******************************
{
  time_t t0, deadLine;
  // Issue JPROG_B command first, to get rid of previous configuration
  setIR("JPROG_B");
  getJTagController().loadIrAllDevices();

  // verify FPGA is in erased state now
  if (true == isProgrammed()){
    throw ROSRobinExceptions(ROSRobinExceptions::TIMEOUT,"XilinxVirtexIIFpga: DONE not low after PROG pulse");
    }
  
  // the timeout should be set to ~5s
  t0 = time(NULL);
  deadLine = t0 + 5;  // max wait time is 5 s
  while (time(NULL) < deadLine){
    if (true == isInitialised()){
      return;
    }
  }
  throw ROSRobinExceptions(ROSRobinExceptions::TIMEOUT,"XilinxVirtexIIFpga: INIT not high after PROG pulse");
}


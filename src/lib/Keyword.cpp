// --------x----------------------------x-----------------------------------x--
/**
 ** \file   Keyword.cpp
 **
 ** \author
 **
 ** \date
 **
 */
// --------x----------------------------x-----------------------------------x--


/* $Id$
 * $Revision$
 * $Log$
 * Revision 1.1  2005/05/17 09:11:11  mmueller
 * Added Files with code for handling PLX Eeprom contents
 *
 *
 *
 */

/**
 * Description:
 *
 */

#include<string>
#include<iostream>
#include "ROSRobin/Keyword.h"
#include "ROSRobin/ROSRobinExceptions.h"

//*************************************
Keyword::Keyword(std::string keyword) :
//*************************************
  m_keyword (keyword)
{

}


//*************************************
std::string Keyword::getKeyword(void) 
//*************************************
{
  return m_keyword;
}

//***********************************************************
std::istream &  operator >> (std::istream & is, Keyword key) 
//***********************************************************
{
  
  std::string value;

  do {
    is >> value;
  }
  while ((value != key.getKeyword()) && (is));

  return is;
}

//********************************************
unsigned int decStringToUInt(std::string str) 
//********************************************
{

  unsigned int retValue = 0, i;

  for (i = 0; i < str.length(); i++) {
    
    if ((str[i] < '0') || (str[i] > '9')) {

      throw ROS::ROSRobinExceptions(ROS::ROSRobinExceptions::FIRMWARE, 
				    "Wrong character in string to int conversion");

    }

    retValue *= 10;
    retValue += static_cast<unsigned int>(str[i] - '0');

  }

  return retValue;

}


//********************************************
unsigned int hexStringToUInt(std::string str) 
//********************************************
{

  unsigned int retValue = 0, i;

  for (i = 0; i < str.length(); i++) {
    
    if (((str[i] < '0') || (str[i] > '9')) && 
	((str[i] < 'a') || (str[i] > 'f')) &&
	((str[i] < 'A') || (str[i] > 'F'))) {

      throw ROS::ROSRobinExceptions(ROS::ROSRobinExceptions::FIRMWARE, 
				    "Wrong character in string to int conversion");

    }

    retValue *= 16;
    if ((str[i] >= '0') && (str[i] <= '9'))
      retValue += static_cast<unsigned int>(str[i] - '0');
    else if ((str[i] >= 'a') && (str[i] <= 'f'))
      retValue += static_cast<unsigned int>(str[i] - 'a') + 10;
    else if ((str[i] >= 'A') && (str[i] <= 'F'))
      retValue += static_cast<unsigned int>(str[i] - 'A') + 10;
  }

  return retValue;

}

// --------X----------------------------x-----------------------------------x--
/**
 ** \file Firmware.cpp
 **
 ** \author
 **
 ** \date
 **
 */
// --------x----------------------------x-----------------------------------x--


/* $Id$
 * $Revision$
 * $Log$
 * Revision 1.7  2008/12/03 10:16:08  joos
 * (some) compiler warnings fixed
 *
 * Revision 1.6  2005/06/28 14:38:16  mmueller
 * Fixed a bug in the FPGA firmware bitstream reading algorithm.
 *
 * Revision 1.4  2005/05/04 09:24:12  mmueller
 * Beautyfication.
 *
 * Revision 1.3  2005/04/26 15:51:32  mmueller
 * Implemented a bit file parser. Needs to be tested.
 *
 * Revision 1.2  2005/04/11 11:24:16  joos
 * Modifications and new files of Matthias
 *
 * Revision 1.1.1.1  2005/04/07 11:34:38  joos
 * added ROSRobin
 *
 *
 */

/**
 * Description:
 *
 */


#include <iostream>
#include <fstream>
#include <string>

#include "ROSRobin/Firmware.h"
#include "ROSRobin/BitStream.h"
#include "ROSRobin/ROSRobinExceptions.h"


const unsigned int ROS::Firmware::c_maxBitFileFieldLength = 1000;

//****************************
ROS::Firmware::Firmware(void) :
//****************************
  m_format(UNKNOWN),
  m_designName("unkown"),
  m_chipType("unkown"),
  m_date("unkown"),
  m_time("unkown"),
  m_firmwareData()
{
}


//*********************************************
ROS::Firmware::Firmware(std::string fileName) :
//*********************************************
  m_format(UNKNOWN),
  m_designName("unkown"),
  m_chipType("unkown"),
  m_date("unkown"),
  m_time("unkown"),
  m_firmwareData()
{
  
  readFromFile(fileName);
  
}


//*****************************
ROS::Firmware::~Firmware(void) 
//*****************************
{

}



//*****************************************************
void ROS::Firmware::readFromFile(std::string fileName) 
//*****************************************************
{

  std::cout << "Looking for firmware file: " << fileName << std::endl;

  // First check the file format
  unsigned int dotPosition = fileName.rfind(".");
  std::string suffix = fileName.substr(dotPosition + 1, fileName.size() - dotPosition - 1);

  std::cout << "Suffix is: " << suffix << std::endl;

  // Try to open the file
  std::ifstream firmwareFile;
  firmwareFile.open(fileName.c_str());

  // If there is something wrong with the file throw an exception
  if (!firmwareFile) {
    throw ROSRobinExceptions(ROS::ROSRobinExceptions::FIRMWARE, 
			     "Could not open file!");
  }

  // Now look for the name of the file extension and
  // launch the assigned reader.
  if ( suffix == "rbt" ) {
    readFromRbtFile(firmwareFile);
  }
  else
    if (suffix == "bit" ) {
      readFromBitFile(firmwareFile);
    }
    else
      if ( suffix == "xsvf" ) {
	readFromXsvfFile(firmwareFile);
      }
      else {
	firmwareFile.close();
	throw ROSRobinExceptions(ROS::ROSRobinExceptions::FIRMWARE, 
				 "Unknown file format");
      }
  
  // Finally close the file
  firmwareFile.close();

}

//************************************************
ROS::BitStream ROS::Firmware::getBitstream(void) 
//************************************************
{
  
  return m_firmwareData;

}



// Reads the firmware data from a bit file
//********************************************************
void ROS::Firmware::readFromBitFile(std::istream &  is) 
//********************************************************
{ 

  char keyChar;

  // Look for the starting point
  is.ignore(0x100, 'a');

  keyChar = 'a';


  // Now read in the fields

  do {
    //Read the length of the field    
    unsigned int length;
    unsigned char l[4];
    
    // Check if we have a valid key character
    if ( (keyChar < 'a') || (keyChar > 'e') ) {
      throw ROSRobinExceptions(ROS::ROSRobinExceptions::FIRMWARE, 
			       "Bit file reader error. Error in file format");      
    }

    // The key char 'e' introduces the design bitstream. It requires 4 length bytes!
    if ( keyChar == 'e' ) {
      is.read(reinterpret_cast<char *>(l),4);

      length = (unsigned int ) l[0];
      length = length << 8;
      length |= (unsigned int ) l[1];
      length = length << 8;
      length |= (unsigned int ) l[2];
      length = length << 8;
      length |= l[3];
    }
    else {
      is.read(reinterpret_cast<char *>(l),2);
      length = (unsigned int ) l[0];
      length = length << 8;
      length |= (unsigned int ) l[1];
    }
    
    // Now reserve some memory to read the data from the file
    unsigned char *buffer = new unsigned char[length + 1];
    
    // And read the field
    is.read(reinterpret_cast<char *>(buffer), length);

    // Make the field char interpretation. Store the information inside the fields
    // All fields except the bitstream are plain test.
    if (keyChar == 'a') {
      // This one is the name of the ncd file during design generation.
      m_designName = reinterpret_cast<char *>(buffer);
    }
    else if (keyChar == 'b') {
      // This is the type of the chip.
      m_chipType = reinterpret_cast<char *>(buffer);
    }
    else if (keyChar == 'c') {
      // File generation date
      m_date = reinterpret_cast<char *>(buffer);
    }
    else if (keyChar == 'd') {
      // File generation time
      m_time = reinterpret_cast<char *>(buffer);
    }
    else if (keyChar == 'e') {
          
      // Allocate space for a 32 bit raw data buffer which has to be used to
      // provide the bitstream to the BitStream object.
      unsigned int *rawData = new unsigned int[(length / 4) + 1];

      // Copy the data. The data in the bit file needs to be sorted into the 
      // data words which go into the bitstream. Bit 8 of each byte in the file
      // is the first bit to be shifted into the FPGA.
      unsigned int i;
      unsigned int wordMask;
      // Rund through the bitstream information from the file.
      for (i = 0; i < length; i++) {
	unsigned int wordIndex = i / 4;
	
	// If we are at a 32 bit boundary we jump to the next rawData array item.
	if ( (i % 4) == 0 ) {
	  rawData[wordIndex] = 0;
	  wordMask = 0x1;
	}

	// Now we take each byte from the file and loop over each bit beginning from the
	// first one to be shifted. This is Bit 8!
	unsigned int j;
	unsigned char byteFileDataMask = 0x80;
	for (j = 0; j < 8; j++) {	  

	  // If the current bit is one, or it into the rawData array word.
	  // There the first bit is the LSB
	  if ((buffer[i] & byteFileDataMask) != 0) {
	    rawData[wordIndex] |= wordMask;
	  }
	  wordMask = wordMask << 1;
	  byteFileDataMask = byteFileDataMask >> 1;
	}
  
      }

      // The bit data
      m_firmwareData.set(rawData, length * 8);
      
      //      std::cout << "Data: " << m_firmwareData << std::endl;
      
      delete[] rawData;

      
    }
    
    // Read the next key character
    is.read(&keyChar, 1);
    
    // Free the memory for the buffer
    delete[] buffer;
    
  }
  while ( !is.eof() );
  
  std::cout << "Design name: " << m_designName << std::endl;
  std::cout << "Chip type:   " << m_chipType << std::endl;
  std::cout << "Date:        " << m_date << std::endl;
  std::cout << "Time:        " << m_time << std::endl;

}


// Read the firmware data from a xsvf file
//*********************************************************
void ROS::Firmware::readFromXsvfFile(std::istream &  /*is*/) 
//*********************************************************
{
  
  throw ROSRobinExceptions(ROS::ROSRobinExceptions::FIRMWARE, 
			   "Unknown file format");

}

// Read the firmware data from a rbt file
//*********************************************************
void ROS::Firmware::readFromRbtFile(std::istream &  /*is*/) 
//*********************************************************
{

  throw ROSRobinExceptions(ROS::ROSRobinExceptions::FIRMWARE, 
			   "Unknown file format");

}


// --------x----------------------------x-----------------------------------x--
/**
 ** \file   JTagDevice.cpp
 **
 ** \author  M. Mueller
 **
 ** \date
 **
 */
// --------x----------------------------x-----------------------------------x--

#include <map>
#include <string>
#include "DFDebug/DFDebug.h"
#include "ROSRobin/BitStream.h"
#include "ROSRobin/JTagController.h"
#include "ROSRobin/JTagDevice.h"
#include "ROSRobin/ROSRobinExceptions.h"

//****************************************************************************************
ROS::JTagDevice::JTagDevice(ROS::JTagController & jtagController, unsigned int IRLength) :
//****************************************************************************************
    m_jtagController(jtagController),
    m_isInChain(false) 
{
  ROS::BitStream bypassCmd;
  unsigned int i;

  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Entering constructor JTagDevice::JTagDevice");
  DEBUG_TEXT(DFDB_ROSROBIN, 20,"IRLength: "<< IRLength);

  for (i = 0; i < IRLength; i++) 
    bypassCmd.append(Bit('1'));

  ROS::BitStream extestCmd;
  for (i = 0; i < IRLength; i++) 
    extestCmd.append(Bit('0'));

  addInstruction("BYPASS", bypassCmd);
  addInstruction("EXTEST", extestCmd);
  
  m_nextIrContent = bypassCmd;
  m_nextDrContent = BitStream("1");

  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Leaving constructor JTagDevice::JTagDevice");
}


//****************************
ROS::JTagDevice::~JTagDevice()
//****************************
{
}


//*******************************************
void ROS::JTagDevice::setIR(std::string name)
//*******************************************
{
  m_nextIrContent = getInstruction(name);
}


//*****************************************
ROS::BitStream ROS::JTagDevice::getIR(void)
//*****************************************
{
  return m_irContent;
}


//**************************************************
void ROS::JTagDevice::setDR(ROS::BitStream dataBits)
//**************************************************
{ 
  m_nextDrContent = dataBits;
}


//*****************************************
ROS::BitStream ROS::JTagDevice::getDR(void)
//*****************************************
{
  return m_drContent;
}


//*************************************************************
ROS::JTagController &  ROS::JTagDevice::getJTagController(void)
//*************************************************************
{
  return m_jtagController;
}


//***********************************************************************
void ROS::JTagDevice::addInstruction(std::string name, BitStream cmdBits)
//***********************************************************************
{
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Adding Instruction " << name << ": " << cmdBits);
  m_instructionPool[name] = cmdBits;
}


//**************************************************************
ROS::BitStream ROS::JTagDevice::getInstruction(std::string name)
//**************************************************************
{
  std::map<std::string, ROS::BitStream>::iterator commandPtr =  m_instructionPool.find(name);

  if ( commandPtr == m_instructionPool.end() ) 
    throw ROSRobinExceptions(ROS::ROSRobinExceptions::JTAG, "JTAG command unknown");

  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Instruction: " << commandPtr->second);
  return commandPtr->second;
}


//*********************************************
ROS::BitStream ROS::JTagDevice::getNextIr(void) 
//*********************************************
{
  return m_nextIrContent;
}


//*********************************************
ROS::BitStream ROS::JTagDevice::getNextDr(void) 
//*********************************************
{
  return m_nextDrContent;
}


//***************************************************************
void ROS::JTagDevice::setIrReturnContent(ROS::BitStream &  value) 
//***************************************************************
{
  m_irContent = value;
}


//***************************************************************
void ROS::JTagDevice::setDrReturnContent(ROS::BitStream &  value) 
//***************************************************************
{
  m_drContent = value;
}


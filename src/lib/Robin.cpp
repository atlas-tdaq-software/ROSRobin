/****************************************************************/
/* file: Robin.cpp						*/
/* author: Markus Joos, CERN-PH/ESS				*/
/****** C 2006 - The software with that certain something *******/

#ifndef HOST
  #define HOST
#endif
 
#include <iostream>
#include <new>           // This forces new to throw an execption if no memory could be allocated instead if returning 0
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "DFDebug/DFDebug.h"
#include "DFThreads/DFThread.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "rcc_error/rcc_error.h"
#include "rcc_time_stamp/tstamp.h"
#include "cmem_rcc/cmem_rcc.h"
#include "ROSRobin/ROSRobinExceptions.h"
#include "ROSRobin/robindriver_common.h"
#include "ROSRobin/Robin.h"
#include "ROSRobin/Rol.h"
#include "ROSRobin/robin.h"

using namespace ROS;


/*******************************************/
Robin::Robin(u_int slotNumber, float timeout) 
/*******************************************/
  : m_is_open(0),
    m_slot_number(slotNumber),
    m_cmem_handle(0),
    m_nFifoSlots(0),
    m_setmemok(0),
    m_reservememok(0),
    m_dev_handle(0),
    m_timeout(timeout),
    m_dpm_oldest_index(0),
    m_dpm_current_index(0),
    m_mem_offset(0),
    m_dpm_offset(0),
    m_pending(0)
{
  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::constructor: Called for card " << slotNumber);
      
  m_rolVector[0] = m_rolVector[1] = m_rolVector[2] = 0;

  for(u_int loop = 0; loop < s_max_msgs; loop++)
    m_dpm_start[loop] = 0;

  // Now create a mutex to protect the messages for this RobIn. The name of this
  // Mutex must be unique. So we use the slot number inside the Mutex name

  std::stringstream mutexName;
  mutexName << "RobinMsgMutex_" << slotNumber << std::ends;
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::constructor: Creating FIFO & DPM mutex " << mutexName.str().c_str());
  m_messageLock = DFFastMutex::Create(const_cast<char *>(mutexName.str().c_str()));

  std::stringstream mutexName2;
  // We need a separate mutex to protect the allocation of misc. memory
  mutexName2 << "RobinMemMutex_" << slotNumber << std::ends;
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::constructor: Creating Misc. MEM mutex " << mutexName2.str().c_str());
  m_miscmemLock = DFFastMutex::Create(const_cast<char *>(mutexName2.str().c_str()));

  std::stringstream mutexName3;
  mutexName3 << "RobinSendMutex_" << slotNumber << std::ends;
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::constructor: Creating send mutex " << mutexName3.str().c_str());
  m_sendmutex = DFFastMutex::Create(const_cast<char *>(mutexName3.str().c_str()));

  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::constructor: OK");
}


/*************/
Robin::~Robin() 
/*************/
{
  //Check if close() has already been called. If not do it. Usually the user should call close()
  //Calling close() from the destructor should only happen as a reaction to an exception
  if (m_is_open)
  {
    m_is_open = 1;  //In case it had been called several times
    close();
  }
  
  m_messageLock->destroy();
  m_miscmemLock->destroy();
  m_sendmutex->destroy();
}


/********************/
void Robin::open(void)
/********************/
{
  //This method will only be called from one defined place. No mutexes required 
  int ret;
  T_link_params link_params;

  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::open: Start of function");
  
  //we need to open the package only once
  if (m_is_open)
  {
    m_is_open++;                                  //keep track of multiple open calls
    DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::open: called " << m_is_open << " times");
    return;
  }

  m_dev_handle = ::open("/dev/robin", O_RDWR, 0);
  if (m_dev_handle < 0)
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::open: Failed to open /dev/robin");
    CREATE_ROS_EXCEPTION(ex4, ROSRobinExceptions, OPEN, "");
    throw (ex4);
  }
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::open: m_dev_handle = " << m_dev_handle);

  ret = CMEM_Open();
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::open: Error from CMEM_Open");
    char m_rcc_err_str[256];
    rcc_error_string(m_rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex5, ROSRobinExceptions, CMEM, m_rcc_err_str);
    throw (ex5);
  }
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::open: CMEM_Open OK");
  
  ret = ts_open(1, TS_DUMMY);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::open: Error from ts_open");
    char m_rcc_err_str[256];
    rcc_error_string(m_rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex6, ROSRobinExceptions, TSTAMPE, m_rcc_err_str);
    throw (ex6);
  }    

  link_params.slot = m_slot_number;
  //Get the PCI addresses of the ROBIN register spaces
  ret = ioctl(m_dev_handle, LINK, &link_params);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::open: Error from ioctl, errno = " << errno);
    char err_str[256];
    sprintf(err_str, "\nThis error came from ioctl(LINK). %s\n", ioctl_strings[errno]);
    CREATE_ROS_EXCEPTION(ex7, ROSRobinExceptions, IOCTL, "This error came from ioctl(LINK). " << ioctl_strings[errno]);
    throw (ex7);
  }
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::open: link_params.plx_base  = 0x" << HEX(link_params.plx_base));
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::open: link_params.plx_size  = 0x" << HEX(link_params.plx_size));
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::open: link_params.fpga_base = 0x" << HEX(link_params.fpga_base));
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::open: link_params.fpga_size = 0x" << HEX(link_params.fpga_size));
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::open: link_params.nrols     = " << link_params.nrols);

  if (!link_params.plx_base || !link_params.fpga_base)
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::open: The driver did not return valid PCI addresses for the PLX and FPGA");
    CREATE_ROS_EXCEPTION(ex8, ROSRobinExceptions, NOPCI, "");
    throw (ex8);
  }

  //Map the PCI base addresses into the virtual address space
  plx_win.user_phys_base = link_params.plx_base;
  plx_win.user_size      = link_params.plx_size;
  mapmem(&plx_win);                                        
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::open: PLX window mapped");

  fpga_win.user_phys_base = link_params.fpga_base;
  fpga_win.user_size      = link_params.fpga_size;
  mapmem(&fpga_win);      
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::open: FPGA window mapped");
      
  m_is_open = 1;

  //Instantiate the ROL objects 
  m_n_rols = link_params.nrols;
  for (u_int loop = 0; loop < m_n_rols; loop++)
  {
     if (m_rolVector[loop] == 0) 
     {
        DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::open: Instantiating Rol object #" << loop);
        m_rolVector[loop] = newRol(loop);
     }
  }

  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::open: End of function");
}


/******************************/
Rol* Robin::newRol(u_int number)
/******************************/
{
  return new Rol(*this, number);
}


/*********************/
void Robin::close(void) 
/*********************/
{
  //This method will only be called from one defined place. No mutexes required 
  int ret;
  
  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::close: Start of function");

  if (!m_is_open) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::close: The library has not yet been opened");
    CREATE_ROS_EXCEPTION(ex9, ROSRobinExceptions, NOTOPEN, "");
    throw (ex9);
  }

  if (m_is_open > 1)
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::close: m_is_open = " << m_is_open);
    m_is_open--;
  }
  else
  {
    m_is_open = 0;
      
    // Delete the Rol objects of this Robin.
    for (u_int i = 0; i < m_n_rols; i++) 
    {
      if (m_rolVector[i] != 0) 
      {
        DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::close: Deleting ROL " << i);
	delete m_rolVector[i];
	m_rolVector[i] = 0;
      }
    }
    
    //return the memory mappings  
    unmapmem(&plx_win);
    unmapmem(&fpga_win);

    ret = CMEM_Close();
    if (ret)
    {
      DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::close: Error from CMEM_Close");
      
      char m_rcc_err_str[256];
      rcc_error_string(m_rcc_err_str, ret);
      CREATE_ROS_EXCEPTION(ex11, ROSRobinExceptions, CMEM, m_rcc_err_str);
      ers::warning(ex11);
    }

    ret = ::close(m_dev_handle);
    if (ret < 0)
    {
      CREATE_ROS_EXCEPTION(ex12, ROSRobinExceptions, CLOSE, "Error from call to <::close(m_dev_handle)>");
      ers::warning(ex12);
    }
  }

  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::close: End of function");
}


/**********************************************************************************************************************/
void Robin::reserveMsgResources(u_int numberOfOutstandingReq, u_int msgInputMemorySize, u_int miscSize, u_int eventSize)
/**********************************************************************************************************************/
{
  //This method will only be called from one defined place. No mutexes required 
  T_msg_req_params msg_params;
  int ret;

  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::reserveMsgResources: Called with numberOfOutstandingReq = " << numberOfOutstandingReq << " ,msgInputMemorySize = " << msgInputMemorySize << ", miscSize = " << miscSize << ", eventSize = " << eventSize);
  isOpen();
  
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::reserveMsgResources: m_reservememok = " << m_reservememok);
  if (m_reservememok != 0)
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::reserveMsgResources: This function has already been executed");
    CREATE_ROS_EXCEPTION(ex1, ROSRobinExceptions, ALREADY_DONE, "");
    throw (ex1);
  }
  
  if (msgInputMemorySize & 0x1f)
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::reserveMsgResources: The parameter msgInputMemorySize is not 32 byte aligned");
    CREATE_ROS_EXCEPTION(ex2, ROSRobinExceptions, ALIGN, "");
    throw (ex2);
  }  
  
  m_nFifoSlots = numberOfOutstandingReq;
 
  msg_params.slot     = m_slot_number;
  msg_params.dpm_size = msgInputMemorySize;
  msg_params.mem_size = miscSize + eventSize;
  msg_params.n_fifo   = m_nFifoSlots;

  ret = ioctl(m_dev_handle, MSG_REG, &msg_params);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::reserveMsgResources: Error from ioctl, errno = " << errno);
    CREATE_ROS_EXCEPTION(ex3, ROSRobinExceptions, IOCTL, "This error came from ioctl(MSG_REG). " << ioctl_strings[errno]);
    throw (ex3);
  } 

  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::reserveMsgResources: ioctl returns: dpm_base   = 0x" << HEX(msg_params.dpm_base));
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::reserveMsgResources: ioctl returns: mem_base   = 0x" << HEX(msg_params.mem_base));
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::reserveMsgResources: ioctl returns: mem_offset = 0x" << HEX(msg_params.mem_offset));
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::reserveMsgResources: ioctl returns: dpm_offset = 0x" << HEX(msg_params.dpm_offset));

  m_mem_offset = msg_params.mem_offset;
  m_dpm_offset = msg_params.dpm_offset;

  //Map the DPM into the virtual address space
  msg_dpm.user_phys_base = msg_params.dpm_base;
  msg_dpm.user_size      = msgInputMemorySize;
  msg_dpm.index          = 0;
  mapmem(&msg_dpm);

  //Map the event memory into the virtual address space
  reply_mem.user_phys_base = msg_params.mem_base;
  reply_mem.user_size      = eventSize + miscSize;
  reply_mem.misc_size      = miscSize;
  mapmem(&reply_mem);  
  
  //Initialize the management structures for the misc. buffer
  for(u_int loop = 0; loop < s_max_msgs; loop++)
  {
    blocklets[loop].used  = BLOCKLET_UNUSED;
    blocklets[loop].start = 0;
    blocklets[loop].size  = 0;
  }
  //Initialise the first element
  blocklets[0].used  = BLOCKLET_FREE;
  blocklets[0].start = (u_long)reply_mem.user_virt_base;
  blocklets[0].size  = miscSize;
  
  m_reservememok = 1;
  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::reserveMsgResources: End of function");
}


/********************************/
void Robin::freeMsgResources(void) 
/********************************/
{
  //This method will only be called from one defined place. No mutexes required 
  int ret;
  
  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::freeMsgResources: Start of function");
  isOpen();

  //Return the memory mappings
  unmapmem(&msg_dpm);
  unmapmem(&reply_mem);
  
  ret = ioctl(m_dev_handle, MSG_FREE, &m_slot_number);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::freeMsgResources: Error from ioctl, errno = " << errno);
    char err_str[256];
    sprintf(err_str, "\nThis error came from ioctl(MSG_FREE). %s\n", ioctl_strings[errno]);
    CREATE_ROS_EXCEPTION(ex10, ROSRobinExceptions, IOCTL, "This error came from ioctl(MSG_FREE). " << ioctl_strings[errno]);
    throw (ex10);
  }  
  
  m_reservememok = 0;
  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::freeMsgResources: End of function");
}


/***************************/
void Robin::getProcInfo(void) 
/***************************/
{      
  int ret;
  char mytext[PROC_MAX_CHARS];

  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::getProcInfo: Start of function");
  isOpen();

  ret = ioctl(m_dev_handle, GETPROC, mytext);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::getProcInfo: Error from ioctl, errno = " << errno);
    char err_str[256];
    sprintf(err_str, "\nThis error came from ioctl(MSG_FREE). %s\n", ioctl_strings[errno]);
    CREATE_ROS_EXCEPTION(ex20, ROSRobinExceptions, IOCTL, "This error came from ioctl(GETPROC). " << ioctl_strings[errno]);
    throw (ex20);
  }
  
  printf("%s", mytext);
}


/**************************************/
void Robin::setDmaRamParams(u_int order) 
/**************************************/
{
  //This method will only be called from one defined place. No mutexes required 
  int ret;
  u_int dmaMemorySize;
  u_long pci_mem_base;
  T_init_dma_params dma_params;
  
  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::setDmaRamParams: Start of function");

  // First check if the board is already opened. This call may throw an exception!!
  isOpen();
  
  // Check if DMA memory has been already set (MM: Is this really necessary? One could
  // ask the driver, free the old memory in case and register a new one.)
  // MJ: it is not a good idea to have m_setmemok as it is reset when an application restarts
  // MJ: move it to the driver 
  if (m_setmemok != 0)
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::setDmaRamParams: This function has already been executed");
    CREATE_ROS_EXCEPTION(ex11, ROSRobinExceptions, ALREADY_DONE2, "");
    throw (ex11);
  }  
  
  // Check if the order exceeds the max order (MM: How is the max order determined. By what the linux kernel
  // can do or what the ROBIN hardware can do? In principle the ROBIN HW can do 256 MByte or even more (when shfting
  // the local bus address)
  if (order > s_max_order)
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::setDmaRamParams: The parameter order is too large");
    CREATE_ROS_EXCEPTION(ex12, ROSRobinExceptions, ILL_ORDER, "");
    throw (ex12);
  }  

  // Calculate the DMA memory size here. This must be a power of two multiplied with 64kByte.
  // To make the proper alignment we have to allocate two times more memory  
  dmaMemorySize = s_64k * (1 << (order + 1));
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::setDmaRamParams: dmaMemorySize = 0x" << HEX(dmaMemorySize));

  // Now allocate the memory with cmem_rcc. We want to have a piece of the memory reserved by the bigphysarea mechanism.
  ret = CMEM_BPASegmentAllocate(dmaMemorySize, "ROBIN_DIRECT_DMA", &m_cmem_handle);
  if (ret)
  {
    // Bah! this did not work. Throw an exception.
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::setDmaRamParams: Error from CMEM_BPASegmentAllocate");
    char m_rcc_err_str[256];
    rcc_error_string(m_rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex13, ROSRobinExceptions, CMEM, m_rcc_err_str);
    throw (ex13);
  } 
  
  // Now lock the memory that it is not destroyed by cmem if the process is finished.
  ret = CMEM_SegmentLock(m_cmem_handle);
  if (ret)
  {
    // In case of an error throw an exception. MM: Need to UNDO the memory allocation here!!!!
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::setDmaRamParams: Error from CMEM_SegmentLock");
    char m_rcc_err_str[256];
    rcc_error_string(m_rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex14, ROSRobinExceptions, CMEM, m_rcc_err_str);
    throw (ex14);
  } 
  
  // Get the physical address of the memory.
  ret = CMEM_SegmentPhysicalAddress(m_cmem_handle, &pci_mem_base);
  if (ret)
  {
    // In case we do not get the physical address throw an exception 
    // MM: Need to UNDO the memory allocation here and locking !!!!
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::setDmaRamParams: Error from CMEM_SegmentPhysicalAddress");    
    char m_rcc_err_str[256];
    rcc_error_string(m_rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex15, ROSRobinExceptions, CMEM, m_rcc_err_str);
    throw (ex15);
  } 
  
  // Now do the alignment
  u_int alignedSize    = dmaMemorySize >> 1;
  u_int alignedPhysAdr = pci_mem_base & (~(alignedSize - 1));
  alignedPhysAdr += alignedSize; 
   
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::setDmaRamParams: DmaSize: 0x" << HEX(dmaMemorySize) << "  Aligned: 0x" << HEX(alignedSize));    
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::setDmaRamParams: DmaPhysAddr 0x" << HEX(pci_mem_base) << "  Aligned: 0x" << HEX(alignedPhysAdr)); 

  // Register it at the driver to be the one and only DMA area for the current ROBIN board.
  dma_params.slot     = m_slot_number;
  dma_params.mem_base = alignedPhysAdr; 
  dma_params.mem_size = alignedSize; 
  ret = ioctl(m_dev_handle, INIT_DMA, &dma_params);
  if (ret)
  {
    // In case we do can not register the new memory at the driver throw an exception.
    // MM: Need to UNDO the memory allocation here and locking !!!!
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::setDmaRamParams: Error from ioctl, errno = " << errno);
    CREATE_ROS_EXCEPTION(ex16, ROSRobinExceptions, IOCTL, "This error came from ioctl(INIT_DMA). " << ioctl_strings[errno]);
    throw (ex16);
  }  

  m_setmemok = 1;

  // Now start to register the memory at the ROBIN card.
  
  // First write the local range. This is the inverse of the DMA memory size minus one.
  u_int localRange = ~(alignedSize - 1);
  writePlx(ROS::Robin::DMRR, localRange);

  // Write the Local Bus Address. This must be a multiple of the range
  u_int localAddr = s_direct_dma_offset & localRange;
  if ( (s_direct_dma_offset & (~localRange))  != 0) 
  {
    // Ugly!! We got a bad parameter set. Throw an exception.
    // MM: Need to UNDO the memory allocation here and locking !!!!
    CREATE_ROS_EXCEPTION(ex17, ROSRobinExceptions, ILL_ORDER, "ROS::BridgePlx9656::EnableDirectMaster: Error: LocalAddress must be a multiple of range!!");
    throw (ex17);
  }
  writePlx(ROS::Robin::DMLBAM, localAddr);
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::setDmaRamParams: DmaPhysAddr 0x" << HEX(pci_mem_base) << "  Aligned: 0x" << HEX(alignedPhysAdr));

  // Write the PCI Bus Remap (the address put on the PCI for Direct Master access)
  if ((alignedPhysAdr & ~localRange) != 0)
  {
    // The PCI base address we got from the kernel is not properly
    // aligned! MM: FIXME: Need to produce a really aligned piece of memory
    //                     by allocating more and align it to the size of the memory!!!!!!!  
    CREATE_ROS_EXCEPTION(ex18, ROSRobinExceptions, ALIGN, "ROS::BridgePlx9656::EnableDirectMaster: Error: LocalAddress must be a multiple of range!!");
    throw (ex18);
  }
  
  // Now write it togther with the direct master enable bit
  u_int pciRemap = 0x1 | (alignedPhysAdr) ;
  writePlx(ROS::Robin::DMPBAM, pciRemap);

  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::setDmaRamParams: End of function");
}


/********************************/
u_long Robin::getVirtMiscBase(void) 
/********************************/
{
  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::getVirtMiscBase: called");
  isOpen();
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::getVirtMiscBase: Virtual base address of misc. memory is " << HEX(reply_mem.user_virt_base));
  return((u_long)reply_mem.user_virt_base);
}


/*********************************/
u_long Robin::getVirtEventBase(void) 
/*********************************/
{
  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::getVirtEventBase: called");
  isOpen();
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::getVirtEventBase: Virtual base address of misc. memory is " << HEX(reply_mem.user_virt_base + reply_mem.misc_size));
  return((u_long)reply_mem.user_virt_base + reply_mem.misc_size);
}


/********************************/
u_int Robin::getPhysMiscBase(void) 
/********************************/
{
  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::getPhysMiscBase: called");
  isOpen();
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::getPhysMiscBase: Physical base address of misc. memory is 0x" << HEX(reply_mem.user_phys_base));
  return((u_int)reply_mem.user_phys_base);
}


/*********************************/
u_int Robin::getPhysEventBase(void) 
/*********************************/
{
  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::getPhysEventBase: called");
  isOpen();
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::getPhysEventBase: Physical base address of misc. memory is 0x" << HEX(reply_mem.user_phys_base + reply_mem.misc_size));
  return((u_int)reply_mem.user_phys_base + reply_mem.misc_size);
}


/******************************************************************/
void Robin::sendMsg(u_int rolId, u_int service, u_long replyAddress) 
/******************************************************************/
{  
  isOpen();
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::sendMsg(1): rolId = " << rolId);
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::sendMsg(1): service = " << service);
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::sendMsg(1): replyAddress = 0x" << HEX(replyAddress));

  u_int data[1];
  sendMsg(rolId, service, replyAddress, 0, data); 
}


/***********************************************************************************/
void Robin::sendMsg(u_int rolId, u_int service, u_long replyAddress, u_int parameter) 
/***********************************************************************************/
{
  isOpen();
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::sendMsg(2): rolId = " << rolId);
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::sendMsg(2): service = " << service);
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::sendMsg(2): replyAddress = 0x" << HEX(replyAddress));
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::sendMsg(2): parameter = " << parameter);

  u_int data[1];
  data[0] = parameter;
  sendMsg(rolId, service, replyAddress, 1, data); 
}


/******************************************************************************************************/
void Robin::sendMsg(u_int rolId, u_int service, u_long replyAddress, u_int parameter1, u_int parameter2) 
/******************************************************************************************************/
{
  isOpen();
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::sendMsg(3): rolId = " << rolId);
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::sendMsg(3): service = " << service);
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::sendMsg(3): replyAddress = 0x" << HEX(replyAddress));
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::sendMsg(3): parameter1 = " << parameter1);
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::sendMsg(3): parameter2 = " << parameter2);

  u_int data[2];
  data[0] = parameter1;
  data[1] = parameter2;
  sendMsg(rolId, service, replyAddress, 2, data);
}


/*******************************************************************************************/
void Robin::sendMsg(u_int rolId, u_int service, u_long replyAddress, u_int size, u_int *data) 
/*******************************************************************************************/
{
  isOpen();

  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::sendMsg(4): rolId = " << rolId);
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::sendMsg(4): service = " << service);
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::sendMsg(4): replyAddress = 0x" << HEX(replyAddress));
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::sendMsg(4): size = 0x" << HEX(size));
  
  u_int msg_size = 12 + (size * 4);
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::sendMsg(4): msg_size = " << msg_size);

  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::sendMsg(4): waiting for m_sendmutex");
  m_sendmutex->lock();
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::sendMsg(4): got m_sendmutex");

  //Get a pointer to the DPM. Wait if it is full
  //Decrement the FIFO counter. Wait if all elements are used
  u_int msg_index = wait_dpm_and_fifo(msg_size);

  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::sendMsg(4): msg_index = 0x" << HEX(msg_index));

  u_long pc_mem_offset = (replyAddress - (u_long)reply_mem.user_virt_base) + m_mem_offset;
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::sendMsg(4): reply_mem.user_virt_base        =  0x" << HEX((u_long)reply_mem.user_virt_base));
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::sendMsg(4): reply_mem.user_phys_base        =  0x" << HEX(reply_mem.user_phys_base));
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::sendMsg(4): pc_mem_offset                   =  0x" << HEX(pc_mem_offset));

  //Write the message to the DPM
  u_int windex = msg_index >> 2;  //convert from byte to word
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::sendMsg(4): windex = 0x" << HEX(windex));
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::sendMsg(4): Writing message to DPM virt. address " << HEX(&msg_dpm.user_virt_base[windex]));

  u_int w1 = (rolId << 16) | service;
  u_int w2 = s_direct_dma_offset | pc_mem_offset;
  u_int w3 = 0;
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::sendMsg(4): msg word 1 = 0x" << HEX(w1));
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::sendMsg(4): msg word 2 = 0x" << HEX(w2));
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::sendMsg(4): msg word 3 = 0x" << HEX(w3));

  msg_dpm.user_virt_base[windex++] = w1;     
  msg_dpm.user_virt_base[windex++] = w2;
  msg_dpm.user_virt_base[windex++] = w3; 
  for (u_int loop = 0; loop < size; loop++)
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::sendMsg(4): msg word " << loop + 4 << " = 0x" << HEX(data[loop]));
    msg_dpm.user_virt_base[windex++] = data[loop]; 
  }
    
  //Clear the first word of the message buffer (for polling in receiveMsg)
  u_int *ptr = reinterpret_cast<u_int *> (replyAddress);
  *ptr++ = 0;
    
  //Write a word to the FIFO    
  u_int fifo_data = ((msg_size >> 2) << 16) | ((m_dpm_offset + msg_index) >> 2);
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::sendMsg(4): writing 0x" << HEX(fifo_data) << " to the message FIFO");
  fpga_win.user_virt_base[s_fifo_index >> 2] = fifo_data;
  
  m_sendmutex->unlock();
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::sendMsg(4): done with m_sendmutex");  
}


/********************************************************/
bool Robin::receiveMsg(u_long replyAddress, bool blocking) 
/********************************************************/
{
  volatile u_int *ptr;
  tstamp startTime, currentTime; 

  isOpen();
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::receiveMsg: replyAddress = 0x" << HEX(replyAddress));
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::receiveMsg: blocking = " << blocking);
  
  ptr = reinterpret_cast<u_int *> (replyAddress);
  // Now wait for the fragment to arrive
  ts_clock(&startTime);
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::receiveMsg: Entring into while loop");
  while ((*ptr == 0) || (*ptr == 1)) 
  {
    if (!blocking)
    {
      DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::receiveMsg: The message has not yet arrived");
      return(false);
    }
    ts_clock(&currentTime);
    if (ts_duration(startTime, currentTime) > m_timeout) 
    {
      DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::receiveMsg: ---> TIMEOUT <--- for replyAddress = 0x" << HEX(replyAddress));      
      CREATE_ROS_EXCEPTION(ex19, ROSRobinExceptions, TIMEOUT, "Timeout when waiting for the reply message. Robinphysical address (slot) = " << m_slot_number);
      throw (ex19);
    }
    
    DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::receiveMsg: m_timeout = " << m_timeout);
    DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::receiveMsg: time elapsed (in seconds) = " << ts_duration(startTime, currentTime));
    DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::receiveMsg: calling yield");
  
    DFThread::yield();
  }
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::receiveMsg: back from while loop");
  
  msg_done();
  return(true);
}


// Returns a ROL
/******************************/
Rol & Robin::getRol(u_int rolId) 
/******************************/
{
  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::getRol: called with rolId = " << rolId);

  // Check if the Robin object has been opened. Throws an exception if not!!
  isOpen();

  // If the client requests a ROL number more then 2, throw an exception!
  if (rolId > 2) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::getRol: Illegal value passed for rolId");
    CREATE_ROS_EXCEPTION(ex20, ROSRobinExceptions, NROLS, "Number of requested ROLs exceeded 3!");
    throw (ex20);
  }

  // Get the Rol object from the array
  Rol *ret = m_rolVector[rolId];

  // Check if it has been filled in. This could not be done in case of a ROBIN prototype
  // which has only two ROLs!
  if (ret == 0)
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::getRol: failed to find the rol object in m_rolVector");
    CREATE_ROS_EXCEPTION(ex21, ROSRobinExceptions, NROLS, "Requrested ROL is not available!");
    throw (ex21);
  }
  
  return *ret;
}


/****************************/
u_int Robin::getNrOfRols(void)
/****************************/
{
  u_int i;
  
  isOpen();
  for (i = 0; i < 3; i++) 
  {
    if (m_rolVector[i] == 0) 
      return i;
  }

  return 3;
}


/**********************************************/
void Robin::writeFpga(u_int address, u_int data) 
/**********************************************/
{  
  isOpen();
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::writeFpga: address = 0x" << HEX(address));
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::writeFpga: data = " << data);
  
  if (address & 0x3)
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::writeFpga: address is not 4 byte aligned");
    CREATE_ROS_EXCEPTION(ex22, ROSRobinExceptions, ALIGN2, "");
    throw (ex22);
  }

  fpga_win.user_virt_base[address >> 2] = data;
}


/**********************************/
u_int Robin::readFpga(u_int address) 
/**********************************/
{
  isOpen();
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::readFpga: address = " << address);
  if (address & 0x3)
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::readFpga: address is not 4 byte aligned");
    CREATE_ROS_EXCEPTION(ex23, ROSRobinExceptions, ALIGN2, "");
    throw (ex23);
  }
  
  return(fpga_win.user_virt_base[address >> 2]);
}


/*********************************************/
void Robin::writePlx(u_int address, u_int data) 
/*********************************************/
{
  isOpen();
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::writePlx: address = " << address);
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::writePlx: data = 0x" << HEX(data));
  if (address & 0x3)
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::writePlx: address is not 4 byte aligned");
    CREATE_ROS_EXCEPTION(ex24, ROSRobinExceptions, ALIGN2, "");
    throw (ex24);
  }

  plx_win.user_virt_base[address >> 2] = data;
}


/*********************************/
u_int Robin::readPlx(u_int address) 
/*********************************/
{
  isOpen();
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::readPlx: address = " << address);
  if (address & 0x3)
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::readPlx: address is not 4 byte aligned");
    CREATE_ROS_EXCEPTION(ex25, ROSRobinExceptions, ALIGN2, "");
    throw (ex25);
  }
  
  return(plx_win.user_virt_base[address >> 2]);
}


/**************************************************/
void Robin::writeEeprom(u_int address, u_short data) 
/**************************************************/
{
  isOpen();
  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::writeEeprom: Start of function with address: 0x" << HEX(address) << " and data 0x" << data);
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::writeEeprom: Disabling write protection");
  
  // Step 1: Disable the write protection!
  setEepromCS(1);  // Enable the chip select
  setEepromDI(1);  // Clock the SB bit
  pulseEepromClk();

  // Clock the EWEN OP code bit
  setEepromDI(0);
  pulseEepromClk();
  setEepromDI(0);
  pulseEepromClk();
  setEepromDI(1);
  pulseEepromClk();
  setEepromDI(1);
  pulseEepromClk();

  // Clock additional data which value is irrelevant
  u_int i;
  for (i = 0; i < 6; i++) 
  {
    setEepromDI(1);
    pulseEepromClk();
  }

  setEepromCS(0);  // Disable the chip select
  pulseEepromClk();
  pulseEepromClk();
  
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::writeEeprom: Writing the data");
  
  // Step 2: Write the data
  setEepromCS(1);  // Enable the chip select
  setEepromDI(1);  // Clock the SB bit
  pulseEepromClk();
  setEepromDI(0);  // Clock the Write OP code bit
  pulseEepromClk();
  setEepromDI(1);
  pulseEepromClk();

  // And now the address
  u_int mask;
  for (mask = 0x80; mask != 0; mask = mask >> 1) 
  {
    setEepromDI( address & mask);
    pulseEepromClk();
  }

  // finally write the data
  u_short writeValue = data;
  for (i = 0; i < 16; i++) 
  {
    setEepromDI(writeValue & 0x8000);
    writeValue = writeValue << 1;
    pulseEepromClk();
  }

  setEepromCS(0);
  pulseEepromClk();
  pulseEepromClk();
  setEepromCS(1);
  pulseEepromClk();

  // Wait for the chip to be finished
  while (getEepromDO() == 0) 
    pulseEepromClk();

  pulseEepromClk();
  setEepromCS(0);    // Disable the chip select
  pulseEepromClk();
  pulseEepromClk();

  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::writeEeprom: Enabling write protection");

  // Step 3: Enable the write protection again
  setEepromCS(1);    // Enable the chip select
  setEepromDI(1);    // Clock the SB bit
  pulseEepromClk();
  setEepromDI(0);    // Clock the EWEN OP code bit
  pulseEepromClk();
  setEepromDI(0);
  pulseEepromClk();
  setEepromDI(0);
  pulseEepromClk();
  setEepromDI(0);
  pulseEepromClk();

  // Clock additional data which value is irrelevant
  for (i = 0; i < 6; i++) 
  {
    setEepromDI(1);
    pulseEepromClk();
  }

  setEepromCS(0);  // Disable the chip select
  pulseEepromClk();
  pulseEepromClk();
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::writeEeprom: End of function");
}


/**************************************/
u_short Robin::readEeprom(u_int address) 
/**************************************/
{
  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::readEeprom: Start of function with address: 0x" << HEX(address));

  isOpen();

  setEepromCS(1);  // Enable the chip select
  setEepromDI(1);  // Clock the SB bit
  pulseEepromClk();
  setEepromDI(1);  // Clock the Read OP code bit
  pulseEepromClk();
  setEepromDI(0);
  pulseEepromClk();

  // And now the address
  u_int mask;
  for (mask = 0x80; mask != 0; mask = mask >> 1) 
  {
    setEepromDI( address & mask);
    pulseEepromClk();
  }

  // finally read the data
  u_int i;
  u_short retValue = 0;
  for (i = 0; i < 17; i++) 
  {
    retValue = retValue << 1;
    retValue |= (getEepromDO() != 0) ? 0x1 : 0;    
    pulseEepromClk();
  }

  pulseEepromClk();
  setEepromCS(0);  // Disable the chip select
  pulseEepromClk();
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::readEeprom: End of function. Returning 0x" << retValue);
  return retValue;
}


/*********************/
void Robin::reset(void) 
/*********************/
{
  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::reset: called");

  isOpen();

  // Reset the MBOX2 register
  writePlx(MBOX2, 0);

  // Save the direct master 
  u_int dmRange     = readPlx(DMRR);
  u_int dmLocalBase = readPlx(DMLBAM);
  u_int dmPciBase   = readPlx(DMPBAM);

  // Pull the reset bit in the PLX control registers
  u_int ctrl = readPlx(CNTRL);
  ctrl |= (1 << 30);

  // make sure the EEPROM control bit30 -EEDO- is reset before a reset occurs otherwise VPD cycles will fail
  ctrl &= ~(1 << 31);
  writePlx(CNTRL, ctrl);

  // Wait a bit
  ts_delay(100);

  // Set the reset bit back
  ctrl &= ~(1 << 30);
  writePlx(CNTRL, ctrl);

  // Wait a bit
  ts_delay(100);

  // Now reload fresh configuration register contents from the EEPROM
  ctrl = readPlx(CNTRL);

  ctrl |= (1 << 29);
  writePlx(CNTRL, ctrl);

  // Wait a bit
  ts_delay(100);

  ctrl &= ~(1 << 29);
  writePlx(CNTRL, ctrl);

  // Reload the direct master settings
  writePlx(DMRR, dmRange);
  writePlx(DMLBAM, dmLocalBase);
  writePlx(DMPBAM, dmPciBase);
  tstamp startTime,currentTime; 

  ts_clock(&startTime);
  while (1) 
  {
    u_int data = readPlx(MBOX2);
    DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::reset: Mailbox 2 contains 0x" << HEX(data));
    if(data == enum_mboxStatusOk || data == enum_mboxStatusWarn)
    {
     DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::reset: Robin is back to life");
     break;  
    }
    
    ts_clock(&currentTime);
    DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::reset: time elapsed (in seconds) = " << ts_duration(startTime, currentTime));
    if (ts_duration(startTime, currentTime) > m_timeout) 
    {
      DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::reset: ---> TIMEOUT <--- with MBOX2 = 0x" << HEX(data));

      std::stringstream mbox_error;
      if (data == 0x00000000) mbox_error << "  Message: not initialised / reset" << std::ends;
      if (data == 0x0055aa00) mbox_error << "  Message: application running with no BIST errors" << std::ends; 
      if (data == 0x0155cc88) mbox_error << "  Message: FPGA being (re-) configured by application" << std::ends; 
      if (data == 0x0055cc88) mbox_error << "  Message: FPGA (re-)configuration completed" << std::ends;
      if (data == 0x0055ccff) mbox_error << "  Message: FPGA (re-)configuration failed" << std::ends; 
      if (data == 0x00aa5500) mbox_error << "  Message: application terminated due to user request" << std::ends; 
      if (data == 0x0055aa88) mbox_error << "  Message: application running with BIST warnings" << std::ends; 
      if (data == 0x0055aaff) mbox_error << "  Message: application running with BIST errors" << std::ends; 
      if (data == 0x00aa55ff) mbox_error << "  Message: application terminated due to BIST or INIT error" << std::ends;
 
      CREATE_ROS_EXCEPTION(ex26, ROSRobinExceptions, TIMEOUT, "Timeout when waiting for the Robin to come up after reset. MBOX2 = " << HEX(data)  << mbox_error.str().c_str());
      throw (ex26);
    }
  }
}


/***********************/
/*  Internal functions */
/***********************/


/*****************************/
inline void Robin::isOpen(void) 
/*****************************/
{
  if (!m_is_open) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::isOpen: The library has not yet been opened");
    CREATE_ROS_EXCEPTION(ex27, ROSRobinExceptions, NOTOPEN, "");
    throw (ex27);
  }
}


/*******************************/
void Robin::mapmem(T_mmap *param)
/*******************************/
{
  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::mapmem: called");
  param->phys_base = param->user_phys_base & 0xfffff000;
  param->offset    = param->user_phys_base & 0xfff;
  param->size      = param->user_size + param->offset; 
  
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::mapmem: param->user_phys_base = 0x" << HEX(param->user_phys_base));
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::mapmem: param->user_size      = 0x" << HEX(param->user_size));
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::mapmem: param->phys_base      = 0x" << HEX(param->phys_base));
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::mapmem: param->offset         = 0x" << HEX(param->offset));
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::mapmem: param->size           = 0x" << HEX(param->size));

  param->virt_base = mmap(0, param->size, (PROT_READ|PROT_WRITE), MAP_SHARED, m_dev_handle, param->phys_base);
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::mapmem: param->virt_base      = 0x" << HEX((u_long)param->virt_base));
  if (param->virt_base == MAP_FAILED)
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::mapmem: Error from call to mmap, errno = " << errno);
    CREATE_ROS_EXCEPTION(ex28, ROSRobinExceptions, MMAP, "A call to mmap returns errno = " << errno);
    throw (ex28);
  }
  param->user_virt_base = (u_int *)((u_long)param->virt_base + param->offset);
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::mapmem: param->user_virt_base = 0x" << HEX((u_long)param->user_virt_base));
  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::mapmem: OK");
}


/*********************************/
void Robin::unmapmem(T_mmap *param)
/*********************************/
{
  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::unmapmem: called");
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::unmapmem: param->virt_base = 0x" << HEX(param->virt_base));
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::unmapmem: param->size      = 0x" << HEX(param->size));
  
  int ret = munmap(param->virt_base, param->size);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::unmapmem: Error from munmap, errno = " << errno);
    CREATE_ROS_EXCEPTION(ex10, ROSRobinExceptions, MUNMAP, "A call to munmap returns errno = " << errno);
    ers::warning(ex10);    
  }
  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::unmapmem: OK");
}

 
// What has to be protected with mutexes?
//
//1) The variable m_nFifoSlots
//   It is used to remember how may free slots in the MSG FIFO are available at any time
//
//2) The structure msg_dpm 
//   It remembers to which DPM offset the next message has to be written to
//
//3) The variable m_dpm_current_index
//   It controls where in the m_dpm_start array information about a message is stored  
/****************************************/
u_int Robin::wait_dpm_and_fifo(u_int size)
/****************************************/
{
  u_int mend, go_ahead, msg_index, yc;
  tstamp startTime, currentTime; 

  //Align the message size to 32 bytes
  if (size & 0x1f)
    size = (size & ~0x1f) + 0x20;    
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::wait_dpm_and_fifo: Effective message size (bytes) = " << size);
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::wait_dpm_and_fifo: msg_dpm.index                  = " << msg_dpm.index);
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::wait_dpm_and_fifo: msg_dpm.user_size              = " << msg_dpm.user_size);

  if (size > msg_dpm.user_size)  
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::wait_dpm_and_fifo: size (" << size << " ) is too big for available DPM window ( " << msg_dpm.user_size << " )");
    m_sendmutex->unlock();
    CREATE_ROS_EXCEPTION(ex29, ROSRobinExceptions, ILLSIZE, "");
    throw (ex29);
  }

  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::wait_dpm_and_fifo: waiting for m_messageLock");
  m_messageLock->lock();
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::wait_dpm_and_fifo: got m_messageLock");

  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::wait_dpm_and_fifo: m_dpm_oldest_index              = " << m_dpm_oldest_index);
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::wait_dpm_and_fifo: m_dpm_start[m_dpm_oldest_index] = " << m_dpm_start[m_dpm_oldest_index]);

  ts_clock(&startTime);  
  go_ahead = 0;
  yc = 0;
  while (!go_ahead)
  {
    ts_clock(&currentTime);
    if (ts_duration(startTime, currentTime) > m_timeout) 
    {
      DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::wait_dpm_and_fifo: ---> TIMEOUT <---");
      m_messageLock->unlock();
      m_sendmutex->unlock();
      CREATE_ROS_EXCEPTION(ex30, ROSRobinExceptions, TIMEOUT, "Timeout when waiting for the FIFO & DPM");
      throw (ex30);
    }  
    go_ahead = 1;
    
    if (m_nFifoSlots == 0)
    {
      go_ahead = 0;
      DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::wait_dpm_and_fifo: All FIFO slots in use");
    }
    
    mend = msg_dpm.index + size;
    DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::wait_dpm_and_fifo: mend = " << mend);
    if(mend <= msg_dpm.user_size)
    {
      msg_index = msg_dpm.index;
      DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::wait_dpm_and_fifo: new message fits into rest of window");
      if(m_pending && (m_dpm_start[m_dpm_oldest_index] < mend) && (m_dpm_start[m_dpm_oldest_index] > msg_dpm.index))
      {
        DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::wait_dpm_and_fifo: Distance to oldest message insufficient");
        DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::wait_dpm_and_fifo: m_dpm_start[m_dpm_oldest_index] = " << m_dpm_start[m_dpm_oldest_index]);
        DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::wait_dpm_and_fifo: mend = " << mend);
        go_ahead = 0;
      }
    }
    else
    {
      DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::wait_dpm_and_fifo: new message has to start at top of window");
      msg_index = 0;
      mend = size;
      if(m_pending && (m_dpm_start[m_dpm_oldest_index] < mend))
      {
        DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::wait_dpm_and_fifo: Distance to oldest message insufficient");
        DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::wait_dpm_and_fifo: m_dpm_start[m_dpm_oldest_index] = " << m_dpm_start[m_dpm_oldest_index]);
        DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::wait_dpm_and_fifo: mend = " << mend);
        go_ahead = 0;
      }
    }
   
    if (!go_ahead)
    {  
      m_messageLock->unlock();
      DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::wait_dpm_and_fifo: released m_messageLock for yield()");
      DFThread::yield();
      DEBUG_TEXT(DFDB_ROSROBIN, 6, "Robin::wait_dpm_and_fifo: Yielded " << ++yc << " times waiting for FIFO & DPM");   
      m_messageLock->lock(); 
      DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::wait_dpm_and_fifo: got m_messageLock after yield()");
    }
  }

  m_pending++;
  m_nFifoSlots--;
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::wait_dpm_and_fifo: FIFO entry claimed (new value = " << m_nFifoSlots << ")"); 
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::wait_dpm_and_fifo: msg_index = " << msg_index);
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::wait_dpm_and_fifo: m_pending = " << m_pending);

  //Register the DPM parameters
  msg_dpm.index = mend;
  m_dpm_start[m_dpm_current_index] = msg_index;
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::wait_dpm_and_fifo: m_dpm_start[" << m_dpm_current_index << "] = " << m_dpm_start[m_dpm_current_index]);
  
  if (m_dpm_current_index == (s_max_msgs - 1))
    m_dpm_current_index = 0;
  else
    m_dpm_current_index++;
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::wait_dpm_and_fifo: new m_dpm_current_index = " << m_dpm_current_index);

  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::wait_dpm_and_fifo: returning m_messageLock");
  m_messageLock->unlock();
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::wait_dpm_and_fifo: done m_messageLock");

  return(msg_index);
}


/************************/
void Robin::msg_done(void)
/************************/
{
  //We also have to have the mutext here to be 100% on the save side
  m_messageLock->lock();
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::msg_done: got m_messageLock");

  m_nFifoSlots++;  
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::msg_done: FIFO entry released"); 
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::msg_done: m_nFifoSlots = " << m_nFifoSlots);
  
  if (m_dpm_oldest_index == (s_max_msgs - 1))
    m_dpm_oldest_index = 0;
  else
    m_dpm_oldest_index++;

  m_pending--;
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::msg_done: m_pending = " << m_pending);
  m_messageLock->unlock();
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::msg_done: done with m_messageLock");
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::msg_done: m_dpm_oldest_index = " << m_dpm_oldest_index);
}  
 
  
/*****************************************************************/
void Robin::getMiscMem(u_int size, u_long *start_add, u_int *handle)
/*****************************************************************/
{
  int b1, b2;
  u_int loop, yc;
  tstamp startTime, currentTime; 

  m_miscmemLock->lock();

  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::getMiscMem: called with size = 0x" << HEX(size));

  ts_clock(&startTime);  
  yc = 0;
  //look for a large enough free blocklet. Wait if all resources are used
  while(1)  //We break out of the loop once we are happy
  {
    b1 = -1;
    b2 = -1;
  
    for(loop = 0; loop < s_max_msgs; loop++)
    {
      DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::getMiscMem: find space: blocklets[" << loop << "].used = " << blocklets[loop].used);
      DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::getMiscMem: find space: blocklets[" << loop << "].size = " << blocklets[loop].size);
      if (blocklets[loop].used == BLOCKLET_FREE && blocklets[loop].size >= size)
      {
	b1 = loop;
        DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::getMiscMem: blocklet " << b1 << " contains sufficient memory");
	break;
      }  
    }

    for(loop = 0; loop < s_max_msgs; loop++)
    {
      if (blocklets[loop].used == BLOCKLET_UNUSED)
      {
	b2 = loop;
        DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::getMiscMem: blocklet " << b2 << " is free");
	break;
      }  
    } 
  
    if (b1 != -1 && b2 != -1)  //We have what we want. No reason to check the time or yield
    {
      DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::getMiscMem: terminating while loop");
      break;
    }

    DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::getMiscMem: Checking time-out");
    ts_clock(&currentTime);
    if (ts_duration(startTime, currentTime) > m_timeout) 
    {
      DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::getMiscMem: ---> TIMEOUT <---");
      m_miscmemLock->unlock();
      CREATE_ROS_EXCEPTION(ex31, ROSRobinExceptions, TIMEOUT, "Timeout when waiting for the misc. memory");
      throw (ex31);
    }
    
    m_miscmemLock->unlock();
    DFThread::yield();
    DEBUG_TEXT(DFDB_ROSROBIN, 6, "Robin::getMiscMem: Yielded " << ++yc << " times waiting for misc. MEM"); 
    m_miscmemLock->lock();
  }
  
  blocklets[b2].used = BLOCKLET_USED;
  blocklets[b2].size = size;
  blocklets[b2].start = blocklets[b1].start;  

  blocklets[b1].size -= size;
  if (blocklets[b1].size == 0)
  {
    blocklets[b1].used = BLOCKLET_UNUSED;
    blocklets[b1].start = 0;
  }
  else
    blocklets[b1].start += size;

  *start_add = blocklets[b2].start;
  *handle = b2;
  
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::getMiscMem: find space: blocklets[" << b1 << "].used = " << blocklets[b1].used);
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::getMiscMem: find space: blocklets[" << b1 << "].size = " << blocklets[b1].size);
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::getMiscMem: find space: blocklets[" << b1 << "].start = 0x" << HEX(blocklets[b1].start));
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::getMiscMem: find space: blocklets[" << b2 << "].used = " << blocklets[b2].used);
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::getMiscMem: find space: blocklets[" << b2 << "].size = " << blocklets[b2].size);
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::getMiscMem: find space: blocklets[" << b2 << "].start = 0x" << HEX(blocklets[b2].start));

  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::getMiscMem: start_add = 0x" << HEX(*start_add));
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::getMiscMem: handle = " << *handle);

  m_miscmemLock->unlock();
}


/**********************************/
void Robin::retMiscMem(u_int handle)
/**********************************/
{
  int bprev = -1, bnext = -1;
  u_int loop;
  
  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::retMiscMem: called with handle = " << handle);

  m_miscmemLock->lock();
  
  if (blocklets[handle].used != BLOCKLET_USED)
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::retMiscMem: Invalid handle:");
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::retMiscMem: blocklets[" << handle << "].used  = " << blocklets[handle].used);
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::retMiscMem: blocklets[" << handle << "].start = 0x" << HEX(blocklets[handle].start));
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::retMiscMem: blocklets[" << handle << "].size  = 0x" << HEX(blocklets[handle].size));
    m_miscmemLock->unlock();
    CREATE_ROS_EXCEPTION(ex32, ROSRobinExceptions, MISCHANDLE, "");
    throw (ex32);
  }
   
  blocklets[handle].used = BLOCKLET_FREE;
    
  //Check if the blocklet can be merged with a predecessor   
  for(loop = 0; loop < s_max_msgs; loop++)
  {
    if (blocklets[loop].used == BLOCKLET_FREE && (blocklets[loop].start + blocklets[loop].size) == blocklets[handle].start)
    {
      DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::retMiscMem: predecessor: blocklets[" << loop << "].used    = " << blocklets[loop].used);
      DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::retMiscMem: predecessor: blocklets[" << loop << "].start   = 0x" << HEX(blocklets[loop].start));
      DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::retMiscMem: predecessor: blocklets[" << loop << "].size    = " << blocklets[loop].size);
      DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::retMiscMem:     current: blocklets[" << handle << "].start = 0x" << HEX(blocklets[handle].start));
      bprev = loop;
      break;
    }
  }
  
  //Check if the blocklet can be merged with a successor   
  for(loop = 0; loop < s_max_msgs; loop++)
  {
    if (blocklets[loop].used == BLOCKLET_FREE && (blocklets[handle].start + blocklets[handle].size) == blocklets[loop].start)
    {
      DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::retMiscMem: successor: blocklets[" << loop << "].used    = " << blocklets[loop].used);
      DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::retMiscMem: successor: blocklets[" << loop << "].start   = 0x" << HEX(blocklets[loop].start));
      DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::retMiscMem: successor: blocklets[" << loop << "].size    = " << blocklets[loop].size);
      DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::retMiscMem:   current: blocklets[" << handle << "].start = 0x" << HEX(blocklets[handle].start));
      bnext = loop;
      break;
    }
  }
  
  if (bprev != -1)
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::retMiscMem: merging " << handle << " with predecessor " << bprev);
    blocklets[handle].start = blocklets[bprev].start;
    blocklets[handle].size += blocklets[bprev].size;
    blocklets[bprev].used = BLOCKLET_UNUSED;
    blocklets[bprev].start = 0;
    blocklets[bprev].size = 0;
    DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::retMiscMem: After merge: size of blocklets[" << handle << "] = " << blocklets[handle].size);
  }
  
  if (bnext != -1)
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::retMiscMem: merging " << handle << " with successor " << bnext);
    blocklets[handle].size += blocklets[bnext].size;
    blocklets[bnext].used = BLOCKLET_UNUSED;
    blocklets[bnext].start = 0;
    blocklets[bnext].size = 0;
    DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::retMiscMem: After merge: size of blocklets[" << handle << "] = " << blocklets[handle].size);
  }

  m_miscmemLock->unlock();
}


/**********************************/
void Robin::setEepromDI(u_int value)
/**********************************/
{
  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::setEepromDI: Called");
  
  u_int regValue = readPlx(ROS::Robin::CNTRL);  // Read the current value
  u_int mask = 1 << 26;   // Set the mask to be bit 26

  // Set the input enable to zero because we want to write
  // This is written together with the value
  regValue &= ~(1 << 31);

  if (value) 
    writePlx(ROS::Robin::CNTRL, regValue | mask);    // Enable the CS by writing a 1
  else 
    writePlx(ROS::Robin::CNTRL, regValue & ~mask);   // Disable the CS by writing a 0

  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::setEepromDI: Done");
}


/****************************/
u_int Robin::getEepromDO(void)
/****************************/
{
  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::getEepromDO: Called");

  u_int regValue = readPlx(ROS::Robin::CNTRL);  // Read the current value

  // Check if the input enable bit was set
  if ((regValue & (1 << 31)) == 0) 
  {
    // No it was not! So we set it and read again
    regValue |= 1 << 31;
    writePlx(ROS::Robin::CNTRL, regValue);
    regValue = readPlx(ROS::Robin::CNTRL);
  }

  u_int mask = 1 << 27;   // Set the mask to be bit 27

  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::getEepromDO: Done");
  return (regValue & mask);
}


/******************************/
void Robin::pulseEepromClk(void)
/******************************/
{
  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::pulseEepromClk: Called");
  // Read the current value
  u_int regValue = readPlx(ROS::Robin::CNTRL);
  u_int mask = 1 << 24;

  writePlx(ROS::Robin::CNTRL, regValue & ~mask);  // Set the clock output to 0
  writePlx(ROS::Robin::CNTRL, regValue | mask);   // Set the clock output to 1
  writePlx(ROS::Robin::CNTRL, regValue & ~mask);  // Set the clock output to 0
  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::pulseEepromClk: Done");
}


/**********************************/
void Robin::setEepromCS(u_int value)
/**********************************/
{
  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::setEepromCS: Called");

  u_int regValue = readPlx(ROS::Robin::CNTRL);       // Read the current value
  u_int mask = 1 << 25;                              // Set the mask to be bit 25

  if (value) 
    writePlx(ROS::Robin::CNTRL, regValue | mask);    // Enable the CS by writing a 1
  else 
  {
    // Disable the CS by writing a 0 Also make sure the EEPROM control bit30 -EEDO- is reset
    regValue &= ~(1 << 31);
    writePlx(ROS::Robin::CNTRL, regValue & ~mask);
  }
  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::setEepromCS: Done");
}


/*************************************************/
u_int Robin::waitForInterrupt(T_irq_info *irq_info)
/*************************************************/
{
  int ret, eno;
  
  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::waitForInterrupt: Called");
  
  ret = ioctl(m_dev_handle, WAIT_IRQ, irq_info);
  eno = errno;
  if (ret != 0 && eno != RD_INTBYSIGNAL)
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5, "Robin::waitForInterrupt: Error from ioctl, errno = " << eno << ", ret = " << ret);
    char err_str[256];
    sprintf(err_str, "\nThis error came from ioctl(WAIT_IRQ). %s\n", ioctl_strings[errno]);
    CREATE_ROS_EXCEPTION(ex7, ROSRobinExceptions, IOCTL, "This error came from ioctl(WAIT_IRQ). " << ioctl_strings[errno]);
    throw (ex7);
  }
  if (ret != 0 && eno == RD_INTBYSIGNAL)
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 20, "Robin::waitForInterrupt: ioctl(WAIT_IRQ) terminated by signal");
    return(RD_INTBYSIGNAL);
  }
  
  DEBUG_TEXT(DFDB_ROSROBIN, 15, "Robin::waitForInterrupt: Done");
  return(ret);
}


/****************************/
u_int Robin::getIdentity(void)
/****************************/
{
  return(m_slot_number);
}

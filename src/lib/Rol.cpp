/****************************************************************/
/* file   Rol.cpp      						*/
/*                     						*/
/* author: Markus Joos 						*/
/*                     						*/
/*****C 2006 - The software with that certain something**********/

#include <iostream>
#include "ROSRobin/Robin.h"
#include "ROSRobin/Rol.h"
#include "ROSRobin/ROSRobinExceptions.h"
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "rcc_time_stamp/tstamp.h"

using namespace ROS;

const u_int ROS::Rol::c_DataChunkSize = 0x70;
const u_int ROS::Rol::c_order = 16;  

/*************************************************/
u_short Rol::crctablefast16(u_short* p, u_long len) 
/*************************************************/
{
  u_long crc = crcinit_direct;
  const u_long crcxor = 0x0000;

  while (len--) 
    crc = (crc << 16) ^ crctab16[((crc >> (c_order - 16)) & 0xffff) ^ *p++];

  crc ^= crcxor;
  crc &= crcmask;
  return((u_short)crc);
}


/*****************************************************************************/
Rol::Rol(Robin & robinBoard, u_int rolId) : m_robin(robinBoard), m_rolId(rolId)
/*****************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::constructor: Called");
  
  //CRC stuff
  u_long crchighbit, bit, crc, i, j;
  const u_long crcinit = 0xffff, polynom = 0x1021;
  m_crc_interval = 0xffffffff;

  //compute constant bit masks for whole CRC and CRC high bit
  crcmask = ((((u_long) 1 << (c_order - 1)) - 1) << 1) | 1;
  crchighbit = (u_long) 1 << (c_order - 1);

  //compute CRC table
  for (i = 0; i < 65536; i++) 
  {
    crc = i;

    for (j = 0; j < 16; j++) 
    {
      bit = crc & crchighbit;
      crc <<= 1;
      if (bit) 
        crc ^= polynom;
    }			

    crc &= crcmask;
    crctab16[i] = crc; 
  }

  //compute missing initial CRC value
  crcinit_direct = crcinit;
  crc = crcinit;
  for (i = 0; i < c_order; i++) 
  {
    bit = crc & 1;
    if (bit) 
      crc ^= polynom;
    crc >>= 1;
    if (bit) 
      crc |= crchighbit;
  }	
}


/*********/
Rol::~Rol() 
/*********/
{
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::destructor: Called");
}


/*************************************************************/
u_long Rol::requestFragment(u_int eventId, u_long replyAddress) 
/*************************************************************/
{
  DEBUG_TEXT(DFDB_ROSROBIN, 10 ,"Rol::requestFragment: Called with eventId = " << eventId << " and replyAddress = 0x" << HEX(replyAddress));
  
  m_robin.sendMsg(m_rolId, enum_reqFragment, replyAddress, eventId);
  return(replyAddress);    //MJ: ATTENTION on 64-bit platforms. We get a u_long and return it in a u_int
}


/*************************************/
u_int * Rol::getFragment(u_long ticket) 
/*************************************/
{
  RobMsgHdr *rob_header;
  u_int *ptr, tfs, loop;
  u_short crc1, crc2, crc_result;
  
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::getFragment: Called with ticket = 0x" << HEX(ticket));
  m_robin.receiveMsg(ticket, true);  //blocking wait
  
  rob_header = reinterpret_cast<RobMsgHdr *> (ticket);
  ptr = (u_int *)(u_long)rob_header;

  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getFragment: rob_header is at " << HEX((u_long)rob_header));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getFragment: rob_header->totalSize = " << rob_header->totalSize);
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getFragment: rob_header->fragStat = " << HEX(rob_header->fragStat));

  //CRC stuff
  tfs = rob_header->totalSize;
  
  u_int l1id = ptr[rob_header->hdrSize + 5];
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getFragment: L1ID = " << HEX(l1id));
  
  bool istruncated = false;
  if (rob_header->fragStat && enum_fragStatusTrunc)
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getFragment: I will not check the CRC of this truncated fragment");
    istruncated = true;
  }
  
  if ((m_crc_interval && (l1id % m_crc_interval) == 0) && !istruncated)
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getFragment: ROD-trailer(1) = " << HEX(ptr[tfs - 4]));
    DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getFragment: ROD-trailer(2) = " << HEX(ptr[tfs - 3]));
    DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getFragment: ROD-trailer(3) = " << HEX(ptr[tfs - 2]));
    DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getFragment: CRC = " << HEX(ptr[tfs - 1]));
    crc1 = ptr[tfs - 1] & 0xffff;
    crc2 = (ptr[tfs - 1] >> 16) & 0xffff;

    u_int asize = tfs - rob_header->hdrSize - 1;  //The "-1" is for the rob_trailer (aka CRC)

    u_short *ev1, *ev2;
    ev1 = (u_short *) malloc(asize * 2);
    if (ev1 == NULL)
    {
      DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getFragment: ev1 is NULL");
      CREATE_ROS_EXCEPTION(ex1, ROSRobinExceptions, MALLOC, "malloc failed for ev1");
      throw (ex1);
    }  
    ev2 = (u_short *) malloc(asize * 2);
    if (ev2 == NULL)
    {
      DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getFragment: ev2 is NULL");
      CREATE_ROS_EXCEPTION(ex2, ROSRobinExceptions, MALLOC, "malloc failed for ev2");
      throw (ex2);
    }  

    //Copy the data
    for (loop = 0; loop < asize; loop++)
    {
      ev1[loop] = ptr[rob_header->hdrSize + loop] & 0xffff;
      ev2[loop] = (ptr[rob_header->hdrSize + loop] >> 16) & 0xffff;
    }

    DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getFragment: Two sub-events of " << asize << " elements created");

    DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getFragment: Event-LSW. Expected CRC = "<< HEX(crc1));
    crc_result = crctablefast16(ev1, asize);
    if (crc_result != crc1)
    {
      DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::getFragment: ERROR in LSW-CRC. Computed CRC = "<< HEX(crc_result));
      u_int rslot = m_robin.getIdentity();
      CREATE_ROS_EXCEPTION(ex1, ROSRobinExceptions, BADCRC, "Rol::getFragment: ERROR in LSW-CRC. Robin = " << rslot << " L1ID = 0x" << HEX(l1id) << " Computed CRC = 0x" << HEX(crc_result) << " Expected CRC = 0x" << HEX(crc1));
      ers::warning(ex1);
    }
    
    DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getFragment: Event-MSW. Expected CRC = "<< HEX(crc2));
    crc_result = crctablefast16(ev2, asize);
    if (crc_result != crc2)
    {
      DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::getFragment: ERROR in MSW-CRC. Computed CRC = "<< HEX(crc_result));
      u_int rslot = m_robin.getIdentity();
      CREATE_ROS_EXCEPTION(ex2, ROSRobinExceptions, BADCRC, "Rol::getFragment: ERROR in MSW-CRC. Robin = " << rslot << " L1ID = 0x" << HEX(l1id) << " Computed CRC = 0x" << HEX(crc_result) << " Expected CRC = 0x" << HEX(crc2));
      ers::warning(ex2);
    }
   
    free(ev1);
    free(ev2);
  }

  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::getFragment: Done");
  return(reinterpret_cast<u_int *> (ticket));   
}


/*************************************************************/
u_int Rol::releaseFragment(const std::vector <u_int> *eventIds)
/*************************************************************/
{
  u_int done = 0, notdeleted = 0, handle, nrOfIds, data[MAX_L1IDS + 1];
  u_long replyAddress;

  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::releaseFragment: Called");
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::releaseFragment: The vector contains " << eventIds->size() << " L1IDs");  
  
  std::vector<u_int>::const_iterator i = eventIds->begin();
  while(!done)
  {
    nrOfIds = 0;
    
    while(i < eventIds->end())
    {
      nrOfIds++;
      //Yes, we have to copy the L1Ids from the STL vector to a u_int array
      data[nrOfIds] = *i;
      DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::releaseFragment: ID (to be deleted) = " << data[nrOfIds]);  
      
      i++;	
      if (nrOfIds == MAX_L1IDS)
      {
        DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::releaseFragment: 100 L1IDs copied");  
        break;
      }
    }
    DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::releaseFragment: nrOfIds = " << nrOfIds);
    
    if (nrOfIds < MAX_L1IDS)
    {
      done = 1;
      DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::releaseFragment: This is the last sub-group");
    } 
    
    if (nrOfIds)
    {
      data[0] = nrOfIds;
      u_int size = sizeof(RespMsg) + (nrOfIds + 2) * sizeof(int);  //2 extra words for "most recent L1ID" and "number of undeleted events"
      m_robin.getMiscMem(size, &replyAddress, &handle);
      m_robin.sendMsg(m_rolId, enum_reqClearAck, replyAddress, nrOfIds + 1, data);
      m_robin.receiveMsg(replyAddress, true);  //blocking wait

      //Check the reply
      RespMsg *responseHeader = reinterpret_cast<RespMsg *> (replyAddress); 
      DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::releaseFragment: responseHeader->marker   = 0x" << HEX(responseHeader->marker));
      DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::releaseFragment: responseHeader->size     = 0x" << HEX(responseHeader->size));
      DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::releaseFragment: responseHeader->version  = 0x" << HEX(responseHeader->version));
      DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::releaseFragment: responseHeader->response = 0x" << HEX(responseHeader->response));

      if (responseHeader->marker != 0x33fedc33) //MJ: Why is there no constant in robin_ppc/robin.h?
      {
	DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::releaseFragment: Robin returns invalid reply marker");
	CREATE_ROS_EXCEPTION(ex2, ROSRobinExceptions, WRONGREPLY, "The ROBIN reply marker was 0x" << HEX(responseHeader->marker) << " instead of 0x33fedc33 when releasing events!");
        m_robin.retMiscMem(handle); 
	throw (ex2);
      }

      if ((responseHeader->response & 0xFF) != enum_respClearAck) 
      {
	DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::releaseFragment: Robin returns invalid response ID");
	CREATE_ROS_EXCEPTION(ex3, ROSRobinExceptions, WRONGREPLY, "The ROBIN did not reply with a clear acknowlege package on a clear request message!");
        m_robin.retMiscMem(handle); 
	throw (ex3);
      }

      u_int *ptr = reinterpret_cast<u_int *> (replyAddress);  
      if (ptr[5])
      {
	DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::releaseFragment: The most recent L1ID is " << ptr[4]);
	DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::releaseFragment: The Robin did not delete " << ptr[5] << " L1IDs. Now dumping un-deleted L1IDs");
	for (u_int loop = 0; loop < ptr[5]; loop++)
	  DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::releaseFragment: L1ID " << ptr[loop + 6] << " has not been deleted");
	//MJ: Maybe this request has to be requeued one time. For now just pass the number of undeleted events back

        CREATE_ROS_EXCEPTION(ex1, ROSRobinExceptions, BADDELETE, "Rol::releaseFragment: The Robin could not delete " 
	<< ptr[5] << " events because they were not in its buffer. This concerns for example L1ID = 0x" 
	<< HEX(ptr[6]) << " The most recently received L1ID is 0x" << HEX(ptr[4]));

        ers::warning(ex1);
      }
      notdeleted += ptr[5];
      //Return the misc. memory
      m_robin.retMiscMem(handle); 
    }
  }
  
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::releaseFragment: Done");
  return(notdeleted);
}


/******************************************************************/
u_int Rol::releaseFragmentNoack(const std::vector <u_int> *eventIds)
/******************************************************************/
{
  u_int done = 0, handle;
  u_int data[MAX_L1IDS + 1];
  u_int nrOfIds;
  u_long replyAddress;

  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::releaseFragmentNoack: Called");
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::releaseFragmentNoack: The vector contains " << eventIds->size() << " L1IDs");  
  
  std::vector<u_int>::const_iterator i = eventIds->begin();
  while(!done)
  {
    nrOfIds = 0;
    
    while(i < eventIds->end())
    {
      nrOfIds++;
      //Yes, we have to copy the L1Ids from the STL vector to a u_int array
      data[nrOfIds] = *i;
      DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::releaseFragmentNoack: ID (to be deleted) = " << data[nrOfIds]);  
      
      i++;	
      if (nrOfIds == MAX_L1IDS)
      {
        DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::releaseFragmentNoack: 100 L1IDs copied");  
        break;
      }
    }
    DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::releaseFragmentNoack: nrOfIds = " << nrOfIds);
    
    if (nrOfIds < MAX_L1IDS)
    {
      done = 1;
      DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::releaseFragmentNoack: This is the last sub-group");
    } 
    
    if (nrOfIds)
    {
      data[0] = nrOfIds;
      u_int size = sizeof(RespMsg);  
      m_robin.getMiscMem(size, &replyAddress, &handle);
      m_robin.sendMsg(m_rolId, enum_reqClear, replyAddress, nrOfIds + 1, data);
      m_robin.receiveMsg(replyAddress, true);  //blocking wait

      //Check the reply
      RespMsg *responseHeader = reinterpret_cast<RespMsg *> (replyAddress); 
      DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::releaseFragmentNoack: responseHeader->marker   = 0x" << HEX(responseHeader->marker));
      DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::releaseFragmentNoack: responseHeader->size     = 0x" << HEX(responseHeader->size));
      DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::releaseFragmentNoack: responseHeader->version  = 0x" << HEX(responseHeader->version));
      DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::releaseFragmentNoack: responseHeader->response = 0x" << HEX(responseHeader->response));

      if (responseHeader->marker != 0x33fedc33) 
      {
	DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::releaseFragmentNoack: Robin returns invalid reply marker");
	CREATE_ROS_EXCEPTION(ex2, ROSRobinExceptions, WRONGREPLY, "The ROBIN reply marker was 0x" << HEX(responseHeader->marker) << " instead of 0x33fedc33 when releasing events!");
        m_robin.retMiscMem(handle); 
	throw (ex2);
      }

      if ((responseHeader->response & 0xFF) != enum_respAck) 
      {
	DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::releaseFragmentNoack: Robin returns invalid response ID");
	CREATE_ROS_EXCEPTION(ex3, ROSRobinExceptions, WRONGREPLY, "The ROBIN did not reply with an acknowlege to a clear request message!");
        m_robin.retMiscMem(handle); 
	throw (ex3);
      }

      //Return the misc. memory
      m_robin.retMiscMem(handle); 
    }
  }
  
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::releaseFragmentNoack: Done");
  return(0);
}


/****************************************************************/
u_int Rol::releaseFragmentAll(const std::vector <u_int> *eventIds)
/****************************************************************/
{
  u_int done = 0, handle, nrOfIds, data[MAX_L1IDS + 1];
  u_long replyAddress;
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::releaseFragmentAll: Called");
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::releaseFragmentAll: The vector contains " << eventIds->size() << " L1IDs");  
  
  std::vector<u_int>::const_iterator i = eventIds->begin();
  while(!done)
  {
    nrOfIds = 0;
    
    while(i < eventIds->end())
    {
      nrOfIds++;
      //Yes, we have to copy the L1Ids from the STL vector to a u_int array
      data[nrOfIds] = *i;
      DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::releaseFragmentAll: ID (to be deleted) = " << data[nrOfIds]);  
      
      i++;	
      if (nrOfIds == MAX_L1IDS)
      {
        DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::releaseFragmentAll: 100 L1IDs copied");  
        break;
      }
    }
    DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::releaseFragmentAll: nrOfIds = " << nrOfIds);
    
    if (nrOfIds < MAX_L1IDS)
    {
      done = 1;
      DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::releaseFragmentAll: This is the last sub-group");
    } 
    
    if (nrOfIds)
    {
      data[0] = nrOfIds;
      u_int size = sizeof(RespMsg); 
      m_robin.getMiscMem(size, &replyAddress, &handle);
      m_robin.sendMsg(m_rolId, enum_reqClearAllRols, replyAddress, nrOfIds + 1, data);
      m_robin.receiveMsg(replyAddress, true);  //blocking wait

      //Check the reply
      RespMsg *responseHeader = reinterpret_cast<RespMsg *> (replyAddress); 
      DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::releaseFragmentAll: responseHeader->marker   = 0x" << HEX(responseHeader->marker));
      DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::releaseFragmentAll: responseHeader->size     = 0x" << HEX(responseHeader->size));
      DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::releaseFragmentAll: responseHeader->version  = 0x" << HEX(responseHeader->version));
      DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::releaseFragmentAll: responseHeader->response = 0x" << HEX(responseHeader->response));

      if (responseHeader->marker != 0x33fedc33) //MJ: Why is there no constant in robin_ppc/robin.h?
      {
	DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::releaseFragmentAll: Robin returns invalid reply marker");
	CREATE_ROS_EXCEPTION(ex2, ROSRobinExceptions, WRONGREPLY, "The ROBIN reply marker was 0x" << HEX(responseHeader->marker) << " instead of 0x33fedc33 when releasing events!");
        m_robin.retMiscMem(handle); 
	throw (ex2);
      }

      if ((responseHeader->response & 0xFF) != enum_respAck) 
      {
	DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::releaseFragmentAll: Robin returns invalid response ID");
	CREATE_ROS_EXCEPTION(ex3, ROSRobinExceptions, WRONGREPLY, "The ROBIN did not reply with an acknowlege to a clearAll request message!");
        m_robin.retMiscMem(handle); 
	throw (ex3);
      }

      //Return the misc. memory
      m_robin.retMiscMem(handle); 
    }
  }
  
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::releaseFragmentAll: Done");
  return(0);
}


/**************************************/
StatisticsBlock Rol::getStatistics(void) 
/**************************************/
{
  u_long replyAddress;
  u_int handle;
  StatisticsBlock *sb, ret;
  
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::getStatistics: Called");
 
  u_int size = sizeof(RespMsg) + sizeof(StatisticsBlock);
  m_robin.getMiscMem(size, &replyAddress, &handle);
  m_robin.sendMsg(m_rolId, enum_reqStatusMonitorData, replyAddress, enum_statVersion);
  m_robin.receiveMsg(replyAddress, true);  //blocking wait
  
  RespMsg *responseHeader = reinterpret_cast<RespMsg *> (replyAddress); 
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getStatistics: responseHeader->marker   = 0x" << HEX(responseHeader->marker));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getStatistics: responseHeader->size     = 0x" << HEX(responseHeader->size));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getStatistics: responseHeader->version  = 0x" << HEX(responseHeader->version));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getStatistics: responseHeader->response = 0x" << HEX(responseHeader->response));
  
  if (responseHeader->marker != 0x33fedc33) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::getStatistics: Robin returns invalid reply marker");
    CREATE_ROS_EXCEPTION(ex4, ROSRobinExceptions, WRONGREPLY, "The ROBIN reply marker was 0x" << HEX(responseHeader->marker) << " instead of 0x33fedc33 when trying to read statistics!");
    m_robin.retMiscMem(handle);
    throw (ex4);
  }
  
  if ((responseHeader->response & 0xFF) == enum_respNack) 
  {
    u_int *vptr = reinterpret_cast<u_int *> (replyAddress + sizeof(RespMsg));
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::getStatistics: Statistics versions are not compatible. The ROBIN can only provide statistics blocks of version 0x" << HEX(*vptr) << " and you have asked for version 0x" << HEX(enum_statVersion));
    CREATE_ROS_EXCEPTION(ex5, ROSRobinExceptions, WRONGVERSION, "The ROBIN can only provide statistics blocks of version 0x" << HEX(*vptr) << " and you have asked for version 0x" << HEX(enum_statVersion));
    m_robin.retMiscMem(handle);
    throw (ex5);
  }
  
  if ((responseHeader->response & 0xFF) != enum_respStatusMonitorData) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::getStatistics: Robin returns invalid response ID");
    CREATE_ROS_EXCEPTION(ex6, ROSRobinExceptions, WRONGREPLY, "The ROBIN did not reply with a statistics package when trying to get the statistics!");
    m_robin.retMiscMem(handle);
    throw (ex6);
  }
  
  sb = reinterpret_cast<StatisticsBlock *> (replyAddress + sizeof(RespMsg));
  ret = *sb;  //Copy the status block

  //Return the misc. memory
  m_robin.retMiscMem(handle);

  return(ret);  
}


/*****************************/
u_int Rol::getTemperature(void)
/*****************************/
{
  u_int *tptr, handle;
  u_long replyAddress;
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::getTemperature: Called");

  u_int size = sizeof(RespMsg) + sizeof(int);
  m_robin.getMiscMem(size, &replyAddress, &handle);
  m_robin.sendMsg(m_rolId, enum_reqTempVal, replyAddress);
  m_robin.receiveMsg(replyAddress, true);  //blocking wait
  RespMsg *responseHeader = reinterpret_cast<RespMsg *> (replyAddress); 

  if (responseHeader->marker != 0x33fedc33) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::getTemperature: Robin returns invalid reply marker");
    CREATE_ROS_EXCEPTION(ex6, ROSRobinExceptions, WRONGREPLY, "The ROBIN reply marker was 0x" << HEX(responseHeader->marker) << " instead of 0x33fedc33 when trying to read the temperature!");
    m_robin.retMiscMem(handle);
    throw (ex6);
  }

  if ((responseHeader->response & 0xFF) != enum_respAck) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::getTemperature: Robin returns invalid response ID");
    CREATE_ROS_EXCEPTION(ex7, ROSRobinExceptions, WRONGREPLY, "The ROBIN did not reply with an ACK when trying to read the temperature!");
    m_robin.retMiscMem(handle);
    throw (ex7);
  }

  tptr = reinterpret_cast<u_int *> (replyAddress + sizeof(RespMsg));
  
  u_int rtemp = (u_int) ((float)(*tptr));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getTemperature: Encoded temperature is " << *tptr);
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getTemperature: The temperature is about " << rtemp << " deg. C");
  
  //Return the misc. memory
  m_robin.retMiscMem(handle);

  return(rtemp);
}


/***************************************/
Rol::ECRStatisticsBlock Rol::getECR(void)
/***************************************/
{
  u_long replyAddress;
  u_int handle, *sb, loop;
  ECRStatisticsBlock ret;

  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::getECR: Called");
 
  u_int size = sizeof(RespMsg) + sizeof(int) * (3 + EVENT_LOG_SIZE); 
  m_robin.getMiscMem(size, &replyAddress, &handle);
  m_robin.sendMsg(m_rolId, enum_reqEventLog, replyAddress);
  m_robin.receiveMsg(replyAddress, true);  //blocking wait
  
  RespMsg *responseHeader = reinterpret_cast<RespMsg *> (replyAddress); 
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getECR: responseHeader->marker   = 0x" << HEX(responseHeader->marker));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getECR: responseHeader->size     = 0x" << HEX(responseHeader->size));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getECR: responseHeader->version  = 0x" << HEX(responseHeader->version));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getECR: responseHeader->response = 0x" << HEX(responseHeader->response));
  
  if (responseHeader->marker != 0x33fedc33) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::getECR: Robin returns invalid reply marker");
    CREATE_ROS_EXCEPTION(ex8, ROSRobinExceptions, WRONGREPLY, "The ROBIN reply marker was 0x" << HEX(responseHeader->marker) << " instead of 0x33fedc33 when trying to read ECR statistics!");
    m_robin.retMiscMem(handle);
    throw (ex8);
  }

  if ((responseHeader->response & 0xFF) != enum_respEventLog) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::getECR: Robin returns invalid response ID");
    CREATE_ROS_EXCEPTION(ex9, ROSRobinExceptions, WRONGREPLY, "The ROBIN did not reply with a ECR statistics package when trying to get the statistics!");
    m_robin.retMiscMem(handle);
    throw (ex9);
  }

  sb = reinterpret_cast<u_int *> (replyAddress + sizeof(RespMsg));
  ret.mostRecentId = sb[0];
  ret.overflow = sb[2];
  ret.necrs = 0; 
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getECR: most recent ID = 0x" << HEX(ret.mostRecentId));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getECR: overflow       = " << ret.overflow);
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getECR: # ECRs         = " << sb[1]);

  for(loop = 0; loop < sb[1]; loop++)
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getECR: ECR # " << loop << " at 0x" << HEX(sb[3 + loop]));
    if (sb[3 + loop] != 0xffffffff)
    {
      ret.ecr[ret.necrs] = sb[3 + loop];
      ret.necrs++;
    } 
  }
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getECR: Effective # ECRs         = " << ret.necrs);
    
  //Return the misc. memory
  m_robin.retMiscMem(handle);

  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::getECR: Done");
  return(ret);   
}


/*******************************************/
Rol::GCBlock Rol::collectGarbage(int eventId)
/*******************************************/
{
  GCBlock ret;
  u_long replyAddress;
  u_int handle, *extra_data;

  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::collectGarbage: Called");
  
  u_int size = sizeof(RespMsg) + 2 * sizeof(int); 
  m_robin.getMiscMem(size, &replyAddress, &handle);
  m_robin.sendMsg(m_rolId, enum_reqGarbCollect, replyAddress, eventId);
  m_robin.receiveMsg(replyAddress, true);  //blocking wait
  
  RespMsg *responseHeader = reinterpret_cast<RespMsg *> (replyAddress); 
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::collectGarbage: responseHeader->marker   = 0x" << HEX(responseHeader->marker));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::collectGarbage: responseHeader->size     = 0x" << HEX(responseHeader->size));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::collectGarbage: responseHeader->version  = 0x" << HEX(responseHeader->version));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::collectGarbage: responseHeader->response = 0x" << HEX(responseHeader->response));
  
  if (responseHeader->marker != 0x33fedc33) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::collectGarbage: Robin returns invalid reply marker");
    CREATE_ROS_EXCEPTION(ex10, ROSRobinExceptions, WRONGREPLY, "The ROBIN reply marker was 0x" << HEX(responseHeader->marker) << " instead of 0x33fedc33 when trying to trigger garbage collection!");
    m_robin.retMiscMem(handle);
    throw (ex10);
  }

  if ((responseHeader->response & 0xFF) != enum_respAck) 
  {
    ret.done = 0;
    ret.deleted = 0;
    ret.free = 0;
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::collectGarbage: Garbage collection did not work or has not been executed");
  }
  else
  {
    extra_data = reinterpret_cast<u_int *> (replyAddress + sizeof(RespMsg));
    ret.done = 1;
    ret.deleted = extra_data[0];
    ret.free = extra_data[1];
    DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::collectGarbage: # of events deleted = " << extra_data[0]);
    DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::collectGarbage: # of free pages     = " << extra_data[1]);
  }
  
  //Return the misc. memory
  m_robin.retMiscMem(handle);

  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::collectGarbage: Done");
  return(ret);   
}


/************************************/
std::vector <CfgParm> Rol::getConfig() 
/************************************/
{
  u_long replyAddress;
  u_int handle;
  std::vector < CfgParm > ret;
  CfgParm *cfgp;

  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::getConfig: Called");

  u_int size = sizeof(RespMsg) + sizeof(CfgParm) * enum_cfgNumItems;
  m_robin.getMiscMem(size, &replyAddress, &handle);
  m_robin.sendMsg(m_rolId, enum_reqGetConfig, replyAddress, enum_cfgVersion);
  m_robin.receiveMsg(replyAddress, true);  //blocking wait

  RespMsg *responseHeader = reinterpret_cast<RespMsg *> (replyAddress);  
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getConfig: responseHeader->marker   = 0x" << HEX(responseHeader->marker));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getConfig: responseHeader->size     = 0x" << HEX(responseHeader->size));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getConfig: responseHeader->version  = 0x" << HEX(responseHeader->version));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getConfig: responseHeader->response = 0x" << HEX(responseHeader->response));

  if (responseHeader->marker != 0x33fedc33) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::getConfig: Robin returns invalid reply marker");
    CREATE_ROS_EXCEPTION(ex11, ROSRobinExceptions, WRONGREPLY, "The ROBIN reply marker was 0x" << HEX(responseHeader->marker) << " instead of 0x33fedc33 when trying to read statistics!");
    m_robin.retMiscMem(handle);
    throw (ex11);
  }

  if ((responseHeader->response & 0xFF) == enum_respNack) 
  {
    u_int *vptr = reinterpret_cast<u_int *> (replyAddress + sizeof(RespMsg));
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::getConfig: Config. versions are not compatible");
    CREATE_ROS_EXCEPTION(ex12, ROSRobinExceptions, WRONGVERSION, "The ROBIN can only provide config. blocks of version " << *vptr << " ! and you have asked for version " << enum_cfgVersion);
    m_robin.retMiscMem(handle);
    throw (ex12);
  }

  if ((responseHeader->response & 0xFF) != enum_respCfgItem) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::getConfig: Robin returns invalid response ID");
    CREATE_ROS_EXCEPTION(ex13, ROSRobinExceptions, WRONGREPLY, "The ROBIN did not reply with a config package when trying to get the configuration!");
    m_robin.retMiscMem(handle);
    throw (ex13);
  }
  
  for(u_int loop = 0; loop < enum_cfgNumItems; loop++)
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getConfig: Copying CfgParm structure #" << loop);
    cfgp = reinterpret_cast<CfgParm *> (replyAddress + sizeof(RespMsg) + loop * sizeof(CfgParm));
    ret.push_back(*cfgp);
  } 

  //Return the misc. memory
  m_robin.retMiscMem(handle);
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::getConfig: Done");
  return(ret);
}


/*******************************************/
void Rol::setConfig(u_int index, u_int value) 
/*******************************************/
{
  u_long replyAddress;
  u_int handle;
  CfgParm *cfgp;

  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::setConfig: Called with index = " << index << " and value = " << value);

  u_int size = sizeof(RespMsg) + sizeof(CfgParm) * enum_cfgNumItems; 
  m_robin.getMiscMem(size, &replyAddress, &handle);
  
  u_int data[3]; 
  data[0] = enum_cfgVersion;
  data[1] = index;
  data[2] = value;
  m_robin.sendMsg(m_rolId, enum_reqSetConfig, replyAddress, 3, &data[0]);
  m_robin.receiveMsg(replyAddress, true);  //blocking wait

  RespMsg *responseHeader = reinterpret_cast<RespMsg *> (replyAddress);
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::setConfig: responseHeader->marker   = 0x" << HEX(responseHeader->marker));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::setConfig: responseHeader->size     = 0x" << HEX(responseHeader->size));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::setConfig: responseHeader->version  = 0x" << HEX(responseHeader->version));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::setConfig: responseHeader->response = 0x" << HEX(responseHeader->response));

  if (responseHeader->marker != 0x33fedc33) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::setConfig: Robin returns invalid reply marker");
    CREATE_ROS_EXCEPTION(ex13, ROSRobinExceptions, WRONGREPLY, "The ROBIN reply marker was 0x" << HEX(responseHeader->marker) << " instead of 0x33fedc33 when trying to read statistics!");
    m_robin.retMiscMem(handle);  
    throw (ex13);
  }
  
  if ((responseHeader->response & 0xFF) == enum_respNack) 
  {
    u_int *vptr = reinterpret_cast<u_int *> (replyAddress + sizeof(RespMsg));
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::setConfig: Config. versions are not compatible");
    CREATE_ROS_EXCEPTION(ex14, ROSRobinExceptions, WRONGVERSION, "The ROBIN can only deal with config. data of version " << *vptr << " !");
    m_robin.retMiscMem(handle);  
    throw (ex14);
  }

  if ((responseHeader->response & 0xFF) != enum_respCfgItem) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::setConfig: Robin returns invalid response ID");
    CREATE_ROS_EXCEPTION(ex15, ROSRobinExceptions, WRONGREPLY, "The ROBIN did not reply with a config package when trying to set a configuration parameter!");
    m_robin.retMiscMem(handle);  
    throw (ex15);
  }
  
  cfgp = reinterpret_cast<CfgParm *> (replyAddress + sizeof(RespMsg));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::setConfig: New value of" << cfgp[index].name << " is " << cfgp[index].value);
  if((value != cfgp[index].value) && (cfgp[index].dynamic == 1))
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::setConfig: The Robin did not accept new value " << value << " for parameter " << cfgp[index].name);
    CREATE_ROS_EXCEPTION(ex16, ROSRobinExceptions, WRONGREPLY, "The ROBIN did not accept the value " << value << " for parameter " << cfgp[index].name);
    m_robin.retMiscMem(handle);  
    throw (ex16);
  }

  //Return the misc. memory
  m_robin.retMiscMem(handle);  
}


/***************/
void Rol::reset() 
/***************/
{
  u_long replyAddress;
  u_int handle;

  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::reset: Called");

  u_int size = sizeof(RespMsg); 
  m_robin.getMiscMem(size, &replyAddress, &handle);
  m_robin.sendMsg(m_rolId, enum_reqRestart, replyAddress);
  m_robin.receiveMsg(replyAddress, true);  //blocking wait

  RespMsg *responseHeader = reinterpret_cast<RespMsg *> (replyAddress);
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::reset: responseHeader->marker   = 0x" << HEX(responseHeader->marker));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::reset: responseHeader->size     = 0x" << HEX(responseHeader->size));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::reset: responseHeader->version  = 0x" << HEX(responseHeader->version));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::reset: responseHeader->response = 0x" << HEX(responseHeader->response));

  if (responseHeader->marker != 0x33fedc33) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::reset: Robin returns invalid reply marker");
    CREATE_ROS_EXCEPTION(ex16, ROSRobinExceptions, WRONGREPLY, "The ROBIN reply marker was 0x" << HEX(responseHeader->marker) << " instead of 0x33fedc33 when trying to reset a ROL!");
    m_robin.retMiscMem(handle);  
    throw (ex16);
  }

  if ((responseHeader->response & 0xFF) != enum_respAck) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::reset: Robin returns invalid response ID");
    CREATE_ROS_EXCEPTION(ex17, ROSRobinExceptions, WRONGREPLY, "The ROBIN did not reply with an ACK when trying to reset a ROL!");
    m_robin.retMiscMem(handle);  
    throw (ex17);
  }
  
  //Return the misc. memory
  m_robin.retMiscMem(handle);  
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::reset: Done");
}


/*************************/
void Rol::clearStatistics() 
/*************************/
{
  u_long replyAddress;
  u_int handle;

  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::clearStatistics: Called");

  u_int size = sizeof(RespMsg); 
  m_robin.getMiscMem(size, &replyAddress, &handle);
  m_robin.sendMsg(m_rolId, enum_reqClearMonitorData, replyAddress);
  m_robin.receiveMsg(replyAddress, true);  //blocking wait

  RespMsg *responseHeader = reinterpret_cast<RespMsg *> (replyAddress);
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::clearStatistics: responseHeader->marker   = 0x" << HEX(responseHeader->marker));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::clearStatistics: responseHeader->size     = 0x" << HEX(responseHeader->size));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::clearStatistics: responseHeader->version  = 0x" << HEX(responseHeader->version));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::clearStatistics: responseHeader->response = 0x" << HEX(responseHeader->response));

  if (responseHeader->marker != 0x33fedc33) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::clearStatistics: Robin returns invalid reply marker");
    CREATE_ROS_EXCEPTION(ex18, ROSRobinExceptions, WRONGREPLY, "The ROBIN reply marker was 0x" << HEX(responseHeader->marker) << " instead of 0x33fedc33 when trying to clear the statistics data!");
    m_robin.retMiscMem(handle);  
    throw (ex18);
  }

  if ((responseHeader->response & 0xFF) != enum_respAck) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::clearStatistics: Robin returns invalid response ID");
    CREATE_ROS_EXCEPTION(ex19, ROSRobinExceptions, WRONGREPLY, "The ROBIN did not reply with an ACK when trying to trying to clear the statistics data!");
    m_robin.retMiscMem(handle);  
    throw (ex19);
  }
  
  //Return the misc. memory
  m_robin.retMiscMem(handle);  
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::clearStatistics: Done");
}


/**************/
void Rol::ping() 
/**************/
{
  u_long replyAddress;
  u_int handle;

  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::ping: Called");

  u_int size = sizeof(RespMsg); 
  m_robin.getMiscMem(size, &replyAddress, &handle);
  m_robin.sendMsg(m_rolId, enum_reqPing, replyAddress);
  m_robin.receiveMsg(replyAddress, true);  //blocking wait

  RespMsg *responseHeader = reinterpret_cast<RespMsg *> (replyAddress);
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::ping: responseHeader->marker   = 0x" << HEX(responseHeader->marker));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::ping: responseHeader->size     = 0x" << HEX(responseHeader->size));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::ping: responseHeader->version  = 0x" << HEX(responseHeader->version));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::ping: responseHeader->response = 0x" << HEX(responseHeader->response));

  if (responseHeader->marker != 0x33fedc33) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::ping: Robin returns invalid reply marker");
    CREATE_ROS_EXCEPTION(ex20, ROSRobinExceptions, WRONGREPLY, "The ROBIN reply marker was 0x" << HEX(responseHeader->marker) << " instead of 0x33fedc33 when trying to ping a ROBIN!");
    m_robin.retMiscMem(handle);  
    throw (ex20);
  }

  if ((responseHeader->response & 0xFF) != enum_respAck) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::ping: Robin returns invalid response ID");
    CREATE_ROS_EXCEPTION(ex21, ROSRobinExceptions, WRONGREPLY, "The ROBIN did not reply with an ACK when trying to ping a ROBIN!");
    m_robin.retMiscMem(handle);  
    throw (ex21);
  }
  
  //Return the misc. memory
  m_robin.retMiscMem(handle);  
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::ping: Done");
}


/*********************************************************/
void Rol::loadFragment(u_int size, u_int *data, u_int mode)
/*********************************************************/
{
  u_long replyAddress;
  u_int handle, tocopy, loop, index, offset, osize;
  RespMsg *responseHeader;

  osize = size;
  
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::loadFragment: Called");
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::loadFragment: size = " << size);
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::loadFragment: data[0]        = " << HEX(data[0]));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::loadFragment: data[1]        = " << HEX(data[1]));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::loadFragment: data[size - 2] = " << HEX(data[size - 2]));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::loadFragment: data[size - 1] = " << HEX(data[size - 1]));

  m_robin.getMiscMem(sizeof(RespMsg), &replyAddress, &handle);
  u_int *DataChunk = new u_int[c_DataChunkSize + 3]; 
  index = 0;
  offset = 0;

  while(size)
  {
    if (size > c_DataChunkSize)
      tocopy = c_DataChunkSize;
    else
      tocopy = size;

    DataChunk[0] = offset;
    DataChunk[1] = tocopy; //Number of data words 
    for (loop = 0; loop < tocopy; loop++)
      DataChunk[loop + 2] = data[index++];

    DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::loadFragment: Sending " << tocopy << " words to offset " << offset << " ....");
    m_robin.sendMsg(m_rolId, enum_reqLoadMem, replyAddress, tocopy + 2, DataChunk);
    m_robin.receiveMsg(replyAddress, true);  //blocking wait
    responseHeader = reinterpret_cast<RespMsg *> (replyAddress);
    DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::loadFragment: respStruct->marker   = 0x" << HEX(responseHeader->marker));
    DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::loadFragment: respStruct->size     = 0x" << HEX(responseHeader->size));
    DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::loadFragment: respStruct->version  = 0x" << HEX(responseHeader->version));
    DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::loadFragment: respStruct->response = 0x" << HEX(responseHeader->response));

    if (responseHeader->marker != 0x33fedc33) 
    {
      DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::loadFragment: Robin returns invalid reply marker");
      CREATE_ROS_EXCEPTION(ex22, ROSRobinExceptions, WRONGREPLY, "The ROBIN reply marker was 0x" << HEX(responseHeader->marker) << " instead of 0x33fedc33 when trying to write to scratch memory!");
      delete DataChunk;
      m_robin.retMiscMem(handle);  
      throw (ex22);
    }

    if ((responseHeader->response & 0xFF) != enum_respAck) 
    {
      DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::loadFragment: Robin returns invalid response ID");
      CREATE_ROS_EXCEPTION(ex23, ROSRobinExceptions, WRONGREPLY, "The ROBIN did not reply with a enum_respAck on a enum_reqLoadMem message!");
      delete DataChunk;
      m_robin.retMiscMem(handle);  
      throw (ex23);
    }

    size -= tocopy;
    offset += tocopy;
    DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::loadFragment: " << tocopy << " words sent. " << size << " words remaining");
  }
  
  //Tell the Robin about the new fragment
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::loadFragment: Sending enum_reqLoadRodFragment message...");
  m_robin.sendMsg(m_rolId, enum_reqLoadRodFragment, replyAddress, mode, osize);
  m_robin.receiveMsg(replyAddress, true);
  responseHeader = reinterpret_cast<RespMsg *> (replyAddress);
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::loadFragment: respStruct->marker   = 0x" << HEX(responseHeader->marker));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::loadFragment: respStruct->size     = 0x" << HEX(responseHeader->size));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::loadFragment: respStruct->version  = 0x" << HEX(responseHeader->version));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::loadFragment: respStruct->response = 0x" << HEX(responseHeader->response));

  if (responseHeader->marker != 0x33fedc33) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::loadFragment: Robin returns invalid reply marker");
    CREATE_ROS_EXCEPTION(ex24, ROSRobinExceptions, WRONGREPLY, "The ROBIN reply marker was 0x" << HEX(responseHeader->marker) << " instead of 0x33fedc33 for a message of type enum_reqLoadRodFragment!");
    delete DataChunk;
    m_robin.retMiscMem(handle);  
    throw (ex24);
  }

  if ((responseHeader->response & 0xFF) != enum_respAck) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::loadFragment: Robin returns invalid response ID");
    CREATE_ROS_EXCEPTION(ex25, ROSRobinExceptions, WRONGREPLY, "The ROBIN did not reply with a enum_respAck on a enum_reqLoadRodFragment message!");
    delete DataChunk;
    m_robin.retMiscMem(handle);  
    throw (ex25);
  }

  delete DataChunk;
  m_robin.retMiscMem(handle);  
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::loadFragment: Done");
}


/********************************************/
void Rol::dumpRol(u_int *data, u_int blocklet) 
/********************************************/
{
  u_int dump_count, loop, loop2, index, handle, *rptr;
  u_long replyAddress;
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::dumpRol: Called");
  index = 0;
  dump_count = (64 * 1024 * 1024) / (4 * blocklet);  //blocklet is in words

  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::dumpRol: dump_count = " << dump_count);

  u_int size = sizeof(RespMsg) + blocklet * 4; 
  m_robin.getMiscMem(size, &replyAddress, &handle);
  
  for (loop = 0; loop < dump_count; loop++)
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::dumpRol: requesting " << blocklet << " words from offset 0x" << HEX(index * 4));
    m_robin.sendMsg(m_rolId, enum_reqDumpBuf, replyAddress, index * 4, blocklet);
    m_robin.receiveMsg(replyAddress, true);  //blocking wait

    RespMsg *responseHeader = reinterpret_cast<RespMsg *> (replyAddress);
    rptr = reinterpret_cast<u_int *> (replyAddress);
    DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::dumpRol: responseHeader->marker   = 0x" << HEX(responseHeader->marker));
    DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::dumpRol: responseHeader->size     = 0x" << HEX(responseHeader->size));
    DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::dumpRol: responseHeader->version  = 0x" << HEX(responseHeader->version));
    DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::dumpRol: responseHeader->response = 0x" << HEX(responseHeader->response));

    if (responseHeader->marker != 0x33fedc33) 
    {
      DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::dumpRol: Robin returns invalid reply marker");
      CREATE_ROS_EXCEPTION(ex26, ROSRobinExceptions, WRONGREPLY, "The ROBIN reply marker was 0x" << HEX(responseHeader->marker) << " instead of 0x33fedc33 when trying dump the ROL buffer!");
      m_robin.retMiscMem(handle);  
      throw (ex26);
    }

    if ((responseHeader->response & 0xFF) != enum_respDumpBuf) 
    {
      DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::dumpRol: Robin returns invalid response ID");
      CREATE_ROS_EXCEPTION(ex27, ROSRobinExceptions, WRONGREPLY, "The ROBIN did not reply with an enum_respDumpBuf when trying to dump the ROL buffer!");
      m_robin.retMiscMem(handle);  
      throw (ex27);
    }
    
    for (loop2 = 0; loop2 < blocklet; loop2++)
      data[index++] = rptr[loop2 + 4];
  }
  
  //Return the misc. memory
  m_robin.retMiscMem(handle);  
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::dumpRol: Done");
}


/**********************************************/
Rol::KeptPage Rol::getRejectedPage(u_int pageno) 
/**********************************************/
{
  u_long replyAddress;
  u_int handle;
  KeptPage ret;
  
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::getRejectedPage: Called");
 
  //ATTENTION: enum_mgmtMaxPageSize is quite large (8K). 
  //We may need to increase the default size of the misc memory
  //buffer in robinscope and the config DB
  u_int size = sizeof(RespMsg) + sizeof(RejectedEntry) + enum_mgmtMaxPageSize * sizeof(int);
  m_robin.getMiscMem(size, &replyAddress, &handle);
  m_robin.sendMsg(m_rolId, enum_reqKeptPage, replyAddress, pageno);
  m_robin.receiveMsg(replyAddress, true);  //blocking wait
  
  RespMsg *responseHeader = reinterpret_cast<RespMsg *> (replyAddress); 
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getRejectedPage: responseHeader->marker   = 0x" << HEX(responseHeader->marker));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getRejectedPage: responseHeader->size     = 0x" << HEX(responseHeader->size));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getRejectedPage: responseHeader->version  = 0x" << HEX(responseHeader->version));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getRejectedPage: responseHeader->response = 0x" << HEX(responseHeader->response));
  
  if (responseHeader->marker != 0x33fedc33) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::getRejectedPage: Robin returns invalid reply marker");
    CREATE_ROS_EXCEPTION(ex4, ROSRobinExceptions, WRONGREPLY, "The ROBIN reply marker was 0x" << HEX(responseHeader->marker) << " instead of 0x33fedc33 when trying to read a rejected event!");
    m_robin.retMiscMem(handle);
    throw (ex4);
  }

  if ((responseHeader->response & 0xFF) != enum_respKeptPage) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::getRejectedPage: Robin returns invalid response ID");
    CREATE_ROS_EXCEPTION(ex5, ROSRobinExceptions, WRONGREPLY, "The ROBIN did not reply with a kept page when trying to get a rejected event!");
    m_robin.retMiscMem(handle);
    throw (ex5);
  }
  
  RejectedEntry *rejected = reinterpret_cast<RejectedEntry *> (replyAddress + sizeof(RespMsg));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getRejectedPage: rejected->mri           = 0x" << HEX(rejected->mri));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getRejectedPage: rejected->info.upfInfo.status   = 0x" << HEX(rejected->info.upfInfo.status));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getRejectedPage: rejected->info.upfInfo.eventId  = 0x" << HEX(rejected->info.upfInfo.eventId));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getRejectedPage: rejected->info.upfInfo.pageNum  = 0x" << HEX(rejected->info.upfInfo.pageNum));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getRejectedPage: rejected->info.upfInfo.pageLen  = 0x" << HEX(rejected->info.upfInfo.pageLen));
  
  //Copy the  data
  ret.rentry = *rejected;
  u_int *rdata  = reinterpret_cast<u_int *> (replyAddress + sizeof(RespMsg) + sizeof(RejectedEntry));
  ret.pagedata_size = responseHeader->size - ((sizeof(RespMsg) + sizeof(RejectedEntry)) / sizeof(int));  //in words
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getRejectedPage: ret.pagedata_size = 0x" << HEX(ret.pagedata_size));
  for (u_int loop = 0; loop < enum_mgmtMaxPageSize; loop++)
    ret.pagedata[loop] = rdata[loop];
	
  //Return the misc. memory
  m_robin.retMiscMem(handle);

  return(ret);  
}


/********************************/
void Rol::clearRejectedPages(void) 
/********************************/
{
  u_long replyAddress;
  u_int handle;
  
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::clearRejectedPages: Called");
 
  u_int size = sizeof(RespMsg);
  m_robin.getMiscMem(size, &replyAddress, &handle);
  m_robin.sendMsg(m_rolId, enum_reqClearKeptPages, replyAddress);
  m_robin.receiveMsg(replyAddress, true);  //blocking wait
  
  RespMsg *responseHeader = reinterpret_cast<RespMsg *> (replyAddress); 
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::clearRejectedPages: responseHeader->marker   = 0x" << HEX(responseHeader->marker));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::clearRejectedPages: responseHeader->size     = 0x" << HEX(responseHeader->size));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::clearRejectedPages: responseHeader->version  = 0x" << HEX(responseHeader->version));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::clearRejectedPages: responseHeader->response = 0x" << HEX(responseHeader->response));
  
  if (responseHeader->marker != 0x33fedc33) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::clearRejectedPages: Robin returns invalid reply marker");
    CREATE_ROS_EXCEPTION(ex4, ROSRobinExceptions, WRONGREPLY, "The ROBIN reply marker was 0x" << HEX(responseHeader->marker) << " instead of 0x33fedc33 when trying to clear the special memory for rejected events!");
    m_robin.retMiscMem(handle);
    throw (ex4);
  }

  if ((responseHeader->response & 0xFF) != enum_respAck) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::clearRejectedPages: Robin returns invalid response ID");
    CREATE_ROS_EXCEPTION(ex5, ROSRobinExceptions, WRONGREPLY, "The ROBIN did not reply with a enum_respAck when trying to clear the special memory for rejected events!");
    m_robin.retMiscMem(handle);
    throw (ex5);
  }
  	
  //Return the misc. memory
  m_robin.retMiscMem(handle);
}


/*************************************************/
std::vector <Rol::L1idRange> Rol::getL1idList(void) 
/*************************************************/
{
  u_int lastid, handle, nl1ids, gotl1ids, tocopy;
  std::vector <L1idRange> ret;
  L1idRange pair;
  u_long replyAddress;

  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::getL1idList: Called");

  u_int size = sizeof(RespMsg) + 1 * sizeof(int);
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getL1idList: size = " << size);

  m_robin.getMiscMem(size, &replyAddress, &handle);
  m_robin.sendMsg(m_rolId, enum_reqEventList, replyAddress);
  m_robin.receiveMsg(replyAddress, true);  //blocking wait
  
  RespMsg *responseHeader = reinterpret_cast<RespMsg *> (replyAddress); 
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getL1idList: responseHeader->marker   = 0x" << HEX(responseHeader->marker));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getL1idList: responseHeader->size     = 0x" << HEX(responseHeader->size));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getL1idList: responseHeader->version  = 0x" << HEX(responseHeader->version));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getL1idList: responseHeader->response = 0x" << HEX(responseHeader->response));
 
  if (responseHeader->marker != 0x33fedc33) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::getL1idList: Robin returns invalid reply marker");
    CREATE_ROS_EXCEPTION(ex4, ROSRobinExceptions, WRONGREPLY, "The ROBIN reply marker was 0x" << HEX(responseHeader->marker) << " instead of 0x33fedc33 in response to a message of type enum_reqEventList!");
    m_robin.retMiscMem(handle);
    throw (ex4);
  }

  if ((responseHeader->response & 0xFF) != enum_respAck) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::getL1idList: Robin returns invalid response ID");
    CREATE_ROS_EXCEPTION(ex5, ROSRobinExceptions, WRONGREPLY, "The ROBIN did not reply with an enum_respAck to a message of type enum_reqEventList!");
    m_robin.retMiscMem(handle);
    throw (ex5);
  }
  
  u_int *ptr = reinterpret_cast<u_int *> (replyAddress);  
  nl1ids = ptr[4];
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getL1idList: There are " << nl1ids << " L1IDs in the event buffer of this ROL");
   	
  //Return the misc. memory
  m_robin.retMiscMem(handle);

  gotl1ids = 0;
  
  std::vector <u_int> l1id_list;
  l1id_list.clear();
  
  while (nl1ids)
  {
    u_int size = 512 * sizeof(int);  //MJ: The size of the scratch memory FIFO is 2048 bytes
    m_robin.getMiscMem(size, &replyAddress, &handle);
    m_robin.sendMsg(m_rolId, enum_reqReadMem, replyAddress, gotl1ids, nl1ids);
    m_robin.receiveMsg(replyAddress, true);  //blocking wait

    RespMsg *responseHeader = reinterpret_cast<RespMsg *> (replyAddress); 
    DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getL1idList: responseHeader->marker   = 0x" << HEX(responseHeader->marker));
    DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getL1idList: responseHeader->size     = 0x" << HEX(responseHeader->size));
    DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getL1idList: responseHeader->version  = 0x" << HEX(responseHeader->version));
    DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getL1idList: responseHeader->response = 0x" << HEX(responseHeader->response));

    if (responseHeader->marker != 0x33fedc33) 
    {
      DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::getL1idList: Robin returns invalid reply marker");
      CREATE_ROS_EXCEPTION(ex6, ROSRobinExceptions, WRONGREPLY, "The ROBIN reply marker was 0x" << HEX(responseHeader->marker) << " instead of 0x33fedc33 in response to a message of type enum_reqReadMem!");
      m_robin.retMiscMem(handle);
      throw (ex6);
    }

    if ((responseHeader->response & 0xFF) != enum_respReadMem) 
    {
      DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::getL1idList: Robin returns invalid response ID");
      CREATE_ROS_EXCEPTION(ex7, ROSRobinExceptions, WRONGREPLY, "The ROBIN did not reply with an enum_respReadMem to a message of type enum_reqReadMem!");
      m_robin.retMiscMem(handle);
      throw (ex7);
    }

    tocopy = (responseHeader->size - (sizeof(RespMsg) >> 2));  //in words
    DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getL1idList: The Robin has returned " << tocopy << " L1IDs");
    
    //copy the L1IDs
    u_int *ptr = reinterpret_cast<u_int *> (replyAddress);
    for (u_int loop = 0; loop < tocopy; loop++)
    {
      DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getL1idList: Pushing L1ID " << ptr[4 + loop]);
      l1id_list.push_back(ptr[4 + loop]);
    }  
    nl1ids -= tocopy;
    gotl1ids += tocopy;
    m_robin.retMiscMem(handle);
   
    DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getL1idList: Still " << nl1ids << " L1IDs to receive");
  }
  
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getL1idList: The vector now contains " << l1id_list.size() << " L1IDs");
  ret.clear();
  
  if (l1id_list.size() == 0)
  {
    pair.first = 0xffffffff;
    pair.last = 0xffffffff;
    ret.push_back(pair);
    DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::getL1idList: Done (vector was empty)");
    return(ret);
  }
  
  else if (l1id_list.size() == 1)
  {
    pair.first = l1id_list[0];
    pair.last = l1id_list[0];
    ret.push_back(pair);
    DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::getL1idList: Done (vector had one L1ID)");
    return(ret);
  }  
  
  sort(l1id_list.begin(), l1id_list.end(), std::less<int>());   

  u_int firstflag = 1;
  u_int thisid;
  for (std::vector<u_int>::iterator it = l1id_list.begin(); it != l1id_list.end(); it++)
  {
    thisid = *it;
    if (firstflag)
    {
      pair.first = *it;
      lastid = *it;
      firstflag = 0;
      DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getL1idList: first pair.first = " << pair.first);
    }
    else
    {
      if (*it == (lastid + 1))
      {
        lastid = *it;
        DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getL1idList: continuing with = " << *it);
      }
      else
      {
	pair.last = lastid;
        DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getL1idList: pair.last = " << pair.last);
        ret.push_back(pair);
	lastid = *it;
        pair.first = *it;
        DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getL1idList: pair.first = " << pair.first);
      }
    }
  }
  pair.last = thisid;
  ret.push_back(pair);
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::getL1idList: last pair.last = " << pair.last);
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::getL1idList: Done");
  return(ret);  
}


/****************************************/
void Rol::configureDiscardMode(u_int mode) 
/****************************************/
{
  //mode = 0 -> Disable discard mode 
  //mode = 1 -> Enable discard mode 

  u_long replyAddress;
  u_int handle;
  
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::configureDiscardMode: Called");
 
  u_int size = sizeof(RespMsg);
  m_robin.getMiscMem(size, &replyAddress, &handle);

  if (mode == 0)   
    m_robin.sendMsg(m_rolId, enum_reqLeaveDiscardMode, replyAddress);
  else
    m_robin.sendMsg(m_rolId, enum_reqEnterDiscardMode, replyAddress);
  
  m_robin.receiveMsg(replyAddress, true);  //blocking wait
  
  RespMsg *responseHeader = reinterpret_cast<RespMsg *> (replyAddress); 
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::configureDiscardMode: responseHeader->marker   = 0x" << HEX(responseHeader->marker));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::configureDiscardMode: responseHeader->size     = 0x" << HEX(responseHeader->size));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::configureDiscardMode: responseHeader->version  = 0x" << HEX(responseHeader->version));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::configureDiscardMode: responseHeader->response = 0x" << HEX(responseHeader->response));
  
  if (responseHeader->marker != 0x33fedc33) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::configureDiscardMode: Robin returns invalid reply marker");
    CREATE_ROS_EXCEPTION(ex4, ROSRobinExceptions, WRONGREPLY, "The ROBIN reply marker was 0x" << HEX(responseHeader->marker) << " instead of 0x33fedc33 when trying to configure the discard mode!");
    m_robin.retMiscMem(handle);
    throw (ex4);
  }

  if ((responseHeader->response & 0xFF) != enum_respAck) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::configureDiscardMode: Robin returns invalid response ID");
    CREATE_ROS_EXCEPTION(ex5, ROSRobinExceptions, WRONGREPLY, "The ROBIN did not reply with a enum_respAck when trying to configure the discard mode!");
    m_robin.retMiscMem(handle);
    throw (ex5);
  }
  	
  //Return the misc. memory
  m_robin.retMiscMem(handle);
}


/**************************/
void Rol::formatBuffer(void) 
/**************************/
{
  u_long replyAddress;
  u_int handle;
  
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::formatBuffer: Called");
 
  u_int size = sizeof(RespMsg);
  m_robin.getMiscMem(size, &replyAddress, &handle);
  m_robin.sendMsg(m_rolId, enum_reqFormatBuffer, replyAddress);
  m_robin.receiveMsg(replyAddress, true);  //blocking wait
  
  RespMsg *responseHeader = reinterpret_cast<RespMsg *> (replyAddress); 
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::formatBuffer: responseHeader->marker   = 0x" << HEX(responseHeader->marker));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::formatBuffer: responseHeader->size     = 0x" << HEX(responseHeader->size));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::formatBuffer: responseHeader->version  = 0x" << HEX(responseHeader->version));
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::formatBuffer: responseHeader->response = 0x" << HEX(responseHeader->response));
  
  if (responseHeader->marker != 0x33fedc33) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::formatBuffer: Robin returns invalid reply marker");
    CREATE_ROS_EXCEPTION(ex4, ROSRobinExceptions, WRONGREPLY, "The ROBIN reply marker was 0x" << HEX(responseHeader->marker) << " instead of 0x33fedc33 when trying to format the event buffer!");
    m_robin.retMiscMem(handle);
    throw (ex4);
  }

  if ((responseHeader->response & 0xFF) != enum_respAck) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::formatBuffer: Robin returns invalid response ID");
    CREATE_ROS_EXCEPTION(ex5, ROSRobinExceptions, WRONGREPLY, "The ROBIN did not reply with a enum_respAck when trying to format the event buffer!");
    m_robin.retMiscMem(handle);
    throw (ex5);
  }
  	
  //Return the misc. memory
  m_robin.retMiscMem(handle);
}


/****************************************/
void Rol::setCRCInterval(int crc_interval) 
/****************************************/
{
  DEBUG_TEXT(DFDB_ROSROBIN, 20 ,"Rol::setCRCInterval: called with interval = " << crc_interval);
  m_crc_interval = crc_interval;
}


/****************************/
void Rol::runTest(u_int ttype)
/****************************/
{
  u_long replyAddress;
  u_int handle;

  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"Rol::runTest: Called");

  u_int size = sizeof(RespMsg) + sizeof(int);
  m_robin.getMiscMem(size, &replyAddress, &handle);
  m_robin.sendMsg(m_rolId, enum_reqTest, replyAddress, ttype);
  m_robin.receiveMsg(replyAddress, true);  //blocking wait
  RespMsg *responseHeader = reinterpret_cast<RespMsg *> (replyAddress); 

  if (responseHeader->marker != 0x33fedc33) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::runTest: Robin returns invalid reply marker");
    CREATE_ROS_EXCEPTION(ex6, ROSRobinExceptions, WRONGREPLY, "The ROBIN reply marker was 0x" << HEX(responseHeader->marker) << " instead of 0x33fedc33 when trying to read the temperature!");
    m_robin.retMiscMem(handle);
    throw (ex6);
  }

  if ((responseHeader->response & 0xFF) != enum_respAck) 
  {
    DEBUG_TEXT(DFDB_ROSROBIN, 5 ,"Rol::runTest: Robin returns invalid response ID");
    CREATE_ROS_EXCEPTION(ex7, ROSRobinExceptions, WRONGREPLY, "The ROBIN did not reply with an ACK when trying to read the temperature!");
    m_robin.retMiscMem(handle);
    throw (ex7);
  }

  //Return the misc. memory
  m_robin.retMiscMem(handle);
}



/************************************************/
/*						*/
/* Authors: F.Giesen,M.M�ller,Hinkelbein	*/
/*						*/
/************************************************/

#include <string>
#include <iostream>
#include <new>
#include "ROSRobin/BitStream.h"
#include "ROSRobin/ROSRobinExceptions.h"

const unsigned int ROS::BitStream::c_bytesPerWord = 32;

// Default constructor. Sets everything to 0.
//*************************
ROS::BitStream::BitStream()
//*************************
  : m_bits(0),
    m_length(0),
    m_nrOfWords(0)
{

}


// Copy constructor. Initialises with another bitstream.
//**************************************************
ROS::BitStream::BitStream(const ROS::BitStream & bs) 
//**************************************************
  : m_bits(0),
    m_length(0),
    m_nrOfWords(0)
{
  set(bs);
}


// This constructor initialises from a string ("0100111010")
//***********************************************
ROS::BitStream::BitStream(const char * bitString) 
//***********************************************
  : m_bits(0),
    m_length(0),
    m_nrOfWords(0)
{
    set(bitString);
}


// The destructor deletes the m_bits array
//******************************
ROS::BitStream::~BitStream(void) 
//******************************
{
  if (m_bits != 0) 
    delete[] m_bits;
  
  m_length = 0;
  m_nrOfWords = 0; 
}


// Sets the bitstream from a character string
//****************************************
void ROS::BitStream::set(const char * str) 
//****************************************
{
  // Use resize to assign the object a size (and delete any old array)
  resize(strlen(str));
  
  // Now add the new bits to the internal m_bits array
  unsigned int i;
  for (i = 0; i < getLength(); i++) 
  {
    if ((str[i] != '0') && (str[i] != '1'))
      throw ROSRobinExceptions(ROS::ROSRobinExceptions::BITSTREAM, "BitStream::set(const char): illegal bit value (must be either '1' or '0')!");
    setBit(i, (str[i] == '1') ? true : false);
  }
}


// Sets the bitstream from another bitstream
//********************************************
void ROS::BitStream::set(const BitStream & bs) 
//********************************************
{
  // Use resize to assign the object a size (and delete the old array)
  resize(bs.getLength());

  // Now copy the words with the bits inside.
  unsigned int i;
  for (i = 0; i < m_nrOfWords; i++) 
    m_bits[i] = bs.m_bits[i];
}


// Sets the bitstream from an array of raw data in low endian
//*********************************************************************
void ROS::BitStream::set (const unsigned int * data, unsigned int size)
//*********************************************************************
{
  // Resize the bitstream to the required size
  resize(size);

  // Now simply copy the data
  unsigned int i;
  for (i = 0; i < m_nrOfWords; i++) 
    m_bits[i] = data[i];
}


// This method returns the number of bits inside the object
//********************************************
unsigned int ROS::BitStream::getLength() const 
//********************************************
{
  return m_length;
}


// Return part of the bitstream in a of 8 bit unsigned integer number
//************************************************************************
unsigned char ROS::BitStream::getUInt8(unsigned int index, Endian e) const 
//************************************************************************
{
  unsigned char l = 0, i;

  // Check if the index is at the end of the bitstream and it is
  // not possible to create a 8 bit value out if the remaining bits at the end.
  if (index + 7 >= getLength())
    throw ROSRobinExceptions(ROS::ROSRobinExceptions::BITSTREAM, "BitStream::GetUInt8(unsigned int, Endian): illegal index!");

  // Ok. Now we start.
  // Check the requested endieness
  if (e == HIGHEND) 
  {
    // High endieness means that the LSB is the highest Bit in the return value.
    for (i = 0; i < 8; i++) 
    {
      l <<= 1;
      l |= (getBit(index + i)) ? 1 : 0;
    }
  } else 
  {
    // Low endieness means that the LSB is the lowest Bit in the return value.
    for (i = 0; i < 8; i++) 
    {
      l <<= 1;
      l |= (getBit(index + 7 - i)) ? 1 : 0;
    }
  }
  return l;
}


// Return part of the bitstream in a of 16 bit unsigned integer number
//**************************************************************************
unsigned short ROS::BitStream::getUInt16(unsigned int index, Endian e) const 
//**************************************************************************
{
  unsigned short l = 0, i;

  // Check if the index is at the end of the bitstream and it is
  // not possible to create a 8 bit value out if the remaining bits at the end.
  if (index + 15 >= getLength())
    throw ROSRobinExceptions(ROS::ROSRobinExceptions::BITSTREAM, "BitStream::GetUInt16(unsigned int, Endian): illegal index!");

  // Ok. Now we start.
  // Check the requested endieness
  if (e == HIGHEND) 
  {
    // High endieness means that the LSB is the highest Bit in the return value.
    for (i = 0; i < 16; i++) 
    {
      l <<= 1;
      l |= (getBit(index + i)) ? 1 : 0;
    }
  } else 
  {
    // Low endieness means that the LSB is the lowest Bit in the return value.
    for (i = 0; i < 16; i++) 
    {
      l <<= 1;
      l |= (getBit(index + 15 - i)) ? 1 : 0;
    }
  }
  return l;
}


// Return part of the bitstream in a of 16 bit unsigned integer number
//************************************************************************
unsigned int ROS::BitStream::getUInt32(unsigned int index, Endian e) const
//************************************************************************
{
  unsigned int l = 0, i;
  
  // Check if the index is at the end of the bitstream and it is
  // not possible to create a 8 bit value out if the remaining bits at the end.
  if (index + 31 >= getLength())
    throw ROSRobinExceptions(ROS::ROSRobinExceptions::BITSTREAM, "BitStream::GetUInt32(unsigned int, Endian): illegal index!");
  
  // Ok. Now we start.
  // Check the requested endieness
  if (e == LOWEND) 
  {
    // Low endieness means that the LSB is the lowest Bit in the return value.
    // Check if the index parameter is exactly on a 32 bit boundary
    if (index % c_bytesPerWord == 0) 
    {
      // Since we store the data in low endian format we can immediatly return the value.     
      return m_bits[index / c_bytesPerWord];
    }
    else 
    {
      // Otherwise construct the return value
      for (i = 0; i < c_bytesPerWord; i++) 
      {
	l <<= 1;
	l |= (getBit(index + 31 - i)) ? 1 : 0;
      }
    }
    
  } else 
  {
    // High endieness means that the LSB is the highest Bit in the return value.
    for (i = 0; i < c_bytesPerWord; i++) 
    {
      l <<= 1;
      l |= (getBit(index + i)) ? 1 : 0;
    }
  }
  return l;
}


// Set a bit of the bitstream object
//****************************************************************
void ROS::BitStream::setAt(unsigned int index, const ROS::Bit bit)
//****************************************************************
{
  // Check if the bit contains 1 or 0
  if ((bit.get() == ROS::Bit::UNDEFINED) || (bit.get() == ROS::Bit::INVALID))
    throw ROSRobinExceptions(ROS::ROSRobinExceptions::BITSTREAM, "BitStream::setAt(unsigned int): illegal bit value""(must be either '1' or '0')!");
  
  setBit(index, (bit.get() == Bit::HIGH) ? true : false);
}


// Get a bit at the specified position
//************************************************************
const ROS::Bit ROS::BitStream::getAt(unsigned int index) const 
//************************************************************
{
  return Bit(getBit(index));
}


// Append a bit to the bitstream
//***********************************************
void ROS::BitStream::append(const ROS::Bit & bit) 
//***********************************************
{
  // Check if the bit contains 1 or 0
  if ((bit.get() == ROS::Bit::UNDEFINED) || (bit.get() == ROS::Bit::INVALID)) 
    throw ROSRobinExceptions(ROS::ROSRobinExceptions::BITSTREAM, "BitStream::Append(Bit &): illegal bit value""(must be either '1' or '0')!");

  // Resize the object to have more space
  resize(getLength() + 1);

  // Store the new bit
  setBit(getLength() - 1, (bit.get() == ROS::Bit::HIGH) ? true : false);
}


// Append a bitstream to the bitstream
//***********************************************
void ROS::BitStream::append(const BitStream & bs) 
//***********************************************
{
  //Calculate the offset within the last word in the internal representation
  int offset = getLength() % c_bytesPerWord;
  unsigned int i, j, m, prevlen = getLength();

  // Resize the bitstream
  resize(bs.getLength() + getLength());

  // First fill in the last bits of the last word if necessary
  if (offset != 0) 
  {
    for (i = 0; (i < c_bytesPerWord - offset) && (i < bs.getLength()); i++)
      setBit(prevlen + i, (bs.getAt(i).get() == ROS::Bit::HIGH) ? true : false);
  }

  // Now fill in the remaining bits as whole 32 bit words    
  // i is the starting point within the target bitstream object
  i = prevlen / c_bytesPerWord + ((offset) ? 1 : 0);
  for (j = 0; ((j * c_bytesPerWord) + offset) <= bs.getLength(); i++, j += c_bytesPerWord) 
  {
    m = bs.getUInt32(offset + j, LOWEND);
    m_bits[i] = m;
  }

  // Finally add all remaining bits.
  for ( ; j < bs.getLength(); j++)
    setBit(prevlen + j, (bs.getAt(j).get() == ROS::Bit::HIGH) ? true : false);
}


// Extract a bitstream
//**************************************************************************************
ROS::BitStream ROS::BitStream::extract(unsigned int position, unsigned int length) const 
//*************************************************************************************
{
  // First create a new bitstream object  with the required length
  ROS::BitStream temp;
  temp.resize(length);

  // Now fill it in.
  unsigned int i;
  for (i = position; i < position + length; i++)
    temp.setAt(i - position, getAt(i));

  return temp;
}


// Shift a bitstream to the left
//********************************************
void ROS::BitStream::shl(unsigned short count) 
//********************************************
{
  unsigned int m = 0, l = 0, i = 0;
  
  // Check if the bitstream is not empty
  if (getLength() == 0) 
    throw ROSRobinExceptions(ROS::ROSRobinExceptions::BITSTREAM, "BitStream::SHL(unsigned short): The bitstream is empty");
  
  // First move whole words
  if (count / c_bytesPerWord) 
  {
    for (i = m_nrOfWords - 1; i > (count / c_bytesPerWord) - 1; i--)
      m_bits[i] = m_bits[i - (count / c_bytesPerWord)];
    for ( ; i < -1; i--)
      m_bits[i] = 0;
  }

  // Now adjust each word by shifting it for (count % c_bytesPerWord)
  if (count % c_bytesPerWord) 
  {
    for (i = m_nrOfWords - 1; i < -1; i--) 
    {
      l = 0;
      m = (m_bits[i] << (count % c_bytesPerWord));
      if (i > 0)
	l = (m_bits[i - 1] >> (c_bytesPerWord - (count % c_bytesPerWord)));
      l |= m;
      m_bits[i] = l;
    }
  }
}


// Shift a bitstream to the left
//********************************************
void ROS::BitStream::shr(unsigned short count) 
//********************************************
{
  unsigned int m = 0, l = 0, i = 0;
  
  // Check if the bitstream is not empty
  if (getLength() == 0) 
    throw ROSRobinExceptions(ROS::ROSRobinExceptions::BITSTREAM, "BitStream::SHR(unsigned short): The bitstream is empty");
  
  // First check if count is a multiple of c_bytesPerWord
  if (count % c_bytesPerWord) 
  {
    // If so we shift each word in the representation by count % c_bytesPerWord
    for (i = 0; i < m_nrOfWords; i++) 
    {
      l = 0;
      m = (m_bits[i] >> (count % c_bytesPerWord));
      if (i < m_nrOfWords - 1)
	l = (m_bits[i + 1] << (c_bytesPerWord - (count % c_bytesPerWord)));
      l |= m;
      m_bits[i] = l;
    }
  }

  if (count / c_bytesPerWord) 
  {
    // Now move the whole words
    for (i = 0; i < m_nrOfWords - count / c_bytesPerWord; i++)
      m_bits[i] = m_bits[i + count / c_bytesPerWord];

    // And fill in 0 to the bits which have moved.
    for (i = m_nrOfWords - count / c_bytesPerWord; i < m_nrOfWords; i++)
      m_bits[i] = 0;
  }
}


// Reverse a bitstream
//****************************
void ROS::BitStream::reverse() 
//****************************
{
  unsigned int l = 0, i = 0, j = 0;
  int m = 0;
  
  // Check if the bitstream is not empty  
  if (getLength() == 0) 
    throw ROSRobinExceptions(ROS::ROSRobinExceptions::BITSTREAM, "BitStream::Reverse: The bitstream is empty");    
  
  // First move all words of the representation
  if (getLength() / c_bytesPerWord) 
  {
    // Go to the half of the bitstream
    for (i = 0; i < m_nrOfWords / 2; i++) 
    {
      // Reverse each word
      l = 0;
      m = m_bits[i];
      for (j = 0; j < c_bytesPerWord; j++) 
      {
	l <<= 1;
	l |= m % 2;
	m >>= 1;
      }

      // Now take the word at the mirror position
      m = m_bits[m_nrOfWords - i - 1];
      
      // Write the previously reversed word there
      m_bits[m_nrOfWords - i - 1] = l;

      // Reverse the wrord from the mirror position
      l = 0;
      for (j = 0; j < c_bytesPerWord; j++) 
      {
        l <<= 1;
        l |= m % 2;
        m >>= 1;
      }

      // And store it
      m_bits[i] = l;
    }

    // Check if we have a odd number of words in the bitstream representation
    if (m_nrOfWords % 2) 
    {
      // Then we have reverse the word in the middle too
      l = 0;
      m = m_bits[m_nrOfWords / 2];
      for (j = 0; j < c_bytesPerWord; j++) 
      {
	l <<= 1;
	l |= m % 2;
	m >>= 1;
      }
      m_bits[m_nrOfWords / 2] = l;
    }
  }
  
  // Finally cover all remaining bits
  if (getLength() % c_bytesPerWord) 
  {
    m = m_bits[m_nrOfWords];
    shl(getLength() % c_bytesPerWord);
    l = 0;
    for (j = 0; j < (getLength() % c_bytesPerWord); j++) 
    {
      l <<= 1;
      l |= m % 2;
      m >>= 1;
    }
    m = m_bits[0];
    m_bits[0] = l | m;
  }
}


// The assignment operator
//*********************************************************************
ROS::BitStream & ROS::BitStream::operator = (const ROS::BitStream & bs) 
//*********************************************************************
{
  if (this != & bs)
    set(bs);
  return * this;
}



// The comparision operator
//****************************************************************
bool ROS::BitStream::operator == (const ROS::BitStream & bs) const 
//****************************************************************
{
  // Check if the bitstreams have the same length
  if (bs.getLength() != getLength())
    return false;

  unsigned int i = 0;
  // First compare all words
  if (getLength() / c_bytesPerWord) 
  {
    for (i = 0 ; i < getLength() / c_bytesPerWord; i++) 
    {
      if (m_bits[i] != bs.m_bits[i])
	return false;
    }
  }
    
  // Then compair all remaining bits
  if (getLength() % c_bytesPerWord)
    for (i *= c_bytesPerWord; i < getLength(); i++)
      if (bs.getAt(i) != getAt(i))
	return false;
  return true;
}


// The secondcomparision operator
//****************************************************************
bool ROS::BitStream::operator != (const ROS::BitStream & bs) const 
//****************************************************************
{
  return !operator == (bs);
}


// Internal (private) method for setting a bit
//************************************************************
void ROS::BitStream::setBit(unsigned int position, bool value) 
//************************************************************
{

  // Check if the length has not been exceeded
  if ( position >= getLength() ) 
    throw ROSRobinExceptions(ROS::ROSRobinExceptions::BITSTREAM, "BitStream::setBit(unsigned int, bool): set position exceeds bitstream length");
  
  // Then check if the bits array does not point to zero
  if (m_bits != 0) 
  {
    unsigned int wordPosition = position / c_bytesPerWord;
    unsigned int mask = 1 << (position % c_bytesPerWord);
    
    if (value) 
      m_bits[wordPosition] |= mask;
    else 
      m_bits[wordPosition] &= ~mask;
  }    
}



// Internal (private) method for getting a bit
//******************************************************
bool ROS::BitStream::getBit(unsigned int position) const
//******************************************************
{
  // Check if the length has not been exceeded
  if ( position >= getLength() ) 
    throw ROSRobinExceptions(ROS::ROSRobinExceptions::BITSTREAM, "BitStream::getBit(unsigned int): get position exceeds bitstream length");

  // Then check if the bits array does not point to zero
  if (m_bits != 0) 
  {
    unsigned int wordPosition = position / c_bytesPerWord;
    unsigned int mask = 1 << (position % c_bytesPerWord);
    return ( m_bits[wordPosition] & mask );
  }    

  return 0;
}


// Resize the memory for the internal bit data representation
//*************************************************
void ROS::BitStream::resize(unsigned int newLength) 
//*************************************************
{
  // First calculate the number of required data words for holding the bitstrteam
  unsigned int nrOfWords = (newLength / c_bytesPerWord) + ((newLength % c_bytesPerWord) == 0 ? 0 : 1);

  // If there is enough space left to hold the new bits (or if we shrink only a bit) no work must be done!
  if ( nrOfWords == m_nrOfWords ) 
  {
    m_length = newLength;
    return;
  }

  // Ok. We really need new memory.
  unsigned int *tempMemory = new unsigned int [nrOfWords];    

  // Now look if there is already data present in the object
  if ( m_bits != 0 ) 
  {
    // Copy the old data to the new place. Cut if the new size is less then the old
    unsigned int i;
    for (i = 0; i < ((m_nrOfWords < nrOfWords) ? m_nrOfWords : nrOfWords); i++) 
      tempMemory[i] = m_bits[i];      

    // Delete the old memory
    delete[] m_bits;    
  }

  // Assign the new values
  m_bits = tempMemory;
  m_length = newLength;
  m_nrOfWords = nrOfWords;
}


// Console output
//******************************************************************************
std::ostream & operator << (std::ostream & os, const ROS::BitStream & bitStream) 
//******************************************************************************
{
  for (unsigned int i = 0; i < bitStream.getLength(); i++)
    os << bitStream.getAt(i);
  return os;
}


// Console input
//************************************************************************
std::istream & operator >> (std::istream & is, ROS::BitStream & bitStream) 
//************************************************************************
{
  std::string s;
  is >> std::ws;
  is >> s;
  bitStream.set(s.c_str());
  return is;
}



// --------x----------------------------x-----------------------------------x--
/**
 ** \file   XilinxCoolRunnerPld.cpp
 **
 ** \author
 **
 ** \date
 **
 */
// --------x----------------------------x-----------------------------------x--


/* $Id$
 * $Revision$
 * $Log$
 * Revision 1.4  2008/12/03 10:17:46  joos
 * (some) compiler warnings fixed
 *
 * Revision 1.3  2005/05/30 14:06:47  mmueller
 * Added proper autodetection of the FPGA and CPLD Jtag chain
 *
 * Revision 1.2  2005/04/11 11:24:16  joos
 * Modifications and new files of Matthias
 *
 * Revision 1.1.1.1  2005/04/07 11:34:38  joos
 * added ROSRobin
 *
 *
 */

/**
 * Description:
 *
 */

#include "ROSRobin/XilinxCoolRunnerPld.h"
#include "ROSRobin/Firmware.h"
#include "ROSRobin/JTagDevice.h"
#include "ROSRobin/JTagController.h"
#include "ROSRobin/BitStream.h"


//*****************************************************************************
ROS::XilinxCoolRunnerPld::XilinxCoolRunnerPld(ROS::JTagController & jtagController) :
//*****************************************************************************
ROS::JTagDevice(jtagController, 8)

{

  addInstruction("IDCODE",           ROS::BitStream("10000000"));
  addInstruction("HIGHZ",            ROS::BitStream("00111111"));
  addInstruction("CLAMP",            ROS::BitStream("01011111"));
  addInstruction("ISC_ENABLE",       ROS::BitStream("00010111"));
  addInstruction("ISC_DISABLE",      ROS::BitStream("00000011"));
  addInstruction("ISC_PROGRAM",      ROS::BitStream("01010111"));
  addInstruction("ISC_NOOP",         ROS::BitStream("00000111"));
  addInstruction("ISC_INIT",         ROS::BitStream("00001111"));
  addInstruction("ISC_ENABLE_CLAMP", ROS::BitStream("10010111"));
  addInstruction("ISC_ERASE",        ROS::BitStream("10110111"));
  addInstruction("ISC_VERIFY",       ROS::BitStream("01110111"));
  addInstruction("ISC_ENABLE_OTF",   ROS::BitStream("00100111"));
  addInstruction("ISC_SRAM_READ",    ROS::BitStream("11100111"));
  addInstruction("ISC_SRAM_WRITE",   ROS::BitStream("01100111"));

}


//***********************************************
ROS::XilinxCoolRunnerPld::~XilinxCoolRunnerPld() 
//***********************************************
{

}

//***************************************
void ROS::XilinxCoolRunnerPld::program(std::string /*firmwareFile*/) 
//***************************************
{

}

//********************************************
bool ROS::XilinxCoolRunnerPld::isProgrammed() 
//********************************************
{
  return false;
}


//****************************************************
ROS::BitStream ROS::XilinxCoolRunnerPld::getDeviceId() 
//****************************************************
{

  setIR("IDCODE");
  setDR(ROS::BitStream("1111111111111111"
		  "1111111111111111"));
  
  getJTagController().loadIrAllDevices();
  getJTagController().loadDrAllDevices();

  return getDR();

}


//*************************************
void ROS::XilinxCoolRunnerPld::clear() 
//*************************************
{
}

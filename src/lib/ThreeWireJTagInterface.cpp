// --------x----------------------------x-----------------------------------x--
/** 
 *  \file   ThreeWireJTagInterface.cpp
 *
 *  \author M.Mueller 
 *  
 *  \date   06.12.2004
 *
 *
 *
 */
// --------x----------------------------x-----------------------------------x--

/*
 * $Id$
 * $Revision$
 * $Log$
 * Revision 1.1  2005/05/09 07:26:29  mmueller
 * Added a class for ROBIN Jtag communication (low level).
 *
 *
*/

/**
 * Description:
 *    This file implements the class ThreeWireJTagInterface. This class enables
 *    hardware access to the ROBIN FPGA and PLD JTAG port.
 *
 */

#include "ROSRobin/Robin.h"
#include "ROSRobin/ThreeWireJTagInterface.h"
#include "rcc_time_stamp/tstamp.h"

#include <iostream>

// The constructor which initialises the object with a reference to the
// dedicated Robin object
//*****************************************************************************
ROS::ThreeWireJTagInterface::ThreeWireJTagInterface(ROS::Robin &  robinBoard) :
//*****************************************************************************
  m_waitTime(0),
  m_tdi(1),
  m_tms(1),
  m_tck(1),
  m_tdo(0),
  m_robinBoard(robinBoard)  
{

  // Switch LINTo# off (LINTo# to high)
  // Any ROBIN hardware access may throw an exception when the driver is not open.
  setLINToInt(0);
  setMBoxInt(1);
  
  // Set the MBOX register content to trigger the assertation
  // of LINTo#
  m_robinBoard.writePlx(ROS::Robin::MBOX0, 0x12345678);
  
  int i;
  for ( i = 0; i < 10; i++) {
    setTck(0);
    setTck(1);
  }

}

// Sets the TDI signal level
//**************************************************
void ROS::ThreeWireJTagInterface::setTdi(int value) 
//**************************************************
{
  m_tdi = value;
}


// Sets the TMS signal level
//**************************************************
void ROS::ThreeWireJTagInterface::setTms(int value) 
//**************************************************
{
  m_tms = value;
}


// Sets the TCK signal level. Commits TDI and TMS on raising and
// falling edge.
//**************************************************
void ROS::ThreeWireJTagInterface::setTck(int value) 
//**************************************************
{

  ts_open(0, TS_DUMMY);

  // Raising edge
  if ((m_tck == 0) && (value != 0)) {    

    //    ts_delay(m_waitTime / 2);

    //    std::cout << "Writing TMS: " << m_tms << std::endl;
    // set TMS
    setThreeWireTdiTms(m_tms);
    
    //    ts_delay(m_waitTime / 2);

    setThreeWireTck(1);    

    m_tck = 1;

  }
  // Falling edge
  else if ((m_tck == 1) && (value == 0)) {

    //    ts_delay(m_waitTime / 2);

    //    std::cout << "Writing TDI: " << m_tdi << std::endl;
    // set TDI
    setThreeWireTdiTms(m_tdi);
    
    //    ts_delay(m_waitTime / 2);

    setThreeWireTck(0);
    
    // Read TDO
    m_tdo = getThreeWireTdo();    

    //    std::cout << "Got TDO: " << m_tdo << std::endl;

    m_tck = 0;
  }

  ts_close(TS_DUMMY);

}


// Return the current TDO value (the last read TDO)
//********************************************
int ROS::ThreeWireJTagInterface::getTdo(void) 
//********************************************
{
  return m_tdo;
}


// Set the wait time for the interface communication
//**********************************************************
void ROS::ThreeWireJTagInterface::setWaitTime(double timeInSeconds) 
//**********************************************************
{
  m_waitTime = (unsigned int )(timeInSeconds * (double )1000000);
}


// Set the TDI or TMS value
//**************************************************************
void ROS::ThreeWireJTagInterface::setThreeWireTdiTms(int value) 
//**************************************************************
{
  if (value == 0)
    setLINToInt(1);
  else
    setLINToInt(0);

}


// Set the TCK value
//***********************************************************
void ROS::ThreeWireJTagInterface::setThreeWireTck(int value) 
//***********************************************************
{

  // Read the control register content
  unsigned int ctrl = m_robinBoard.readPlx(ROS::Robin::CNTRL);

  // Set the USERo signal level
  if (value != 0)
    ctrl |= 0x10000;   // Bit16
  else
    ctrl &= ~0x10000;   // Bit16
  
  // Write it to the board
  m_robinBoard.writePlx(ROS::Robin::CNTRL, ctrl);
}


// Set the TDO value
//*************************************************
int ROS::ThreeWireJTagInterface::getThreeWireTdo() 
//*************************************************
{
  int ret;

  // Read the control register content
  unsigned int ctrl = m_robinBoard.readPlx(ROS::Robin::CNTRL);

  // Filter the USERi bit
  if ((ctrl & 0x20000) != 0)   // Bit17
    ret = 1;
  else
    ret = 0;

  // Return its value
  return ret;

}


// Basic access method to assert or de-assert LINTo
//****************************************************************
void ROS::ThreeWireJTagInterface::setLINToInt(unsigned int value)
//****************************************************************
{
  
  unsigned int intcsr = m_robinBoard.readPlx(ROS::Robin::INTCSR);

  if (value != 0)
    intcsr |= 0x10000;   // Bit16
  else
    intcsr &= ~0x10000;  // Bit16
  
  m_robinBoard.writePlx(ROS::Robin::INTCSR, intcsr);

}

// Enables or disables the MBox register as source of interrupt
//***************************************************************
void ROS::ThreeWireJTagInterface::setMBoxInt(unsigned int value) 
//***************************************************************
{
  unsigned int intcsr = m_robinBoard.readPlx(ROS::Robin::INTCSR);

  if (value != 0)
    intcsr |= 0x8;    // Bit3
  else
    intcsr &= ~0x8;   // Bit3
  
  m_robinBoard.writePlx(ROS::Robin::INTCSR, intcsr);  

}


/********************************/
/*				*/
/* Authors: M.M�ller,Hinkelbein	*/
/*				*/
/********************************/

#include <iostream>
#include "ROSRobin/Bit.h"
#include "ROSRobin/ROSRobinExceptions.h"

// Constructor initialising with a bool value
//*******************
ROS::Bit::Bit(bool b) 
//*******************
{
  value = b ? HI : LO;
}


// Constructor initialising with a char
//*************************
ROS::Bit::Bit(const char c) 
//*************************
{
  set(c);
}


// A set method for making a Bit from a char ("1", "0", "X")
//******************************
void ROS::Bit::set(const char c) 
//******************************
{
  switch (c) 
  {
  case '1':
    set(ROS::Bit::HIGH);
    break;
  case '0':
    set(ROS::Bit::LOW);
    break;
  case 'x':
  case 'X':
    set(ROS::Bit::UNDEFINED);
    break;
  default:
    throw ROSRobinExceptions(ROS::ROSRobinExceptions::BITSTREAM, "Bit::set(const char): illegal bit value (must be either '1', '0', or 'X')!");
  }
}


// An assignement operator
//****************************************************
ROS::Bit & ROS::Bit::operator = (const ROS::Bit & bit) 
//****************************************************
{
  value = bit.value;
  return * this;
}


// A comparision operator
//*****************************************************
bool ROS::Bit::operator == (const ROS::Bit & bit) const 
//*****************************************************
{
  return (value == bit.value) ? true : false;
}


// Conversion operator to bool
//*****************************
ROS::Bit::operator bool() const 
//*****************************
{
  switch (value) 
  {
  case HI:
    return true;
  case LO:
    return false;
  default:
    throw ROSRobinExceptions(ROS::ROSRobinExceptions::BITSTREAM, "ROS::Bit::operator bool (): not a valid value!");
  }
}


// A second comparision operator
//*****************************************************
bool ROS::Bit::operator != (const ROS::Bit & bit) const 
//*****************************************************
{
  return (value != bit.value) ? true : false;
}


//******************************************************************
std::ostream & operator << (std::ostream & os, const ROS::Bit & bit) 
//******************************************************************
{
  switch (bit.get()) 
  {
  case ROS::Bit::INVALID:
    os << "~";
    break;
  case ROS::Bit::UNDEFINED:
    os << "X";
    break;
  case ROS::Bit::HIGH:
    os << "1";
    break;
  case ROS::Bit::LOW:
    os << "0";
    break;
  }
  return os;
}


//************************************************************
std::istream & operator >> (std::istream & is, ROS::Bit & bit) 
//************************************************************
{
  char ch;
  is >> ch;
  bit.set(ch);
  return is;
}


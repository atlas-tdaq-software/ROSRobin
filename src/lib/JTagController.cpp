// --------x----------------------------x-----------------------------------x--
/**
 **  \file   JTagController.cpp
 **
 **  \author M. Mueller
 **
 **  \date   03.04.2003
 **
 **
 **/
// --------x----------------------------x-----------------------------------x--


#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSRobin/JTagController.h"
#include "ROSRobin/JTagDevice.h"
#include "ROSRobin/ThreeWireJTagInterface.h"
#include "ROSRobin/BitStream.h"
#include "ROSRobin/Bit.h"
#include "ROSRobin/ROSRobinExceptions.h"

const struct ROS::JTagController::TAPTransitions ROS::JTagController::m_tapFSM[16] =
    {

    { JTagController::TAPIdle ,
      JTagController::TAPReset        }, // TAPReset,

    { JTagController::TAPIdle ,
      JTagController::TAPSelectDrScan }, // TAPIdle,

    { JTagController::TAPCaptureDr ,
      JTagController::TAPSelectIrScan }, // TAPSelectDrScan,

    { JTagController::TAPShiftDr ,
      JTagController::TAPExit1Dr      }, // TAPCaptureDr,

    { JTagController::TAPShiftDr ,
      JTagController::TAPExit1Dr      }, // TAPShiftDr,

    { JTagController::TAPPauseDr ,
      JTagController::TAPUpdateDr     }, // TAPExit1Dr,

    { JTagController::TAPPauseDr ,
      JTagController::TAPExit2Dr      }, // TAPPauseDr,

    { JTagController::TAPShiftDr ,
      JTagController::TAPUpdateDr     }, // TAPExit2Dr,

    { JTagController::TAPIdle ,
      JTagController::TAPSelectDrScan }, //  TAPUpdateDr,

    { JTagController::TAPCaptureIr ,
      JTagController::TAPReset        }, // TAPSelectIrScan,

    { JTagController::TAPShiftIr ,
      JTagController::TAPExit1Ir      }, // TAPCaptureIr,

    { JTagController::TAPShiftIr ,
      JTagController::TAPExit1Ir      }, // TAPShiftIr,

    { JTagController::TAPPauseIr ,
      JTagController::TAPUpdateIr     }, // TAPExit1Ir,

    { JTagController::TAPPauseIr ,
      JTagController::TAPExit2Ir      }, // TAPPauseIr,

    { JTagController::TAPShiftIr ,
      JTagController::TAPUpdateIr     }, // TAPExit2Ir,

    { JTagController::TAPIdle ,
      JTagController::TAPSelectDrScan } //  TAPUpdateIr,
  };


//*************************************************************
ROS::JTagController::JTagController(ROS::Robin &  robinBoard) :
//************************************************************* 
  m_jtagPort(robinBoard),
  m_currentState(JTagController::TAPReset),
  m_nrOfDevices(0),
  m_irSize(0)
{
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"JTagController::constructor: Called.");

  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"JTagController::constructor: calling reset");

  // Calling reset now to bring the chain in a proper state.
  reset();

  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"JTagController::constructor: calling detectChainParameters");
  detectChainParameters(m_nrOfDevices, m_irSize);

  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"JTagController::constructor: number of devices is: " << m_nrOfDevices);
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"JTagController::constructor: IR register size is: " << m_irSize);

  gotoState(ROS::JTagController::TAPReset);


  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"JTagController::constructor: done!");
  
  
}


//*****************************************
ROS::JTagController::~JTagController(void) 
//*****************************************
{

}


//************************************
void ROS::JTagController::reset(void) 
//************************************
{

  unsigned int i;
  for (i = 0; i < 10; i++) {
    pulseTms(1);
  }
}


//*********************************************************
void ROS::JTagController::loadIrAllDevices(void)
//*********************************************************
{

  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Entering JTagController::loadIrAllDevices");

  // Move to shiftIR state for all devices
  gotoState(ROS::JTagController::TAPShiftIr);
  
  // Start at the end of the JTag chain
  std::vector<JTagDevice *  >::reverse_iterator device;

  // Now go through all devices
  for (device = m_jtagChain.rbegin(); (device != m_jtagChain.rend()); device++) {
    
    // Take the IR bitstream to shift into thedevice
    ROS::BitStream newIrContent = (*device)->getNextIr();
    ROS::BitStream returningIrContent = (*device)->getNextIr();;

    DEBUG_TEXT(DFDB_ROSROBIN, 20, "Writing to device: " << newIrContent);

    unsigned int i;
    for (i = 0; i < newIrContent.getLength(); i++) {

      // Clock the bit value
      setTdi(newIrContent.getAt(i));
      if ((*device == m_jtagChain.front()) && (i == (newIrContent.getLength() - 1 ))) {
	gotoState(ROS::JTagController::TAPExit1Ir);
      }
      else {
	gotoState(ROS::JTagController::TAPShiftIr);      
      }

      // And store what is coming back
      returningIrContent.setAt(i, getTdo());
    }

    (*device)->setIrReturnContent(returningIrContent);

  }
 

  gotoState(ROS::JTagController::TAPIdle);

  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Leaving JTagController::loadIrAllDevices");

}


//*********************************************************
void ROS::JTagController::loadDrAllDevices(void) 
//*********************************************************
{

  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Entering JTagController::loadDrAllDevices");
  // Move to shiftIR state for all devices
  gotoState(ROS::JTagController::TAPShiftDr);
  
  // Start at the end of the JTag chain
  std::vector<JTagDevice *  >::reverse_iterator device = m_jtagChain.rbegin();

  // Now go through all devices
  for (;device != m_jtagChain.rend(); device++) {
    
    // Take the IR bitstream to shift into thedevice
    ROS::BitStream newDrContent = (*device)->getNextDr();
    ROS::BitStream returningDrContent = (*device)->getNextDr();

    unsigned int i;
    for (i = 0; i < newDrContent.getLength(); i++) {

      // Clock the bit value
      setTdi(newDrContent.getAt(i));
      if ((*device == m_jtagChain.front()) && (i == (newDrContent.getLength() - 1 ))) {
	gotoState(ROS::JTagController::TAPExit1Dr);
      }
      else {
	gotoState(ROS::JTagController::TAPShiftDr);      
      }

      // And store what is coming back
      returningDrContent.setAt(i, getTdo());
    }

    (*device)->setDrReturnContent(returningDrContent);

  }
  
  gotoState(ROS::JTagController::TAPIdle);

  DEBUG_TEXT(DFDB_ROSROBIN, 20, "Leaving JTagController::loadDrAllDevices");

}


//***********************************************************************************
void ROS::JTagController::gotoState(enum ROS::JTagController::TAPStates targetState) 
//***********************************************************************************
{

    unsigned int targetStateScore = (unsigned int) targetState;

    unsigned int i = 0;

    do {

        unsigned int currentStateScore = (unsigned int) m_currentState;

        if (i > 200) {
///old code  throw ROS::ROSRobinExceptions(ROS::ROSRobinExceptions::JTAG, "Error when walking through the JTag FSM. The path to the target state could not be found!!");
          CREATE_ROS_EXCEPTION(ex1, ROS::ROSRobinExceptions, JTAG, "Error when walking through the JTag FSM. The path to the target state could not be found!!");
          throw (ex1);

        }
        else {
            i++;
        }

        if (m_tapFSM[currentStateScore].m_stateTms0 == targetState) {
            pulseTms(0);
            m_currentState = m_tapFSM[currentStateScore].m_stateTms0;
      // Switch to m_stateTms0
        }
        else if (m_tapFSM[currentStateScore].m_stateTms1 == targetState) {
      // Switch to m_stateTms1
            pulseTms(1);
            m_currentState = m_tapFSM[currentStateScore].m_stateTms1;
        }
        else {

            unsigned int nextState0Score = (unsigned int) m_tapFSM[currentStateScore].m_stateTms0;
            unsigned int nextState1Score = (unsigned int) m_tapFSM[currentStateScore].m_stateTms1;

            if ((nextState0Score < targetStateScore) && (targetStateScore < nextState1Score)) {
                pulseTms(0);
                m_currentState = m_tapFSM[currentStateScore].m_stateTms0;
      // Switch to m_stateTms0
            }
            else if ((nextState1Score < targetStateScore) && (targetStateScore < nextState0Score)) {
                pulseTms(1);
                m_currentState = m_tapFSM[currentStateScore].m_stateTms1;
      // Switch to m_stateTms1
            }
            else if (nextState0Score < nextState1Score) {
                pulseTms(1);
                m_currentState = m_tapFSM[currentStateScore].m_stateTms1;
            }
            else {
                pulseTms(0);
                m_currentState = m_tapFSM[currentStateScore].m_stateTms0;
            }
        }
    } while (m_currentState != targetState);
}


//***********************************************************
void ROS::JTagController::appendToChain(JTagDevice * device)
//***********************************************************
{
  m_jtagChain.push_back(device);
}


//*************************************************************
void ROS::JTagController::removeFromChain(JTagDevice * device)
//*************************************************************
{
  std::vector<JTagDevice *  >::iterator toBeDeleted;
  toBeDeleted = find(m_jtagChain.begin(), m_jtagChain.end(), device);
  if (toBeDeleted == m_jtagChain.end()) 
  {
    CREATE_ROS_EXCEPTION(ex2, ROS::ROSRobinExceptions, JTAG, "Trying to delete a device from a JTAG chain which is not present");
    throw (ex2);
  }

  m_jtagChain.erase(toBeDeleted);
}



//************************************************************************
inline std::vector<ROS::JTagDevice * > ROS::JTagController::getChain(void)
//************************************************************************
{

  return m_jtagChain;

}

// Returns the number of JTag Devices in the hardware chain
//***************************************************************
unsigned int ROS::JTagController::getNumberOfDevices(void)
//***************************************************************
{
  return m_nrOfDevices;
}

// Returns the size of the IR register of all devices together
//***************************************************************
unsigned int ROS::JTagController::getIRRegistersSize(void)
//***************************************************************
{
  return m_irSize;
}



/* Detects the chain parameters of the JTag chain.
   Warning: This method chances the IR register contents and does not restore them!
*/
//**************************************************************************************************
void ROS::JTagController::detectChainParameters(unsigned int &  nrOfDevices, unsigned int &  irSize)
//**************************************************************************************************
{

  // A timeout counter to make all the loops in this method ending after maxSize shift operations
  unsigned int timeout, maxSize = 10000;


  // This teststream is used to detect the size of the IR register.
  ROS::BitStream returnSyncStream  ("0000000000000000");
  ROS::BitStream expectedSyncStream("0101010101010101");
  
  gotoState(ROS::JTagController::TAPShiftIr);
  
  // first shift in a series of "01" bits for sync
  
  bool sync = false;
  bool nextBit = true;
  timeout = 0;
  
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"JTagController::detectChainParameters: detecting the IR length!");
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"JTagController::detectChainParameters: first shift in a sync bitstream");
  
  // Now sync the bitstream in the IR. This is required to get
  // a starting point (a well defined bitstream coming from TDO) 
  // for the register size test.
  while (!sync) {

    if (timeout > maxSize) 
    {
      CREATE_ROS_EXCEPTION(ex3, ROS::ROSRobinExceptions, JTAG, "Error when validating the JTag chain. Sync of the IR register could not be done. I never get out what I put in. The chain seems to be endless");
      throw (ex3);
    }
    timeout++;

    // Shift the result stream by 2 to get space for two new bits
    returnSyncStream.shl(1);

    // Send a 1 and then a 0 and get the TDO values.
    setTdi(Bit(nextBit));
    gotoState(ROS::JTagController::TAPShiftIr);
    returnSyncStream.setAt(1, getTdo());
    nextBit = !nextBit;

    DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"JTagController::detectChainParameters: return stream: " << returnSyncStream);

    // wait until we see at least 8 series of 10.
    if (returnSyncStream == expectedSyncStream) {
      DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"JTagController::detectChainParameters: sync ok");
      sync = true;
    }
  }

  ROS::BitStream returnTestStream  ("0000000000000000");
  ROS::BitStream expectedTestStream("1111111111111111");

  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"JTagController::detectChainParameters: detecting the IR length now");

  // Now shift in a series of 1 and count the delay clocks
  // until it appears at TDO. This delay is the overall IR 
  // register size.
  bool gotSize=false;
  timeout = 0;
  irSize = 0;

  // shift in a zero to be shure not to confuse the next algorithm
  setTdi(Bit('0'));
  gotoState(ROS::JTagController::TAPShiftIr);

  while (!gotSize) {

    if (timeout > maxSize) 
    {
      CREATE_ROS_EXCEPTION(ex4, ROS::ROSRobinExceptions, JTAG, "Error when validating the JTag chain. Detecting IR register size could not be done. I never get out what I put in. The chain seems to be endless");
      throw (ex4);
    }
    timeout++;

    // Increment the IR register size valiable.
    irSize++;

    // Shift the return stream by 1
    returnTestStream.shl(1);
    
    // Send a 1 and get the TDO value
    setTdi(Bit('1'));
    gotoState(ROS::JTagController::TAPShiftIr);
    returnTestStream.setAt(0, getTdo());

    DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"JTagController::detectChainParameters: return stream: " << returnTestStream);

    // wait until we see at least 16 values of 1.
    if (returnTestStream == expectedTestStream) {
      gotSize = true;
    }
  }

  // Now the size is the clock cycles required to see sixteen 1 bits minus the clocks
  // to shift the 16 one bits.
  irSize = irSize - returnTestStream.getLength();

  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"JTagController::detectChainParameters: IR length is " << m_irSize);

  // Now go back to Runtest/Idle state
  setTdi(Bit('1'));
  gotoState(ROS::JTagController::TAPExit1Ir);

  gotoState(ROS::JTagController::TAPIdle);

  // All devices are in bypass now.
  // Now test the number of devices by testing the size of all DR registers alltogether.
  // Since the devices are in Bypass mode the DR size of each device is exactly one.

  gotoState(ROS::JTagController::TAPShiftDr);

  // first shift in a series of "01" bits for sync

  sync = false;
  nextBit = true;
  timeout = 0;
  returnSyncStream.set("0000000000000000");

  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"JTagController::detectChainParameters: detecting the DR length!");
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"JTagController::detectChainParameters: first shift in a sync bitstream");

  // Now sync the bitstream in the DR. This is required to get
  // a starting point (a well defined bitstream coming from TDO) 
  // for the register size test.
  while (!sync) {

    if (timeout > maxSize) 
    {
      CREATE_ROS_EXCEPTION(ex5, ROS::ROSRobinExceptions, JTAG, "Error when validating the JTag chain. Sync of the DR register could not be done. I never get out what I put in. The chain seems to be endless");
      throw (ex5);
    }
    timeout++;

    // Shift the result stream by 2 to get space for two new bits
    returnSyncStream.shl(1);

    // Send a 1 and then a 0 and get the TDO values.
    setTdi(Bit(nextBit));
    gotoState(ROS::JTagController::TAPShiftDr);
    returnSyncStream.setAt(1, getTdo());
    nextBit = !nextBit;
    
    DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"JTagController::detectChainParameters: return stream: " << returnSyncStream);

    // wait until we see at least 8 series of 10.
    if (returnSyncStream == expectedSyncStream) {
      DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"JTagController::detectChainParameters: sync ok");
      sync = true;
    }
  }

  gotSize = false;
  returnTestStream.set  ("0000000000000000");
  nrOfDevices = 0;

  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"JTagController::detectChainParameters: detecting the DR length now");

  // shift in a zero to be shure not to confuse the next algorithm
  setTdi(Bit('0'));
  gotoState(ROS::JTagController::TAPShiftDr);

  // Now shift in a series of 1 and count the delay clocks
  // until it appears at TDO. This delay is the overall DR 
  // register size.
  gotSize=false;
  timeout = 0;
  while (!gotSize) {

    if (timeout > maxSize) 
    {
      CREATE_ROS_EXCEPTION(ex6, ROS::ROSRobinExceptions, JTAG, "Error when validating the JTag chain. Detecting DR register size could not be done. I never get out what I put in. The chain seems to be endless");
      throw (ex6);
    }
    timeout++;

    // Increment the number of devices variable.
    nrOfDevices++;

    // Shift the return stream by 1
    returnTestStream.shl(1);
    
    // Send a 1 and get the TDO value
    setTdi(Bit('1'));
    gotoState(ROS::JTagController::TAPShiftDr);
    returnTestStream.setAt(0, getTdo());

    DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"JTagController::detectChainParameters: return stream: " << returnTestStream);

    // wait until we see at least 16 values of 1.
    if (returnTestStream == expectedTestStream) {
      gotSize = true;
    }
  }

  // Now the number of devices is the clock cycles required to see sixteen 1 bits minus the clocks
  // to shift the 16 one bits.
  nrOfDevices = nrOfDevices - returnTestStream.getLength();

  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"JTagController::detectChainParameters: Number of devices is: " << nrOfDevices);

  // Now go back to Runtest/Idle state
  setTdi(Bit('1'));
  gotoState(ROS::JTagController::TAPExit1Dr);

  gotoState(ROS::JTagController::TAPIdle);

}



//********************************************
void ROS::JTagController::pulseTms(int value) 
//********************************************
{
    m_jtagPort.setTms(value);
    m_jtagPort.setTck(1);
    m_jtagPort.setTck(0);
    m_jtagPort.setTck(1);
  //  std::cout << "PulseTms: " << value << std::endl;
}


//*****************************************************
void ROS::JTagController::setTdi(const ROS::Bit value) 
//*****************************************************
{
   //   std::cout << "SetTDI: " << value << std::endl;
    if (value == Bit('0')) {
        m_jtagPort.setTdi(0);
    }
    else if (value == Bit('1')) {
        m_jtagPort.setTdi(1);
    }
}


//*****************************************
ROS::Bit ROS::JTagController::getTdo(void) 
//*****************************************
{

    if (m_jtagPort.getTdo() == 0) {
        return Bit('0');
    }
    else if (m_jtagPort.getTdo() == 1) {
        return Bit('1');
    }

    return Bit('1');

}

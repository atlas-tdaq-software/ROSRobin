/*****************************************************************************/
/*                                                                           */
/* File Name        : robin_slink_dst.cpp                                    */
/*                                                                           */
/* Author           : Markus Joos			                     */
/*                                                                           */
/***** C 2006 - A nickel program worth a dime ********************************/

#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <cmdl/cmdargs.h>
#include <tmgr/tmresult.h>
#include "ROSEventFragment/ROBFragment.h"
#include "ROSRobin/ROSRobinExceptions.h"
#include "ROSRobin/Robin.h"
#include "ROSRobin/Rol.h"
#include "ROSBufferManagement/Buffer.h"
#include "ROSMemoryPool/MemoryPage.h"
#include "ROSMemoryPool/WrapperMemoryPool.h"
#include "ROSGetInput/get_input.h"
#include "DFDebug/DFDebug.h"
#include "rcc_time_stamp/tstamp.h"
#include "ROSRobin/robin.h"

using namespace ROS;
using namespace daq::tmgr;

#ifndef TRUE
  #define TRUE                  0x01
#endif

#ifndef FALSE
  #define FALSE                 0x00
#endif

// global variables
int verbose = FALSE;
int print = FALSE;
int dcheck = TRUE;
int looponevents = FALSE;
u_int loopl1id = 0;
int crccheck = FALSE;
int datagen = FALSE;
int L1extdcheck = TRUE; 
int npackets = -1;
int occurence = 1;
int useecrs = FALSE;
int nolreset = FALSE;
int testsize = 100;
int maxrxpages = 2;
int robinpagesize = 512;
int usdelay = 0;
int test_status = TmPass;
int writefile = FALSE;
int outputFile;
char filename[200] = {0};
int checkfile = FALSE;
int inputFile;
int firstl1id = 0;
u_int mrid = 0;   
char filename2[200] = {0};
int rawdump = 0, dev, flg_error, ipacket, lpacket, delta_time, fragmentSize;
long start_time,end_time;
float t_per_packet, packet_per_s, mb_per_s;
u_int rec_size,exp_size, last_l1id = 999999;
char refbuf[0x100000];
u_int dblevel = 0, dbpackage = 14;
bool DVS = FALSE;
Rol *prol = 0;
Robin *rorobin;
u_int li1d_list[64 * 1024];   
static u_int mori = 0, noecr = 0;   

typedef enum printType { SUMMARY=0 , FULL=1 } PrintType;
typedef enum checkType { SIZE , L1ID , GLOBAL } CheckType;

CheckType ccode;
int errors[GLOBAL + 1]    = { GLOBAL * 0 , 0 };
int errorsTot[GLOBAL + 1] = { GLOBAL * 0 , 0 };

// Constants
#define MAX_PACKET_SIZE  (1024 * 1024)  // = 1 MB 
#define MAXROLS          18 


//Prototypes
u_int next_l1id_ecr(Rol *prol, u_int *l1id);
u_int fill_ecr_list(Rol *prol);
void dumpids(Rol *prol);


/**************/
void usage(void)
/**************/
{
  std::cout << "Valid options are ..." << std::endl;
  std::cout << "-o x: Occurence (= ROL number 1..N)                -> Default: " << occurence << std::endl;
  std::cout << "-n x: Number of packets                            -> Default: infinite" << std::endl;
  std::cout << "-l x: Restart from L1ID=0 when L1ID reaches x      -> Default: off" << std::endl;
  std::cout << "-I x: Start with L1ID=x                            -> Default: 1" <<std::endl;
  std::cout << "-a  : Check ROB CRC                                -> Default: FALSE" << std::endl;
  std::cout << "-g  : Start without link reset (just a test)       -> Default: FALSE" << std::endl;
  std::cout << "-b  : Use RolDataGen                               -> Default: FALSE" << std::endl;
  std::cout << "  -S: TestSize in words                            -> Default: 100" << std::endl;
  std::cout << "-e  : Use ECRs                                     -> Default: FALSE" << std::endl;
  std::cout << "-p  : Print packets                                -> Default: FALSE" << std::endl;
  std::cout << "-c  : Disable data check                           -> Default: FALSE" << std::endl;  
  std::cout << "  Modifiers of -c option:" << std::endl;
  std::cout << "  -L: Disable checking that L1ids are consecutive  -> Default: FALSE" << std::endl;  
  std::cout << "-f x: Write data to file. The parameter is the path and the name of the file" << std::endl;
  std::cout << "-C x: Compare with data from file. The parameter is the path and the name of the reference file" << std::endl;
  std::cout << "-v  : Verbose output                               -> Default: FALSE" << std::endl;
  std::cout << "-r x: Number of raw data words to be dumped        -> Default: 0" << std::endl;
  std::cout << "-w x: Number of microseconds to wait after" << std::endl;
  std::cout << "      the reception of a packet                    -> Default: 0" << std::endl;  
  std::cout << "-s  : ROBIN page size (in words)                   -> Default: 512" << std::endl;
  std::cout << "-m  : Max number of pages per event (1..512)       -> Default: 2" << std::endl;
  std::cout << "-d  : Debug level                                  -> Default: 0" << std::endl;
  std::cout << "-D  : Debug package                                -> Default: 14" << std::endl;
  std::cout << "--DVS : To be used by DVS                          -> Default: FALSE" << std::endl;
  std::cout << std::endl;
}


/***************************/
void printStat(PrintType arg) 
/***************************/
{
  std::cout << std::endl << std::endl << "----------Test Statistics-----------" << std::endl;

  if (arg) 
  {
    std::cout << " # packets transferred       = " << (ipacket - lpacket) << std::endl;
    std::cout << " # corrupted packets         = " << errors[GLOBAL] << std::endl;
    std::cout << "      wrong size             = " << errors[SIZE] << std::endl;
    std::cout << "      wrong L1id             = " << errors[L1ID] << std::endl;
    std::cout << " time elapsed (s)            = " << delta_time << std::endl;
    std::cout << "------------------------------------" << std::endl;
  }
  
  std::cout << " Total # packets transferred = " << (ipacket - 1) << std::endl;
  std::cout << " Total # corrupted packets   = " << errorsTot[GLOBAL] << std::endl;
  std::cout << "            wrong size       = " << errorsTot[SIZE] << std::endl;
  std::cout << "            wrong L1id       = " << errorsTot[L1ID] << std::endl;
  
  if (arg && (ipacket - lpacket)) 
  {    
    t_per_packet = ((float) delta_time * 1000000) / (ipacket - lpacket);
    packet_per_s = (float)1000000 / t_per_packet;
    mb_per_s = ((float)(ipacket - lpacket) * fragmentSize * 4) / ((float)delta_time * 1024 * 1024);
    std::cout << "------------------------------------" << std::endl;
    std::cout << " time/packet = " << t_per_packet << " microseconds" << std::endl;
    std::cout << " packet/s    = " << packet_per_s << " packet/s" << std::endl;
    std::cout << " Mbyte/s     = " << mb_per_s << std::endl;
  }

  std::cout << "------------------------------------" << std::endl << std::endl;
}


/*****************************/
void errorFound(CheckType type) 
/*****************************/
{
  if (!flg_error) 
  {
    errors[GLOBAL]++;
    errorsTot[GLOBAL]++;
  }
  flg_error = 1;
  errors[type]++;
  errorsTot[type]++;
  test_status = TmFail;
}


/*********************/
void terminate_it(void) 
/*********************/
{
  err_type code;

  std::cout << "printing last statistics" << std::endl;
  printStat(SUMMARY);

  std::cout << std::endl << std::endl <<"Closing S-link ..." << std::endl;

  prol->setConfig(enum_cfgRolEnabled, 0);
  std::cout << "ROL disabled" << std::endl;

  delete prol;
  std::cout << "prol deleted" << std::endl;
  rorobin->freeMsgResources(); 
  std::cout << "message resources returned" << std::endl;
  delete rorobin;  
  std::cout << "rorobin deleted" << std::endl;

  code = ts_close(TS_DUMMY);
  if (code) 
    rcc_error_print(stdout, code);
  std::cout << "ts_close done" << std::endl;

  if (writefile)
    close (outputFile);
}


// sigquit handler
/**********************************/
void sigquit_handler(int /*signum*/)
/**********************************/
{
  int i;
  
  if (start_time) 
  {
    time(&end_time);
    delta_time = end_time - start_time;
    if (delta_time) 
      printStat(FULL);
  }
  
  // reset flags
  lpacket = ipacket;
  for (i = 0; i < GLOBAL; i++) 
    errors[i] = 0;
  time(&start_time);
}


// sigint handler
/*********************************/
void sigint_handler(int /*signum*/)
/*********************************/
{
  terminate_it();
  exit(TmPass);
}


/*****************************/
int main(int argc, char **argv)
/*****************************/
{
  int c, robin_nr, rol_nr;
  u_int next_ok, l1id, code, maxdump, loop;
  u_long *fragBufferAddress;
  u_int numberOfOutstandingReq = 1, msgInputMemorySize = 0x20, miscSize = 0x2000;
  u_int eventBufferSize = MAX_PACKET_SIZE;
  u_long ticket;
  struct sigaction sa, saint;
  sigset_t smask;
  MemoryPage *mem_page;
  WrapperMemoryPool *m_memoryPool;
  ROBFragment *robFragment;
  std::vector <u_int> l1ids;

  static struct option long_options[] = {"DVS", no_argument, NULL, '1'}; 
  while ((c = getopt_long(argc, argv, "I:abegho:n:l:pcLvf:C:d:s:S:m:D:r:w:1", long_options, NULL)) != -1)
    switch (c) 
    {
    case 'h':
      std::cout << "Usage: " << argv[0] << " [options]: "<< std::endl;
      usage();
      exit(TmUnresolved);
      break;

    case 'o':
      occurence = atoi(optarg);
      if (occurence <= 0 || occurence > MAXROLS)
      { 
	std::cout << "Occurence exceeds allowed bounds" << std::endl;
        exit(TmUnresolved);
      }
      break;

    case 'n':
      npackets = atoi(optarg);
      if (npackets<0) 
      {
	std::cout << "number of packets must be positive" << std::endl;
	exit(TmUnresolved);
      } 
      break;
                  
    case 'f':   
      writefile = TRUE;
      sscanf(optarg,"%s", filename);
      std::cout << "writing data to file " << filename << std::endl;
      break;

    case 'l':   
      looponevents = TRUE;
      loopl1id = atoi(optarg);
      break;

     case 'C':   
      checkfile = TRUE;
      sscanf(optarg, "%s", filename2);
      std::cout << "Reading reference fragment from file " << filename2 << std::endl;
      break;
   
    case 'r': rawdump = atoi(optarg);       break; 
    case 'S': testsize = atoi(optarg);      break; 
    case 'd': dblevel = atoi(optarg);       break;
    case 'D': dbpackage = atoi(optarg);     break;                   
    case 'I':            
      if (sscanf(optarg, "%x", &firstl1id) != 1)
        printf("Failed to decode first L1ID\n");
      break;               
    case 'w': usdelay = atoi(optarg);       break;                   
    case 'p': print = TRUE;                 break;
    case 'g': nolreset = TRUE;              break;
    case 'c': dcheck = FALSE;               break;
    case 'L': L1extdcheck = FALSE;          break;
    case 'e': useecrs = TRUE;               break;
    case 'a': crccheck = TRUE;              break;
    case 'b': datagen = TRUE;               break;
    case 'v': verbose = TRUE;               break;
    case 's': robinpagesize = atoi(optarg); break;
    case 'm': maxrxpages = atoi(optarg);    break;
    case '1': DVS = TRUE;                   break;

    default:
      std::cout << "Invalid option " << c << std::endl;
      std::cout << "Usage: " << argv[0] << " [options]: " << std::endl;
      usage();
      exit (TmUnresolved);
    }

  if ((robinpagesize * maxrxpages * 4) > MAX_PACKET_SIZE)
  {
    std::cout << " robinpagesize * maxrxpages * 4 = " << robinpagesize * maxrxpages * 4 << std::endl;
    std::cout << " MAX_PACKET_SIZE = " << MAX_PACKET_SIZE << std::endl;
    exit (TmUnresolved);
  }

  std::cout << " Ready to receive ROD events of up to " << robinpagesize * maxrxpages << " words" << std::endl;

  // Initialize the debug macro
  DF::GlobalDebugSettings::setup(dblevel, dbpackage);      
      
  // Install signal handler for SIGQUIT (ctrl + \)
  sigemptyset(&sa.sa_mask);
  sa.sa_flags   = 0;
  sa.sa_handler = sigquit_handler;

  // Dont block in intercept handler
  if (sigaction(SIGQUIT, &sa, NULL) < 0) 
  {
    std::cout << "main: sigaction() FAILED with ";
    code = errno;
    if (code == EFAULT) std::cout << "EFAULT" << std::endl;
    if (code == EINVAL) std::cout << "EINVAL" << std::endl;
    exit(TmUnresolved);
  }

  // Install signal handler for SIGINT (ctrl + c) 
  sigemptyset(&saint.sa_mask);
  saint.sa_flags   = 0;
  saint.sa_handler = sigint_handler;

  if (sigaction(SIGINT, &saint, NULL) < 0) 
  {
    std::cout << "main: sigaction() FAILED with ";
    code = errno;
    if (code == EFAULT) std::cout << "EFAULT" << std::endl;
    if (code == EINVAL) std::cout << "EINVAL" << std::endl;
    exit(TmUnresolved);
  }

  code = ts_open(0, TS_DUMMY);
  if (code) 
  {
    rcc_error_print(stdout, code);
    exit (TmFail);
  }

  robin_nr = (occurence - 1) / 3;
  rol_nr = (occurence - 1) % 3;
  std::cout << "Opening access to ROL " << (rol_nr + 1) << " on ROBIN " << (robin_nr + 1) << std::endl;
   
  try
  {
    rorobin = new Robin(robin_nr, 100.0);
    prol = new Rol(*rorobin, rol_nr);
    rorobin->open();
    rorobin->reserveMsgResources(numberOfOutstandingReq, msgInputMemorySize, miscSize, eventBufferSize); 
    
    if (nolreset == FALSE)
    {
      prol->setConfig(enum_cfgPageSize, robinpagesize);
      prol->setConfig(enum_cfgMaxRxPages, maxrxpages);
    }
    if (datagen)
    {
      if(verbose)
	std::cout << "Enabling internal fragment generation for fragments of " << testsize << " words" << std::endl;
      if (nolreset == FALSE)
      {
        prol->setConfig(enum_cfgTestSize, testsize);
        prol->setConfig(enum_cfgRolDataGen, 1);
      }
    }
    else
      if (nolreset == FALSE)
        prol->setConfig(enum_cfgRolDataGen, 0);

    if (looponevents)
      prol->setConfig(enum_cfgRolEmu, enum_emuTypeUpload);
    else
      if (nolreset == FALSE)
        prol->setConfig(enum_cfgRolEmu, 0);
  
  
    if (nolreset == FALSE)
      prol->setConfig(enum_cfgRolEnabled, 1);

    if (crccheck)
    {
      if(verbose)
	std::cout << "Enabling CRC check for every ROB fragment" << std::endl;
      prol->setCRCInterval(1);
    }
  
    //Create the Memory Pool
    u_long eventBase       = rorobin->getVirtEventBase();
    u_int physicalAddress  = rorobin->getPhysEventBase();
    u_int npages           = 1;
    if (verbose)
    {      
      std::cout << "getVirtEventBase returns: 0x" << HEX(eventBase) << std::endl;
      std::cout << "getPhysEventBase returns: 0x" << HEX(physicalAddress) << std::endl;
    }
    m_memoryPool = new WrapperMemoryPool(npages, eventBufferSize, eventBase, physicalAddress);
  }
  catch (ROSRobinExceptions& e)
  {
    std::cout << e << std::endl;
    exit(TmUnresolved);
  }

  //Get a page
  try
  {
    mem_page = m_memoryPool->getPage();  //MJ: Check for error!!
    fragBufferAddress = static_cast<u_long *> (mem_page->reserve(mem_page->capacity()));
    if (verbose)
      std::cout << "fragBufferAddress = 0x" << HEX(fragBufferAddress) << std::endl;
  }
  catch (MemoryPoolException& e)
  {
    std::cout << e;
    exit (TmFail);
  }
  
  // Open the file for output
  if (writefile)
  {
    outputFile = open(filename, O_WRONLY|O_CREAT|O_LARGEFILE, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
    if (outputFile < 0)
    {
      std::cout << "Failed to open " << filename << std::endl;
      exit(TmUnresolved);
    }
  }
        
  // Open the reference file
  if (checkfile)
  {
    inputFile = open(filename2, O_RDONLY);
    if (inputFile < 0)
    {
      std::cout << "Failed to open " << filename2 << std::endl;
      exit(TmUnresolved);
    }

    int refsize = read(inputFile, refbuf, 0x100000);
    std::cout << "The reference file contains " << refsize << " bytes" << std::endl;
    if (refsize < 44)
    {
      std::cout << "The reference file contains less than 44 bytes (i.e. ROD header + trailer)" << std::endl;
      exit(TmUnresolved);
    }
    if (refsize == 0x100000)
    {
      std::cout << "The reference file contains more than 1 MB" << std::endl;
      exit(TmUnresolved);
    }
    close(inputFile);
  }
  
  time(&start_time);
  lpacket = 1;

  std::cout << std::endl << "Press ctrl-\\ to output statistics" << std::endl;
  std::cout << "Press ctrl-c to quit" << std::endl << std::endl;
  
  // Creation of synchronisation file. After this file is succesfully created, 
  // the second test (source test) is started
  //if (DVS)
  //  pmg_initSync();
  
  ipacket = 1;
  l1id = firstl1id;
  mrid = firstl1id;
  
  if(verbose)
  {
    std::cout << "l1id = " << l1id << std::endl;
    std::cout << "firstl1id = " << firstl1id << std::endl;
  }
  
  sigemptyset(&smask);
  
  while(ipacket <= ((npackets >= 0) ? npackets : (ipacket))) 
  {
    sigaddset(&smask, SIGINT);
    sigprocmask(SIG_BLOCK, &smask, NULL);  // Temporarily block ctrl + c

    ticket = prol->requestFragment(l1id, reinterpret_cast<u_long> (fragBufferAddress));
    if(verbose)
    {
      std::cout << "Requesting L1ID = " << l1id << std::endl;
      std::cout <<  "ticket         = " << HEX(ticket) << std::endl;
    }
    
    prol->getFragment(ticket);
    if(verbose)
      std::cout << "Event fragment for L1ID " << l1id << " received" << std::endl;
  
    sigprocmask(SIG_UNBLOCK, &smask, NULL);  // Re-enable ctrl + c
  
    fragmentSize = (*((u_int *)ticket + 1)) * sizeof(u_int); // in bytes
    mem_page->release(mem_page->usedSize() - fragmentSize);  
    robFragment = new ROBFragment(mem_page); 
 
    if (robFragment->status() & enum_fragStatusPending) 
    {
      if (verbose)
        std::cout << "Event fragment for L1ID " << l1id << " is still pending" << std::endl;
      delete robFragment;
    }
    else
    {
      if(verbose)
        std::cout << "ticket = 0x" << HEX(ticket) << std::endl;
  
      fragmentSize = fragmentSize >> 2; //byte to word
      if (rawdump)
      {
	if (fragmentSize > rawdump)
          maxdump = rawdump;
	else
          maxdump = fragmentSize;

        u_long virtaddr;
	virtaddr = reinterpret_cast<u_long>(fragBufferAddress);      
	if(verbose)
          std::cout << "virtaddr = 0x" << HEX(virtaddr) << std::endl;

	u_int *dataptr;
	dataptr = (u_int *)virtaddr;
	
	std::cout << "Dumping the first " << maxdump << " words of the ROB data at 0x" << HEX(dataptr) << std::endl;
	for(loop = 0; loop < maxdump; loop++)
          std::cout << "Word " << loop << " = 0x" << HEX(*dataptr++) << std::endl;
      }
                           
      if (print) 
        std::cout << *robFragment;
      
      if (writefile)
      {
        const Buffer* buffer = robFragment->buffer();
        Buffer::page_iterator page = buffer->begin(); 
        if (verbose)
          std::cout << "writing " << fragmentSize * 4 << " bytes to file" << std::endl; 
        int isok = write(outputFile, (void *)((u_long)(*page)->address()), fragmentSize * 4);
        if (isok < 0)
        {
          std::cout << "Error in writing to file" << std::endl;
          exit(TmUnresolved);
        }
      }
                                  
      // Check the event
      if(dcheck)
      {
        flg_error = 0;
        ccode = (CheckType) robFragment->check(ipacket - 1, L1extdcheck);
        if (code && verbose)
          std::cout << "Check of ROD fragment returns error " << ccode << std::endl;
        if (ccode)
          errorFound(ccode);
      }
    
      if (checkfile)
      {
        u_int *refptr = (u_int *)refbuf;
        u_int *dataptr = (u_int *)mem_page->address();
        for (int dataword = 0; dataword < fragmentSize; dataword++)
        {
          if (*refptr != *dataptr)
            std::cout << "Data mismatch: Offset = 0x" << HEX(dataword * 4) << " Received data = 0x" << HEX(*dataptr) << "   Expected data = 0x" << HEX(*refptr) << std::endl;
          refptr++;
          dataptr++;
        }
      }

      // We don't need the fragment any more
      delete robFragment;

      if (!looponevents)
      {
	l1ids.push_back(l1id);
	u_int nfailed = prol->releaseFragment(&l1ids);
	if (nfailed)
        {
	  std::cout << "Failed to delete " << nfailed << " events. First (and only) L1ID = " << l1id << std::endl;
	  std::cout << "===============================================================" << std::endl;
	  dumpids(prol);
	  std::cout << "===============================================================" << std::endl;
        }
	l1ids.clear();
      }
      
      ipacket++;
     
      if(useecrs)
      {
	next_ok = 0;
	while (!next_ok) 
	{
	  next_ok = next_l1id_ecr(prol, &l1id);
          if (verbose && next_ok)
            std::cout << "Now requesting L1ID = 0x" << HEX(l1id) << std::endl;
        }
      }
      else
        l1id++;

      if (looponevents && l1id > loopl1id)
        l1id = 0;
	
      if (verbose)
        std::cout << "Number of events received: " << (ipacket - 1) << std::endl;
      }
    
    if (usdelay)
      ts_delay(usdelay);
  }
  terminate_it();
  exit(TmPass); 
}


/****************************/
u_int fill_ecr_list(Rol *prol)
/****************************/
{      
  static Rol::ECRStatisticsBlock ecr_info;
  u_int startid, llp;   
  sigset_t sigint;
  
  //Disable SIGINT to avoid a race condition with the code called on exit
  sigemptyset(&sigint);
  sigaddset(&sigint, SIGINT);
  sigprocmask(SIG_BLOCK, &sigint, NULL);
  
  ecr_info = prol->getECR();
  sigprocmask(SIG_UNBLOCK, &sigint, NULL);  

  if (mori != ecr_info.mostRecentId || noecr != ecr_info.necrs)
  {
    mori = ecr_info.mostRecentId;
    noecr = ecr_info.necrs;
  }
  
  if (verbose)
  {
    std::cout << "fill_ecr_list: Most recent Id   = 0x" << HEX(ecr_info.mostRecentId) << std::endl;
    std::cout << "fill_ecr_list: Overflow         = " << ecr_info.overflow << std::endl;
    std::cout << "fill_ecr_list: Number of ECRs   = " << ecr_info.necrs << std::endl;
    for (u_int ecrloop = 0; ecrloop < ecr_info.necrs; ecrloop++)
      std::cout << "fill_ecr_list: Last L1ID before ECR # "<< ecrloop + 1 << " = 0x" << HEX(ecr_info.ecr[ecrloop]) << std::endl;
  }
  
  if (ecr_info.overflow)
  {
    std::cout << "fill_ecr_list: ECR Overflow detected" << std::endl;
    std::cout << "fill_ecr_list: It is therefore impossible to compute the proper L1IDs" << std::endl;
    std::cout << "fill_ecr_list: Have a nice day" << std::endl;
    exit(-1);
  }

  llp = 0;

  for (u_int ecrloop = 0; ecrloop < ecr_info.necrs; ecrloop++)
  {
    if(ecrloop == 0)
    {
      startid = mrid + 1;
      if (verbose)
	std::cout << "fill_ecr_list: first ECR block. startid = 0x" << HEX(startid) << std::endl;
    }
    else
    {
      startid = ecr_info.ecr[ecrloop] & 0xff000000;
      if (verbose)
	std::cout << "fill_ecr_list: another ECR block. startid = 0x" << HEX(startid) << std::endl;
    }

    for(u_int l1idloop = startid; l1idloop < (ecr_info.ecr[ecrloop] + 1); l1idloop++)
    {
      if (verbose)
	std::cout << "fill_ecr_list: ECR: pushing L1ID = 0x" << HEX(l1idloop) << std::endl;
      li1d_list[llp++] = l1idloop;
    }
  }

  if (ecr_info.necrs)
  {
    for (u_int l1idloop = (ecr_info.mostRecentId & 0xff000000); l1idloop < ecr_info.mostRecentId + 1; l1idloop++) 
    {
      if (verbose)
        std::cout << "fill_ecr_list: MR: pushing L1ID = 0x" << HEX(l1idloop) << std::endl;
      li1d_list[llp++] = l1idloop;
    }
  }
  else
  {
    for (u_int l1idloop = mrid + 1; l1idloop < ecr_info.mostRecentId + 1; l1idloop++) 
    {
      if (verbose)
        std::cout << "fill_ecr_list: justMR: pushing L1ID = 0x" << HEX(l1idloop) << std::endl;
      li1d_list[llp++] = l1idloop;
    }
  }

  mrid = ecr_info.mostRecentId;
  
  if (llp > (64 * 1024))
  {
    std::cout << "fill_ecr_list: ERROR: llp overflow" << std::endl;
    exit(-1);
  }
  return(llp);
}


/*****************************************/      
u_int next_l1id_ecr(Rol *prol, u_int *l1id)
/*****************************************/      
{
  static u_int have_l1ids = 0, lptr = 0;
  
  if (have_l1ids == 0)
  {
    have_l1ids = fill_ecr_list(prol);
    lptr = 0;
  }
  
  if (have_l1ids == 0)
  { 
    if (verbose)
      std::cout << "next_l1id_ecr: Please wait...." << std::endl;
    return(0);  //Please wait
  } 
    
  if (lptr < have_l1ids)
  {
    if  (verbose)
      std::cout << "next_l1id_ecr: lptr = " << lptr << ", have_l1ids = " << have_l1ids << ", li1d_list[lptr] = 0x" << HEX(li1d_list[lptr]) << std::endl;
    
    *l1id = li1d_list[lptr++];
    if (lptr == have_l1ids)
      have_l1ids = 0;
    return(1);
  }  
  return(0);
}


/*********************/      
void dumpids(Rol *prol)
/*********************/      
{
  std::vector<Rol::L1idRange> idlist;
  u_int totnum = 0;

  try
  {
    idlist = prol->getL1idList();
  }
  catch (ROSRobinExceptions& e)
  {
    std::cout << e << std::endl;
    exit(-1);
  }

  for (std::vector<Rol::L1idRange>::const_iterator it2 = idlist.begin(); it2 != idlist.end(); it2++)
  {
    Rol::L1idRange pair = *it2;
    if (pair.first == pair.last)
    {
      std::cout << "L1ID = 0x" << HEX(pair.first) << std::endl;
      if (pair.first != 0xffffffff)
	totnum++;
    }
    else
    {
      std::cout << "First L1ID = 0x" << HEX(pair.first) << "  Last L1ID = 0x" << HEX(pair.last) << "  ( " << pair.last - pair.first + 1 << " events)" << std::endl;
      totnum += pair.last - pair.first + 1;
    }
  }
  std::cout << "There are currently " << totnum << " event in memory" << std::endl;
}








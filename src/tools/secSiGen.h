/// secSiGen.h: generate secure sector information

#include <string>

#ifndef SECSI_GEN_H
#define SECSI_GEN_H

#define CONVERT_ENDIAN_32(X) ((Dword)( (((X) & 0xff000000) >> 24) | (((X) & 0x00ff0000) >> 8) | \
                                       (((X) & 0x0000ff00) << 8)  | (((X) & 0x000000ff) << 24)))


void dumpSecSi(SecSiInfo *secSi);
void usage(char *name);
unsigned int readParm(const char parm, char **argv);

class RobVariant 
{
  public:
  typedef enum 
  {
    initial1to3 = 0,
    initial4to6,
    initial7to10,
    preseries1to5,
    preseries6to25,
    preseries26to50,
    gerVolume1to4,
    gerVolume5to29,
    gerVolume30to350,
    ukVolume1to350,
    gerAddVolume1to70,
    ukVolume1030to1080,
    invalid
  } Variation;

  static Variation getSernumRange(int serNum);
  // members
  static std::string asmName;
  static std::string pcbName;
  static std::string pcbId;
  static unsigned int schemDate;
  static unsigned int schemVersion;
  static unsigned int bomVersion;
  static unsigned int gerberVersion;
  static Byte *id;
};

#endif // SECSI_GEN_H

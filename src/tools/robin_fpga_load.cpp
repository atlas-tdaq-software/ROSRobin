// --------x----------------------------x-----------------------------------x--
/**
 ** \file   robin_fpga_load.cpp
 **
 ** \author Matthias Mueller
 **
 ** \date  
 **
 */
// --------x----------------------------x-----------------------------------x--
#include <string>

#include <iostream>
#include <stdlib.h>
#include <exception>

#include "ROSRobin/Robin.h"
#include "ROSRobin/ROSRobinExceptions.h"

#include "ROSRobin/JTagController.h"
#include "ROSRobin/XilinxVirtexIIFpga.h"
#include "ROSRobin/XilinxCoolRunnerPld.h"
#include "ROSRobin/Firmware.h"

#include "DFDebug/DFDebug.h"

#include "CmdLineArgumentParser.h"

void printUsage(void);
void info(void);

int main(int argc, char ** argv)
{

	int rc = 0;	// return code
  // The command line argument parser
  CmdLineArgumentParser parameters;
  
  // Publish the availables switches to the command line argument parser
  parameters.addSwitch("v", false);      // Verbose
  parameters.addSwitch("d", false);      // Debug
  parameters.addSwitch("reset", false);    // Access mode
  parameters.addSwitch("erase", false);    // Access mode
  
  // Now parse the arguments
  parameters.parse(argc, argv);

  // Check the parameters
  if (parameters.nrOfParams() != 2) {
    info();
    std::cout << "ERROR: too few parameters! " << std::endl;
    printUsage();
    return -1;
  }

  // Get the other parameters
  bool verbose    = parameters.testForSwitch("v");
  bool debug      = parameters.testForSwitch("d");
  bool forceReset = parameters.testForSwitch("reset"); 
  bool forceErase = parameters.testForSwitch("erase"); 

  if (debug) {
    DF::GlobalDebugSettings::setup(15, DFDB_ROSROBIN);
  }


  // Copy the parameters to local variables
  unsigned int boardNr          =  parameters.getParamInt(0);
  std::string firmwareFileName  =  parameters.getParamString(1);  
  

  try {
    // Make a Robin object assigned to the requested board
    ROS::Robin robinBoard(boardNr, 60);
    
    // Open it
    robinBoard.open();
    
    if (verbose)
      std::cout << "Configuring FPGA on board " << boardNr << std::endl;

    // Generate a JTag controller for the chain
    ROS::JTagController jtag(robinBoard);

    ROS::XilinxCoolRunnerPld * pldDevice;

    // If we see a Jtag chain of two devices, the first one is the CPLD!
    if (jtag.getNumberOfDevices() == 2) {
      pldDevice = new ROS::XilinxCoolRunnerPld(jtag);
      jtag.appendToChain(pldDevice);
    }

    // In all cases the FPGA is present
    ROS::XilinxVirtexIIFpga * fpgaDevice = new ROS::XilinxVirtexIIFpga(jtag);    
    jtag.appendToChain(fpgaDevice);

    // Get the Device ID of the FPGA
    if (verbose)
      std::cout << "FPGA has JTag ID: " << fpgaDevice->getDeviceId() << std::endl;    

    if (jtag.getNumberOfDevices() == 2) {
      // and the one of the PLD too if present
      std::cout << "PLD has JTag ID : " << pldDevice->getDeviceId() << std::endl;
      pldDevice->setIR("BYPASS");
      pldDevice->setDR(ROS::BitStream("1"));
    }


    // Now do the Job
    if (true == forceErase){
      std::cout << "ERASE requested" << std::endl;
      fpgaDevice->clear();
      std::cout << "Device erased" << std::endl;
    } else {
      std::cout << "PROGRAM requested" << std::endl;
      fpgaDevice->program(firmwareFileName);

      if ( fpgaDevice->isProgrammed() ) {
        std::cout << "Device programming OK" << std::endl;
      }
      else {
        std::cout << "Device programming NOT OK" << std::endl;
        return -1;
      }
    }

    jtag.removeFromChain(fpgaDevice);
    delete fpgaDevice;

    if (jtag.getNumberOfDevices() == 2) {
      jtag.removeFromChain(pldDevice);
      delete pldDevice;
    }

    if (forceReset) {
      if (verbose) {
	std::cout << "Resetting now!" << std::endl;
      }
      robinBoard.reset();
    }


    // Close it again
    robinBoard.close();
    }
  catch (std::exception &  ex) {
    // Catches all exceptions inherit from std::exception
    // This includes all ROSExceptions
    std::cout << "An exception occured: " << std::endl;
    std::cout << ex.what() << std::endl;
    return -1;
  }
  
  catch (...) {
    std::cout << "An unknown exception occured: " << std::endl;
    return -1;
  }

  return(0);

}

void printUsage(void) {

  
  std::cout << "Usage: " << std::endl;
  std::cout << "\t robin_fpga_load [--reset] [--erase] [-v] [-d] <Board Number> <FPGA firmware file>  " << std::endl;

}

void info(void) {
  std::cout << "This is robin_fpga_load!" << std::endl;
  std::cout << "\t by M Joos and M.Mueller (c) 2005." << std::endl;
}


/* $Id$
 * $Revision$
 * $Log$
 * Revision 1.7  2005/09/12 13:52:16  akugel
 * Timeout for erase + reset sequence increased to allow PPC to program FPGA
 *
 * Revision 1.6  2005/09/12 13:32:09  akugel
 * New command line option --erase added to allow erasing of FPGA
 *
 * Revision 1.5  2005/08/18 14:57:16  joos
 * globally: some bugs fixed. some features added. Some files re-formatted
 *
 * Revision 1.4  2005/07/22 11:26:13  mmueller
 * Added a reset switch
 *
 * Revision 1.3  2005/05/30 14:08:48  mmueller
 * Added proper autodetection of the FPGA and CPLD Jtag chain
 *
 * Revision 1.2  2005/05/04 16:09:58  mmueller
 * Initial implementation
 *
 * Revision 1.1.1.1  2005/04/07 11:34:38  joos
 * added ROSRobin
 *
 *
 */

/***************************************************************/
/* file:   robin_irq_catch.cpp                                 */
/* author: Markus Joos, CERN-PH/ESS                            */
/********* C 2008 - A nickel program worth a dime **************/


#ifndef HOST
  #define HOST
#endif

#include <iostream>
#include <string>
#include <iomanip>
#include <getopt.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <errno.h>
#include <cmdl/cmdargs.h>
#include "DFDebug/DFDebug.h"
#include "ROSGetInput/get_input.h"
#include "ROSEventFragment/ROBFragment.h"
#include "ROSMemoryPool/MemoryPage.h"
#include "ROSMemoryPool/WrapperMemoryPool.h"
#include "ROSRobin/robin.h"
#include "ROSRobin/robin_bufman.h"
#include "ROSRobin/ROSRobinExceptions.h"
#include "ROSRobin/Robin.h"
#include "ROSRobin/Rol.h"

using namespace ROS;


/**************/    
/* Prototypes */
/**************/    
void dump_guest_event(u_int id, u_int rol_no); 
void dump_inmate_event(u_int id, u_int rol_no);  
std::string decodetype(int etype);
void decodestatus(u_int mode, u_int status);

    
/***********/
/* Globals */
/***********/
int c;
u_int card, cont, rol_nr = 0, robin_nr = 0, loop, numberOfOutstandingReq = 1;
u_int msgInputMemorySize = 0x20, miscSize = 0x3000, eventSize = 0, npages = 10, pageSize = 0x1000;
struct sigaction sa;
float timeout = 100.0;
Rol *prol[3];
Robin *rorobin;
WrapperMemoryPool *m_memoryPool;
T_irq_info irq_info;
ROBFragment *robFragment;


/**************/
void usage(void)
/**************/
{
  std::cout << "Valid options are ..." << std::endl;
  std::cout << "-m <robin>: Define the number of the Robin (0..N-1) Default = 0" << std::endl;
  std::cout << "-h: Show help" << std::endl;
  std::cout << "-D:         Enable debugging" << std::endl;
  std::cout << std::endl;
}


// sigquit handler
/**********************************/
void sigquit_handler(int /*signum*/)
/**********************************/
{
  cont = 0;
  
  for (loop = 0; loop < 3; loop++)
    delete prol[loop];
    
  rorobin->freeMsgResources(); 
  rorobin->close();
  delete rorobin;  
}


/*****************************/
int main(int argc, char **argv)
/*****************************/
{
  while ((c = getopt(argc, argv, "hm:D")) != -1)
    switch (c) 
    {
      case 'h': usage(); exit(0); break;                   
      case 'm': robin_nr = atoi(optarg);                           break;                   
      case 'D': DF::GlobalDebugSettings::setup(20, DFDB_ROSROBIN); break;                
      default:
	std::cout << "Invalid option " << c << std::endl;
	std::cout << "Usage: " << argv[0] << " [options]: " << std::endl;
	usage();
	exit (0);
    }
 
  // Install signal handler for SIGQUIT (ctrl + \)
  sigemptyset(&sa.sa_mask);
  sa.sa_flags   = 0;
  sa.sa_handler = sigquit_handler;

  // Dont block in intercept handler
  if (sigaction(SIGQUIT, &sa, NULL) < 0) 
  {
    std::cout << "main: sigaction() FAILED with ";
    int code = errno;
    if (code == EFAULT) std::cout << "EFAULT" << std::endl;
    if (code == EINVAL) std::cout << "EINVAL" << std::endl;
    exit(0);
  }
 
  //Open access to one Robin
  rorobin = new Robin(robin_nr, timeout);
  rorobin->open();
  eventSize = npages * pageSize;
  rorobin->reserveMsgResources(numberOfOutstandingReq, msgInputMemorySize, miscSize, eventSize); 
  // Create a memoryPool structure around the event buffer
  u_long eventBase       = rorobin->getVirtEventBase();
  u_int physicalAddress = rorobin->getPhysEventBase();
  m_memoryPool = new WrapperMemoryPool(npages, pageSize, eventBase, physicalAddress);

  //Initialize all 3 ROLs
  for (loop = 0; loop < 3; loop++)
  {
    prol[loop] = new Rol(*rorobin, loop);
    //Enable the EBIST (but without loopback) 
    prol[loop]->setConfig(enum_cfgRolEnabled, 1);
    prol[loop]->setConfig(enum_cfgIrqEnable, 1);
  }

  cont = 1;
  while(cont)
  {
    std::cout << "Waiting for an interrupt....." << std::endl;
    try
    {
      rorobin->waitForInterrupt(&irq_info); 
    }
    catch (ROSRobinExceptions& e)
    {
      std::cout << e << std::endl;
      break;
    }
    std::cout << ".....interrupt received" << std::endl;

    for(card = 0; card < irq_info.ncards; card++)
    {
      std::cout << "Card = " << card << ", channel = 0, type = " << decodetype(irq_info.ch0_type[card]) << ", ID = " << irq_info.ch0_id[card] << std::endl;
      std::cout << "Card = " << card << ", channel = 1, type = " << decodetype(irq_info.ch1_type[card]) << ", ID = " << irq_info.ch1_id[card] << std::endl;
      std::cout << "Card = " << card << ", channel = 2, type = " << decodetype(irq_info.ch2_type[card]) << ", ID = " << irq_info.ch2_id[card] << std::endl;
    }

    for(card = 0; card < irq_info.ncards; card++)
    {
     if (irq_info.ch0_type[card])
      {
  	if(irq_info.ch0_type[card] == INMATE_EVENT)
	  std::cout << "Card = " << card << ", channel = 0, type = " << decodetype(irq_info.ch0_type[card]) << ", Page number = " << irq_info.ch0_id[card] << std::endl;
  	else
	  std::cout << "Card = " << card << ", channel = 0, type = " << decodetype(irq_info.ch0_type[card]) << ", L1ID = " << irq_info.ch0_id[card] << std::endl;

        if ((card == robin_nr) && irq_info.ch0_type[card] == INMATE_EVENT)
          dump_inmate_event(irq_info.ch0_id[card], 0);  
        if ((card == robin_nr) && irq_info.ch0_type[card] == GUEST_EVENT)
          dump_guest_event(irq_info.ch0_id[card], 0);  
      }
      if (irq_info.ch1_type[card])
      {
  	if(irq_info.ch1_type[card] == INMATE_EVENT)
  	  std::cout << "Card = " << card << ", channel = 1, type = " << decodetype(irq_info.ch1_type[card]) << ", Page number = " << irq_info.ch1_id[card] << std::endl;
  	else
  	  std::cout << "Card = " << card << ", channel = 1, type = " << decodetype(irq_info.ch1_type[card]) << ", L1ID = " << irq_info.ch1_id[card] << std::endl;

        if ((card == robin_nr) && irq_info.ch1_type[card] == INMATE_EVENT)
          dump_inmate_event(irq_info.ch1_id[card], 1);  
        if ((card == robin_nr) && irq_info.ch1_type[card] == GUEST_EVENT)
          dump_guest_event(irq_info.ch1_id[card], 1);  
      }
      if (irq_info.ch2_type[card])
      {
  	if(irq_info.ch2_type[card] == INMATE_EVENT)
    	  std::cout << "Card = " << card << ", channel = 2, type = " << decodetype(irq_info.ch2_type[card]) << ", Page number = " << irq_info.ch2_id[card] << std::endl;
  	else
    	  std::cout << "Card = " << card << ", channel = 2, type = " << decodetype(irq_info.ch2_type[card]) << ", L1ID = " << irq_info.ch2_id[card] << std::endl;

        if ((card == robin_nr) && irq_info.ch2_type[card] == INMATE_EVENT)
          dump_inmate_event(irq_info.ch2_id[card], 2);  
        if ((card == robin_nr) && irq_info.ch2_type[card] == GUEST_EVENT)
          dump_guest_event(irq_info.ch2_id[card], 2);  
      }
    }   
  }
}


/*******************************/
std::string decodetype(int etype)
/*******************************/
{
  if (etype == INMATE_EVENT)
    return("REJECTED_EVENT");
  else if (etype == GUEST_EVENT)   
    return("REGULAR_EVENT");
  else
    return("No event");
}


/********************************************/
void dump_inmate_event(u_int id, u_int rol_no)  
/********************************************/
{
  Rol::KeptPage keptpage;
  u_int loop, page_offset = 0;

  while(1)  //we break out once we have read the last page of the bad event
  {
    keptpage = prol[rol_no]->getRejectedPage(id + page_offset);
    if (keptpage.rentry.info.upfInfo.pageNum == 0)
    {
      std::cout << "ERROR: You have requested an invalid page. Page number = " << id + page_offset << std::endl;
      return;
    }
    
    std::cout << "Rol                                  = " << rol_no << std::endl;
    std::cout << "First page                           = " << id  << std::endl;
    std::cout << "page_offset                          = " << page_offset << std::endl;
    std::cout << "Most recent L1ID                     = 0x" << HEX(keptpage.rentry.mri) << std::endl;
    std::cout << "info.upfInfo.status                  = 0x" << HEX(keptpage.rentry.info.upfInfo.status) << std::endl;
    decodestatus(1, keptpage.rentry.info.upfInfo.status);
    std::cout << "info.upfInfo.eventId                 = 0x" << HEX(keptpage.rentry.info.upfInfo.eventId) << std::endl;
    std::cout << "info.upfInfo.pageNum                 = 0x" << HEX(keptpage.rentry.info.upfInfo.pageNum) << std::endl;
    std::cout << "info.upfInfo.pageLen                 = 0x" << HEX(keptpage.rentry.info.upfInfo.pageLen) << std::endl;
    std::cout << "info.upfInfo.runNum                  = 0x" << HEX(keptpage.rentry.info.upfInfo.runNum) << std::endl;
    std::cout << "Number of words in rejected fragment = " << keptpage.pagedata_size << std::endl;

    for (loop = 0; loop < keptpage.pagedata_size; loop ++)
      std::cout << "Word " << loop << " = 0x" << HEX(keptpage.pagedata[loop]) << std::endl;

    page_offset++;
    
    if((keptpage.rentry.info.upfInfo.status & enum_upfEndMask) == enum_upfEndMarker)
    {
      std::cout << "This was the last page of the event" << std::endl;
      break;
    }
  }
}

	
/*******************************************/
void dump_guest_event(u_int id, u_int rol_no)  
/*******************************************/
{
  MemoryPage *mem_page = NULL;
  u_int *fptr;
  u_long ticket;
      
  mem_page = m_memoryPool->getPage();

  if (mem_page == 0) // Check if the allocated page is OK
  {						
    std::cout << "ERROR: Failed to get a page from the memory pool" << std::endl;
    return;
  }

  std::cout << "Requesting fragment with L1ID = " << id << " from ROL #" << rol_no << std::endl;
  u_int *fragBufferAddress = static_cast<u_int *> (mem_page->reserve(mem_page->capacity()));
  ticket = prol[rol_no]->requestFragment(id, reinterpret_cast<u_long> (fragBufferAddress));
  fptr = prol[rol_no]->getFragment(ticket);      
  robFragment = new ROBFragment(mem_page);
  robFragment->print();
  decodestatus(0, robFragment->status());
  delete robFragment;
  mem_page->free();
}


/*****************************************/
void decodestatus(u_int mode, u_int status)
/*****************************************/
{
  u_int lstatus;
  
  std::cout << "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv" << std::endl;
  
  if(mode == 1)   
  {
    std::cout << "Decoding status of rejected page:" << std::endl;
   
    if((status & enum_upfValid) == enum_upfValid) 
      std::cout << "Valid flag: Set" << std::endl;
    else                                          
      std::cout << "Valid flag: Not set (Report this to a Robin expert)" << std::endl;

    if((status & enum_upfEndMask) == enum_upfEndMarker) 
      std::cout << "Last page: Yes" << std::endl;
    else if((status & enum_upfEndMask) == enum_upfMoreMarker)                                        
      std::cout << "Last page: No" << std::endl;
    else  
      std::cout << "Last page: Illegal value (Report this to a Robin expert)" << std::endl;
     
    if(status & enum_upfEventIdFlag)               
      std::cout << "Extended L1ID Word Received" << std::endl;
    if(status & enum_upfTrigTypeFlag) 	        
      std::cout << "Trigger Type word Received" << std::endl;
    if(status & enum_upfRunNumberFlag)  	        
      std::cout << "Run Number Word received" << std::endl;
    if(status & enum_upfFormatErrorFlag) 	
      std::cout << "Format Version Error" << std::endl;
    if(status & enum_upfHdrMarkerErrorFlag) 	
      std::cout << "Header Marker Error" << std::endl;
    if(status & enum_upfMissingBofErrorFlag) 	
      std::cout << "Missing BOF Error" << std::endl;
    if(status & enum_upfMissingEofErrorFlag) 	
      std::cout << "Missing EOF Error" << std::endl;
    if(status & enum_upfShortHdrErrorFlag)	
      std::cout << "Incomplete Header Error" << std::endl;
    if(status & enum_upfNoHeaderErrorFlag) 	
      std::cout << "No Header Error" << std::endl;
    if(status & enum_upfControlErrorFlag) 	
      std::cout << "S-LINK Control Word Error" << std::endl;
    if(status & enum_upfDataErrorFlag)  	        
      std::cout << "S-LINK Data Word Error" << std::endl;
    if(status & enum_upfBofFlag) 	        
      std::cout << "BOF Control Word Received" << std::endl;
    if(status & enum_upfEofFlag) 	        
      std::cout << "EOF Control Word Received" << std::endl;
    if(status & enum_upfControlWordFlag) 	
      std::cout << "Control Word Received" << std::endl;
    if(status & enum_upfFragSizeErrorFlag) 	
      std::cout << "Fragment Size Error" << std::endl;
  }
  else            
  {
    lstatus = status >> 16;
    std::cout << "Decoding status of accepted event:" << std::endl;
    if(lstatus & enum_upfEventIdFlag)               
      std::cout << "enum_upfEventIdFlag detected (Report this to a Robin expert)" << std::endl;
    if(lstatus & enum_upfTrigTypeFlag) 	        
      std::cout << "Pending: the ROBIN did not have a fragment for the requested L1ID but this fragment may still arrive. It therefore generated an empty fragment" << std::endl;
    if(lstatus & enum_upfRunNumberFlag)  	        
      std::cout << "Lost: the ROBIN did not have a fragment for the requested L1ID. It therefore generated an empty fragment" << std::endl;
    if(lstatus & enum_upfFormatErrorFlag) 	
      std::cout << "Short fragment: the amount of data between the S-Link control words (BOF and EOF) was less than the size of an empty ROD fragment (ROD header + ROD trailer)" << std::endl;
    if(lstatus & enum_upfHdrMarkerErrorFlag) 	
      std::cout << "Truncation: the amount of data sent across S-Link for this fragment was larger than the maximum fragment size the ROBIN was configured to handle. Therefore this fragment has been truncated" << std::endl;
    if(lstatus & enum_upfMissingBofErrorFlag) 	
      std::cout << "Tx error: general flag for a S-Link transmission or formatting error. See bits 16 thru 23" << std::endl;
    if(lstatus & enum_upfMissingEofErrorFlag) 	
      std::cout << "Sequence error: the L1ID of this ROD fragment was not in sequence with the L1ID of the fragment previously received (L1ID_new not_equal L1ID_old + 1)" << std::endl;
    if(lstatus & enum_upfShortHdrErrorFlag)	
      std::cout << "Duplicate event: when this fragment was received the ROBIN still had a fragment with the same L1ID in memory. The new fragment has replaced the older one" << std::endl;
    if(lstatus & enum_upfNoHeaderErrorFlag) 	
      std::cout << "Format error: the major format version (upper 16 bit) don't match expected format (currently 0x0301)" << std::endl;
    if(lstatus & enum_upfControlErrorFlag) 	
      std::cout << "Invalid header marker: the header marker was neither 0xee1234ee nor 0xee3412ee" << std::endl;
    if(lstatus & enum_upfDataErrorFlag)  	        
      std::cout << "Missing EOF: new fragment started with BOF, without the preceeding one terminated by EOF" << std::endl;
    if(lstatus & enum_upfBofFlag) 	        
      std::cout << "Missing BOF: new fragment started without BOF (after preceeding one terminated with EOF)" << std::endl;
    if(lstatus & enum_upfEofFlag) 	        
      std::cout << "CTL word error: S-LINK transmission error on control word (EOF or BOF)" << std::endl;
    if(lstatus & enum_upfControlWordFlag) 	
      std::cout << "Data block error: S-LINK transmission error on data block" << std::endl;
    if(lstatus & enum_upfFragSizeErrorFlag) 	
      std::cout << "Fragment size error: actual number of words does not match header + trailer + number of data words + number of status words (indicated in trailer)" << std::endl;

  }    
  std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
}


    
	
	
	
		
			
			
		
	




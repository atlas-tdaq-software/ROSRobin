/// secSiGen.cpp: generate secure sector information

#include <stdio.h>
#include <iostream>
#include <time.h>
#define HOST
#include "ROSRobin/robin.h"
#include "secSiGen.h"

// static members
std::string RobVariant::asmName = "Unknown";
std::string RobVariant::pcbName = "Unknown";
std::string RobVariant::pcbId = "Unknown";
unsigned int RobVariant::schemDate;
unsigned int RobVariant::schemVersion;
unsigned int RobVariant::bomVersion;
unsigned int RobVariant::gerberVersion;
Byte *RobVariant::id;

const Byte idRh[] = {0x2,0x0,0x0,0x3,0x0,0x8,0x2,0x1,0x0,0x0,0x0,0x0,0x0,0x0};	// default
const Byte idMa[] = {0x2,0x0,0x0,0x3,0x0,0x8,0x4,0x1,0x0,0x0,0x0,0x0,0x0,0x0};	// default

#define CONVERT_ENDIAN_16(X) ((Word)(((X) >> 8) + ((X) << 8)))
#define CONVERT_ENDIAN_32(X) ((Dword)( (((X) & 0xff000000) >> 24) | (((X) & 0x00ff0000) >> 8) | \
                                       (((X) & 0x0000ff00) << 8)  | (((X) & 0x000000ff) << 24)))

// set fixed values 
RobVariant::Variation RobVariant::getSernumRange(int serNum)
{
	RobVariant::Variation variation = invalid;
	// BOM version
	if	(((serNum >= 129) && (serNum <= 131))){
		variation = RobVariant::initial1to3;
		RobVariant::asmName = "EBS";
		RobVariant::pcbName = "CAE";
		RobVariant::id = (Byte*)idMa;
		RobVariant::bomVersion = 1;
		RobVariant::gerberVersion = 1;
		RobVariant::schemVersion = 1;
		RobVariant::schemDate = 20040702;
	}
	if	(((serNum >= 132) && (serNum <= 134))){
		variation = initial4to6;
		RobVariant::asmName = "GEBAUER";
		RobVariant::pcbName = "ANDUS";
		RobVariant::id = (Byte*)idMa;
		RobVariant::bomVersion = 2;
		RobVariant::gerberVersion = 2;
		RobVariant::schemVersion = 2;
		RobVariant::schemDate = 20041122;
	}
	if	(((serNum >= 135) && (serNum <= 138))){
		variation = initial7to10;
		RobVariant::asmName = "GEBAUER";
		RobVariant::pcbName = "ANDUS";
		RobVariant::id = (Byte*)idMa;
		RobVariant::bomVersion = 3;
		RobVariant::gerberVersion = 4;
		RobVariant::schemVersion = 3;
		RobVariant::schemDate = 20051201;
	}
	if	(((serNum >= 150) && (serNum <= 169))){
		variation = preseries6to25;
		RobVariant::asmName = "CEMGRAFT";
		RobVariant::pcbName = "GRAPHIC PLC";
		RobVariant::id = (Byte*)idRh;
		RobVariant::bomVersion = 4;
		RobVariant::gerberVersion = 5;
		RobVariant::schemVersion = 4;
		RobVariant::schemDate = 20050429;
		RobVariant::pcbId = "9624-0003";
	}
	if	(((serNum >= 195) && (serNum <= 199))){
		variation = preseries1to5;
		RobVariant::asmName = "CEMGRAFT";
		RobVariant::pcbName = "GRAPHIC PLC";
		RobVariant::id = (Byte*)idRh;
		RobVariant::bomVersion = 4;
		RobVariant::gerberVersion = 5;
		RobVariant::schemVersion = 4;
		RobVariant::schemDate = 20050429;
	}
	if	(((serNum >= 170) && (serNum <= 194))){
		variation = preseries26to50;
		RobVariant::asmName = "CEMGRAFT";
		RobVariant::pcbName = "GRAPHIC PLC";
		RobVariant::id = (Byte*)idRh;
		RobVariant::bomVersion = 5;
		RobVariant::gerberVersion = 7;
		RobVariant::schemVersion = 6;
		RobVariant::schemDate = 20050000;
	}
	if	(((serNum >= 200) && (serNum <= 204))){
		variation = gerVolume1to4;
		RobVariant::asmName = "GEBAUER";
		RobVariant::pcbName = "KSG";
		RobVariant::id = (Byte*)idMa;
		RobVariant::bomVersion = 6;
		RobVariant::gerberVersion = 6;
		RobVariant::schemVersion = 5;
		RobVariant::schemDate = 20050506;
		RobVariant::pcbId = "4-200091p";
	}
	if	(((serNum >= 205) && (serNum <= 223))){
		variation = gerVolume5to29;
		RobVariant::asmName = "GEBAUER";
		RobVariant::pcbName = "KSG";
		RobVariant::id = (Byte*)idMa;
		RobVariant::bomVersion = 6;
		RobVariant::gerberVersion = 6;
		RobVariant::schemVersion = 5;
		RobVariant::schemDate = 20050506;
		RobVariant::pcbId = "4-200091p";
	}
	if	(((serNum >= 224) && (serNum <= 549))){
		variation = gerVolume30to350;
		RobVariant::asmName = "GEBAUER";
		RobVariant::pcbName = "KSG";
		RobVariant::id = (Byte*)idMa;
		RobVariant::bomVersion = 7;
		RobVariant::gerberVersion = 6;
		RobVariant::schemVersion = 6;
		RobVariant::schemDate = 0;
	}
	if	(((serNum >= 600) && (serNum <= 949))){
		variation = ukVolume1to350;
		RobVariant::asmName = "CEMGRAFT";
		RobVariant::pcbName = "GRAPHIC PLC";
		RobVariant::id = (Byte*)idRh;
		RobVariant::bomVersion = 7;
		RobVariant::gerberVersion = 7;
		RobVariant::schemVersion = 6;
		RobVariant::schemDate = 0;
	}
	// new batch of 70 ROBINs, produced in 2007. 50 board from 550, 20 boards from 950
	if	(((serNum >= 550) && (serNum <= 599)) || ((serNum >= 950) && (serNum <= 969))){
		variation = gerAddVolume1to70;
		RobVariant::asmName = "GEBAUER";
		RobVariant::pcbName = "KSG";
		RobVariant::id = (Byte*)idMa;
		RobVariant::bomVersion = 7;
		RobVariant::gerberVersion = 6;
		RobVariant::schemVersion = 5;
		RobVariant::schemDate = 20050506;
		RobVariant::pcbId = "4-200091p";
	}

	if( serNum >= 1030 && serNum < 1080 ) {
	  variation = ukVolume1030to1080;
	  RobVariant::asmName = "CEMGRAFT";
	  RobVariant::pcbName = "GRAPHIC_PC3414";
	  RobVariant::id = (Byte*)idRh;
	  RobVariant::bomVersion = 8;
	  RobVariant::gerberVersion = 8;
	  RobVariant::schemVersion = 8;
	  RobVariant::schemDate = 20090915;
	}

	if (invalid == variation){
			throw "Invalid serial number";
	}
	return variation;
}


typedef enum {
	e_sernum = 0,
	e_asmId,
	e_numOfTags
} TagList;

char parmTag[e_numOfTags];

unsigned int parmVal[e_numOfTags] = {0};


int main (int argc, char **argv)
{
	SecSiInfo secSi;
	unsigned int i;

	for (i=0;i<e_numOfTags;i++){
		switch(i){
			case e_sernum:
				parmTag[i] = 's';
				break;
			case e_asmId:
				parmTag[i] = 'a';
				break;
		}
	}

	// read paramters
	try {
	  for (i=0;i< sizeof(parmTag);i++){
			parmVal[i] = readParm(parmTag[i],argv);
		}
	}
    catch( char * str )
    {
		std::cout << "Exception raised: " << str << '\n';
		usage(argv[0]);
		exit(-1);
    }

	// clear buffer
	memset((char*)&secSi,0,sizeof(secSi));

	// Note: all non-string values have to be byte swapped

	// this initialises all fixed paramters
	try {
	RobVariant::Variation variant = RobVariant::getSernumRange(parmVal[e_sernum]);
	}
    catch( char * str )
    {
		std::cout << "Exception raised: " << str << '\n';
		usage(argv[0]);
		exit(-1);
    }

	memcpy(&secSi.id,RobVariant::id,sizeof(secSi.id));

	// basics
	secSi.magic = CONVERT_ENDIAN_32(enum_secSiMagic);
	secSi.secSiVersion = CONVERT_ENDIAN_16(enum_secSiVersion);

	// insert serial number
	printf("Serial number is %6.6d\n",parmVal[e_sernum]);
	const unsigned int serLen = sizeof(secSi.id.sernum);
	char serString[serLen + 1];
	sprintf(serString,"%6.6d",parmVal[e_sernum]);
	for (i=0;i<serLen;i++){
		secSi.id.sernum[i] = serString[i] - '0';
	}

	// insert ASM id
	secSi.asmId = CONVERT_ENDIAN_32(parmVal[e_asmId]);
	printf("Asm id is %d\n",parmVal[e_asmId]);

	// insert fixed fields
	strcpy((char*)secSi.pcbName,RobVariant::pcbName.c_str());
	strcpy((char*)secSi.pcbId,RobVariant::pcbId.c_str());
	strcpy((char*)secSi.asmName,RobVariant::asmName.c_str());
	secSi.bomVersion = CONVERT_ENDIAN_32(RobVariant::bomVersion);
	secSi.gerberId = CONVERT_ENDIAN_32(RobVariant::gerberVersion);
	secSi.schemId = CONVERT_ENDIAN_32(RobVariant::schemVersion);


	struct tm *newtime;
	time_t aclock;
    time( &aclock );   // Get time in seconds
    newtime = localtime( &aclock );   // Convert time to struct tm form 
	int day, month,year;
	day = newtime->tm_mday;
	month = newtime->tm_mon + 1; // starts at 0 (January)
	year = newtime->tm_year + 1900;	// offset 1900
	char currentDate[9];
	sprintf(currentDate,"%4.4d%2.2d%2.2d",year,month,day);
	// printf("current date: %s\n",currentDate);

	// insert production date
	for (i=0;i< sizeof(secSi.prodDate);i++){
		secSi.prodDate[i] = currentDate[i] - '0';
	}

	// insert schematic date
	sprintf(currentDate,"%8.8d",RobVariant::schemDate);
	for (i=0;i<sizeof(secSi.schemDate);i++){
		secSi.schemDate[i] = currentDate[i] - '0';
	}

	dumpSecSi(&secSi);


	FILE *f;
	char fName[129];
	sprintf(fName,"secsi%6.6d.bin",parmVal[e_sernum]);
#ifdef WIN32
	f = fopen(fName,"wb");
#else
	f = fopen(fName,"w");
#endif
	fwrite((void*)&secSi,sizeof(secSi),1,f);
	fclose(f);
	return 0;
}

// ---------------------------------------------------------------------------------
void usage(char *name)
{
	printf("Usage: %s <parameter list>\n",name);
	printf("  Paramters are: \n");
	printf("  -s <serial number>. Serial number from 1 .. 1023\n");
	// printf("  -d <production date>. Date format YYYYMMDD (as number)\n");
	printf("  -a <asm id>. Board id from assembly company\n");
	// printf("  -v <asm version>. Assembly version code\n");
	// printf("  -p <pcb id>. PCB identification code\n");
	// printf("  -b <bom version>. BOM identification code\n");
	// printf("  -g <gerber version>. Gerber identification code\n");
	printf("\n");
}

// ---------------------------------------------------------------------------------
unsigned int readParm(const char parm, char **argv)
{
	int i = 0;
	unsigned int val;
	char tag[3] = {'-',0,0};
	tag[1] = parm;
	while (argv[i] != NULL){
		if (0 == strncmp(argv[i],tag,2)){
			sscanf(argv[i+1],"%ud",&val);
			return val;
		}
		i++;
	}
	char msg[200];
	sprintf(msg,"Parameter -%c not found\n",parm);
	throw msg;
	return -1;
}


// ---------------------------------------------------------------------------------
void dumpSecSi(SecSiInfo *secSi)
{
	unsigned int i;
	AtlasPartId *id;
	Dword project, institute;

	printf("\n\nDumpSecSi\nSecSi MAGIC: 0x%x\n",CONVERT_ENDIAN_32(secSi->magic));
	if (enum_secSiMagic != CONVERT_ENDIAN_32(secSi->magic)){
		printf("Warning: SecSi sector has no valid content\n");
	}
	printf("SecSi Format version: 0x%x\n",CONVERT_ENDIAN_16(secSi->secSiVersion));

	printf("SecSi ID: \n");
	for (i=0;i<sizeof(secSi->id);i++){
		printf("%1.1d ",((Byte*)&secSi->id)[i]);
	}
	printf("\n");

	id = &secSi->id;
	project = (id->project[0] * 10) + id->project[1];
	institute = (id->institute[0] * 100) + (id->institute[1] * 10) + id->institute[2];
	printf("Atlas Part Identification\n");
	printf("  Project: %d, System %d, SubSystem %d, Institute %3.3d, PreFix %d.",
		project,id->system[0], id->subSystem[0],institute,id->prefix[0]);
	printf("SerNum: ");
	for (i=0;i<sizeof(secSi->id.sernum);i++){
		printf("%1.1d",((Byte*)&secSi->id.sernum)[i]);
	}
	printf("\n");

	printf("SecSi Production Date: \n");
	for (i=0;i<sizeof(secSi->prodDate);i++){
		printf("%1.1d ",secSi->prodDate[i]);
	}
	printf("\n");

	printf("SecSi Schematic Date: \n");
	for (i=0;i<sizeof(secSi->schemDate);i++){
		printf("%1.1d ",secSi->schemDate[i]);
	}
	printf(". Version: 0x%x\n",CONVERT_ENDIAN_32(secSi->schemId));

	printf("Gerber version: 0x%x\n",CONVERT_ENDIAN_32(secSi->gerberId));

	printf("SecSi PCB Name: %s\n",secSi->pcbName);

	printf("SecSi PCB identification: %s\n",secSi->pcbId);

	printf("BOM version: 0x%x\n",CONVERT_ENDIAN_32(secSi->bomVersion));

	printf("SecSi ASM Name: %s\n",secSi->asmName);

	printf("SecSi ASM ID: 0x%x\n",CONVERT_ENDIAN_32(secSi->asmId));
}


// --------x----------------------------x-----------------------------------x--
/**
 ** \file   robin_firmware_update.cpp
 **
 ** \author Matthias Mueller
 **
 ** \date  
 **
 */
// --------x----------------------------x-----------------------------------x--

#include <exception>
#include <iostream>
#include <string>
#include <vector>
#include <fstream>

#ifndef HOST
 #define HOST
#endif

#include "DFDebug/DFDebug.h"
#include "CmdLineArgumentParser.h"
#include "rcc_time_stamp/tstamp.h"
#include "ROSRobin/robin.h"
#include "ROSRobin/ROSRobinExceptions.h"
#include "ROSRobin/Robin.h"
#include "ROSRobin/Rol.h"
#include "ROSRobin/PlxEepromFile.h"
#include "ROSRobin/JTagController.h"
#include "ROSRobin/XilinxCoolRunnerPld.h"
#include "ROSRobin/XilinxVirtexIIFpga.h"
#include "ROSRobin/BitStream.h"
#include "FlashProgrammer.h"
#include "PPCEnvironment.h"

using namespace ROS;

/*************/
/* Constants */
/*************/

// The size of the Plx EEPROM
const u_int c_eepromSize = 0xFF;

// Message passing
const u_int c_outstandingRequest = 1;
const u_int c_msgInputMemorySize = 0x1000;
const u_int c_miscSize           = 0x1000;

// PPC Flash addresses
const u_int c_robinApplicationAddress = 0xff800000;
const u_int c_fpgaFirmwareAddress     = 0xff900000;
const u_int c_ubootAddressROBIN       = 0xfffc0000;
const u_int c_ubootAddressPrototype   = 0xfffe0000;
const u_int c_envAddress              = 0xfff80000;

const u_int c_timeout  = 100;  // in seconds
const u_int c_maxcards = 10;


/***********************************************************/
/* Functions which implement the application functionality */
/***********************************************************/

// Reads the contents of the Plx EEPROM and dumps it to a file
void storePlxEepromToFile(Robin & robin, std::string fileName);

// Writes the contents of the Plx EEPROM from a given file
void writePlxEepromFromFile(Robin & robin, std::string fileName);

// Writes the PPC application stored in a .elf to the flash via message passing
void writePPCFirmwareFromFile(Robin & robin, std::string fileName);

// Writes the FPGA firmware file stored in a .bit to the flash via message passing
void writeFPGAFirmwareFromFile(Robin & robin, std::string fileName);

// Writes the SecSI OTP record stored in a .bin to the flash via message passing
void writeSecSiFromFile(Robin & robin, std::string fileName);

// Gets the contents of the PPC environment via direct flash acces and writes them to a file
void storeEnvToFile(FlashProgrammer & flash, std::string fileName);

// Reads the PPC environment files from a file and writes them to the flash
void writeEnvFromFile(FlashProgrammer & flash, std::string fileName);

void storeFlashContentToFile(FlashProgrammer & flash, std::string fileName, u_int address, u_int size);
void writeFlashContentFromFile(FlashProgrammer & flash, std::string fileName, u_int address);
void eraseFlash(FlashProgrammer & flash, u_int nrOfRols, u_int startAddress, u_int endAddress);

// A helper to write a ifstream to the scratch area
u_int writeToScratch(Robin & robin, std::vector<u_int> data);

// A helper to convert the enianess
u_int byteSwap(u_int data);

// Print program information
void info(void);

// Print usage information
void usage(void);


/*****************************/
int main(int argc, char **argv)
/*****************************/
{
  u_int dblevel, dbpackage, ppcversion;

  try 
  {
    // The command line argument parser
    CmdLineArgumentParser parameters;

    // Publish the availables switches to the command line argument parser
    parameters.addSwitch("v", false);         
    parameters.addSwitch("q", false);         
    parameters.addSwitch("mode", true);       
    parameters.addSwitch("reset", false);     
    parameters.addSwitch("dl", true);     
    parameters.addSwitch("dp", true);    
    parameters.addSwitch("ppcver", true);    

    // Now parse the arguments
    parameters.parse(argc, argv);

    // The verbose and quite switches are optional
    bool verbose    = parameters.testForSwitch("v");
    bool quiet      = parameters.testForSwitch("q");
    bool forceReset = parameters.testForSwitch("reset"); 
    
    // Now read the access mode switch. ppcmessage is the default
    // and is taken if no mode is given on the command line.
    // Valid modes are: 
    //    - ppcmessage:   Write a ppc .elf file to 0xff800000 and a fpga -bit file to 0xff900000
    //                    using the ROBIN message passing
    //    - eeprom        Reads or writes the PLX eeprom contents
    //    - ppcflash:     Write a ppc .elf file to 0xff800000 or a fpga -bit file to 0xff900000
    //                    using the special ppcflash FPGA firmware. Therefore the PowerPC is not 
    //                    required
    //    - directflash:  Random read, write and erase of the PowerPC flash.
    //    - otp:          Write SecSI/OTP production data via message interface
 
    std::string accessMode;

    // Try to get the access mode. Set it to ppcflash if no mode is given
    try 
    {
      accessMode = parameters.getSwitchParamString("mode");
    }
    catch (CmdLineArgumentException & ex) 
    {
      accessMode = "ppcmessage";
    }
    
    // Check for plausibility
    if ((accessMode != "ppcmessage") &&	(accessMode != "eeprom") && (accessMode != "ppcflash") &&
        (accessMode != "directflash") && (accessMode != "otp") && (accessMode != "scan")) 
    {
      info();
      std::cout << "ERROR in command line parameters: mode parameter wrong!" << std::endl;
      usage();
      return -1;
    }

    // Get the debug parameters
    try 
    {
      dblevel = parameters.getSwitchParamInt("dl");
    }
    catch (CmdLineArgumentException & ex) 
    {
      dblevel = 0;
    }
    
    try 
    {
      dbpackage = parameters.getSwitchParamInt("dp");
    }
    catch (CmdLineArgumentException & ex) 
    {
      dbpackage = DFDB_ROSROBIN;
    }  
    std::cout << "Debug level   = " << dblevel << std::endl;
    std::cout << "Debug package = " << dbpackage << std::endl;
    DF::GlobalDebugSettings::setup(dblevel, dbpackage);

    // Now switch between the access modes
    
    // ********************************************************************************
    // First the ppcmessage mode. Updates firmware via ROBIN message passing
    if (accessMode == "ppcmessage") 
    {
      // The first parameter is the board number. This is common for all access modes.
      u_int boardNr = parameters.getParamInt(0);

      // Check the number of parameters. There has to be the ROBIN ID and the two firmware files!
      if (parameters.nrOfParams() != 3) 
      {
	info();
	std::cout << "ERROR in command line parameters: The number of arguments did not match" << " the expected!"<< std::endl;
	usage();
	return -1;
      }
      
      // Assign the file names for the firmware files from the parameters
      std::string ppcFirmware = parameters.getParamString(1);
      std::string fpgaFirmware = parameters.getParamString(2);
      
      // Check if the suffixes match the expected.
      
      // First determine the suffix of the PPC firmware file. It must be .elf!
      u_int dotPositionPpc = ppcFirmware.rfind(".");
      std::string suffixPpc = ppcFirmware.substr(dotPositionPpc + 1, ppcFirmware.size() - dotPositionPpc - 1);

      // Next determine the suffix of the FPGA firmware file. It must be .bit!
      u_int dotPositionFpga = fpgaFirmware.rfind(".");
      std::string suffixFpga = fpgaFirmware.substr(dotPositionFpga + 1, fpgaFirmware.size() - dotPositionFpga - 1);
      
      // First check if the user has given the files in the wrong order. This we will accept!
      if ((suffixPpc == "bit") && (suffixFpga == "elf")) 
      {
	// Yes. so lets swap the file names
	std::string dummy;
	dummy = ppcFirmware;
	ppcFirmware = fpgaFirmware;
	fpgaFirmware = dummy;
      }
      else if ((suffixFpga != "bit") || (suffixPpc != "elf")) 
      {
	info();
	std::cout << "ERROR in command line parameters: The firmware files do not have the right ending!" << std::endl;
	std::cout << "                                  The PowerPC needs a .elf file, the FPGA a .bit file!" << std::endl;
	usage();
	return -1;	
      }
      
      if (!quiet)
	info();
      
      Robin robin(boardNr, c_timeout);
      
      // open the board.
      robin.open();

      // Reserve resources for message passing
      robin.reserveMsgResources(c_outstandingRequest, c_msgInputMemorySize, c_miscSize);

      //Get the desired PPC version from the command line
      try 
      {
	ppcversion = parameters.getSwitchParamInt("ppcver");
      }
      catch (CmdLineArgumentException &ex) 
      {
	ppcversion = 0;
      }
      if (ppcversion)
        std::cout << "Updating Robin to version 0x" << std::hex << ppcversion << std::dec << " of the PPC application" << std::endl;

      //Get the current PPC version from the Robin
      Rol *prol = 0;
      prol = new Rol(robin, 0);
      StatisticsBlock statblock = prol->getStatistics();
      std::cout << "Current PPC version is 0x" << std::hex << statblock.swVersion << std::dec << std::endl;
     
      if (statblock.swVersion == (int)ppcversion)
        std::cout << "The Robin is already up to date. Have a nice day." << std::endl;
      else
      {
	writePPCFirmwareFromFile(robin, ppcFirmware);
	writeFPGAFirmwareFromFile(robin, fpgaFirmware);
	robin.freeMsgResources();

	if (verbose) 
	  std::cout << "Resetting now." << std::endl;
	robin.reset();
      }      
      // Close the board.
      robin.close();
    }

    // ********************************************************************************
    // SecSi OTP upload. Updates firmware via ROBIN message passing
    if (accessMode == "otp") 
    {
      // The first parameter is the board numnber. This is common for all access modes.
      u_int boardNr = parameters.getParamInt(0);

      // Check the number of parameters. There has to be the ROBIN ID and the OTP file!
      if (parameters.nrOfParams() != 2) 
      {
	info();
	std::cout << "ERROR in command line parameters: The number of arguments did not match" << " the expected!"<< std::endl;
	usage();
	return -1;
      }
      
      // Assign the file name for the firmware file from the parameters
      std::string secSiFile = parameters.getParamString(1);
      
      // Check if the suffixes match the expected.
      
      // First determine the suffix of the PPC firmware file. It must be .elf!
      u_int dotPositionPpc = secSiFile.rfind(".");
      std::string suffixSecSi = secSiFile.substr(dotPositionPpc + 1, secSiFile.size() - dotPositionPpc - 1);

      if (suffixSecSi != "bin") 
      {
	info();
	std::cout << "ERROR in command line parameters: The OTP file needs extension .bin!" << std::endl;
	usage();
	return -1;	
      }
      
      if (!quiet)
	info();
      
      // try to open the file
      std::ifstream theFile;
      theFile.open(secSiFile.c_str());
      if (theFile.fail())
      {
        std::cout << "ERROR in file specification: " << secSiFile << " not found" << std::endl;
	usage();
	return -1;	
      }
      theFile.close();

      Robin robin(boardNr, c_timeout);
      
      // open the board.
      robin.open();

      // Reserve resources for message passing
      robin.reserveMsgResources(c_outstandingRequest, c_msgInputMemorySize, c_miscSize);
      writeSecSiFromFile(robin, secSiFile);
      robin.freeMsgResources();
      
      if (verbose) { std::cout << "Resetting now." << std::endl; }
      robin.reset();
	
      // Close the board.
      robin.close();
    }

    // ********************************************************************************
    // Next is eeprom mode. Reads or writes the contents of the PLX eeprom memory
    if (accessMode == "eeprom") 
    {
      // The first parameter is the board numnber. This is common for all access modes.
      u_int boardNr = parameters.getParamInt(0);

      // Check the number of parameters. There has to be the ROBIN ID the access direction (read/write)
      // and the eeprom file!
      if (parameters.nrOfParams() != 3) 
      {
	info();
	std::cout << "ERROR in command line parameters: The number of arguments did not match" << " the expected!"<< std::endl;
	usage();
	return -1;
      }

      // Get the remaining parameters
      std::string direction = parameters.getParamString(1);
      std::string eepromFile = parameters.getParamString(2);

      // See if the suffix of the eeprom file is .etx
      u_int dotPositionEeprom = eepromFile.rfind(".");
      std::string suffixEeprom = eepromFile.substr(dotPositionEeprom + 1, eepromFile.size() - dotPositionEeprom - 1);
      
      if (suffixEeprom != "etx") 
      {
	info();
	std::cout << "ERROR in command line parameters: Wrong file for eeprom programming!" << "Filename must end with .etx"<< std::endl;
	usage();
	return -1;
      }

      if ((direction != "read") && (direction != "write")) 
      {
	info();
	std::cout << "ERROR in command line parameters: Access direction must be either" << " read or write!"<< std::endl;
	usage();
	return -1;
      }

      Robin robin(boardNr, c_timeout);
      
      // open the board.
      robin.open();

      if (direction == "read") 
	storePlxEepromToFile(robin, eepromFile);
      else 
	writePlxEepromFromFile(robin, eepromFile);

      robin.close();
    }

    // ********************************************************************************
    // Next is ppcflash mode. Writes the firmware files directly to the PowerPC flash
    if (accessMode == "ppcflash") 
    {
      // The first parameter is the board numnber. This is common for all access modes.
      u_int boardNr = parameters.getParamInt(0);

      // Now check the number of parameters
      if (parameters.nrOfParams() != 4) 
      {
	info();
	std::cout << "ERROR in command line parameters: The number of arguments did not match" << " the expected!"<< std::endl;
	usage();
	return -1;
      }

      // Check target firmware part
      std::string firmwarePart = parameters.getParamString(1);
      if ((firmwarePart != "uboot") && (firmwarePart != "fpga") && (firmwarePart != "ppc") && (firmwarePart != "env")) 
      {
	info();
	std::cout << "ERROR in command line parameters: Wrong firmware part parameter." << "I do not know about " << firmwarePart << "!" << std::endl;
	usage();
	return -1;
      }
      
      // Get the direction (read / write)
      std::string direction = parameters.getParamString(2);
      if ((direction != "read") && (direction != "write")) 
      {
	info();
	std::cout << "ERROR in command line parameters: Access direction must be either" << " read or write!"<< std::endl;
	usage();
	return -1;
      }
      
      // Get the file name
      std::string fileName = parameters.getParamString(3);
      
      if (!quiet)
	info();
      
      Robin robin(boardNr, c_timeout);
      
      // open the board.
      robin.open();
      
      FlashProgrammer ppcflash(robin);
      
      ppcflash.reset();     
      
      if (firmwarePart == "ppc") 
      {
	if (direction == "read") 
	  storeFlashContentToFile(ppcflash, fileName, c_robinApplicationAddress, 0xfffff);
	else 
	{
	  std::string bakFileName = fileName + ".bak";
	  if (!quiet) { std::cout << "Creating backup file." << std::endl; }
	  storeFlashContentToFile(ppcflash, bakFileName, c_robinApplicationAddress, 0xfffff);
	  if (!quiet) { std::cout << "Erasing." << std::endl; }
	  eraseFlash(ppcflash, robin.getNrOfRols(), c_robinApplicationAddress, c_robinApplicationAddress + 0xfffff);
	  if (!quiet) { std::cout << "Writing." << std::endl; }
	  writeFlashContentFromFile(ppcflash, fileName, c_robinApplicationAddress);
	}
      }
      
      if (firmwarePart == "fpga") 
      {
	if (direction == "read") 
	  storeFlashContentToFile(ppcflash, fileName, c_fpgaFirmwareAddress, 0xfffff);
	else 
	{
	  std::string bakFileName = fileName + ".bak";
	  if (!quiet) { std::cout << "Creating backup file." << std::endl; }
	  storeFlashContentToFile(ppcflash, bakFileName, c_fpgaFirmwareAddress, 0xfffff);
	  if (!quiet) { std::cout << "Erasing." << std::endl; }
	  eraseFlash(ppcflash, robin.getNrOfRols(), c_fpgaFirmwareAddress, c_fpgaFirmwareAddress + 0xfffff);
	  if (!quiet) { std::cout << "Writing." << std::endl; }
	  writeFlashContentFromFile(ppcflash, fileName, c_fpgaFirmwareAddress);
	}
      }
      
      if (firmwarePart == "uboot") 
      {
	u_int ubootAddress, ubootSize;

	if ((robin.getNrOfRols() == 2) || (robin.getNrOfRols() == 3)) 
	{
	  if (robin.getNrOfRols() == 2) 
	    ubootAddress = c_ubootAddressPrototype;
	  else if (robin.getNrOfRols() == 3) 
	    ubootAddress = c_ubootAddressROBIN;	    
	  
	  ubootSize = 0xFFFFFFFF - ubootAddress;
	  
	  if (direction == "read") 
	    storeFlashContentToFile(ppcflash, fileName, ubootAddress, ubootSize);
	  else 
	  {
	    std::string bakFileName = fileName + ".bak";
	    if (!quiet) { std::cout << "Creating backup file." << std::endl; }
	    storeFlashContentToFile(ppcflash, bakFileName, ubootAddress, ubootSize);
	    if (!quiet) { std::cout << "Erasing." << std::endl; }
	    eraseFlash(ppcflash, robin.getNrOfRols(), ubootAddress, 0xFFFFFFFF);
	    if (!quiet) { std::cout << "Writing." << std::endl; }
	    writeFlashContentFromFile(ppcflash, fileName, ubootAddress);
	  }
	}
	else 
	  std::cout << "ERROR: Writing uboot on this board is not supported!" << std::endl;
      }

      if (firmwarePart == "env") 
      {
	if (direction == "read") 
	  storeEnvToFile(ppcflash, fileName);
	else 
	{
	  u_int dotPosition = fileName.rfind(".");
	  std::string fileNameNOSuffix = fileName.substr(0, dotPosition);
	  std::string bakFileName = fileNameNOSuffix + ".bak.env";
	  if (!quiet) { std::cout << "Creating backup file." << std::endl; }
	  storeEnvToFile(ppcflash, bakFileName);
	  if (!quiet) { std::cout << "Erasing." << std::endl; }
	  eraseFlash(ppcflash, robin.getNrOfRols(), c_envAddress, c_envAddress + 0xffff);
	  if (!quiet) { std::cout << "Writing." << std::endl; }
	  writeEnvFromFile(ppcflash, fileName);
	}
      }
      
      if (forceReset) 
      {
	if (verbose) { std::cout << "Resetting now." << std::endl; }
	robin.reset();
      }

      // Close the board.
      robin.close();
    }

    // ********************************************************************************
    // Finally is directflash mode. Free access to the PowerPC flash memory
    if (accessMode == "directflash") 
    {
      if (!quiet) { std::cout << "Direct Flash mode!" << std::endl; }

      // This is direct flash mode. Here the PPC flash device can be
      // read or written byte-by-byte.

      // Get the board number
      u_int boardNr = parameters.getParamInt(0);
      
      if (verbose) { std::cout << "Board number: " << boardNr << std::endl; }

      // Get the access mode
      std::string direction = parameters.getParamString(1);
      if ((direction != "read") && (direction != "write") && (direction != "erase")) 
      {
	info();
	usage();
	return -1;
      }

      if (verbose) { std::cout << "Direction " << direction << std::endl; }

      u_int address = parameters.getParamInt(2);

      if (verbose) { std::cout << "Address: " << std::hex << address << std::dec << std::endl; }

      // Check the number of parameters depending on the access mode
      // and read additional parameters
      u_int size = 0;
      u_int endAddress = 0;
      std::string fileName;
      if (direction == "read") 
      {
	if (parameters.nrOfParams() != 5) 
	{
	  info();
	  usage();
	  return -1;
	}
	size = parameters.getParamInt(3);
	fileName = parameters.getParamString(4);
      }
      else if (direction == "write") 
      {
	if (parameters.nrOfParams() != 4) 
	{
	  info();
	  usage();
	  return -1;
	}
	fileName = parameters.getParamString(3);
      }
      else if (direction == "erase") 
      {
	if (parameters.nrOfParams() != 4) 
	{
	  info();
	  usage();
	  return -1;
	}
	endAddress = parameters.getParamInt(3);
      }            

      Robin robin(boardNr, 100);
      
      // open the board.
      robin.open();
      
      FlashProgrammer ppcflash(robin);
      
      ppcflash.reset();


      if (direction == "read") 
	storeFlashContentToFile(ppcflash, fileName, address, size);
      else if (direction == "write") 
	writeFlashContentFromFile(ppcflash, fileName, address);
      else if (direction == "erase") 
	eraseFlash(ppcflash, robin.getNrOfRols(), address, endAddress);

      if (forceReset) 
      {
	if (verbose) { std::cout << "Resetting now." << std::endl; }
	robin.reset();
      }
      robin.close();      
    }

    // ********************************************************************************
    // Finally is directflash mode. Free access to the PowerPC flash memory
    if (accessMode == "scan") 
    {
      u_int boardId = 0;

      std::cout << "  BoardId" << "\tBoard Type" << "\tSerial     " << "\tRevision" << std::endl;
      while (boardId < c_maxcards) 
      {
	try 
	{
	  Robin *robin = new Robin(boardId, 10);
	  robin->open();
	  PlxEepromFile eepromFile;                        // First we need an empty eeprom file
	  std::vector<u_short> rawEepromContent(c_eepromSize);  // Next we need an array that takes the raw data.

	  u_int address;
	  for (address = 0; address < c_eepromSize; address++) 
	    rawEepromContent[address] = robin->readEeprom(address);    
	  
	  if (rawEepromContent[0] != 0xFFFF)                    // Check if the eeprom is empty
	    eepromFile.setFromRawData(rawEepromContent);

	  std::cout << "     " << boardId  << "\t        " << eepromFile.getBoardType() << "\t       " << eepromFile.getSerialNumber() << "\t       " << eepromFile.getRevisionNumber() << std::endl;
	  robin->close();
	  delete robin;
	}

	catch(ROSRobinExceptions &  ex) 
	{
	  std::cout << "Terminating scan on board " << boardId << std::endl;
	  break;
	}
	boardId++;
      }
    }    
  }

  catch (CmdLineArgumentException & ex) 
  {
    info();
    std::cout << "ERROR: " << ex.what() << std::endl;
    usage();
  }  
  catch (std::exception & ex) 
  {
    std::cout << "ERROR: " << ex.what() << std::endl;
  }

  return(0);
}


// Reads the contents of the Plx EEPROM and dumps it to a file
/*************************************************************/
void storePlxEepromToFile(Robin &  robin, std::string fileName) 
/*************************************************************/
{
  // First we need an empty eeprom file
  PlxEepromFile eepromFile;

  // Next we need an array that takes the raw data.
  std::vector<u_short> rawEepromContent(c_eepromSize);

  // This we fill in with the contents from the PLX EEPROM
  u_int address;
  for (address = 0; address < c_eepromSize; address++) 
    rawEepromContent[address] = robin.readEeprom(address);    

  eepromFile.setFromRawData(rawEepromContent);
  eepromFile.write(fileName);
}


// Writes the contents of the Plx EEPROM from a given file
/***************************************************************/
void writePlxEepromFromFile(Robin &  robin, std::string fileName)
/***************************************************************/
{
  // First we one the eeprom file
  PlxEepromFile eepromFile(fileName);
  
  // Now get the raw data
  std::vector<u_short> rawEepromContent = eepromFile.getRawData();

  // and write it ro the EEPROM
  u_int address;
  for (address = 0; (address < c_eepromSize) && (address < rawEepromContent.size()); address++) 
    robin.writeEeprom(address, rawEepromContent[address]);    
}


// Writes the PPC application stored in a .elf to the flash via 
// message passing
/*****************************************************************/
void writePPCFirmwareFromFile(Robin &  robin, std::string fileName)
/*****************************************************************/
{
  std::ifstream firmwareFile;

  std::cout << "Opening the PPC application file " << fileName.c_str() << std::endl;
  firmwareFile.open(fileName.c_str());

  if (!firmwareFile.is_open())
  {
    std::cout << "Failed to open the PPC application file" << std::endl;
    exit(-1);  
  }

  std::vector<u_int> data;
  // Read the firmware file
  while (firmwareFile) 
  {
    u_int dword;
    char *dwordPtr = reinterpret_cast<char *>(&dword);
    firmwareFile.read(dwordPtr, 4);
    data.push_back(dword);
  }

  std::cout << "Writing PPC firmware to scratch" << std::endl;
  u_int dataWritten = writeToScratch(robin, data);  
  std::cout << "done!" << std::endl;

  firmwareFile.close();

  // Get the misc base for reply addresses
  u_long replyAddress = robin.getVirtMiscBase();

  // Tell the ROBIN to copy the new firmware to the flash...
  std::cout << "Tell the PPC to copy it to the flash" << std::endl;
  robin.sendMsg(0, enum_reqReFlashCpu, replyAddress, dataWritten);
  
  // and wait for the acknowledge
  robin.receiveMsg(replyAddress, true);
  
  // Check for transmission errors
  RespMsg *respStruct = reinterpret_cast<RespMsg *>(replyAddress);
  
  if (respStruct->marker != 0x33fedc33) 
    throw ROSRobinExceptions(ROSRobinExceptions::FIRMWARE, "Robin did reply with the wrong marker when flashing the PPC code");
  if ((respStruct->response & 0xFF) != enum_respAck) 
    throw ROSRobinExceptions(ROSRobinExceptions::FIRMWARE, "Robin reported an error when flashing the PPC code");
}


/******************************************************************/
void writeFPGAFirmwareFromFile(Robin &  robin, std::string fileName) 
/******************************************************************/
{
  std::ifstream firmwareFile;

  std::cout << "Opening the FPGA firmware file " << fileName.c_str() << std::endl;
  firmwareFile.open(fileName.c_str());

  if (!firmwareFile.is_open())
  {
    std::cout << "Failed to open the FPGA firmware file" << std::endl;
    exit(-1);  
  }

  std::vector<u_int> data;
  // Read the firmware file
  while (firmwareFile) 
  {
    u_int dword = 0xFFFFFFFF;
    char *dwordPtr = reinterpret_cast<char *>(&dword);
    firmwareFile.read(dwordPtr, 4);
    data.push_back(dword);
  }

  u_int dataWritten = writeToScratch(robin, data);  

  // Get the misc base for reply addresses
  u_long replyAddress = robin.getVirtMiscBase();  
  
  // Tell the PowerPC to copy the data to the flash
  robin.sendMsg(0, enum_reqReFlashFpga, replyAddress, dataWritten);
  
  // and wait for the acknowledge
  robin.receiveMsg(replyAddress, true);
  
  // Check for transmission errors
  RespMsg *respStruct = reinterpret_cast<RespMsg *>(replyAddress);
 
   if (respStruct->marker != 0x33fedc33) 
    throw ROSRobinExceptions(ROSRobinExceptions::FIRMWARE, "Robin did reply with the wrong marker when flashing the FPGA code");
  if ((respStruct->response & 0xFF) != enum_respAck) 
  {
    std::cout << "Could not program FPGA with firmware file!" << std::endl;
    throw ROSRobinExceptions(ROSRobinExceptions::FIRMWARE, "Robin reported an error when programming the FPGA code");
  }
  
  // Tell the PowerPC to re-program the FPGA.
  robin.sendMsg(0, enum_reqReProgramFpga, replyAddress);  
  
  ts_delay(14000000);
}


// Writes the PPC application stored in a .elf to the flash via message passing
/**********************************************************/
void writeSecSiFromFile(Robin & robin, std::string fileName)
/**********************************************************/
{
  std::ifstream firmwareFile;

  std::cout << "Opening the firmware file" << std::endl;
  firmwareFile.open(fileName.c_str());

  if (!firmwareFile.is_open())
  {
    std::cout << "Failed to open the firmware file" << std::endl;
    exit(-1);  
  }

  std::vector<u_int> data;
  // Read the firmware file
  while (firmwareFile) 
  {
    u_int dword;
    char *dwordPtr = reinterpret_cast<char *>(&dword);
    firmwareFile.read(dwordPtr, 4);
    data.push_back(dword);
  }

  std::cout << "Writing SecSi record to scratch" << std::endl;
  u_int dataWritten = writeToScratch(robin, data);  
  std::cout << "done!" << std::endl;

  firmwareFile.close();

  // Get the misc base for reply addresses
  u_long replyAddress = robin.getVirtMiscBase();

  // Tell the ROBIN to copy the new firmware to the flash...
  std::cout << "Tell the PPC to copy it to the flash" << std::endl;
  robin.sendMsg(0, enum_reqSetSecSi, replyAddress, dataWritten);
  
  // and wait for the acknowledge
  robin.receiveMsg(replyAddress, true);
  
  // Check for transmission errors
  RespMsg *respStruct = reinterpret_cast<RespMsg *>(replyAddress);
  
  if (respStruct->marker != 0x33fedc33) 
    throw ROSRobinExceptions(ROSRobinExceptions::FIRMWARE, "Robin did reply with the wrong marker when flashing the PPC code");
  if ((respStruct->response & 0xFF) != enum_respAck) 
    throw ROSRobinExceptions(ROSRobinExceptions::FIRMWARE, "Robin reported an error when flashing the SecSi record code");
}


/****************************************************************/
void storeEnvToFile(FlashProgrammer & flash, std::string fileName)
/****************************************************************/
{
  // This vector will keep the env data until we push it into the PPCEnvironment object
  std::vector<u_char> data;
    
  // Address counter
  u_int address = c_envAddress;
  
  u_int i;
  
  // First read the first five chars. These contain the crc checksum.
  // and the letter of the first entry. At the same time check
  // if these five are 0xff. In that case the env sector inside the
  // flash memory is empty!

  bool isEmpty = true;
  for (i = 0; i < 4; i++) 
  {
    u_char ch = flash.read(address++);
    
    // If we find only one char which is not 0xFF the sector and the Env is in use!
    if (ch != 0xFF) 
      isEmpty = false; 

    // Store the data.
    data.push_back(ch);
  }

  if (isEmpty) 
  {
    // If the sector is empty: create an empty file.
    std::ofstream file;
    file.open(fileName.c_str());
    file.close();
  }
  else
   {
    // Otherwise continue with reading data until we meet two zeros.
    do
    {
      data.push_back(flash.read(address++));
      i++;
    }
    while (!((data[i - 2] == '\0') && (data[i - 1] == '\0')));

    PPCEnvironment env;
    env.setFromRawData(data);
    env.write(fileName);
  }  
}


/******************************************************************/
void writeEnvFromFile(FlashProgrammer & flash, std::string fileName)
/******************************************************************/
{
  // Get the data from the file
  PPCEnvironment env(fileName);
  std::vector<u_char> data = env.getRawData();

  // The address counter
  u_int address = c_envAddress;
  flash.cmdBaseAddress = c_envAddress;

  std::vector<u_char>::iterator iter;
  int total_size = 0;
  for(iter = data.begin(); iter != data.end(); iter++) {
    total_size++;
    flash.write(address++, *iter);    
  }
  std::cout<<"Env. buffer size: 0x"<<total_size<<" bytes\n";
}


/****************************************************************************************************/
void storeFlashContentToFile(FlashProgrammer & flash, std::string fileName, u_int address, u_int size) 
/****************************************************************************************************/
{
  // The file where the flash contents go into
  std::ofstream flashContentFile;
  flashContentFile.open(fileName.c_str());

  if (!flashContentFile.is_open())
  {
    std::cout << "Failed to open the Flash content file" << std::endl;
    exit(-1);  
  }

  // read from the flash
  u_int i;
  for (i = 0; i < size; i++) 
  {
    char data = static_cast<char> (flash.read(address + i));
    flashContentFile.write(&data, 1);
  }

  flashContentFile.close();
}


/******************************************************************************************/
void writeFlashContentFromFile(FlashProgrammer & flash, std::string fileName, u_int address) 
/******************************************************************************************/
{
  // The file where the flash contents go into
  std::ifstream flashContentFile;
  flashContentFile.open(fileName.c_str());

  if (!flashContentFile.is_open())
  {
    std::cout << "Failed to open the Flash content file" << std::endl;
    exit(-1);  
  }

  flashContentFile.seekg(0, std::ios::end);
  u_int size = flashContentFile.tellg();
  flashContentFile.seekg(0, std::ios::beg);

  std::cout << "Size: " << size << std::endl;

  flash.cmdBaseAddress = address;
  // read from the flash
  u_int i,fracDone,fracDone_old;
  std::cout.setf(std::ios::dec,std::ios::basefield);
  std::cout << std::endl;
  fracDone_old = 0;
  std::cout<<"Progress:\n0                        50                        100 %\n|                        |                        |\n "<<std::flush;
  for (i = 0; i < size; i++) 
  {
    char data;
    flashContentFile.read(&data, 1);
    flash.write(address + i, static_cast<char> (data));
    fracDone = 100*i/size;
    if( !(fracDone%2) && fracDone != fracDone_old ) 
    {
      std::cout<<"="<<std::flush;
      fracDone_old = fracDone;
    }
    /*if ((i % 0x100) == 0) 
      std::cout << ".";
    if ((i % 0x4000) == 0) 
    std::cout << std::endl;*/
  }
  fracDone = 100*i/size;
  std::cout<<"=\n"<<std::flush;
  std::cout <<"\n\n";
  //std::cout.setf(std::ios::hex,std::ios::basefield);
  flashContentFile.close();
}


/********************************************************************************************/
void eraseFlash(FlashProgrammer & flash, u_int nrOfRols, u_int startAddress, u_int endAddress)
/********************************************************************************************/
{

  u_int sectorAddressStep;
  // Check plausability here
  if (nrOfRols == 2) 
    sectorAddressStep = 0x20000;
  else if (nrOfRols == 3) 
    sectorAddressStep = 0x10000;	  
  else 
    std::cout << "Erase on this board is not supported!" << std::endl;
  
  if ((startAddress & (sectorAddressStep - 1)) != 0) 
    std::cout << "The start address is not on a sector boundary!" << std::endl;
  else if ((endAddress & (sectorAddressStep - 1)) != (sectorAddressStep - 1)) 
    std::cout << "The end address does not point at the end of a sector!" << std::endl;	  
  else 
  {
    u_int addressCounter;
    for (addressCounter = startAddress;(addressCounter < endAddress) && (addressCounter >= startAddress); addressCounter += sectorAddressStep) 
    {
      std::cout << "Erasing sector " << std::hex << addressCounter << std::endl;
      flash.erase(addressCounter);
    }
  }
}


/**********************************************************/
u_int writeToScratch(Robin & robin, std::vector<u_int> data)
/**********************************************************/
{
  // Get the misc base for reply addresses
  u_long replyAddress = robin.getVirtMiscBase();
  
  // Set the chunck size for the different data chunks sent via ROBIN MP
  u_int firmwareDataChunkSize = 0x70;
  
  // This is the memory for the chunk
  u_int * firmwareDataChunk = new u_int[firmwareDataChunkSize + 2]; 
  
  // We set all to zero in advance to allow the or operation in the while loop.
  u_int i;
  for (i = 0; i < firmwareDataChunkSize + 2; i++) 
    firmwareDataChunk[i] = 0;

  // The data counter which counts the chars read from the file.
  // This counter is reset to zero with each chunk
  u_int dataCounter = 0;
  
  // The offset inside the scratch area.
  u_int scratchOffset = 0;
  
  std::vector<u_int>::iterator iter;
  
  // Now write the firmware file
  for (iter = data.begin(); iter != data.end(); iter++) 
  {
    // Store the data to the u_int array in big endian format!
    firmwareDataChunk[dataCounter + 2] = byteSwap(*iter);
    
    // Increment the data counter
    dataCounter++;
    
    // If we reach the end of a chunk we have to write it now.
    if (dataCounter == firmwareDataChunkSize) 
    {
      std::cout << ".";

      firmwareDataChunk[0] = scratchOffset;
      firmwareDataChunk[1] = firmwareDataChunkSize;
      // Send the chunk to the ROBIN
      robin.sendMsg(0, enum_reqLoadMem, replyAddress, firmwareDataChunkSize + 2, firmwareDataChunk);

      // and wait for the acknowledge
      robin.receiveMsg(replyAddress, true);

      // Check for transmission errors
      RespMsg *respStruct = reinterpret_cast<RespMsg *>(replyAddress);
     
      if (respStruct->marker != 0x33fedc33) 
	throw ROSRobinExceptions(ROSRobinExceptions::FIRMWARE, "Robin did reply with the wrong marker when uploading to scratch area");
      if ((respStruct->response & 0xFF) != enum_respAck)
      { 
	std::cout << "respStruct->marker   = 0x" << std::hex << respStruct->marker << std::dec << std::endl;
	std::cout << "respStruct->size     = 0x" << std::hex << respStruct->size << std::dec << std::endl;
	std::cout << "respStruct->version  = 0x" << std::hex << respStruct->version << std::dec << std::endl;
	std::cout << "respStruct->response = 0x" << std::hex << respStruct->response << std::dec << std::endl;
	throw ROSRobinExceptions(ROSRobinExceptions::FIRMWARE, "Robin reported an error when uploading to scratch area.");
      }
      // Adjust the offset within the scratch area
      scratchOffset += firmwareDataChunkSize;

      // We set all to zero in advance to allow the or operation in the while loop.
      for (i = 0; i < firmwareDataChunkSize + 2; i++) 
	firmwareDataChunk[i] = 0;
      
      // And we reset the dataCounter too as it is indexing within the chunk memory.
      dataCounter = 0;
    }
  }

  u_int finalChunkSize = 0;

  // Finally if there is any additional data at the end which has not been send do this now!
  if (dataCounter != 0) 
  {

    finalChunkSize = dataCounter;

    firmwareDataChunk[0] = scratchOffset;
    firmwareDataChunk[1] = finalChunkSize;
    
    // Send the chunk to the ROBIN
    robin.sendMsg(0, enum_reqLoadMem, replyAddress, finalChunkSize + 2, firmwareDataChunk);
    
    // and wait for the acknowledge
    robin.receiveMsg(replyAddress, true);
    
    // Check for transmission errors
    RespMsg *respStruct = reinterpret_cast<RespMsg *>(replyAddress);
    
    if (respStruct->marker != 0x33fedc33) 
      throw ROSRobinExceptions(ROSRobinExceptions::FIRMWARE, "Robin did reply with the wrong marker when uploading to scratch area");
    if ((respStruct->response & 0xFF) != enum_respAck) 
      throw ROSRobinExceptions(ROSRobinExceptions::FIRMWARE, "Robin reported an error when uploading to scratch area");
  }
  return (scratchOffset + finalChunkSize);
}


/************************/
u_int byteSwap(u_int data)
/************************/
{
  u_int ret = 0;

  ret = data << 24;
  ret |= (data << 8)  & 0x00FF0000;
  ret |= (data >> 8)  & 0x0000FF00;
  ret |= (data >> 24) & 0x000000FF;

  return ret;
}


/**************/
void usage(void) 
/**************/
{
  std::cout << "Usage: " << std::endl << std::endl;
  std::cout << " 1) For firmware updates with ROBIN message passing use: " << std::endl;
  std::cout << "        robin_firmware_update [-qv] [--dl=<level>] [--dp=<package>] [--ppcver=<swVersion>] [--reset] [--mode=ppcmessage] <ROBIN ID>  <PPC file> <FPGA file>" << std::endl;
  std::cout << "    Where: " << std::endl;
  std::cout << "  \t" << "<swVersion>     :  The desired (hexadecimal) version of the PPC application" << std::endl;
  std::cout << "  \t" << "<ROBIN ID>      :  The logical number of the ROBIN (0..n)" << std::endl;
  std::cout << "  \t" << "<PPC file>      :  The file containing the PowerPC appliction (.elf file)" << std::endl;
  std::cout << "  \t" << "<FPGA file>     :  The file containing the FPGA firmware (.bit file)" << std::endl;
  std::cout << "  \t" << "This is the default setting when no --mode parameter is given!" << std::endl;
  std::cout << std::endl;
  std::cout << " 2) For updating the PLX eeprom contents: " << std::endl;
  std::cout << "        robin_firmware_update [-qv] [--dl=<level>] [--dp=<package> [--reset] --mode=eeprom <ROBIN ID> <read/write> <file>" << std::endl;
  std::cout << "    Where: " << std::endl;
  std::cout << "  \t" << "<ROBIN ID>      :  The logical number of the ROBIN (0..n)" << std::endl;
  std::cout << "  \t" << "<read/write>         :  Sets read or write direction" << std::endl;
  std::cout << "  \t" << "<file>               :  Sets the Plx eeprom file to be programmed. This file has to " << std::endl;
  std::cout << "  \t" << "                        end with .etx! In case of read, the file " << std::endl;
  std::cout << "  \t" << "                        should not exist. The program will not overwrite existing files!" << std::endl;
  std::cout << std::endl;
  std::cout << " 3) For firmware updates with direct PowerPC flash memory access. Requires special FPGA firmware to be" << std::endl;
  std::cout << "    loaded in advance with robin_fpga_load: " << std::endl;
  std::cout << "        robin_firmware_update [-qv] [--dl=<level>] [--dp=<package> [--reset] --mode=ppcflash <ROBIN ID> <firmware part> <read/write> <file> " << std::endl;
  std::cout << "    Where: " << std::endl;
  std::cout << "  \t" << "<ROBIN ID> :  The logical number of the ROBIN (0..n)" << std::endl;
  std::cout << "  \t" << "<firmware part>      :  The part of the firmware to be read or written." << std::endl;
  std::cout << "  \t" << "                        This may be:" << std::endl;
  std::cout << "  \t" << "                            uboot  : The PowerPC boot monitor " << std::endl;
  std::cout << "  \t" << "                            fpga   : The FPGA firmware in the PowerPC flash " << std::endl;
  std::cout << "  \t" << "                            ppc    : The PPC robin application in the PowerPC flash " << std::endl;
  std::cout << "  \t" << "                            env    : The PowerPC environment variables " << std::endl;
  std::cout << "  \t" << "<read/write>         :  Sets read or write direction" << std::endl;
  std::cout << "  \t" << "<file>               :  Sets the file to be programmed. In case of read, the file " << std::endl;
  std::cout << "  \t" << "                        should not exist. The program will not overwrite existing files!" << std::endl;
  std::cout << std::endl;
  std::cout << " 4) For random access to the PowerPC flash memory. Requires special FPGA firmware to be" << std::endl;
  std::cout << "    loaded in advance with robin_fpga_load: " << std::endl;
  std::cout << "    robin_firmware_update [-qv] [--dl=<level>] [--dp=<package> [--reset] --mode=directflash <ROBIN ID> <read/write/erase> <address> [<size/endAddress>] [<file>] " << std::endl;
  std::cout << "    Where: " << std::endl;
  std::cout << "  \t" << "<ROBIN ID> :  The logical number of the ROBIN (0..n)" << std::endl;
  std::cout << "  \t" << "<read/write/erase>   :  Sets read, writes or erases the flash memory" << std::endl;  
  std::cout << "  \t" << "<size/endAddress>    :  In case of read the size (number of char) for the read operation has to be given " << std::endl;
  std::cout << "  \t" << "                     :  In case of write this parameter is not required" << std::endl;
  std::cout << "  \t" << "                     :  In case of erase this parameter gives the end address of the sector to be erased" << std::endl;
  std::cout << std::endl;
  std::cout << " 5) For scanning the available boards:" << std::endl;
  std::cout << "    robin_firmware_update [-qv] [--dl=<level>] [--dp=<package> --mode=scan" << std::endl;
  std::cout << std::endl;
  std::cout << " 6) For writing OTP production information to the SecSi area with ROBIN message passing use: " << std::endl;
  std::cout << "        robin_firmware_update [-qv] [--dl=<level>] [--dp=<package> [--reset] [--mode=otp] <ROBIN ID>  <SecSi file>" << std::endl;
  std::cout << "    Where: " << std::endl;
  std::cout << "  \t" << "<ROBIN ID>      :  The logical number of the ROBIN (0..n)" << std::endl;
  std::cout << "  \t" << "<SecSi file>    :  The file containing the SecSi information (.bin file)" << std::endl;
  std::cout << std::endl;
  std::cout << "  Switches common to all modes: " << std::endl;
  std::cout << "  \t" << " -q      : Quiet. Switch to suppress all outputs except error messages" << std::endl;
  std::cout << "  \t" << " -v      : Verbose output" << std::endl;
  std::cout << "  \t" << " --reset : Forces a reset after the operations have been finished" << std::endl;
  std::cout << "  \t" << " --dl    : Defines the debug level" << std::endl;
  std::cout << "  \t" << " --dp    : Defines the debug package" << std::endl;
}


/*************/
void info(void) 
/*************/
{
  std::cout << "This is robin_firmware_update. A tool to update the ATLAS ROBIN firmware." << std::endl;
  std::cout << "M. Mueller 06/2005" << std::endl;
  std::cout << "Added PCI-Express support: A. Misiejuk, 2009"<<std::endl;
}


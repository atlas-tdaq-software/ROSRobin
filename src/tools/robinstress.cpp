/***************************************************************/
/* file:   robinstress.cpp                                     */
/* author: Markus Joos, CERN-PH/ESS                            */
/***** C 2007 - The software with that certain something *******/


#ifndef HOST
  #define HOST
#endif

#include <iostream>
#include <string>
#include <iomanip>
#include <getopt.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include <signal.h>	  
#include <unistd.h>
#include <errno.h>
#include "ROSEventFragment/ROBFragment.h"
#include "ROSRobin/ROSRobinExceptions.h"
#include "ROSRobin/Robin.h"
#include "ROSRobin/Rol.h"
#include "ROSMemoryPool/MemoryPage.h"
#include "ROSMemoryPool/WrapperMemoryPool.h"
#include "ROSGetInput/get_input.h"
#include "DFDebug/DFDebug.h"
#include "ROSRobin/robin.h"
#include "ROSRobin/robin_statistics_map.h"
#include "rcc_time_stamp/tstamp.h"
#include <cmdl/cmdargs.h>
#include <tmgr/tmresult.h>

using namespace ROS;
using namespace daq::tmgr;

/**********/    
/* Macros */
/**********/    
#define CONVERT_ENDIAN_16(X) ((Word)(((X) >> 8) + ((X) << 8)))
#define CONVERT_ENDIAN_32(X) ((Dword)( (((X) & 0xff000000) >> 24) | (((X) & 0x00ff0000) >> 8) | (((X) & 0x0000ff00) << 8) | (((X) & 0x000000ff) << 24)))


/*************/    
/* Constants */
/*************/    
#define MAX_TICKET 100
#define DATA_SIZE 100
    
    
/***********/
/* Globals */
/***********/
u_int getstat = 0, ncards, cont = 0, dblevel = 5, dbpackage = DFDB_ROSROBIN;
Rol *prol[15];
unsigned long long basetime;


/**************/
/* Prototypes */
/**************/
void loadram(u_int rol, u_int l1id);
void sigquit_handler(int signum);
void timeelapsed(u_int *sec, u_int *usec);
void AlarmHandler(int signum);
void dumpstat(void);
void decodestat(StatisticsBlock *statblock);
void outtext(std::string text, Byte *data, u_int len, u_int mode);


// sigquit handler
/**********************************/
void sigquit_handler(int /*signum*/)
/**********************************/
{
  cont = 0;
}


/**************/
void usage(void)
/**************/
{
  std::cout << "Valid options are ..." << std::endl;
  std::cout << "-D:         Enable debugging" << std::endl;
  std::cout << std::endl;
}


/*****************************/
int main(int argc, char **argv)
/*****************************/
{
  struct sigaction sa, sa2;
  struct timeval basetp;
  int rol_error[18], c, ok, nerrors = 0;
  u_int nrols, *fptr;
  u_long ticket;
  int ret;
  
  sigemptyset(&sa2.sa_mask);
  sa2.sa_flags = 0;
  sa2.sa_handler = AlarmHandler;
  ret = sigaction(SIGALRM, &sa2, NULL);
  if (ret < 0)
  {
    printf("Cannot install signal handler (error=%d)\n", ret);
    exit(0);
  }
  
      
  while ((c = getopt(argc, argv, "D")) != -1)
    switch (c) 
    {
      case 'D': DF::GlobalDebugSettings::setup(20, DFDB_ROSROBIN); break;                

      default:
	std::cout << "Invalid option " << c << std::endl;
	std::cout << "Usage: " << argv[0] << " [options]: " << std::endl;
	usage();
	exit (TmUnresolved);
    }
    
  // Install signal handler for SIGQUIT (ctrl + \)
  sigemptyset(&sa.sa_mask);
  sa.sa_flags   = 0;
  sa.sa_handler = sigquit_handler;

  // Dont block in intercept handler
  if (sigaction(SIGQUIT, &sa, NULL) < 0) 
  {
    std::cout << "ERROR in main: sigaction() FAILED with ";
    int code = errno;
    if (code == EFAULT) std::cout << "EFAULT" << std::endl;
    if (code == EINVAL) std::cout << "EINVAL" << std::endl;
    exit(TmUnresolved);
  }
 
  Robin *rorobin[5];
  u_long eventBase[5];
  u_int physicalAddress[5];
  WrapperMemoryPool *m_memoryPool[5];  
  MemoryPage *mem_page[5];
  u_int *fragBufferAddress[5];

  u_int timeout                = 20;
  u_int numberOfOutstandingReq = 2;
  u_int msgInputMemorySize     = 0x200;
  u_int miscSize               = 0x4000;
  u_int npages                 = 10;
  u_int pageSize               = 0x2000;
  u_int eventSize              = npages * pageSize; 
      
  ncards = 0;
  nrols = 0;
  for (u_int cloop = 0; cloop < 6; cloop++)
  {
    try
    {
      rorobin[ncards] = new Robin(ncards, timeout);
      rorobin[ncards]->open();
      rorobin[ncards]->reserveMsgResources(numberOfOutstandingReq, msgInputMemorySize, miscSize, eventSize); 

      eventBase[ncards]       = rorobin[ncards]->getVirtEventBase();
      physicalAddress[ncards] = rorobin[ncards]->getPhysEventBase();
      m_memoryPool[ncards]    = new WrapperMemoryPool(npages, pageSize, eventBase[ncards], physicalAddress[ncards]);
      mem_page[ncards]        = m_memoryPool[ncards]->getPage();
      if (mem_page[ncards] == 0) 
      {						
	std::cout << "ERROR: Failed to get a page from the memory pool" << std::endl;
	exit(-1);
      }
      fragBufferAddress[ncards] = static_cast<u_int *> (mem_page[ncards]->reserve(mem_page[ncards]->capacity()));

      std::cout << "Robin " << ncards << " OK. Now creating 3 ROL objects." << std::endl;

      for(u_int loop = 0; loop < 3; loop++)
      {
	prol[nrols] = new Rol(*rorobin[ncards], loop);   
	
	if (loop == 0)
	{
          StatisticsBlock statblock;
	  statblock = prol[nrols]->getStatistics();
	  std::cout << "Robin " << ncards << " serial number = " << statblock.serNum << std::endl;
        }

	prol[nrols]->setConfig(enum_cfgRolEmu, enum_emuTypeUpload);
	prol[nrols]->setConfig(enum_cfgRolDataGen, 0);
	prol[nrols]->setConfig(enum_cfgRolEnabled, 1);
        prol[nrols]->setCRCInterval(1);   //Check the CRC of all fragments
        loadram(nrols, 0);
        loadram(nrols, 0xfffe);
        loadram(nrols, 0xffff);
	
        usleep(10000);  //delay 10 ms
	
	prol[nrols]->setConfig(enum_cfgRolEnabled, 0);
	rol_error[nrols] = 0;
        std::cout << "ROL " << nrols << " OK" << std::endl;
	nrols++;
      }
      ncards++; 
    }
    catch (ROSRobinExceptions& e)
    {
      std::cout << "Robin " << ncards << " does not respond" << std::endl;
      std::cout << e << std::endl;
      break;
    }   
  }
  std::cout << ncards << " Robins found " << std::endl;
  
  cont = 1;
  std::cout << "Press <ctrl>+\\ to terminate the test" << std::endl;
  u_int ftested = 0;
  gettimeofday(&basetp, 0);
  basetime = basetp.tv_sec * 1000000 + basetp.tv_usec; 

  alarm(1);  //Get first statistics after one second

  while (cont)
  {
  
    if (getstat)
      dumpstat();
  
    for (u_int card = 0; card < ncards; card++)
    {
      for (u_int link = 0; link < 3; link++)
      {
	for (u_int el1id = 0; el1id < 2; el1id++)
	{ 
	  u_int l1id;
	  
	  if (el1id)
	    l1id = 0xffff;
	  else
	    l1id = 0;
	    
	  u_int rol = card * 3 + link; 
	  try
	  {
	    ticket = prol[rol]->requestFragment(l1id, reinterpret_cast<u_long> (fragBufferAddress[card]));
	    fptr = prol[rol]->getFragment(ticket);      
	  }
	  catch (ROSRobinExceptions& e)
	  {
	    std::cout << "ERROR: " << e << std::endl;
	    return(-1);
	  }    
	  
	  ok = 1;  

          if (fptr[14] != l1id)
	  {
            std::cout << "ERROR: Card: " << card << "  Link: " << link << "  L1ID is " << fptr[14] << " instead of " << l1id << std::endl;
            ok = 0;
	  }
	    
	  for (u_int dword = 0; dword < DATA_SIZE; dword++)
	  {
	    if ((l1id == 0) && ((dword & 0x1) == 0) && (fptr[19 + dword] != 0x55555555))
            { 
              std::cout << "ERROR: Card=" << card << ", Link=" << link << ", L1ID=" << l1id << ". Data word # " << dword << " is 0x" << HEX(fptr[19 + dword]) << " instead of 0x55555555" << std::endl;
              ok = 0;
	    }  

	    if ((l1id == 0) && ((dword & 0x1) == 1) && (fptr[19 + dword] != 0xaaaaaaaa))
            { 
              std::cout << "ERROR: Card=" << card << ", Link=" << link << ", L1ID=" << l1id << ". Data word # " << dword << " is 0x" << HEX(fptr[19 + dword]) << " instead of 0xaaaaaaaa" << std::endl;
              ok = 0;
	    }  
	    
	    if ((el1id == 1) && ((dword & 0x1) == 0) && (fptr[19 + dword] != 0xaaaaaaaa))
            { 
              std::cout << "ERROR: Card=" << card << ", Link=" << link << ", L1ID=" << l1id << ". Data word # " << dword << " is 0x" << HEX(fptr[19 + dword]) << " instead of 0xaaaaaaaa" << std::endl;
              ok = 0;
	    }  
	    
	    if ((el1id == 1) && ((dword & 0x1) == 1) && (fptr[19 + dword] != 0x55555555))
            { 
              std::cout << "ERROR: Card=" << card << ", Link=" << link << ", L1ID=" << l1id << ". Data word # " << dword << " is 0x" << HEX(fptr[19 + dword]) << " instead of 0x55555555" << std::endl;
              ok = 0;
	    }  
	  }

          if (!ok)
	  {
	    std::cout << ftested << " fragments tested" << std::endl;
   	    u_int sec, usec;
            timeelapsed(&sec, &usec);

            std::cout << "Time stamp of error: " << sec << "." << usec << " seconds after start of program" << std::endl;
            std::cout << "Dumping ROB/ROD header: " << std::endl;
            for (u_int hword = 0; hword < 19; hword++)
    	      std::cout << "Word " << hword << " = " << fptr[hword] << " (0x" << HEX(fptr[hword]) << ") " << std::endl;
	    nerrors++;
	    if (nerrors > 1000)
	      exit(0);
	      
            //reload the event	
	    rol_error[rol]++;
	    if (rol_error[rol] > 5)
	    {
 	      rol_error[rol] = 0;           
	      std::cout << "Re-loading event with L1ID = " << l1id << " into ROL " << rol << std::endl;
	      prol[rol]->setConfig(enum_cfgRolEnabled, 1);
              loadram(rol, l1id);
	      prol[rol]->setConfig(enum_cfgRolEnabled, 0);
            }
	  }

  	  ftested++;
	  if (!(ftested & 0xfffff))
	  {
	    u_int sec, usec;
            timeelapsed(&sec, &usec);
	    std::cout << ftested << " fragments tested so far. Time elapsed since start of program: " << sec << "." << usec << " seconds" << std::endl; 
	    
	  }  
	}
      }      
    }
  }  
  
  for (u_int loop = 0; loop < nrols; loop++)
    delete prol[loop];
  
  for (u_int loop = 0; loop < ncards; loop++)
  {
    mem_page[loop]->free();      
    rorobin[loop]->freeMsgResources(); 
    rorobin[loop]->close();
    delete rorobin[loop]; 
  }

  return(0);
}


/*********************************/
void loadram(u_int rol, u_int l1id)
/*********************************/
{
  u_int word, data[1024];

  word = 0;
  data[word++] = 0xb0f00000; //start control word
  data[word++] = 0xee1234ee; //header marker
  data[word++] = 0x00000009; //header size
  data[word++] = 0x03010000; //format version
  data[word++] = 0x00000011; //source ID
  data[word++] = 0x00000022; //run number 
  data[word++] = l1id;       //L1ID
  data[word++] = 0x00000033; //BCID
  data[word++] = 0x00000044; //trigger type
  data[word++] = 0x00000055; //detector event type
  data[word++] = 0x00000000; //status word

  for (u_int loop = 0; loop < DATA_SIZE; loop += 2)
  {
    if(l1id)
    {
      data[word++] = 0xaaaaaaaa;
      data[word++] = 0x55555555;
    }
    else
    {
      data[word++] = 0x55555555;
      data[word++] = 0xaaaaaaaa;
    }
  }
  data[word++] = 0x00000001; //number of status elements
  data[word++] = DATA_SIZE;  //number of data elements
  data[word++] = 0x00000000; //status block position (before data)
  data[word++] = 0xe0f00000; //end control word

  std::cout << "Uploading fragment with L1ID " << l1id << std::endl;
  try
  {
    prol[rol]->loadFragment(word, data, enum_emuEventNoError);
  }
  catch (ROSRobinExceptions& e)
  {
    std::cout << "ERROR: " << e << std::endl;
    exit(-1);
  }
}


/***************************************/
void timeelapsed(u_int *sec, u_int *usec)
/***************************************/
{
  struct timeval tp;
  unsigned long long t2;
  
  gettimeofday(&tp, 0);
  t2 = tp.tv_sec * 1000000 + tp.tv_usec; 
  *usec = (u_int)((t2 - basetime) % 1000000);
  *sec = (u_int)((t2 - basetime) / 1000000);
}


/***************************/
void AlarmHandler(int signum)
/***************************/
{
  getstat = 1;
}


/*****************/
void dumpstat(void)
/*****************/
{
  StatisticsBlock statblock;

  getstat = 0;
  for (u_int card = 0; card < ncards; card++)
  {
    for (u_int link = 0; link < 3; link++)
    {
      u_int rol = card * 3 + link; 
      try
      {
        std::cout << "Dumping statistics for ROL " << rol << std::endl;
 	statblock = prol[rol]->getStatistics();
        decodestat(&statblock);
      }
      catch (ROSRobinExceptions& e)
      {
	std::cout << "ERROR: " << e << std::endl;
	exit(-1);
      } 
    }
  }
  alarm(3600);  //From now on once per hour
}


/*****************************************/
void decodestat(StatisticsBlock *statblock)
/*****************************************/
{
  u_int loop;
  std::string text;
  
  if (statblock->statVersion != enum_statVersion)
  {
    std::cout << "statVersion = 0x" << HEX(statblock->statVersion) << std::endl;
    std::cout << "Sorry. I was expecting 0x" << HEX(enum_statVersion) << std::endl;
    std::cout << "serNum                = " << statblock->serNum << std::endl;
  }
  else
  {      
    std::cout << "statVersion           = 0x" << HEX(statblock->statVersion) << std::endl;
    std::cout << "endianFlag            = 0x" << HEX(statblock->endianFlag) << std::endl;
    std::cout << "statByteSize          = 0x" << HEX(statblock->statByteSize) << std::endl;
    std::cout << "serNum                = " << statblock->serNum << std::endl;
    std::cout << "swVersion             = 0x" << HEX(statblock->swVersion) << std::endl;
    std::cout << "cfgVersion            = 0x" << HEX(statblock->cfgVersion) << std::endl;
    std::cout << "cfgNumItems           = 0x" << HEX(statblock->cfgNumItems) << std::endl;
    std::cout << "designVersion         = 0x" << HEX(statblock->designVersion) << std::endl;
    std::cout << "fragFormat            = 0x" << HEX(statblock->fragFormat) << std::endl;
    std::cout << "bufByteSize           = 0x" << HEX(statblock->bufByteSize) << std::endl;
    std::cout << "numChannels           = 0x" << HEX(statblock->numChannels) << std::endl;
    std::cout << "cpuSpeed              = " << statblock->cpuSpeed << std::endl;
    std::cout << "robinModel            = 0x" << HEX(statblock->robinModel) << std::endl;
    std::cout << "appType               = 0x" << HEX(statblock->appType) << std::endl;
    std::cout << "switches              = 0x" << HEX(statblock->switches) << std::endl;
    std::cout << "cDate                 = " << statblock->cDate << std::endl;           
    std::cout << "cTime                 = " << statblock->cTime << std::endl; 
    // check, eventaully convert endianness
    SecSiInfo newSecSi, *secSiPtr;
    Dword *secSiSrc = (Dword*)&statblock->secSi,*secSiDest = (Dword*)&newSecSi;

    std::cout << "Byte swapping SECSI record " << std::endl;
    for (loop=0;loop<sizeof(SecSiInfo)/sizeof(Dword);loop++)	
      secSiDest[loop] = CONVERT_ENDIAN_32(secSiSrc[loop]);
    secSiPtr = &newSecSi;
    std::cout << "SECSI magic word      = 0x" << HEX(CONVERT_ENDIAN_32(secSiPtr->magic)) << std::endl;
    std::cout << "SECSI version         = " << CONVERT_ENDIAN_16(secSiPtr->secSiVersion) << std::endl;
 
    std::cout << "SECSI ATLAS Part ID   = ";
    std::cout << static_cast<u_int>(secSiPtr->id.project[0]); 
    std::cout << static_cast<u_int>(secSiPtr->id.project[1]) << "-"; 
    std::cout << static_cast<u_int>(secSiPtr->id.system[0]) << "-"; 
    std::cout << static_cast<u_int>(secSiPtr->id.subSystem[0]) << "-"; 
    std::cout << static_cast<u_int>(secSiPtr->id.institute[0]); 
    std::cout << static_cast<u_int>(secSiPtr->id.institute[1]); 
    std::cout << static_cast<u_int>(secSiPtr->id.institute[2]) << "-"; 
    std::cout << static_cast<u_int>(secSiPtr->id.prefix[0]) << "-"; 
    std::cout << static_cast<u_int>(secSiPtr->id.sernum[0]); 
    std::cout << static_cast<u_int>(secSiPtr->id.sernum[1]); 
    std::cout << static_cast<u_int>(secSiPtr->id.sernum[2]); 
    std::cout << static_cast<u_int>(secSiPtr->id.sernum[3]); 
    std::cout << static_cast<u_int>(secSiPtr->id.sernum[4]); 
    std::cout << static_cast<u_int>(secSiPtr->id.sernum[5]); 
    std::cout << std::endl;

    text ="SECSI prodDate        = ";
    outtext (text, &secSiPtr->prodDate[0], 8, 1);

    text ="SECSI schemDate       = ";
    outtext (text, &secSiPtr->schemDate[0], 8, 1);

    std::cout << "SECSI schemId         = " << CONVERT_ENDIAN_32(secSiPtr->schemId) << std::endl;

    text ="SECSI pcbName         = ";
    outtext (text, &secSiPtr->pcbName[0], 16,0);

    text ="SECSI pcbId           = ";
    outtext (text, &secSiPtr->pcbId[0], 16,0);

    std::cout << "SECSI gerberId        = " << CONVERT_ENDIAN_32(secSiPtr->gerberId) << std::endl;

    text ="SECSI asmName         = ";
    outtext (text, &secSiPtr->asmName[0], 16, 0);

    std::cout << "SECSI asmId           = " << CONVERT_ENDIAN_32(secSiPtr->asmId) << std::endl;
    std::cout << "SECSI bomVersion      = " << CONVERT_ENDIAN_32(secSiPtr->bomVersion) << std::endl;
    std::cout << "pagesFree             = 0x" << HEX(statblock->pagesFree) << std::endl;		
    std::cout << "pagesInUse            = 0x" << HEX(statblock->pagesInUse) << std::endl;	
    std::cout << "idleCount             = 0x" << HEX(statblock->idleCount) << std::endl;        
    std::cout << "errCntSize            = 0x" << HEX(statblock->errCntSize) << std::endl;			
    std::cout << "fragStatSize          = 0x" << HEX(statblock->fragStatSize) << std::endl;		
    std::cout << "pageStatSize          = 0x" << HEX(statblock->pageStatSize) << std::endl;	
    std::cout << "msgStatSize           = 0x" << HEX(statblock->msgStatSize) << std::endl;		
    std::cout << "histoSize             = 0x" << HEX(statblock->histoSize) << std::endl;		
    std::cout << "bistResult            = 0x" << HEX(statblock->bistResult) << std::endl;
    std::cout << "mostRecentId          = 0x" << HEX(statblock->mostRecentId) << std::endl;
    std::cout << "savedRejectedPages    = " << statblock->savedRejectedPages << std::endl;
    std::cout << "eventLog.logEnabled   = 0x" << HEX(statblock->eventLog.logEnabled) << std::endl;
    std::cout << "eventLog.logSize      = 0x" << HEX(statblock->eventLog.logSize) << std::endl;
    std::cout << "eventLog.current      = 0x" << HEX(statblock->eventLog.current) << std::endl;
    std::cout << "eventLog.overflow     = 0x" << HEX(statblock->eventLog.overflow) << std::endl;
    std::cout << "eventLog.eventArray   = 0x" << HEX(statblock->eventLog.eventArray) << std::endl;
    std::cout << "bufferFull            = 0x" << HEX(statblock->bufferFull) << std::endl;    
    std::cout << "rolXoffStat           = 0x" << HEX(statblock->rolXoffStat) << std::endl;    
    std::cout << "rolDownStat           = 0x" << HEX(statblock->rolDownStat) << std::endl;   
    std::cout << "tempOk                = " << statblock->tempOk << std::endl;

    for(loop = 0; loop < enum_LAST_ERRCNT; loop++)
      std::cout << "errors[" << std::setw(2) << loop << "](" << std::setw(54) << robinErrorStrings[loop] << ") = 0x" << HEX(statblock->errors[loop]) << std::endl;
    for(loop = 0; loop < enum_LAST_FRAGSTAT; loop++)
      std::cout << "fragStat[" << std::setw(2) << loop << "](" << std::setw(52) << robinFragStatStrings[loop] << ") = 0x" << HEX(statblock->fragStat[loop]) << std::endl;
    for(loop = 0; loop < enum_LAST_PAGESTAT; loop++)
      std::cout << "pageStat[" << std::setw(2) << loop << "](" << std::setw(52) << robinPageStatStrings[loop] << ") = 0x" << HEX(statblock->pageStat[loop]) << std::endl;
    for(loop = 0; loop < enum_LAST_MSGSTAT; loop++)
      std::cout << "msgStat[" << std::setw(2) << loop << "](" << std::setw(53) << robinMsgStatStrings[loop] << ") = 0x" << HEX(statblock->msgStat[loop]) << std::endl;
    for(loop = 0; loop < enum_histEntries; loop++)
      std::cout << "fragHisto[" << std::setw(2) << loop << "] = 0x" << HEX(statblock->fragHisto[loop]) << std::endl;
    for(loop = 0; loop < enum_histEntries; loop++)
      std::cout << "bufHisto[" << std::setw(2) << loop << "]  = 0x" << HEX(statblock->bufHisto[loop]) << std::endl;
  }
}

/***************************************************************/
void outtext(std::string text, Byte *data, u_int len, u_int mode)
/***************************************************************/
{
  u_int loop;

  std::cout << text;

  if (mode == 0) 
  { 
    for(loop = 0; loop < len; loop++)
      std::cout << data[loop];
  }
  
  if (mode == 1)
  {
    std::cout << static_cast<u_int>(data[0]);
    std::cout << static_cast<u_int>(data[1]);
    std::cout << static_cast<u_int>(data[2]);
    std::cout << static_cast<u_int>(data[3]) << "/";
    std::cout << static_cast<u_int>(data[4]);
    std::cout << static_cast<u_int>(data[5]) << "/";
    std::cout << static_cast<u_int>(data[6]);
    std::cout << static_cast<u_int>(data[7]);
  }
   
  std::cout << std::endl;
}


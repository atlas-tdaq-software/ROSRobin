/****************************/
/* file:   robinconfig.cpp  */
/*                          */
/* author: Matthias Mueller */
/****************************/

//To be done (according to Matthias):
// - Proper command line parameters!
// - Check the validity of the parameters
 
#ifndef HOST
  #define HOST
#endif

#include <iostream>
#include <stdlib.h>
#include <exception>

#include "DFDebug/DFDebug.h"
#include "ROSRobin/ROSRobinExceptions.h"
#include "ROSRobin/JTagController.h"
#include "ROSRobin/XilinxVirtexIIFpga.h"
#include "ROSRobin/XilinxCoolRunnerPld.h"
#include "ROSRobin/Robin.h"
#include "ROSRobin/Rol.h"

using namespace ROS;

void printUsage(void);

/*****************************/
int main(int argc, char **argv)
/*****************************/
{
  u_int timeout = 100, dlevel = 0, memoryOrder, boardNr;
  int templim;
  bool isProgrammed = false;
  
  std::cout << "This is robinconfig!" << std::endl;
  std::cout << "\t by M. Joos and M. Mueller (c) 2007." << std::endl;

  // Check the parameters
  if (argc != 5 && argc != 4 && argc != 3) 
  {
    printUsage();
    return(0);
  }

  // Copy the parameters to local variables
  boardNr     = atoi(argv[1]);
  memoryOrder = atoi(argv[2]);

  if (argc == 4)
    templim = atoi(argv[3]);
  else
    templim = 90; //this is about 40 deg. C
  if (templim > 127)
   templim = 127;
  if (templim < 0)
   templim = 0;

  std::cout << "Encoded temperature limit = " << templim << std::endl;
  
  if (argc == 5)
    dlevel = atoi(argv[4]);

  DF::GlobalDebugSettings::setup(dlevel, 14);
  
  try 
  {
    // Make a Robin object assigned to the requested board
    Robin *robinBoard;
    robinBoard = new Robin(boardNr, timeout);
    
    // Open it
    robinBoard->open();

    // Get a JTag controller for FPGA access via JTag
    JTagController jtag(*robinBoard);

    XilinxCoolRunnerPld * pldDevice;
    
    // If we see a Jtag chain of two devices, the first one is the CPLD!
    if (jtag.getNumberOfDevices() == 2) 
    {
      pldDevice = new XilinxCoolRunnerPld(jtag);
      jtag.appendToChain(pldDevice);
    }

    // Create an object which can deal with the JTag on the FPGA
    XilinxVirtexIIFpga * fpgaDevice = new XilinxVirtexIIFpga(jtag);

    // Append it to the JTag chain.
    jtag.appendToChain(fpgaDevice);

    // Get the Device ID of the FPGA
    std::cout << "FPGA has JTag ID: " << fpgaDevice->getDeviceId() << std::endl;    

    if (jtag.getNumberOfDevices() == 2) 
    {
      // and the one of the PLD too if present
      std::cout << "PLD has JTag ID : " << pldDevice->getDeviceId() << std::endl;
      pldDevice->setIR("BYPASS");
      pldDevice->setDR(BitStream("1"));
    }

    // Check if the device is programmed
    isProgrammed = fpgaDevice->isProgrammed();

    // Clean up JTag;
    jtag.removeFromChain(fpgaDevice);
    delete fpgaDevice;

    if (jtag.getNumberOfDevices() == 2) 
    {
      jtag.removeFromChain(pldDevice);
      delete pldDevice;
    }

   if (isProgrammed) 
   {
      std::cout << "FPGA firmware is loaded" << std::endl;
      // Set the DMA parameters
      robinBoard->setDmaRamParams(memoryOrder);    
    }
    else 
      std::cout << "FPGA firmware is NOT loaded!! No DMA memory allocated!" << std::endl;

    //Initialise the temperature watchdog
    robinBoard->reserveMsgResources(2, 0x20, 0x2000, 0); 
    Rol *prol = 0;
    prol = new Rol(*robinBoard, 0);    
    prol->setConfig(enum_cfgTempAlarmValue, templim);
    robinBoard->reset();
    delete prol;
    robinBoard->freeMsgResources(); 

    // Close it again
    robinBoard->close();
  }
  catch (std::exception &  ex) 
  {
    // Catches all exceptions inherit from std::exception
    // This includes all ROSExceptions
    std::cout << "An exception occured: " << std::endl;
    std::cout << ex.what() << std::endl;
  }
  
  catch (...) 
  {
    std::cout << "An unknown exception occured: " << std::endl;
  }

  return(0);
}


/*******************/
void printUsage(void) 
/*******************/
{
  std::cout << "Usage: " << std::endl;
  std::cout << "\t robinconfig <Board Number> <DMA MemoryOrder> [Temperature limit] [Debug level] " << std::endl;
}


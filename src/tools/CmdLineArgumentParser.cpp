// --------x----------------------------x-----------------------------------x--
/**
 ** \file   CmdLineArgumentParser.cpp
 **
 ** \author
 **
 ** \date
 **
 */
// --------x----------------------------x-----------------------------------x--

/**
 * Description:
 *    This is the implementation of the class CmdLineArgumentParser. An object of this 
 *    class parses the  main() command lines and enables a client to pick
 *    the arguments, parameters, switches, or whatever is given by the user.
 *    Supported argument formats:
 *        1) aProgram param1 param2 param3    ( Simple parameters )
 *        2) aProgram -s -adf --anotherSwitch ( Some switches with one char 
 *                                              introduced by a "-". These can be
 *                                              grouped together. Or switches by
 *						with many chars introduced by "--")
 *        3) aProgram -t 12 -l "a value"       ( named single char parameters 
 *                                               with values )
 *        4) aProgram --testsize=12            ( named parameters with values )
 *
 *   The above cases may be mixed except that the simple parameters in bullit 1)
 *   have to be at the end of the argument queue after all switches etc.
 *   
 *   Switches have to be added with addSwitch otherwise the parse method throws an
 *   exception. Contrary, if switches have been added but are not present on the
 *   command line, the exception will be thrown when calling testForSwitch(). This
 *   means that all switches are optional. The application must deal with
 *   mandantory switches.
 *
 *   The class simpley parses what is present on the command line and gives
 *   it to the user access through a set of messages. It is up to the user
 *   to check the integrety of parameters and print a usage message to the terminal.
 */


#include <string>
#include <vector>
#include <map>

#include <iostream>

#include "CmdLineArgumentParser.h"
#include "ROSRobin/Keyword.h"
#include "ROSRobin/ROSRobinExceptions.h"

/** The standard constructor.
 */
//*************************************************
CmdLineArgumentParser::CmdLineArgumentParser(void) :
//*************************************************
  hasBeenParsed(false)
{

}


/** Parses the parameters given from main(). If there are options used which have not
    been added by addSwitch this method throws an exception!
*/
//*******************************************************
void CmdLineArgumentParser::parse(int argc, char **argv)
//*******************************************************
{

  // Look if someone wants to add aswitch when the command line has been
  // already parsed
  if (hasBeenParsed) {
    throw CmdLineArgumentException("CmdLineArgumentParser::parse tries to re-parse the arguments.");
  }

  // Index for the command line arguments
  int argIdx;

  // A temp array holding the switches added by the user
  std::vector< std::string > givenSwitches = m_switches;
  m_switches.clear();

  // A temp array holding the switches with values added by the user
  std::vector< std::string > givenValueSwitches;

  // Pick the switches which expect a value from the map and put
  // them into a vector.
  std::map< std::string, std::string >::iterator iter;
  for (iter = m_values.begin(); iter != m_values.end(); iter++) {
    givenValueSwitches.push_back(iter->first);
  }

  m_values.clear();

  // Now run through the arguments. Remember: the first one is
  // the program name. So we start from one not from 0 here.
  argIdx = 1;
  while (argIdx < argc) {

    std::string arg(argv[argIdx]);

    // Look if this parameter is a switch
    if (arg[0] == '-') {
      std::string swString;
      
      // If there have been already some parameters which are not switches
      // we throw an exception here. We want to exclude things 
      // like: aProgram -s 2 abdce -t 5 since this is bad readable and
      // could be a lost switch with parameter. 
      if (m_params.size() != 0) {
	throw CmdLineArgumentException("CmdLineArgumentParser::parse: Parse error.");
      }

      // if the there is a second - we take the whole word as a switch
      if (arg[1] == '-') {
	swString.assign(arg, 2, arg.size() - 2);
	
	// Look if there is a parameter
	std::string::size_type equalPos = swString.find('=');
	if (equalPos != std::string::npos) {
	  // Then put the parameter / value pair into the m_values map
	  std::string value(swString, equalPos + 1, swString.size() - (equalPos + 1));
	  std::string key(swString, 0, equalPos);

	  if (givenValueSwitches.end() == find(givenValueSwitches.begin(), givenValueSwitches.end(), key)) {
	    // This switch has not been declared to take a value. Throw an exception
	    throw CmdLineArgumentException("CmdLineArgumentParser::parse: Unknown switch.");
	  }
	  // Everything is ok now. Store the switch and its value
	  m_values[key] = value;
	}
	else {
	  // Store only the key
	  m_switches.push_back(swString);
	}
      }
      else {
	// OK there is only one '-'. Store all letters now as single switches
	
	swString.assign(arg, 1, arg.size() - 1);
	
	for (std::string::size_type i = 0; i < swString.size(); i++) {
	  
	  std::string letter(swString, i, 1);
	  
	  std::cout << "testing switch: " << letter << std::endl;

	  // Look if it is contained in the given switches
	  if (givenSwitches.end() != find(givenSwitches.begin(), givenSwitches.end(), letter)) {
	    //Ok the we store it
	    m_switches.push_back(letter);
	  }
	  else {
	    // This switch has not been added before. So look in the switches array which contain
	    // a value. But before check if this is the last letter in the parameter
	    if (i == (swString.size() - 1)) {
	      if (givenValueSwitches.end() != find(givenValueSwitches.begin(), givenValueSwitches.end(), letter)) {
		// So we take the next cmd line parameter as value. This must not start with a '-'!
		argIdx++;

		if (argIdx >= argc) {
		  throw CmdLineArgumentException("CmdLineArgumentParser::parse: Missing argument value.");
		}

		arg = argv[argIdx];

		if (arg[0] == '-') {
		  throw CmdLineArgumentException("CmdLineArgumentParser::parse: Missing argument value.");
		}

		m_values[letter] = arg;

	      }
	      else {
		throw CmdLineArgumentException("CmdLineArgumentParser::parse: Unknown switch.");
	      }
	    }
	    else {
	      // Otherwise it can not have a parameter. So this is an error situation and we throw
	      // an exception
	      throw CmdLineArgumentException("CmdLineArgumentParser::parse: Unknown switch.");
	    }
	  } // of else
	  	  
	} // of for
	
      } // of else
      
    } // of if (arg[0] == '-')
    else {
      m_params.push_back(arg);
    }
    
    argIdx++;    

  } // of while (argIdx < argc)
  
  hasBeenParsed = true;

}

/** Addes a switch to the command line which should be recognised while parsing.
 */
//*********************************************************************
void CmdLineArgumentParser::addSwitch(std::string name, bool hasParam)
//*********************************************************************
{
  
  // Look if someone wants to add aswitch when the command line has been
  // already parsed
  if (hasBeenParsed) {
    throw CmdLineArgumentException("CmdLineArgumentParser::addSwitch: Trying to add a "
				   " switch but the cmd line parameters have been already parsed");
  }

  // Check if the switch has to be added takes a parameter
  if (hasParam) {

    // Look if this switch has already been defined to take no parameter!
    if (m_switches.end() != find(m_switches.begin(), m_switches.end(), name)) {
      throw CmdLineArgumentException("CmdLineArgumentParser::addSwitch: Switch has been already defined");
    }

    // If all is ok, add it to the map
    m_values[name] = "";
  }
  else {
    // Otherwise add it to the vector of switches, but only if it has not been
    // added yet

    // Look first if this switch has already been defined to take a parameter!
    if (m_values.end() != m_values.find(name)) {
      throw CmdLineArgumentException("CmdLineArgumentParser::addSwitch: Switch has been already defined");
    }
	
    // Look if the switch has been already defined
    if (m_switches.end() == find(m_switches.begin(), m_switches.end(), name)) {
      // If all is ok, add it to the vector
      m_switches.push_back(name);
    }
  }

}

//***************************************************
unsigned int CmdLineArgumentParser::nrOfParams(void) 
//***************************************************
{

  return m_params.size();

}


/** Gets a parameter as a string by its position (switches introduced with "-" 
    not included). The position parameter starts with 1.
*/
//***********************************************************************
std::string CmdLineArgumentParser::getParamString(unsigned int position)
//***********************************************************************
{
  // Check if the parameter is not available
  if (position >= m_params.size()) {
    throw CmdLineArgumentException("CmdLineArgumentParser::getParamString: Parameter not available");
    
  }

  return m_params[position];
}

/** Gets a parameter as an integer number by its position (switches introduced with "-" 
    not included). The position parameter starts with 1. If the parameter can not
    be converted to an integer an exception is thrown!
*/
//************************************************************
int CmdLineArgumentParser::getParamInt(unsigned int position)
//************************************************************
{

  std::string conv = getParamString(position);

  try {
    if (conv.substr(0,2) == "0x") {
      return hexStringToUInt(conv.substr(2, conv.size() - 2));
    }
  
    return decStringToUInt(conv);
  }
  catch (ROS::ROSRobinExceptions ex) {

    throw CmdLineArgumentException("Parameter is not a number");
  }
  
}


/** This method returns true if a switch string (a parameter starting with a "-" or a
    "--" can be found. This does not include switches with values (e.g. -s 12)
*/
//************************************************************
bool CmdLineArgumentParser::testForSwitch(std::string sw)
//************************************************************
{

  if (!hasBeenParsed) {
    throw CmdLineArgumentException("CmdLineArgumentParser::testForSwitch: Cmd line arguments "
				   "have not been parsed yet.");

  }

  if (m_switches.end() == find(m_switches.begin(), m_switches.end(), sw)) {
    return false;
  }

  return true;

}


/** This method returns a string parameter defined with a switch. 
 */
//**************************************************************************
std::string CmdLineArgumentParser::getSwitchParamString(std::string sw)
//**************************************************************************
{

  if (!hasBeenParsed) {
    throw CmdLineArgumentException("CmdLineArgumentParser::getSwitchParamString: Cmd line arguments "
				   "have not been parsed yet.");
  }

  std::map<std::string, std::string>::iterator iter;

  // Look for the switch
  iter = m_values.find(sw);
  
  if (iter == m_values.end()) {
    throw CmdLineArgumentException("CmdLineArgumentParser::getSwitchParamString: Could not find switch.");
  }

  return iter->second;

}


/** This method returns a integer parameter defined with a switch. 
 */
//***************************************************************
int CmdLineArgumentParser::getSwitchParamInt(std::string sw)
//***************************************************************
{

  try {
    std::string conv = getSwitchParamString(sw);
    
    if (conv.substr(0,2) == "0x") {
      return hexStringToUInt(conv.substr(2, conv.size() - 2));
    }
    
    return decStringToUInt(conv);
  }
  catch (ROS::ROSRobinExceptions ex) {

    throw CmdLineArgumentException("Parameter is not a number");
  }

}


/** The Exception! 
 */
//******************************************************************
CmdLineArgumentException::CmdLineArgumentException(std::string msg) :
//******************************************************************
m_msg("")
{
  m_msg = "Command Line Parser Exception! " + msg;
}


//****************************************************
CmdLineArgumentException::~CmdLineArgumentException() throw ()
//****************************************************
{

}


//**********************************************************  
const char * CmdLineArgumentException::what() const throw ()
//**********************************************************
{
  
  return m_msg.c_str();

}

// --------x----------------------------x-----------------------------------x--
/**
 ** \file   CmdLineArgumentParser.h
 **
 ** \author
 **
 ** \date
 **
 */
// --------x----------------------------x-----------------------------------x--

/**
 * Description:
 *    This is the definition of the class CmdLineArgumentParser. An object of this 
 *    class parses the  main() command lines and enables a client to pick
 *    the arguments, parameters, switches, or whatever is given by the user.
 *    Supported argument formats:
 *        1) aProgram param1 param2 param3    ( Simple parameters )
 *        2) aProgram -s -adf --anotherSwitch ( Some switches with one char 
 *                                              introduced by a "-". These can be
 *                                              grouped together. Or switches by
 *						with many chars introduced by "--")
 *        3) aProgram -t 12 -l "a value"       ( named single char parameters 
 *                                               with values )
 *        4) aProgram --testsize=12            ( named parameters with values )
 *
 *   The above cases may be mixed except that the simple parameters in bullit 1)
 *   have to be at the end of the argument queue after all switches etc.
 *
 *   Switches have to be added with addSwitch otherwise the parse method throws an
 *   exception. Contrary, if switches have been added but are not present on the
 *   command line, the exception will be thrown when calling testForSwitch(). This
 *   means that all switches are optional. The application must deal with
 *   mandantory switches.
 *
 *
 *   The class simpley parses what is present on the command line and gives
 *   it to the user access through a set of messages. It is up to the user
 *   to check the integrety of parameters and print a usage message to the terminal.
 */

#ifndef CMDLINEARGUMENTPARSER_H
#define CMDLINEARGUMENTPARSER_H

#include <vector>
#include <string>
#include <map>
#include <exception>

class CmdLineArgumentParser {

 public:

  /** The standard constructor.
  */
  CmdLineArgumentParser(void);

  /** Parses the parameters given from main(). If there are options used which have not
      been added by addSwitch this method throws an exception!
   */
  void parse(int argc, char **argv);

  /** Addes a switch to the command line which should be recognised while parsing.
   */
  void addSwitch(std::string name, bool hasParam);

  /** Returns the number of parameters
   */
  unsigned int nrOfParams(void);

  /** Gets a parameter as a string by its position (switches introduced with "-" 
      not included). 
   */
  std::string getParamString(unsigned int position);

  /** Gets a parameter as an integer number by its position (switches introduced with "-" 
      not included). If the parameter can not
      be converted to an integer an exception is thrown!
   */
  int getParamInt(unsigned int position);

  /** This method returns true if a switch string (a parameter starting with a "-" or a
      "--" can be found. This does not include switches with values (e.g. -s 12)
  */
  bool testForSwitch(std::string sw);

  /** This method returns a string parameter defined with a switch. 
   */
  std::string getSwitchParamString(std::string sw);

  /** This method returns a integer parameter defined with a switch. 
   */
  int getSwitchParamInt(std::string sw);

 protected:

 private:

  std::vector< std::string > m_params;    // the array holding the parameters 
                                          // (param1 param2 ...)

  std::vector< std::string > m_switches;  // the array holding the switches 
                                          // (-s --search)

  // The map holding a pair of switches with values
  // (-t 12 --testsize=124)
  std::map<std::string, std::string> m_values;
  
  bool hasBeenParsed;

};



/** A exception class for CmdLineArgumentParser
 */
class CmdLineArgumentException : public std::exception {

 public:
  
  CmdLineArgumentException(std::string msg);
  ~CmdLineArgumentException(void) throw ();
  
  virtual const char * what() const throw ();

 private:
  std::string m_msg;

};


#endif

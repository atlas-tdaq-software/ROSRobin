// --------x----------------------------x-----------------------------------x--
/** 
 *  \file   FlashProgrammer.h
 *
 *  \author M.Mueller (mmueller@ti.uni-mannheim.de)
 *  
 *  \date   27.01.2005
 *
 *  
 *
 */
// --------x----------------------------x-----------------------------------x--

/*
 * $Id$
 * $Revision$
 * $Log$
 * Revision 1.1  2005/06/28 14:41:29  mmueller
 * Added ppcflash functionality.
 *
 *
 *
 */

/** This file defines the class FlashProgrammer for read, write and erase accesses 
    to the ROBIN 
    PowerPC Flash through a special FPGA design. This special FPGA design
    has to be programmed in advance and has to match to the version expected by
    this class.
 */

// --------x----------------------------x-----------------------------------x--

#ifndef PPCFLASHPROGRAMMER_H
#define PPCFLASHPROGRAMMER_H

// --------x----------------------------x-----------------------------------x--


// --------x----------------------------x-----------------------------------x--


namespace ROS {

 class FlashProgrammerException : public ROSRobinExceptions {
  public:
    FlashProgrammerException(std::string message);
    virtual ~FlashProgrammerException(void) throw();

  protected:
    virtual std::string getErrorString(unsigned int errorId) const;   

    std::string m_message;
  };


  class Robin;
  
  /** This class allowes the usage of a special FPGA design for PPC flash 
      programming on either the ROBIN prototype board (2 ROL version) or the 
      3-link ROBIN. An object is constructed with a reference on one of these 
      boards. A reference on the abstract base class RobInBoard is not sufficient 
      since the read and write methods to the flash devices differ! After object 
      construction the PPC flash can be accessed with read write and erase operations. 
      Read and write occures byte-wise, erase sectore-wise.
  */
  
  class FlashProgrammer {
  
 public:
  
  /** This first constructor creates an FlashProgrammer object 
      with a reference on a ROBIN board.
      @param robinBoard A reference on an object representing a ROBIN
      board.
  */
  FlashProgrammer(Robin &  robinBoard);
  
  /** This destructor closes the ROBIN board */
  ~FlashProgrammer(void);

  void reset(void);
  
  
  /** Writes a data byte to a specific address in the PPC flash.
      PPC flash addresses start at 0xffe00000 equal to the appearence
      in the PowerPC U-Boot monitor.
      @param address The target address for the write operation.
      @param data The byte value to write.
  */
  void write(unsigned int address, unsigned char data);
  
  /** Writes a whole bunch of data bytes to a specific address in the 
      PPC flash. PPC flash addresses start at 0xff800000 equal to the 
      appearence in the PowerPC U-Boot monitor.
      @param address The target address for the write operation.
      @param data An array of byte values for writing.
      @param size The number of values to write.
  */
  void write(unsigned int address, unsigned char *data, unsigned int size);
  
  /** Reads a byte value from the PPC flash.
      @param address The target address for the read operation.
      @return The requested byte from the flash.
  */
  unsigned char read(unsigned int address);

  /** Reads a bunch of byte values from the PPC flash.
      @param address The target address for the read operation.
      @param data An array of byte values arrived from the PPC flash.
                  The memory must be already allocated!
      @param size The number of values to read.
  */
  void read(unsigned int address, unsigned char *data, unsigned int size);

  /** Erases a sector of the PPC flash.
   */
  void erase(unsigned int sectorAddress);

  /** Erases the complete PPC flash. Every byte! Use with care!!!
   */
  void eraseAll(void);

 public:

  // The design ID constants
  static const unsigned int c_flashDesignId_ROBIN1_1500;
  static const unsigned int c_flashDesignId_ROBIN1_2000;
  static const unsigned int c_flashDesignId_ROBIN2_2000;

  // FPGA addresses
  static const unsigned int c_designIdRegister;
  static const unsigned int c_controlRegister;
  static const unsigned int c_addressRegister;
  static const unsigned int c_dataOutRegister;
  static const unsigned int c_dataInRegister;

  // Flash dedicated constants
  static const unsigned int c_flashOffset;
  static const unsigned int c_sectorSize_robin1;
  static const unsigned int c_numSectors_robin1;
  static const unsigned int c_sectorSize_robin2;
  static const unsigned int c_numSectors_robin2;

  
  // define control register bits
  // use lower 16 bits only
  static const unsigned int c_ctlFCS;
  static const unsigned int c_ctlOE;
  static const unsigned int c_ctlBE0;
  static const unsigned int c_ctlBE1;
  static const unsigned int c_ctlLEDBase;
  static const unsigned int c_ctlLastCTLBit;	// 4 user leds

  static const unsigned int c_statHoldAck;
  unsigned int cmdBaseAddress;
 

 protected:
  
  enum BoardType {
    ROBIN1_BOARD,
    ROBIN2_BOARD,
    UNKNOWN
  };
  
  ROS::Robin &  m_robin;
  enum BoardType m_boardType;
  
  unsigned int m_timeout;
  
  };

}

#endif


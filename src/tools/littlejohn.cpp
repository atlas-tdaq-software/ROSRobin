/***************************************************************************/
/* file:   littlejohn.cpp                                                  */
/* author: Markus Joos, CERN-PH/ESS                                        */
/*                                                                         */
/****** C 2008 - Ecosoft. Made from at least 80% recycled source code ******/


#ifndef HOST
  #define HOST
#endif

#include <iostream>
#include <string>
#include <iomanip>
#include <getopt.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <errno.h>
#include "ROSEventFragment/ROBFragment.h"
#include "ROSRobin/ROSRobinExceptions.h"
#include "ROSRobin/Robin.h"
#include "ROSRobin/Rol.h"
#include "ROSMemoryPool/MemoryPage.h"
#include "ROSMemoryPool/WrapperMemoryPool.h"
#include "ROSGetInput/get_input.h"
#include "DFDebug/DFDebug.h"
#include "ROSRobin/robin.h"
#include "ROSRobin/robin_statistics_map.h"
#include "rcc_time_stamp/tstamp.h"
#include <cmdl/cmdargs.h>

using namespace ROS;

/**********/    
/* Macros */
/**********/    
#define CONVERT_ENDIAN_16(X) ((Word)(((X) >> 8) + ((X) << 8)))
#define CONVERT_ENDIAN_32(X) ((Dword)( (((X) & 0xff000000) >> 24) | (((X) & 0x00ff0000) >> 8) | (((X) & 0x0000ff00) << 8) | (((X) & 0x000000ff) << 24)))

    
/***********/
/* Globals */
/***********/
u_int cont = 0, dblevel = 5, dbpackage = DFDB_ROSROBIN;


/**************/
/* Prototypes */
/**************/
void usage(void);
void printstat(u_int robin, u_int rol);
void decodestat(StatisticsBlock *statblock);
void printcfg(u_int robin, u_int rol);
void decodecfg(std::vector <CfgParm> cfg);
void outtext(std::string text, Byte *data, u_int len, u_int mode);
void dump_inmate(Rol *prol);


/**************/
void usage(void)
/**************/
{
  std::cout << "Valid options are ..." << std::endl;
  std::cout << "-m <robin>: Define the number of the Robin (0..N-1) Default = 0" << std::endl;
  std::cout << "-l <rol>:   Define the number of the ROL (0, 1 or 2) Default = 0" << std::endl;
  std::cout << "-g <L1ID>:  Request the event fragment with the given L1ID" << std::endl;
  std::cout << "-s:         Print the statistics block of the selected ROL" << std::endl;
  std::cout << "-c:         Print the configuration parameters of the selected ROL" << std::endl;
  std::cout << "-e:         Print the L1ID of all events that are currently in the buffer of the selected ROL" << std::endl;
  std::cout << "-p:         Print the status of the driver" << std::endl;
  std::cout << "-r:         Reset the ROL and the associated buffers in the Robin" << std::endl;
  std::cout << "-R:         Reset the S/W running on the Robin" << std::endl;
  std::cout << "-P:         Just check if the Robin responds to a message (ping)" << std::endl;
  std::cout << std::endl;
}


/*****************************/
int main(int argc, char **argv)
/*****************************/
{
  int c, rol_nr = 0, robin_nr = 0, mode_stat = 0, mode_ppcreset = 0, mode_reset = 0;
  int mode_proc = 0, mode_cfg = 0, mode_ping = 0, mode_evdump = 0, mode_get = 0, ev_nr = 0;

  std::cout << "This is littlejohn (the best friend of Robin)" << std::endl;

  while ((c = getopt(argc, argv, "m:l:hscerpPRg:")) != -1)
    switch (c) 
    {
      case 'm': robin_nr = atoi(optarg);                           break;                   
      case 'l': rol_nr = atoi(optarg);                             break;                   
      case 'g': mode_get = 1; ev_nr = atoi(optarg);                break;                   
      case 'h': usage(); exit(0);                                  break;                  
      case 's': mode_stat = 1;                                     break;              
      case 'c': mode_cfg = 1;                                      break;                
      case 'r': mode_reset = 1;                                    break;                
      case 'R': mode_ppcreset = 1;                                 break;                
      case 'P': mode_ping = 1;                                     break;                
      case 'e': mode_evdump = 1;                                   break;                
      case 'p': mode_proc = 1;                                     break;                

      default:
	std::cout << "Invalid option " << c << std::endl;
	std::cout << "Usage: " << argv[0] << " [options]: " << std::endl;
	usage();
	exit (-1);
    }
 
  if (mode_get)
  {
    WrapperMemoryPool *m_memoryPool;
    float timeout = 100.0;
    u_int *fptr, numberOfOutstandingReq = 1, msgInputMemorySize = 0x20, miscSize = 0x2000, eventSize = 0x10000;  //MJ: hard wire max. event size?
    u_int npages = 1;
    Rol *prol = 0;
    Robin *rorobin;

    try
    {
      rorobin = new Robin(robin_nr, timeout);
      rorobin->open();
      rorobin->reserveMsgResources(numberOfOutstandingReq, msgInputMemorySize, miscSize, eventSize); 
      // Create a memoryPool structure around the event buffer
      u_long eventBase       = rorobin->getVirtEventBase();
      u_int physicalAddress = rorobin->getPhysEventBase();
      m_memoryPool = new WrapperMemoryPool(npages, eventSize, eventBase, physicalAddress);

      prol = new Rol(*rorobin, rol_nr);
      std::cout << "Requesting L1ID 0x" << HEX(ev_nr) << " from Robin " << robin_nr << ", Rol " << rol_nr << std::endl;

      MemoryPage *mem_page = m_memoryPool->getPage();
      if (mem_page == 0) 
      {						
    	  std::cout << " Failed to get a page from the memory pool" << std::endl;
    	  exit(-1);
      }

      u_int *fragBufferAddress = static_cast<u_int *> (mem_page->reserve(mem_page->capacity()));
      u_long ticket = prol->requestFragment(ev_nr, reinterpret_cast<u_long> (fragBufferAddress));
      fptr = prol->getFragment(ticket);      
      ROBFragment *robFragment = new ROBFragment(mem_page);
      if(fptr[0] == 0xFFFFFFFF){


          std::cout << "ERROR: Corrupted Event header recieved! Dumping first 10 words ....." << std::endl;
          for (u_int loop = 0; loop < 10; loop++)
            std::cout << "Word " << loop << " = 0x" << HEX(fptr[loop]) << std::endl;


      }
      else{
    	  std::cout << "Event recieved. Dumping raw data....." << std::endl;
    	  for (u_int loop = 0; loop < fptr[1]; loop++)
    		  std::cout << "Word " << loop << " = 0x" << HEX(fptr[loop]) << std::endl;

    	  robFragment->print();
    	  robFragment->check(ev_nr, 0);
      }
      delete robFragment;
      mem_page->free();
    }
    catch (ROSRobinExceptions& e)
    {
      std::cout << e << std::endl;
      exit(-1);
    }

    delete prol;
    delete m_memoryPool;
    rorobin->freeMsgResources(); 
    rorobin->close();
    delete rorobin;
  }

  if (mode_ping)
  {
    float timeout = 100.0;
    u_int numberOfOutstandingReq = 1, msgInputMemorySize = 0x20, miscSize = 0x2000, eventSize = 0;
    Rol *prol = 0;
    Robin *rorobin;

    try
    {
      rorobin = new Robin(robin_nr, timeout);
      rorobin->open();
      rorobin->reserveMsgResources(numberOfOutstandingReq, msgInputMemorySize, miscSize, eventSize); 
      prol = new Rol(*rorobin, rol_nr);
      prol->ping();
    }
    catch (ROSRobinExceptions& e)
    {
      std::cout << e << std::endl;
      exit(-1);
    }

    std::cout << "The ROBIN is alive" << std::endl;
    delete prol;
    rorobin->freeMsgResources(); 
    rorobin->close();
    delete rorobin;
  }

  if (mode_proc)
  {
    float timeout = 100.0;
    u_int numberOfOutstandingReq = 1, msgInputMemorySize = 0x20, miscSize = 0x2000, eventSize = 0;
    Robin *rorobin;

    try
    {
      rorobin = new Robin(robin_nr, timeout);
      rorobin->open();
      rorobin->reserveMsgResources(numberOfOutstandingReq, msgInputMemorySize, miscSize, eventSize);
      rorobin->getProcInfo(); 
    }
    catch (ROSRobinExceptions& e)
    {
      std::cout << e << std::endl;
      exit(-1);
    }

    rorobin->freeMsgResources(); 
    rorobin->close();
    delete rorobin;
  }

  if (mode_evdump)
  {
    std::vector<Rol::L1idRange> idlist;
    float timeout = 100.0;
    u_int numberOfOutstandingReq = 1, msgInputMemorySize = 0x20, miscSize = 0x2000, eventSize = 0;
    Rol *prol = 0;
    Robin *rorobin;

    try
    {
      rorobin = new Robin(robin_nr, timeout);
      rorobin->open();
      rorobin->reserveMsgResources(numberOfOutstandingReq, msgInputMemorySize, miscSize, eventSize); 
      prol = new Rol(*rorobin, rol_nr);
      idlist = prol->getL1idList();
    }
    catch (ROSRobinExceptions& e)
    {
      std::cout << e << std::endl;
      exit(-1);
    }

    u_int totnum = 0;
    for (std::vector<Rol::L1idRange>::const_iterator it2 = idlist.begin(); it2 != idlist.end(); it2++)
    {
      Rol::L1idRange pair = *it2;
      if (pair.first == pair.last)
      {
        std::cout << "L1ID = 0x" << HEX(pair.first) << std::endl;
  	if (pair.first != 0xffffffff)
	  totnum++;
      }
      else
      {
        std::cout << "First L1ID = 0x" << HEX(pair.first) << "  Last L1ID = 0x" << HEX(pair.last) << "  ( " << pair.last - pair.first + 1 << " events)" << std::endl;
	totnum += pair.last - pair.first + 1;
      }
    }
    std::cout << "There are currently " << totnum << " event in memory" << std::endl;
  }
  
  if (mode_stat)
    printstat(robin_nr, rol_nr);

  if (mode_cfg)
    printcfg(robin_nr, rol_nr);

  if (mode_ppcreset)
  {
    float timeout = 100.0;
    u_int numberOfOutstandingReq = 1, msgInputMemorySize = 0x20, miscSize = 0x2000, eventSize = 0;
    Robin *rorobin;

    try
    {
      rorobin = new Robin(robin_nr, timeout);
      rorobin->open();
      rorobin->reserveMsgResources(numberOfOutstandingReq, msgInputMemorySize, miscSize, eventSize); 
      rorobin->reset(); 
    }
    catch (ROSRobinExceptions& e)
    {
      std::cout << e << std::endl;
      exit(-1);
    }

    rorobin->freeMsgResources(); 
    rorobin->close();
    delete rorobin;
    std::cout << "The PowerPC has beeen restarted" << std::endl;
  }

  if (mode_reset)
  {
    float timeout = 100.0;
    u_int numberOfOutstandingReq = 1, msgInputMemorySize = 0x20, miscSize = 0x2000, eventSize = 0;
    Rol *prol = 0;
    Robin *rorobin;

    try
    {
      rorobin = new Robin(robin_nr, timeout);
      rorobin->open();
      rorobin->reserveMsgResources(numberOfOutstandingReq, msgInputMemorySize, miscSize, eventSize); 
      prol = new Rol(*rorobin, rol_nr);
      prol->reset();
    }
    catch (ROSRobinExceptions& e)
    {
      std::cout << e << std::endl;
      exit(-1);
    }

    delete prol;
    rorobin->freeMsgResources(); 
    rorobin->close();
    delete rorobin;
    std::cout << "The S-link has beeen reset" << std::endl;
  }
  
  return(0);
}

  

/************************************/
void printstat(u_int robin, u_int rol)
/************************************/
{
  u_int temp, timeout, numberOfOutstandingReq, msgInputMemorySize, miscSize, eventSize;
  Rol *prol = 0;
  Robin *rorobin;
  StatisticsBlock statblock;
  
  std::cout << "Dumping statistics for ROL " << rol << " on Robin " << robin << std::endl;
  
  timeout                = 100;
  numberOfOutstandingReq = 1;
  msgInputMemorySize     = 0x20;
  miscSize               = 0x2000;
  eventSize              = 0; 
    
  try
  {
    rorobin = new Robin(robin, timeout);
    rorobin->open();
    rorobin->reserveMsgResources(numberOfOutstandingReq, msgInputMemorySize, miscSize, eventSize); 


    prol = new Rol(*rorobin, rol);
    statblock = prol->getStatistics();

    decodestat(&statblock);

    temp = prol->getTemperature();
    std::cout << "Approximative Robin temperature [deg. C]: " << temp << std::endl;
  }
  catch (ROSRobinExceptions& e)
  {
    std::cout << e << std::endl;
    exit(-1);
  }
  delete prol;
  rorobin->freeMsgResources(); 
  rorobin->close();
  delete rorobin;
}


/***********************************/
void printcfg(u_int robin, u_int rol)
/***********************************/
{
  u_int timeout, numberOfOutstandingReq, msgInputMemorySize, miscSize, eventSize;
  Rol *prol = 0;
  Robin *rorobin;
  std::vector <CfgParm> cfg;
  
  std::cout << "Dumping configuration parameters of ROL " << rol << " on Robin " << robin << std::endl;
  
  timeout                = 100;
  numberOfOutstandingReq = 1;
  msgInputMemorySize     = 0x20;
  miscSize               = 0x2000;
  eventSize              = 0; 
    
  try
  {
    rorobin = new Robin(robin, timeout);
    rorobin->open();
    rorobin->reserveMsgResources(numberOfOutstandingReq, msgInputMemorySize, miscSize, eventSize); 


    prol = new Rol(*rorobin, rol);
    cfg = prol->getConfig();
    decodecfg(cfg);
  }
  catch (ROSRobinExceptions& e)
  {
    std::cout << e << std::endl;
    exit(-1);
  }
  delete prol;
  rorobin->freeMsgResources(); 
  rorobin->close();
  delete rorobin;
}


/***************************************/
void decodecfg(std::vector <CfgParm> cfg)
/***************************************/
{
  u_int somebad = 0;

  std::cout << "Abbreviations:" << std::endl;
  std::cout << "#  = Number of the configuration parameter" << std::endl;
  std::cout << "Ex = 1: Expert parameter       0: Standard parameter" << std::endl;
  std::cout << "Dy = 1: Dynamic parameter      0: Static parameter" << std::endl;
  std::cout << "Bo = 1: Boolean parameter      0: Integer parameter" << std::endl;
  std::cout << "Gl = 1: ROBIN-wide parameter   0: ROL-specific parameter" << std::endl;
  std::cout << "Fi = 1: Fixed value parameter  0: Modifyable parameter" << std::endl;
  std::cout << "In = 1: Requires reinitialisation of buffer management (erase data)" << std::endl;
  std::cout << std::endl;
  std::cout << "           Name|  #| Ex| Dy| Bo| Gl| Fi| In|      Value|    Default|        Min|        Max|" << std::endl;
  std::cout << "===============|===|===|===|===|===|===|===|===========|===========|===========|===========|" << std::endl;

  for (u_int loop = 0; loop < enum_cfgNumItems; loop++)
  {
     std::cout << std::setw(15) << cfg[loop].name << "|"
               << std::setw(3)  << loop << "|"
	       << std::setw(3)  << static_cast<int>(cfg[loop].expert) << "|"
	       << std::setw(3)  << static_cast<int>(cfg[loop].dynamic) << "|"
	       << std::setw(3)  << static_cast<int>(cfg[loop].isBool) << "|"
	       << std::setw(3)  << static_cast<int>(cfg[loop].global) << "|"
	       << std::setw(3)  << static_cast<int>(cfg[loop].fixed) << "|"
	       << std::setw(3)  << static_cast<int>(cfg[loop].init) << "|"
    	       << std::setw(11) << cfg[loop].value << "|"  
    	       << std::setw(11) << cfg[loop].defVal << "|" 
    	       << std::setw(11) << cfg[loop].minVal << "|"
    	       << std::setw(11) << cfg[loop].maxVal << "|" << std::endl;

    u_int dw = cfg[loop].defVal;
    u_int cw = cfg[loop].value;
    u_int ex = static_cast<int>(cfg[loop].expert);
    u_int fi = static_cast<int>(cfg[loop].fixed);

    if((loop != enum_cfgTempAlarmValue) && (cw != dw) && ex && !fi)
      somebad = 1;
  }

  if (somebad)
  {
    std::cout << std::endl;
    std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
    std::cout << "Some Expert parameters do not have the default value." << std::endl;
    std::cout << "You may want to check this!" << std::endl;

    for (u_int loop = 0; loop < enum_cfgNumItems; loop++)
    {
      u_int dw = cfg[loop].defVal;
      u_int cw = cfg[loop].value;
      u_int ex = static_cast<int>(cfg[loop].expert);
      u_int fi = static_cast<int>(cfg[loop].fixed);
    
      if ((loop != enum_cfgTempAlarmValue) && !fi && (cw != dw) && ex)
        std::cout << "Parameter " << std::setw(15) << cfg[loop].name << " is " << cfg[loop].value << " instead of " << cfg[loop].defVal << std::endl;
    }
    std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
  }  
}


/**********************************************************/
void outtext(std::string text, Byte *data, u_int len, u_int mode)
/**********************************************************/
{
  u_int loop;

  std::cout << text;

  if (mode == 0) 
  { 
    for(loop = 0; loop < len; loop++)
      std::cout << data[loop];
  }
  
  if (mode == 1)
  {
    std::cout << static_cast<u_int>(data[0]);
    std::cout << static_cast<u_int>(data[1]);
    std::cout << static_cast<u_int>(data[2]);
    std::cout << static_cast<u_int>(data[3]) << "/";
    std::cout << static_cast<u_int>(data[4]);
    std::cout << static_cast<u_int>(data[5]) << "/";
    std::cout << static_cast<u_int>(data[6]);
    std::cout << static_cast<u_int>(data[7]);
  }
   
  std::cout << std::endl;
}


/*****************************************/
void decodestat(StatisticsBlock *statblock)
/*****************************************/
{
  u_int loop;
  std::string text;
  
  if (statblock->statVersion != enum_statVersion)
  {
    std::cout << "statVersion = 0x" << HEX(statblock->statVersion) << std::endl;
    std::cout << "Sorry. I was expecting 0x" << HEX(enum_statVersion) << std::endl;
    std::cout << "serNum                = " << statblock->serNum << std::endl;
  }
  else
  {      
    std::cout << "statVersion           = 0x" << HEX(statblock->statVersion) << std::endl;
    std::cout << "endianFlag            = 0x" << HEX(statblock->endianFlag) << std::endl;
    std::cout << "statByteSize          = 0x" << HEX(statblock->statByteSize) << std::endl;
    std::cout << "serNum                = " << statblock->serNum << std::endl;
    std::cout << "swVersion             = 0x" << HEX(statblock->swVersion) << std::endl;
    std::cout << "cfgVersion            = 0x" << HEX(statblock->cfgVersion) << std::endl;
    std::cout << "cfgNumItems           = 0x" << HEX(statblock->cfgNumItems) << std::endl;
    std::cout << "designVersion         = 0x" << HEX(statblock->designVersion) << std::endl;
    std::cout << "fragFormat            = 0x" << HEX(statblock->fragFormat) << std::endl;
    std::cout << "bufByteSize           = 0x" << HEX(statblock->bufByteSize) << std::endl;
    std::cout << "numChannels           = 0x" << HEX(statblock->numChannels) << std::endl;
    std::cout << "cpuSpeed              = " << statblock->cpuSpeed << std::endl;
    std::cout << "robinModel            = 0x" << HEX(statblock->robinModel) << std::endl;
    std::cout << "appType               = 0x" << HEX(statblock->appType) << std::endl;
    std::cout << "switches              = 0x" << HEX(statblock->switches) << std::endl;
    std::cout << "cDate                 = " << statblock->cDate << std::endl;           
    std::cout << "cTime                 = " << statblock->cTime << std::endl; 
    
    // check, eventaully convert endianness
    SecSiInfo newSecSi, *secSiPtr;
    Dword *secSiSrc = (Dword*)&statblock->secSi, *secSiDest = (Dword*)&newSecSi;

    for (loop = 0; loop < sizeof(SecSiInfo) / sizeof(Dword);loop++)	
      secSiDest[loop] = CONVERT_ENDIAN_32(secSiSrc[loop]);
    secSiPtr = &newSecSi;
    std::cout << "SECSI magic word      = 0x" << HEX(CONVERT_ENDIAN_32(secSiPtr->magic)) << std::endl;
    std::cout << "SECSI version         = " << CONVERT_ENDIAN_16(secSiPtr->secSiVersion) << std::endl;
 
    std::cout << "SECSI ATLAS Part ID   = ";
    std::cout << static_cast<u_int>(secSiPtr->id.project[0]); 
    std::cout << static_cast<u_int>(secSiPtr->id.project[1]) << "-"; 
    std::cout << static_cast<u_int>(secSiPtr->id.system[0]) << "-"; 
    std::cout << static_cast<u_int>(secSiPtr->id.subSystem[0]) << "-"; 
    std::cout << static_cast<u_int>(secSiPtr->id.institute[0]); 
    std::cout << static_cast<u_int>(secSiPtr->id.institute[1]); 
    std::cout << static_cast<u_int>(secSiPtr->id.institute[2]) << "-"; 
    std::cout << static_cast<u_int>(secSiPtr->id.prefix[0]) << "-"; 
    std::cout << static_cast<u_int>(secSiPtr->id.sernum[0]); 
    std::cout << static_cast<u_int>(secSiPtr->id.sernum[1]); 
    std::cout << static_cast<u_int>(secSiPtr->id.sernum[2]); 
    std::cout << static_cast<u_int>(secSiPtr->id.sernum[3]); 
    std::cout << static_cast<u_int>(secSiPtr->id.sernum[4]); 
    std::cout << static_cast<u_int>(secSiPtr->id.sernum[5]); 
    std::cout << std::endl;

    text ="SECSI prodDate        = ";
    outtext (text, &secSiPtr->prodDate[0], 8, 1);

    text ="SECSI schemDate       = ";
    outtext (text, &secSiPtr->schemDate[0], 8, 1);

    std::cout << "SECSI schemId         = " << CONVERT_ENDIAN_32(secSiPtr->schemId) << std::endl;

    text ="SECSI pcbName         = ";
    outtext (text, &secSiPtr->pcbName[0], 16,0);

    text ="SECSI pcbId           = ";
    outtext (text, &secSiPtr->pcbId[0], 16,0);

    std::cout << "SECSI gerberId        = " << CONVERT_ENDIAN_32(secSiPtr->gerberId) << std::endl;

    text ="SECSI asmName         = ";
    outtext (text, &secSiPtr->asmName[0], 16, 0);

    std::cout << "SECSI asmId           = " << CONVERT_ENDIAN_32(secSiPtr->asmId) << std::endl;
    std::cout << "SECSI bomVersion      = " << CONVERT_ENDIAN_32(secSiPtr->bomVersion) << std::endl;
    std::cout << "pagesFree             = 0x" << HEX(statblock->pagesFree) << std::endl;		
    std::cout << "pagesInUse            = 0x" << HEX(statblock->pagesInUse) << std::endl;	
    std::cout << "idleCount             = 0x" << HEX(statblock->idleCount) << std::endl;        
    std::cout << "errCntSize            = 0x" << HEX(statblock->errCntSize) << std::endl;			
    std::cout << "fragStatSize          = 0x" << HEX(statblock->fragStatSize) << std::endl;		
    std::cout << "pageStatSize          = 0x" << HEX(statblock->pageStatSize) << std::endl;	
    std::cout << "msgStatSize           = 0x" << HEX(statblock->msgStatSize) << std::endl;		
    std::cout << "histoSize             = 0x" << HEX(statblock->histoSize) << std::endl;		
    std::cout << "bistResult            = 0x" << HEX(statblock->bistResult) << std::endl;
    std::cout << "mostRecentId          = 0x" << HEX(statblock->mostRecentId) << std::endl;
    std::cout << "savedRejectedPages    = " << statblock->savedRejectedPages << std::endl;
    std::cout << "eventLog.logEnabled   = 0x" << HEX(statblock->eventLog.logEnabled) << std::endl;
    std::cout << "eventLog.logSize      = 0x" << HEX(statblock->eventLog.logSize) << std::endl;
    std::cout << "eventLog.current      = 0x" << HEX(statblock->eventLog.current) << std::endl;
    std::cout << "eventLog.overflow     = 0x" << HEX(statblock->eventLog.overflow) << std::endl;
    std::cout << "eventLog.eventArray   = 0x" << HEX(statblock->eventLog.eventArray) << std::endl;
    std::cout << "bufferFull            = 0x" << HEX(statblock->bufferFull) << std::endl;    
    std::cout << "rolXoffStat           = 0x" << HEX(statblock->rolXoffStat) << std::endl;    
    std::cout << "rolDownStat           = 0x" << HEX(statblock->rolDownStat) << std::endl;   
    std::cout << "tempOk                = " << statblock->tempOk << std::endl;

    for(loop = 0; loop < enum_LAST_ERRCNT; loop++)
      std::cout << "errors[" << std::setw(2) << loop << "](" << std::setw(54) << robinErrorStrings[loop] << ") = 0x" << HEX(statblock->errors[loop]) << std::endl;

    for(loop = 0; loop < enum_LAST_FRAGSTAT; loop++)
      std::cout << "fragStat[" << std::setw(2) << loop << "](" << std::setw(52) << robinFragStatStrings[loop] << ") = 0x" << HEX(statblock->fragStat[loop]) << std::endl;

    for(loop = 0; loop < enum_LAST_PAGESTAT; loop++)
      std::cout << "pageStat[" << std::setw(2) << loop << "](" << std::setw(52) << robinPageStatStrings[loop] << ") = 0x" << HEX(statblock->pageStat[loop]) << std::endl;

    for(loop = 0; loop < enum_LAST_MSGSTAT; loop++)
      std::cout << "msgStat[" << std::setw(2) << loop << "](" << std::setw(53) << robinMsgStatStrings[loop] << ") = 0x" << HEX(statblock->msgStat[loop]) << std::endl;

    for(loop = 0; loop < enum_histEntries; loop++)
      std::cout << "fragHisto[" << std::setw(2) << loop << "] = 0x" << HEX(statblock->fragHisto[loop]) << std::endl;

    for(loop = 0; loop < enum_histEntries; loop++)
      std::cout << "bufHisto[" << std::setw(2) << loop << "]  = 0x" << HEX(statblock->bufHisto[loop]) << std::endl;
  }
}



/*************************/      
void dump_inmate(Rol *prol)
/*************************/      
{      
  std::cout << "Enter the page number (0..99): " << std::endl;
  u_int pagenum = getdecd(0);
  Rol::KeptPage keptpage;

  keptpage = prol->getRejectedPage(pagenum);

  std::cout << "Most recent L1ID                     = 0x" << HEX(keptpage.rentry.mri) << std::endl;
  std::cout << "info.upfInfo.status                  = 0x" << HEX(keptpage.rentry.info.upfInfo.status) << std::endl;
  std::cout << "info.upfInfo.eventId                 = 0x" << HEX(keptpage.rentry.info.upfInfo.eventId) << std::endl;
  std::cout << "info.upfInfo.pageNum                 = 0x" << HEX(keptpage.rentry.info.upfInfo.pageNum) << std::endl;
  std::cout << "info.upfInfo.pageLen                 = 0x" << HEX(keptpage.rentry.info.upfInfo.pageLen) << std::endl;
  std::cout << "info.upfInfo.runNum                  = 0x" << HEX(keptpage.rentry.info.upfInfo.runNum) << std::endl;
  std::cout << "Number of words in rejected fragment = " << keptpage.pagedata_size << std::endl;

  if (keptpage.rentry.info.upfInfo.pageNum == 0)
    std::cout << "You have requested an invalid page" << std::endl;
  else
  {
    std::cout << "Now dumping the data of the rejected event" << std::endl;
    for (u_int loop = 0; loop < keptpage.pagedata_size; loop++)
      std::cout << "Word " << loop << " = 0x" << HEX(keptpage.pagedata[loop]) << std::endl;
  }
  std::cout << std::endl;
  std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
  std::cout << std::endl;
}
      





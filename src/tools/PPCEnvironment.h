// --------x----------------------------x-----------------------------------x--
/** 
 *  \file   PPCEnvironment.h
 *
 *  \author M.Mueller (mmueller@ti.uni-mannheim.de)
 *  
 *  \date   27.01.2005
 *
 *  
 *
 */
// --------x----------------------------x-----------------------------------x--

/*
 * $Id$
 * $Revision$
 * $Log$
 * Revision 1.1  2005/07/21 14:58:06  mmueller
 * robin_firmware_update bugfixes + PPC environment support
 *
 *
 *
 */

/** This file defines a class for handling the environment variables for the
    ROBIN PowerPC. They can be parsed and prepared to be programmed directly
    to the PPC flash memory.
 */

// --------x----------------------------x-----------------------------------x--

#ifndef PPCENVIRONMENT_H
#define PPCENVIRONMENT_H

// --------x----------------------------x-----------------------------------x--


#include <map>
#include <vector>


// --------x----------------------------x-----------------------------------x--


namespace ROS {

  class PPCEnvironment {

  public:
    
    /** Standard constructor which opens a empty environment. 
     */
    PPCEnvironment(void);

    /** Constructor which opens a text file containing pairs of
	variables and values.
     */
    PPCEnvironment(std::string fileName);    

    /** Reads the data from a file           
     */
    void read(std::string fileName);

    /** Writes the data to a file
     */
    void write(std::string fileName);

    /** Gets the raw environment data for programming into the flash memory
     */
    std::vector<unsigned char> getRawData(void);

    /** Sets the environment information from the raw data read 
	from the flash memory
    */
    void setFromRawData(std::vector<unsigned char> &  rawData);    

  protected:

    /** Calculate the CRC value of the environment data
     */
    unsigned int crc32(std::vector<unsigned char> data);

    /// A map that takes the pairs of environment variables and values
    std::map<std::string, std::string> m_variables;

    /// An array which is used for the CRC generation
    static const unsigned int c_crcTable[256];  

    static const unsigned int c_maxEnvSize;
  };
}

#endif

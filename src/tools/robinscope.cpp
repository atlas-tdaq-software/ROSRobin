/***************************************************************/
/* file:   robinscope.cpp                                      */
/* author: Markus Joos, CERN-PH/ESS                            */
/***** C 2012 - The software with that certain something *******/


#ifndef HOST
  #define HOST
#endif

#include <iostream>
#include <string>
#include <iomanip>
#include <getopt.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <errno.h>
#include "ROSEventFragment/ROBFragment.h"
#include "ROSRobin/ROSRobinExceptions.h"
#include "ROSRobin/Robin.h"
#include "ROSRobin/Rol.h"
#include "ROSMemoryPool/MemoryPage.h"
#include "ROSMemoryPool/WrapperMemoryPool.h"
#include "ROSGetInput/get_input.h"
#include "DFDebug/DFDebug.h"
#include "ROSRobin/robin.h"
#include "ROSRobin/robin_statistics_map.h"
#include "rcc_time_stamp/tstamp.h"
#include <cmdl/cmdargs.h>
#include <tmgr/tmresult.h>

using namespace ROS;
using namespace daq::tmgr;

/**********/    
/* Macros */
/**********/    
#define CONVERT_ENDIAN_16(X) ((Word)(((X) >> 8) + ((X) << 8)))
#define CONVERT_ENDIAN_32(X) ((Dword)( (((X) & 0xff000000) >> 24) | (((X) & 0x00ff0000) >> 8) | (((X) & 0x0000ff00) << 8) | (((X) & 0x000000ff) << 24)))


/*************/    
/* Constants */
/*************/    
enum
{
  CREATE = 1,
  OPEN,
  SETDMARAMPARAMS,
  RESERVEMSGRESOURCES,
  GETVIRTEVENTBASE,
  GETPHYSEVENTBASE,
  GETVIRTMISCBASE,
  GETPHYSMISCBASE,
  SEND1,
  SEND2,
  SEND3,
  SEND4,
  RECEIVE,
  RPLX,
  WPLX,
  RFPGA,
  WFPGA,
  RESET,
  GETMISC,
  RETMISC,
  FREEMSGRESOURCES,
  GETPROCINFO,
  IRQWAIT,
  CLOSE,
  DESTRUCT
};    

enum
{
  ROL_CREATE = 1,
  ROL_ENABLE,
  ROL_REQF,
  ROL_GETF,
  ROL_RELF,
  ROL_GETS,
  ROL_GETC,
  ROL_SETC,
  ROL_GC,
  ROL_GETECR,
  ROL_GETTEMP,
  ROL_RESET,  
  ROL_CLEARSTAT,  
  ROL_PING,
  ROL_GETIDS,
  ROL_GETINMATE,
  ROL_FORMAT,
  ROL_DUMP,
  ROL_DISCARD,
  ROL_CRC,
  ROL_DESTRUCT,
  ROL_GETONE,
  ROL_GETMANY,
  ROL_GETMANYFAST,
  ROL_SETDEBUG
};       

enum
{
  ERR_1 = 1,
  ERR_2,
  ERR_3,
  ERR_4,
  ERR_5,
  ERR_6,
  ERR_7,
  ERR_8,
  ERR_9,
  ERR_10,
  ERR_11,
  ERR_12,
  ERR_13,
  ERR_14,
  ERR_15,
  ERR_16,
  ERR_17,
  ERR_18,
  ERR_19,
  ERR_20,
  ERR_21,
  ERR_22
};
    
#define MAX_TICKET 100
    
/***********/
/* Globals */
/***********/
u_int cont = 0, dblevel = 5, dbpackage = DFDB_ROSROBIN;


/**************/
/* Prototypes */
/**************/
int setdebug(void);
int robinmenu(void);
int testmenu(void);
int benchmark(void);
int test_message(void);
int rolmenu(void);
int decodecfg(std::vector <CfgParm> cfg);
int errmenu(void);
void usage(void);
void printstat(u_int robin, u_int rol, u_int resetstat);
void printcfg(u_int robin, u_int rol, u_int setdef);
void decodestat(StatisticsBlock *statblock);
void outtext(std::string text, Byte *data, u_int len, u_int mode);
void emulate_err1(u_int l1id, Rol *prol);
void emulate_err2(Rol *prol);
void emulate_err3(u_int mode, u_int l1id, Rol *prol);
void emulate_err4(u_int mode, u_int l1id, Rol *prol);
void emulate_err5(u_int mode, u_int l1id, Rol *prol);
void emulate_err6(u_int l1id, Rol *prol);
void emulate_err7(u_int l1id, Rol *prol);
void emulate_err8(u_int l1id, Rol *prol);
void emulate_err9(u_int mode, u_int l1id, Rol *prol);
void stat_decode(u_int data);
void dump_inmate(Rol *prol);
u_int64_t swap64(u_int64_t data_in);


/**************/
void usage(void)
/**************/
{
  std::cout << "Valid options are ..." << std::endl;
  std::cout << "-m <robin>: Define the number of the Robin (0..N-1) Default = 0" << std::endl;
  std::cout << "-l <rol>:   Define the number of the ROL (0, 1 or 2) Default = 0" << std::endl;
  std::cout << "-s:         Print the statistics block of the selected ROL" << std::endl;
  std::cout << "  -z:       In combination with -s: Reset the statistics data once they have been read" << std::endl;
  std::cout << "-c:         Print the configuration parameters of the selected ROL" << std::endl;
  std::cout << "  -d:       In combination with -c: Set all parameters to defaults" << std::endl;
  std::cout << "-e:         Print the L1ID of all events that are currently in the buffer of the selected ROL" << std::endl;
  std::cout << "-p:         Print the status of the driver" << std::endl;
  std::cout << "-r:         Reset the ROL and the associated buffers in the Robin" << std::endl;
  std::cout << "-R:         Reset the Robin" << std::endl;
  std::cout << "-P:         Just check if the Robin responds to a message (ping)" << std::endl;
  std::cout << "-t:         Test mode: reset Robin, check ping response, check EBIST result, check temperature" << std::endl;
  std::cout << "-D:         Enable debugging" << std::endl;
  std::cout << std::endl;
}


// sigquit handler
/**********************************/
void sigquit_handler(int /*signum*/)
/**********************************/
{
  cont = 0;
}


/*****************************/
int main(int argc, char **argv)
/*****************************/
{
  static int fun = 1;
  int c, rol_nr = 0, robin_nr = 0, mode_statreset = 0, mode_test = 0, mode_stat = 0, mode_ppcreset = 0, mode_reset = 0;
  int mode_setdef = 0, mode_proc = 0, mode_cfg = 0, mode_ping = 0, mode_evdump = 0;
  struct sigaction sa;

  while ((c = getopt(argc, argv, "dm:l:hscerpPtzRD")) != -1)
    switch (c) 
    {
      case 'm': robin_nr = atoi(optarg);                           break;                   
      case 'l': rol_nr = atoi(optarg);                             break;                   
      case 'h': usage(); exit(TmPass);                             break;                  
      case 's': mode_stat = 1;                                     break;              
      case 'c': mode_cfg = 1;                                      break;                
      case 'r': mode_reset = 1;                                    break;                
      case 'R': mode_ppcreset = 1;                                 break;                
      case 'P': mode_ping = 1;                                     break;                
      case 'e': mode_evdump = 1;                                   break;                
      case 'p': mode_proc = 1;                                     break;                
      case 't': mode_test = 1;                                     break;                
      case 'z': mode_statreset = 1;                                break;                
      case 'd': mode_setdef = 1;                                   break;                      
      case 'D': DF::GlobalDebugSettings::setup(20, DFDB_ROSROBIN); break;                

      default:
	std::cout << "Invalid option " << c << std::endl;
	std::cout << "Usage: " << argv[0] << " [options]: " << std::endl;
	usage();
	exit (TmUnresolved);
    }
 
  // Install signal handler for SIGQUIT (ctrl + \)
  sigemptyset(&sa.sa_mask);
  sa.sa_flags   = 0;
  sa.sa_handler = sigquit_handler;

  // Dont block in intercept handler
  if (sigaction(SIGQUIT, &sa, NULL) < 0) 
  {
    std::cout << "main: sigaction() FAILED with ";
    int code = errno;
    if (code == EFAULT) std::cout << "EFAULT" << std::endl;
    if (code == EINVAL) std::cout << "EINVAL" << std::endl;
    exit(TmUnresolved);
  }
 
  if (mode_test)
  {
    float timeout = 100.0;
    u_int ecode = 0, temp, numberOfOutstandingReq = 1, msgInputMemorySize = 0x20, miscSize = 0x2000, eventSize = 0;
    Rol *prol = 0;
    Robin *rorobin;
    StatisticsBlock statblock;

    try
    {
      rorobin = new Robin(robin_nr, timeout);
      prol = new Rol(*rorobin, rol_nr);
      rorobin->open();
      rorobin->reserveMsgResources(numberOfOutstandingReq, msgInputMemorySize, miscSize, eventSize); 

      //Enable the EBIST (but without loopback) 
      prol->setConfig(enum_cfgRolExtLoop, 0);
      prol->setConfig(enum_cfgExtendedBist, 1);

      rorobin->reset(); 
      prol->ping();
      std::cout << "The ROBIN is alive" << std::endl;
      statblock = prol->getStatistics();
      std::cout << "EBIST Result = 0x" << HEX(statblock.bistResult) << std::endl;
      if(statblock.bistResult != 0)
      {
       std::cout << "The ROBIN failed the BIST" << std::endl;
       ecode = 1;
      }
      
      //Reset the EBIST
      prol->setConfig(enum_cfgExtendedBist, 0);
      //As EBIST is a static parameter we have to reset the ROBIN once more
      rorobin->reset(); 

      temp = prol->getTemperature();
      std::cout << "Approximative Robin temperature [deg. C]: " << temp << std::endl;
      if (temp > 70)
      {
        std::cout << "The temperature of this ROBIN is too high" << std::endl;
        ecode = 2;
      }
    }
    catch (ROSRobinExceptions& e)
    {
      std::cout << e << std::endl;
      exit(TmUnresolved);
    }
 
    delete prol;
    rorobin->freeMsgResources(); 
    rorobin->close();
    delete rorobin;  
    
    if (ecode)
      exit(TmUnresolved);
  }
  
  if (mode_ping)
  {
    float timeout = 100.0;
    u_int numberOfOutstandingReq = 1, msgInputMemorySize = 0x20, miscSize = 0x2000, eventSize = 0;
    Rol *prol = 0;
    Robin *rorobin;

    try
    {
      rorobin = new Robin(robin_nr, timeout);
      rorobin->open();
      rorobin->reserveMsgResources(numberOfOutstandingReq, msgInputMemorySize, miscSize, eventSize); 
      prol = new Rol(*rorobin, rol_nr);
      prol->ping();
    }
    catch (ROSRobinExceptions& e)
    {
      std::cout << e << std::endl;
      exit(TmUnresolved);
    }

    std::cout << "The ROBIN is alive" << std::endl;
    delete prol;
    rorobin->freeMsgResources(); 
    rorobin->close();
    delete rorobin;
  }

  if (mode_proc)
  {
    float timeout = 100.0;
    u_int numberOfOutstandingReq = 1, msgInputMemorySize = 0x20, miscSize = 0x2000, eventSize = 0;
    Robin *rorobin;

    try
    {
      rorobin = new Robin(robin_nr, timeout);
      rorobin->open();
      rorobin->reserveMsgResources(numberOfOutstandingReq, msgInputMemorySize, miscSize, eventSize);
      rorobin->getProcInfo(); 
    }
    catch (ROSRobinExceptions& e)
    {
      std::cout << e << std::endl;
      exit(TmUnresolved);
    }

    rorobin->freeMsgResources(); 
    rorobin->close();
    delete rorobin;
  }

  if (mode_evdump)
  {
    std::vector<Rol::L1idRange> idlist;
    float timeout = 100.0;
    u_int numberOfOutstandingReq = 1, msgInputMemorySize = 0x20, miscSize = 0x2000, eventSize = 0;
    Rol *prol = 0;
    Robin *rorobin;

    try
    {
      rorobin = new Robin(robin_nr, timeout);
      rorobin->open();
      rorobin->reserveMsgResources(numberOfOutstandingReq, msgInputMemorySize, miscSize, eventSize); 
      prol = new Rol(*rorobin, rol_nr);
      idlist = prol->getL1idList();


    u_int totnum = 0;
    for (std::vector<Rol::L1idRange>::const_iterator it2 = idlist.begin(); it2 != idlist.end(); it2++)
    {
      Rol::L1idRange pair = *it2;
      if (pair.first == pair.last)
      {
        std::cout << "L1ID = 0x" << HEX(pair.first) << std::endl;
	if (pair.first != 0xffffffff)
	  totnum++;
      }
      else
      {
        std::cout << "First L1ID = 0x" << HEX(pair.first) << "  Last L1ID = 0x" << HEX(pair.last) << "  ( " << pair.last - pair.first + 1<< " events)" << std::endl;
	totnum += pair.last - pair.first + 1;
      }
    }
    std::cout << "There are currently " << totnum << " events in memory" << std::endl;
    }
    catch (ROSRobinExceptions& e)
    {
      std::cout << e << std::endl;
      exit(TmUnresolved);
    }
  }
  
  if (mode_stat)
    printstat(robin_nr, rol_nr, mode_statreset);

  if (mode_cfg)
    printcfg(robin_nr, rol_nr, mode_setdef);

  if (mode_ppcreset)
  {
    float timeout = 100.0;
    u_int numberOfOutstandingReq = 1, msgInputMemorySize = 0x20, miscSize = 0x2000, eventSize = 0;
    Robin *rorobin;

    try
    {
      rorobin = new Robin(robin_nr, timeout);
      rorobin->open();
      rorobin->reserveMsgResources(numberOfOutstandingReq, msgInputMemorySize, miscSize, eventSize); 
      rorobin->reset(); 
    }
    catch (ROSRobinExceptions& e)
    {
      std::cout << e << std::endl;
      exit(TmUnresolved);
    }

    rorobin->freeMsgResources(); 
    rorobin->close();
    delete rorobin;
  }

  if (mode_reset)
  {
    float timeout = 100.0;
    u_int numberOfOutstandingReq = 1, msgInputMemorySize = 0x20, miscSize = 0x2000, eventSize = 0;
    Rol *prol = 0;
    Robin *rorobin;

    try
    {
      rorobin = new Robin(robin_nr, timeout);
      rorobin->open();
      rorobin->reserveMsgResources(numberOfOutstandingReq, msgInputMemorySize, miscSize, eventSize); 
      prol = new Rol(*rorobin, rol_nr);
      prol->reset();
    }
    catch (ROSRobinExceptions& e)
    {
      std::cout << e << std::endl;
      exit(TmUnresolved);
    }

    delete prol;
    rorobin->freeMsgResources(); 
    rorobin->close();
    delete rorobin;
  }
  
  if (mode_proc || mode_cfg || mode_stat || mode_reset || mode_ping || mode_test || mode_ppcreset || mode_evdump)
    exit(TmPass);

  DF::GlobalDebugSettings::setup(dblevel, dbpackage);
  std::cout << "This is robinscope" << std::endl;

  while(fun != 0)
  {
    std::cout << std::endl;
    std::cout << "Select an option:" << std::endl;
    std::cout << "  1 Set debug parameters" << std::endl;
    std::cout << "  2 Exercise methods of class Robin" << std::endl;
    std::cout << "  3 Exercise methods of class Rol" << std::endl;
    std::cout << "  4 Test fragments with errors" << std::endl;
    std::cout << "  5 Run H/W tests" << std::endl;
    std::cout << "  6 Benchmark message passing" << std::endl;
    std::cout << "  7 Run test messages" << std::endl;
    std::cout << "  0 Exit" << std::endl;
    fun = getdecd(fun);
    if (fun == 1) setdebug();
    if (fun == 2) robinmenu();
    if (fun == 3) rolmenu();
    if (fun == 4) errmenu();
    if (fun == 5) testmenu();
    if (fun == 6) benchmark();
    if (fun == 7) test_message();
  }

  return(0);
}


/*****************/
int benchmark(void)
/*****************/
{
  static u_int ret, timeout = 100, slotNumber = 0, rolId = 0, npages = 1, pageSize = 0x100;
  static u_int numberOfOutstandingReq = 3, msgInputMemorySize = 1024, miscSize = 0x800, eventSize;
  u_int delid[3] = {0, 0, 0};
  u_int l1id, ticket, nfailed0, nfailed1, nfailed2;
  tstamp ts1, ts2;
  float delta;
  Rol *prol[3] = {0, 0, 0};
  Robin *rorobin;
  StatisticsBlock statblock;
  std::vector <u_int> l1ids0;
  std::vector <u_int> l1ids1;
  std::vector <u_int> l1ids2;
  WrapperMemoryPool *m_memoryPool;
  MemoryPage *mem_page = NULL;
  ROBFragment *robFragment;

  ret = ts_open(0, TS_DUMMY);
  if (ret) 
    rcc_error_print(stdout, ret);

  eventSize = npages * pageSize; 
    
  std::cout << "Enter the number of the Robin to use (0..N-1)" << std::endl;
  slotNumber = getdecd(slotNumber);
        
  try
  {
    rorobin = new Robin(slotNumber, timeout);
    rorobin->open();
    rorobin->reserveMsgResources(numberOfOutstandingReq, msgInputMemorySize, miscSize, eventSize); 
    u_long eventBase       = rorobin->getVirtEventBase();
    u_int physicalAddress = rorobin->getPhysEventBase();
    m_memoryPool = new WrapperMemoryPool(npages, pageSize, eventBase, physicalAddress);
  }
  catch (ROSRobinExceptions& e)
  {
    std::cout << e << std::endl;
    return(-1);
  }

  try
  {
    for(rolId = 0; rolId < 3; rolId++)
    {
      prol[rolId] = new Rol(*rorobin, rolId);
      std::cout << "The Rol object for ROL " << rolId << " has been created" << std::endl;
      
      std::cout << "Resetting ROL:" << std::endl;
      prol[rolId]->reset();

      std::cout << "Setting RolDataGen:" << std::endl;
      prol[rolId]->setConfig(enum_cfgRolDataGen, 1);

      std::cout << "Enabling link:" << std::endl;
      prol[rolId]->setConfig(enum_cfgRolEnabled, 1);
    } 
    sleep(1);
    statblock = prol[0]->getStatistics();
    std::cout << "Before first clear: mostRecentId[0] = " << statblock.mostRecentId << std::endl;
    statblock = prol[1]->getStatistics();
    std::cout << "Before first clear: mostRecentId[1] = " << statblock.mostRecentId << std::endl;
    statblock = prol[2]->getStatistics();
    std::cout << "Before first clear: mostRecentId[2] = " << statblock.mostRecentId << std::endl;
        
    u_int nl1ids = 100;  
    std::cout << "Enter the size of the delete group: " << std::endl;
    nl1ids = getdecd(nl1ids);
    
    //Prepare delete group	
    std::cout << std::endl; std::cout << std::endl;
    for (u_int loop = 0; loop < nl1ids; loop++)
    {
      l1ids0.push_back(delid[0]);
      delid[0]++;
    }
       
    ts_clock(&ts1);
    nfailed0 = prol[0]->releaseFragment(&l1ids0);
    ts_clock(&ts2);
    if (nfailed0)
      std::cout << "Failed to delete " << nfailed0 << " fragments" << std::endl;
    l1ids0.clear();
    delta = ts_duration(ts1, ts2);
    std::cout << "Duration of one call to releaseFragment() for one ROL and " << nl1ids << " L1IDs = " << 1000000.0 * delta << " us" << std::endl;
    
    //Prepare delete group	
    std::cout << std::endl;
    for (u_int loop = 0; loop < nl1ids; loop++)
    {
      l1ids0.push_back(delid[0]);
      delid[0]++;
      l1ids1.push_back(delid[1]);
      delid[1]++;
      l1ids2.push_back(delid[2]);
      delid[2]++;
    }
       
    ts_clock(&ts1);
    nfailed0 = prol[0]->releaseFragment(&l1ids0);
    nfailed1 = prol[1]->releaseFragment(&l1ids1);
    nfailed2 = prol[2]->releaseFragment(&l1ids2);
    ts_clock(&ts2);
    if (nfailed0)
      std::cout << "ROL 0: Failed to delete " << nfailed0 << " fragments (3 ROLs)" << std::endl;
    if (nfailed1)
      std::cout << "ROL 1: Failed to delete " << nfailed1 << " fragments (3 ROLs)" << std::endl;
    if (nfailed2)
      std::cout << "ROL 2: Failed to delete " << nfailed2 << " fragments (3 ROLs)" << std::endl;
    l1ids0.clear();
    l1ids1.clear();
    l1ids2.clear();
    delta = ts_duration(ts1, ts2);
    std::cout << "Duration of three consecutive calls to releaseFragment(). (One per ROL for " << nl1ids << " L1IDs) = " << 1000000.0 * delta << " us" << std::endl;
    
    //Prepare delete group	
    std::cout << std::endl;
    for (u_int loop = 0; loop < nl1ids; loop++)
    {
      l1ids0.push_back(delid[0]);
      delid[0]++;
    }
       
    ts_clock(&ts1);
    prol[0]->releaseFragmentNoack(&l1ids0);
    ts_clock(&ts2);
    l1ids0.clear();
    delta = ts_duration(ts1, ts2);
    std::cout << "Duration of one call to releaseFragmentNoack() for one ROL and " << nl1ids << " L1IDs= " << 1000000.0 * delta << " us" << std::endl;
   
    //Prepare delete group	
    std::cout << std::endl;
    for (u_int loop = 0; loop < nl1ids; loop++)
    {
      l1ids0.push_back(delid[0]);
      delid[0]++;
      l1ids1.push_back(delid[1]);
      delid[1]++;
      l1ids2.push_back(delid[2]);
      delid[2]++;
    }
       
    ts_clock(&ts1);
    prol[0]->releaseFragmentNoack(&l1ids0);
    prol[1]->releaseFragmentNoack(&l1ids1);
    prol[2]->releaseFragmentNoack(&l1ids2);
    ts_clock(&ts2);
    l1ids0.clear();
    l1ids1.clear();
    l1ids2.clear();
    delta = ts_duration(ts1, ts2);
    std::cout << "Duration of three consecutive calls to releaseFragmentNoack() (One per ROL for" << nl1ids << " L1IDs) = " << 1000000.0 * delta << " us" << std::endl;

    
    //Prepare delete group	
    std::cout << std::endl;
    for (u_int loop = 0; loop < nl1ids; loop++)
    {
      l1ids0.push_back(delid[0]);
      delid[0]++;
    }
       
    ts_clock(&ts1);
    prol[0]->releaseFragmentAll(&l1ids0);
    ts_clock(&ts2);
    l1ids0.clear();
    delta = ts_duration(ts1, ts2);
    std::cout << "Duration of one call to releaseFragmentAll() for three ROLs and " << nl1ids << " L1IDs = " << 1000000.0 * delta << " us" << std::endl;
    
    //Test single clears
    std::cout << std::endl;
    ts_clock(&ts1);
    for (u_int loop = 0; loop < 10000; loop++)
    {
      l1ids0.clear();
      l1ids0.push_back(delid[0]);
      delid[0]++;
      nfailed0 = prol[0]->releaseFragment(&l1ids0);
      if (nfailed0)
        std::cout << "Failed to delete " << nfailed0 << " fragments" << std::endl;
    }
    ts_clock(&ts2);
    delta = ts_duration(ts1, ts2);
    std::cout << "Duration of 10000 calls to releaseFragment() for one ROL and one L1ID = " << 1000000.0 * delta << " us" << std::endl;

    //Test clear groups of 10
    std::cout << std::endl;
    ts_clock(&ts1);
    for (u_int loop = 0; loop < 1000; loop++)
    {
      l1ids0.clear();
      for (u_int loop2 = 0; loop2 < 10; loop2++)
      {
        l1ids0.push_back(delid[0]);
        delid[0]++;
      }
      nfailed0 = prol[0]->releaseFragment(&l1ids0);
      if (nfailed0)
        std::cout << "Failed to delete " << nfailed0 << " fragments" << std::endl;
    }
    ts_clock(&ts2);
    delta = ts_duration(ts1, ts2);
    std::cout << "Duration of 1000 calls to releaseFragment() for one ROL and 10 L1ID2 = " << 1000000.0 * delta << " us" << std::endl;

    //Test clear groups of 100
    std::cout << std::endl;
    ts_clock(&ts1);
    for (u_int loop = 0; loop < 100; loop++)
    {
      l1ids0.clear();
      for (u_int loop2 = 0; loop2 < 100; loop2++)
      {
        l1ids0.push_back(delid[0]);
        delid[0]++;
      }
      nfailed0 = prol[0]->releaseFragment(&l1ids0);
      if (nfailed0)
        std::cout << "Failed to delete " << nfailed0 << " fragments" << std::endl;
    }
    ts_clock(&ts2);
    delta = ts_duration(ts1, ts2);
    std::cout << "Duration of 100 calls to releaseFragment() for one ROL and 100 L1ID2 = " << 1000000.0 * delta << " us" << std::endl;
///////////////////////////////////////////////////////////////
    //Test single clears
    std::cout << std::endl;
    ts_clock(&ts1);
    for (u_int loop = 0; loop < 10000; loop++)
    {
      l1ids0.clear();
      l1ids0.push_back(delid[0]);
      delid[0]++;
      prol[0]->releaseFragmentNoack(&l1ids0);
    }
    ts_clock(&ts2);
    delta = ts_duration(ts1, ts2);
    std::cout << "Duration of 10000 calls to releaseFragmentNoack() for one ROL and one L1ID = " << 1000000.0 * delta << " us" << std::endl;

    //Test clear groups of 10
    std::cout << std::endl;
    ts_clock(&ts1);
    for (u_int loop = 0; loop < 1000; loop++)
    {
      l1ids0.clear();
      for (u_int loop2 = 0; loop2 < 10; loop2++)
      {
        l1ids0.push_back(delid[0]);
        delid[0]++;
      }
      prol[0]->releaseFragmentNoack(&l1ids0);
    }
    ts_clock(&ts2);
    delta = ts_duration(ts1, ts2);
    std::cout << "Duration of 1000 calls to releaseFragmentNoack() for one ROL and 10 L1ID2 = " << 1000000.0 * delta << " us" << std::endl;

    //Test clear groups of 100
    std::cout << std::endl;
    ts_clock(&ts1);
    for (u_int loop = 0; loop < 100; loop++)
    {
      l1ids0.clear();
      for (u_int loop2 = 0; loop2 < 100; loop2++)
      {
        l1ids0.push_back(delid[0]);
        delid[0]++;
      }
      prol[0]->releaseFragmentNoack(&l1ids0);
    }
    ts_clock(&ts2);
    delta = ts_duration(ts1, ts2);
    std::cout << "Duration of 100 calls to releaseFragmentNoack() for one ROL and 100 L1ID2 = " << 1000000.0 * delta << " us" << std::endl;
////////////////////////////////////////////////////////////////////////////////////


    //Test 1000 fragment requests
    std::cout << std::endl;
    try
    {
      mem_page = m_memoryPool->getPage();
    } 
    catch(std::exception& e)
    {
      std::cout << "Exception: " << e.what() << std::endl;
    }  

    // Check if the allocated page is OK
    if (mem_page == 0) 
    {						
      std::cout << " Failed to get a page from the memory pool" << std::endl;
      return(-1);
    }

    l1id = delid[0];
    for (u_int loop = 0; loop < 1000; loop++)
    {
      u_int *fragBufferAddress = static_cast<u_int *> (mem_page->reserve(mem_page->capacity()));

      ts_clock(&ts1);
      ticket = prol[0]->requestFragment(l1id, reinterpret_cast<u_long> (fragBufferAddress));
      u_int *fptr = prol[0]->getFragment(ticket);
      robFragment = reinterpret_cast<ROBFragment *>(fptr);
      ts_clock(&ts2);
    }
    delta = ts_duration(ts1, ts2) * 1000000.0;
    std::cout << "Duration of 1000 calls to requestFragment() / getFragment() for one ROL and one L1ID = " << 1000.0 * delta << " ms" << std::endl;





    
    
    for(rolId = 0; rolId < 3; rolId++)
      delete prol[rolId];    
  }
  catch(std::exception& e)
  {
    std::cout << "Exception: " << e.what() << std::endl;
  }  

  ret = ts_close(TS_DUMMY);
  if (ret) 
    rcc_error_print(stdout, ret);
    
  rorobin->freeMsgResources(); 
  rorobin->close();
  delete rorobin;
  return(0);
}


/********************/
int test_message(void)
/********************/
{
  static u_int ret, timeout = 100, slotNumber = 0;
  static u_int numberOfOutstandingReq = 3, msgInputMemorySize = 1024, miscSize = 0x800;
  u_int test_mode = 0;
  tstamp ts1, ts2;
  Rol *prol = 0;
  Robin *rorobin;
  float delta;
  
  ret = ts_open(0, TS_DUMMY);
  if (ret) 
    rcc_error_print(stdout, ret);
  
  std::cout << "Enter the number of the Robin to use (0..N-1)" << std::endl;
  slotNumber = getdecd(slotNumber);

  std::cout << "Enter the test mode" << std::endl;
  test_mode = getdecd(test_mode);
  
  try
  {
    rorobin = new Robin(slotNumber, timeout);
    rorobin->open();
    rorobin->reserveMsgResources(numberOfOutstandingReq, msgInputMemorySize, miscSize, 0x100); 
  }
  catch (ROSRobinExceptions& e)
  {
    std::cout << e << std::endl;
    return(-1);
  }

  try
  {
    prol = new Rol(*rorobin, 0);
    std::cout << "The Rol object has been created" << std::endl;

    std::cout << "Resetting ROL:" << std::endl;
    prol->reset();
       
    std::cout << "Running test message:" << std::endl;
    ts_clock(&ts1);
    prol->runTest(test_mode);
    ts_clock(&ts2);

    delta = ts_duration(ts1, ts2) * 1000000.0;
    std::cout << "Message delay (us):" << delta << std::endl;

    delete prol;    
  }
  catch(std::exception& e)
  {
    std::cout << "Exception: " << e.what() << std::endl;
  }  

  ret = ts_close(TS_DUMMY);
  if (ret) 
    rcc_error_print(stdout, ret);
    
  rorobin->freeMsgResources(); 
  rorobin->close();
  delete rorobin;
  return(0);
}


/***************/
int rolmenu(void)
/***************/
{
  int fun = 1;
  static u_int timeout = 100, slotNumber = 0, rolId = 0, l1id = 0, initdma = 1;
  static u_int dmaMemoryOrder = 3, numberOfOutstandingReq = 3, msgInputMemorySize = 1024, miscSize = 0x5000, eventSize;
  static u_int npages = 10, pageSize = 0x1000, nget = 1000, nl1ids = 5, index = 0, value = 0;
  u_int *fptr, ticket = 0, tickets[MAX_TICKET], index_in, index_out;
  Rol *prol = 0;
  Robin *rorobin;
  ROBFragment* robFragment;
  std::vector <u_int> l1ids;
  std::vector <CfgParm> cfg;
  std::vector <Rol::L1idRange> idlist;
  WrapperMemoryPool *m_memoryPool;
  MemoryPage *mem_page = NULL;
  tstamp ts1, ts2;
  StatisticsBlock statblock;
  Rol::ECRStatisticsBlock ddt_info;
  Rol::GCBlock gc_info;
  float delta;
  
  std::cout << "Before you can exercise the Rol methods a Robin object has to be created" << std::endl;
  std::cout << "To create it a number of parameters are required:" << std::endl;

  std::cout << "Enter the value for Robin slotNumber: " << std::endl;
  slotNumber = getdecd(slotNumber);

  std::cout << "Enter the value for timeout: " << std::endl;
  timeout = getdecd(timeout);

  std::cout << "Did you execute <robinconfigure> (1 = yes  0 = no)" << std::endl;
  initdma = getdecd(initdma);
  
  if (!initdma)
  {
    std::cout << "Enter the value for dmaMemoryOrder: " << std::endl;
    dmaMemoryOrder = gethexd(dmaMemoryOrder);
  }
  
  std::cout << "Enter the value for numberOfOutstandingReq: " << std::endl;
  numberOfOutstandingReq = getdecd(numberOfOutstandingReq);

  std::cout << "Enter the value for msgInputMemorySize: " << std::endl;
  msgInputMemorySize = gethexd(msgInputMemorySize);

  std::cout << "Enter the value for miscSize: " << std::endl;
  miscSize = gethexd(miscSize);

  std::cout << "Enter the number of pages: " << std::endl;
  npages = getdecd(npages);
  
  std::cout << "Enter the page size: " << std::endl;
  pageSize = gethexd(pageSize);
  
  eventSize = npages * pageSize; 
    
  try
  {
    rorobin = new Robin(slotNumber, timeout);
    rorobin->open();
    if (!initdma)
      rorobin->setDmaRamParams(dmaMemoryOrder); 
    rorobin->reserveMsgResources(numberOfOutstandingReq, msgInputMemorySize, miscSize, eventSize); 

    // Create a memoryPool structure around the event buffer
    u_long eventBase       = rorobin->getVirtEventBase();
    u_int physicalAddress = rorobin->getPhysEventBase();
    m_memoryPool = new WrapperMemoryPool(npages, pageSize, eventBase, physicalAddress);
  }
  catch (ROSRobinExceptions& e)
  {
    std::cout << e << std::endl;
    return(-1);
  }
  
  std::cout << std::endl << "=========================================================================" << std::endl;
  while (fun != 0)  
  {
    std::cout << std::endl;
    std::cout << "Select a test:" << std::endl;
    std::cout << "    " << ROL_CREATE      << " constructor" << std::endl;
    std::cout << "    " << ROL_ENABLE      << " enable S-Link" << std::endl;
    std::cout << "    " << ROL_REQF        << " requestFragment" << std::endl;
    std::cout << "    " << ROL_GETF        << " getFragment" << std::endl;
    std::cout << "    " << ROL_RELF        << " releaseFragment" << std::endl;
    std::cout << "    " << ROL_GETS        << " getStatistics" << std::endl;
    std::cout << "    " << ROL_GETC        << " getConfig" << std::endl;
    std::cout << "    " << ROL_SETC        << " setConfig" << std::endl;
    std::cout << "    " << ROL_GC          << " collectGarbage" << std::endl;
    std::cout << "   " << ROL_GETECR      << " getECR" << std::endl;
    std::cout << "   " << ROL_GETTEMP     << " getTemperature" << std::endl;
    std::cout << "   " << ROL_RESET       << " reset" << std::endl;
    std::cout << "   " << ROL_CLEARSTAT   << " clear statistics data" << std::endl;
    std::cout << "   " << ROL_PING        << " ping the Robin" << std::endl;
    std::cout << "   " << ROL_GETIDS      << " get all L1IDs" << std::endl;
    std::cout << "   " << ROL_GETINMATE   << " get a corrupted fragment from the special kept pages" << std::endl;
    std::cout << "   " << ROL_FORMAT      << " format the event buffer (makes only sense for Rolemu = 1)" << std::endl;
    std::cout << "   " << ROL_DUMP        << " dump fragment data buffer (64 MB) to file" << std::endl;
    std::cout << "   " << ROL_DISCARD     << " configure the discard mode" << std::endl;
    std::cout << "   " << ROL_CRC         << " Set the CRC checking interval" << std::endl;
    std::cout << "   " << ROL_DESTRUCT    << " destructor" << std::endl;
    std::cout << "   " << ROL_GETONE      << " Get one ROB fragment and check it" << std::endl;
    std::cout << "   " << ROL_GETMANY     << " Get many ROB fragments as fast as possible but sequentially" << std::endl;
    std::cout << "   " << ROL_GETMANYFAST << " Get many ROB fragments as fast as possible and in parallel" << std::endl;
    std::cout << "   " << ROL_SETDEBUG    << " Set debug parameters" << std::endl;
    std::cout << "   0 Exit" << std::endl;
    std::cout << "Your choice" << std::endl;
    fun = getdecd(fun);   

    if (fun == ROL_CREATE) 
    {
      std::cout << "Enter the value for rolId: " << std::endl;
      rolId = getdecd(rolId);

      prol = new Rol(*rorobin, rolId);
      std::cout << "The Rol object has been created" << std::endl;
    }
    
    else if (prol == 0)
    {
      std::cout << "Seems you did not call the constructor yet...." << std::endl;
    }

    else if (fun == ROL_REQF) 
    {
      std::cout << "Enter the L1ID: 0x" << std::endl;
      l1id = gethexd(l1id);      
    
      try
      {
        mem_page = m_memoryPool->getPage();
      } 
      catch(std::exception& e)
      {
        std::cout << "Exception: " << e.what() << std::endl;
      }  

      // Check if the allocated page is OK
      if (mem_page == 0) 
      {						
	std::cout << " Failed to get a page from the memory pool" << std::endl;
	return(-1);
      }

      u_int *fragBufferAddress = static_cast<u_int *> (mem_page->reserve(mem_page->capacity()));
      ts_clock(&ts1);
      ticket = prol->requestFragment(l1id, reinterpret_cast<u_long> (fragBufferAddress));
      ts_clock(&ts2);
      std::cout << "requestFragment returns ticket 0x: " << HEX(ticket) << std::endl;
      delta = ts_duration(ts1, ts2) * 1000000.0;
      std::cout << "command duration [us]: " << delta << std::endl;
    }

    else if (fun == ROL_ENABLE) 
    {
      prol->setConfig(enum_cfgRolEnabled, 1);
      std::cout << "S-Link enabled. You can now send fragments " << std::endl;
    }
    
    else if (fun == ROL_GETF) 
    {
      std::cout << "Enter the ticket: " << std::endl;
      ticket = gethexd(ticket); 
      ts_clock(&ts1);
      u_int *fptr = prol->getFragment(ticket);
      ts_clock(&ts2);
      std::cout << "The fragment has arrived" << std::endl;
      delta = ts_duration(ts1, ts2) * 1000000.0;
      std::cout << "command duration [us]: " << delta << std::endl;

      robFragment = reinterpret_cast<ROBFragment *>(fptr);
      //robFragment->print();
    }

    else if (fun == ROL_RELF) 
    {
      std::cout << "Enter the number of L1IDs to delete: " << std::endl;
      nl1ids = getdecd(nl1ids);
      for (u_int loop = 0; loop < nl1ids; loop++)
      {
        std::cout << "Enter the L1IDs #" << loop << ": " << std::endl;
        l1id = gethexd(l1id); 
        l1ids.push_back(l1id);
        l1id++;
      }
       
      u_int nfailed = prol->releaseFragment(&l1ids);
      if (nfailed)
        std::cout << "Failed to delete " << nfailed << " fragments" << std::endl;
      else
        std::cout << "The fragments have been released" << std::endl;
      l1ids.clear();
    }

    else if (fun == ROL_GETS) 
    {
      ts_clock(&ts1);
      statblock = prol->getStatistics();
      ts_clock(&ts2);
      delta = ts_duration(ts1, ts2) * 1000000.0;
      std::cout << "command duration [us]: " << delta << std::endl;

      decodestat(&statblock);
    }
    
    else if (fun == ROL_GETC) 
    {
      ts_clock(&ts1);
      cfg = prol->getConfig();
      ts_clock(&ts2);
      delta = ts_duration(ts1, ts2) * 1000000.0;
      std::cout << "command duration [us]: " << delta << std::endl;

      decodecfg(cfg);
    }

    else if (fun == ROL_SETC) 
    {
      std::cout << "Enter the index number of the parameter: " << std::endl;
      index = getdecd(index);

      std::cout << "Enter the value of the parameter: " << std::endl;
      value = getdecd(value);     
      
      try
      {
        prol->setConfig(index, value);
      } 
      catch(std::exception& e)
      {
        std::cout << "Exception: " << e.what() << std::endl;
      }  
    }

    else if (fun == ROL_GC) 
    {
      std::cout << "Enter the oldest valid L1ID: 0x" << std::endl;
      l1id = gethexd(l1id);
      ts_clock(&ts1);
      gc_info = prol->collectGarbage(l1id);
      ts_clock(&ts2);
      delta = ts_duration(ts1, ts2);
      std::cout << "collectGarbage returned:" << std::endl;
      std::cout << "done    = " << gc_info.done << std::endl;
      std::cout << "deleted = " << gc_info.deleted << std::endl;
      std::cout << "free    = " << gc_info.free << std::endl;
      std::cout << "Execution delay = " << delta << " seconds" << std::endl;
    }
    
    else if (fun == ROL_GETECR) 
    {
      u_int loop;
      
      ddt_info = prol->getECR();
      std::cout << "Most recent Id   = 0x" << HEX(ddt_info.mostRecentId) << std::endl;
      std::cout << "Overflow         = " << ddt_info.overflow << std::endl;
      std::cout << "Number of ECRs   = " << ddt_info.necrs << std::endl;
      for (loop = 0; loop < ddt_info.necrs; loop++)
        std::cout << "Last L1ID before ECR # "<< loop + 1 << " = 0x" << HEX(ddt_info.ecr[loop]) << std::endl;
    }
    
    else if (fun == ROL_GETTEMP) 
    {
      u_int temp;
      
      temp = prol->getTemperature();
      std::cout << "The temperature of the Robin is about " << temp << " deg. C" << std::endl;
    }
    
    else if (fun == ROL_RESET) 
    {
      prol->reset();
      std::cout << "The ROL has been reset: " << std::endl;
    }

    else if (fun == ROL_CLEARSTAT) 
    {
      prol->clearStatistics();
      std::cout << "The statistics data has been reset: " << std::endl;
    }
  
    else if (fun == ROL_PING)
    {
      try
      {
        prol->ping();
      }
      catch (ROSRobinExceptions& e)
      {
	std::cout << e << std::endl;
	exit(TmUnresolved);
      }
      std::cout << "The Robin is alive. " << std::endl;
    }
    
    else if (fun == ROL_GETIDS)
    {
      try
      {
        idlist = prol->getL1idList();
      }
      catch (ROSRobinExceptions& e)
      {
	std::cout << e << std::endl;
	exit(TmUnresolved);
      }

      u_int totnum = 0;
      for (std::vector<Rol::L1idRange>::const_iterator it2 = idlist.begin(); it2 != idlist.end(); it2++)
      {
	Rol::L1idRange pair = *it2;
	if (pair.first == pair.last)
	{
          std::cout << "L1ID = 0x" << HEX(pair.first) << std::endl;
  	  if (pair.first != 0xffffffff)
	    totnum++;
	}
	else
	{
          std::cout << "First L1ID = 0x" << HEX(pair.first) << "  Last L1ID = 0x" << HEX(pair.last) << "  ( " << pair.last - pair.first + 1 << " events)" << std::endl;
	  totnum += pair.last - pair.first + 1;
	}
      }
      std::cout << "There are currently " << totnum << " events in memory" << std::endl;
    } 
    
    else if (fun == ROL_GETINMATE)
    {
      dump_inmate(prol);
    }

    else if (fun == ROL_FORMAT)
    {
      try
      {
        prol->formatBuffer();
      }
      catch (ROSRobinExceptions& e)
      {
	std::cout << e << std::endl;
	exit(TmUnresolved);
      }
      std::cout << "The event buffer of the Rol has been filled with emulated events " << std::endl;
    }

    else if (fun == ROL_DUMP)
    {
      u_int *roldata, rolmemsize; 
      int outputFile, isok;
      char fname[256];

      rolmemsize = 64 * 1024 * 1024;
      roldata = (u_int *)malloc(rolmemsize);
      if (roldata == NULL)
      {
	std::cout << "Failed to get memory " << std::endl;
	exit(TmUnresolved);
      }
     
      std::cout << "Enter the file name " << std::endl;
      getstrd(fname, (char *)"./roldata");
      
      // Open the file for output
      outputFile = open(fname, O_WRONLY|O_CREAT|O_LARGEFILE, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
      if (outputFile < 0)
      {
	std::cout << "Failed to open " << fname << std::endl;
	exit(TmUnresolved);
      }
  
      try
      {
        //Read memory in blocklets of 0x1000 words
	//Works only if MiscMem > 0x1000 words
        std::cout << "Requesting data from Robin. Please be patient..... " << std::endl;
        prol->dumpRol(roldata, 0x1000); 
      }
      catch (ROSRobinExceptions& e)
      {
	std::cout << e << std::endl;
	exit(TmUnresolved);
      }
    
      std::cout << "Writing data to file. Please be even more patient..... " << std::endl;
      isok = write(outputFile, (void *)roldata, rolmemsize);
      if (isok < 0)
      {
        std::cout << "Error in writing to file" << std::endl;
        exit(TmUnresolved);
      }
      
      close (outputFile);
    }

    else if (fun == ROL_DISCARD)
    {
      u_int discard_mode = 0;
      
      std::cout << "0 = Disable discard mode: " << std::endl;
      std::cout << "1 = Enable discard mode: " << std::endl;
      std::cout << "Enter the mode: " << std::endl;
      discard_mode = getdecd(discard_mode);

      try
      {
        prol->configureDiscardMode(discard_mode);
      }
      catch (ROSRobinExceptions& e)
      {
	std::cout << e << std::endl;
	exit(TmUnresolved);
      }
      std::cout << "New discard mode active" << std::endl;
    }
    
    else if (fun == ROL_CRC)
    {
      std::cout << "Enter the interval for checking the CRC: " << std::endl;
      u_int crc_interval = getdecd(100);
      prol->setCRCInterval(crc_interval);
    }
    
    else if (fun == ROL_DESTRUCT) 
    {
      delete prol;
      std::cout << "The Rol object has been deleted" << std::endl;
    }

    else if (fun == ROL_GETONE) 
    {
      std::cout << "Enter the L1ID: 0x" << std::endl;
      l1id = gethexd(l1id);

      try
      {
        mem_page = m_memoryPool->getPage();
      } 
      catch(std::exception& e)
      {
        std::cout << "Exception: " << e.what() << std::endl;
      } 

      // Check if the allocated page is OK
      if (mem_page == 0) 
      {						
	std::cout << " Failed to get a page from the memory pool" << std::endl;
	return(-1);
      }

      u_int *fragBufferAddress = static_cast<u_int *> (mem_page->reserve(mem_page->capacity()));
      try
      {
        ticket = prol->requestFragment(l1id, reinterpret_cast<u_long> (fragBufferAddress));
	fptr = prol->getFragment(ticket);      
      }
      catch (ROSRobinExceptions& e)
      {
        std::cout << e << std::endl;
        break;
      }
      robFragment = new ROBFragment(mem_page);

      std::cout << "Dumping raw data....." << std::endl;
      for (u_int loop = 0; loop < fptr[1]; loop++)
        std::cout << "Word " << loop << " = 0x" << HEX(fptr[loop]) << std::endl;

      robFragment->print();
      robFragment->check(l1id, 0);
      delete robFragment;
      mem_page->free();
    }

    else if (fun == ROL_GETMANY) 
    {
      std::cout << "Enter the L1ID of the first fragment: 0x" << std::endl;
      l1id = gethexd(l1id);

      std::cout << "Enter the number of fragments to get: " << std::endl;
      nget = getdecd(nget);

      try
      {
        mem_page = m_memoryPool->getPage();
      }
      catch(std::exception& e)
      {
        std::cout << "Exception: " << e.what() << std::endl;
      }  
      
      // Check if the allocated page is OK
      if (mem_page == 0) 
      {						
	std::cout << " Failed to get a page from the memory pool" << std::endl;
	return(-1);
      }
      u_int *fragBufferAddress = static_cast<u_int *> (mem_page->reserve(mem_page->capacity()));
      
      ts_clock(&ts1);
      for(u_int floop = 0; floop < nget; floop++)
      {
        ticket = prol->requestFragment(l1id++, reinterpret_cast<u_long> (fragBufferAddress));
        prol->getFragment(ticket); //blocking
      }
      ts_clock(&ts2);
      delta = ts_duration(ts1, ts2);
      std::cout << nget << " ROB fragments received in " << delta << " seconds" << std::endl; 
      mem_page->free();
     }
   
    else if (fun == ROL_GETMANYFAST) 
    {   
      std::cout << "Enter the L1ID of the first fragment: 0x" << std::endl;
      l1id = gethexd(l1id);

      std::cout << "Enter the number of fragments to get (should be at least ~1000): " << std::endl;
      nget = getdecd(nget);
  
      try
      {
        mem_page = m_memoryPool->getPage();
      }
      catch(std::exception& e)
      {
        std::cout << "Exception: " << e.what() << std::endl;
      }  
            
      // Check if the allocated page is OK
      if (mem_page == 0) 
      {						
	std::cout << " Failed to get a page from the memory pool" << std::endl;
	return(-1);
      }
      u_int *fragBufferAddress = static_cast<u_int *> (mem_page->reserve(mem_page->capacity()));    

      //Post request to give the Robin enough to do
      index_in = 0;
      index_out = 0;
      for(u_int loop = 0; loop < numberOfOutstandingReq; loop++)
        tickets[index_in++] = prol->requestFragment(l1id++, reinterpret_cast<u_long> (fragBufferAddress));

      ts_clock(&ts1);
      for(u_int floop = 0; floop < nget; floop++)
      {
        prol->getFragment(tickets[index_out++]); //blocking
        tickets[index_in++] = prol->requestFragment(l1id++, reinterpret_cast<u_long> (fragBufferAddress));
	if (index_in == MAX_TICKET)
	  index_in = 0;
	if (index_out == MAX_TICKET)
	  index_out = 0;
      }
      ts_clock(&ts2);
      delta = ts_duration(ts1, ts2);
      mem_page->free();
      std::cout << nget << " ROB fragments received in " << delta << " seconds" << std::endl; 
    }   

    if (fun == ROL_SETDEBUG) 
      setdebug();
  }
  
  rorobin->freeMsgResources(); 
  rorobin->close();
  delete rorobin;
  return(0);
}
  

/*****************************************************/
void printstat(u_int robin, u_int rol, u_int resetstat)
/*****************************************************/
{
  u_int temp, timeout, numberOfOutstandingReq, msgInputMemorySize, miscSize, eventSize;
  Rol *prol = 0;
  Robin *rorobin;
  StatisticsBlock statblock;
  
  std::cout << "Dumping statistics for ROL " << rol << " on Robin " << robin << std::endl;
  
  timeout                = 100;
  numberOfOutstandingReq = 1;
  msgInputMemorySize     = 0x20;
  miscSize               = 0x2000;
  eventSize              = 0; 
    
  try
  {
    rorobin = new Robin(robin, timeout);
    rorobin->open();
    rorobin->reserveMsgResources(numberOfOutstandingReq, msgInputMemorySize, miscSize, eventSize); 

    prol = new Rol(*rorobin, rol);
    statblock = prol->getStatistics();
    if (resetstat)
    	prol->clearStatistics();
  
    decodestat(&statblock);
  
    temp = prol->getTemperature();
    std::cout << "Approximative Robin temperature [deg. C]: " << temp << std::endl;

  	  }
  	  catch (ROSRobinExceptions& e)
  	  {
  		  std::cout << e << std::endl;
  		  exit(TmUnresolved);
  	  }

  delete prol;
  rorobin->freeMsgResources(); 
  rorobin->close();
  delete rorobin;
}


/*************************************************/
void printcfg(u_int robin, u_int rol, u_int setdef)
/*************************************************/
{
  u_int nodef, timeout, numberOfOutstandingReq, msgInputMemorySize, miscSize, eventSize;
  Rol *prol = 0;
  Robin *rorobin;
  std::vector <CfgParm> cfg;
  
  std::cout << "Dumping configuration parameters of ROL " << rol << " on Robin " << robin << std::endl;
  
  timeout                = 100;
  numberOfOutstandingReq = 1;
  msgInputMemorySize     = 0x20;
  miscSize               = 0x2000;
  eventSize              = 0; 
    
  try
  {
    rorobin = new Robin(robin, timeout);
    rorobin->open();
    rorobin->reserveMsgResources(numberOfOutstandingReq, msgInputMemorySize, miscSize, eventSize); 

    prol = new Rol(*rorobin, rol);
    cfg = prol->getConfig();
    nodef = decodecfg(cfg);
  
    if (setdef && nodef)
    {
    	std::cout << "Now forcing all parameters back to default values" << std::endl;

    	u_int doreset = 0;
    	//Static parameters
    	for (u_int loop = 0; loop < enum_cfgNumItems; loop++)
    	{
    		u_int dw = cfg[loop].defVal;
    		u_int cw = cfg[loop].value;
    		u_int dy = static_cast<int>(cfg[loop].dynamic);
    		u_int fi = static_cast<int>(cfg[loop].fixed);
      
    		if ((cw != dw) && !dy && !fi)
    		{
    			prol->setConfig(loop, cfg[loop].defVal);
    			doreset = 1;
    		}
    	}
    
    	if (doreset)
    		rorobin->reset();

    	//Read the parameters once more
    	cfg = prol->getConfig();
    	
    	//Dynamic parameters
    	for (u_int loop = 0; loop < enum_cfgNumItems; loop++)
    	{
    		u_int dw = cfg[loop].defVal;
    		u_int cw = cfg[loop].value;
    		u_int dy = static_cast<int>(cfg[loop].dynamic);
    		u_int fi = static_cast<int>(cfg[loop].fixed);
      
    		if ((cw != dw) && dy && !fi && loop != enum_cfgTempAlarmValue)
    			prol->setConfig(loop, cfg[loop].defVal);
    	}
    }

  }
  catch (ROSRobinExceptions& e)
  {
    std::cout << e << std::endl;
    exit(TmUnresolved);
  }

  delete prol;
  rorobin->freeMsgResources(); 
  rorobin->close();
  delete rorobin;
}


/**************************************/
int decodecfg(std::vector <CfgParm> cfg)
/**************************************/
{
  u_int somebad = 0;

  std::cout << "Abbreviations:" << std::endl;
  std::cout << "#  = Number of the configuration parameter" << std::endl;
  std::cout << "Ex = 1: Expert parameter       0: Standard parameter" << std::endl;
  std::cout << "Dy = 1: Dynamic parameter      0: Static parameter" << std::endl;
  std::cout << "Bo = 1: Boolean parameter      0: Integer parameter" << std::endl;
  std::cout << "Gl = 1: ROBIN-wide parameter   0: ROL-specific parameter" << std::endl;
  std::cout << "Fi = 1: Fixed value parameter  0: Modifyable parameter" << std::endl;
  std::cout << "In = 1: Requires reinitialisation of buffer management (erase data)" << std::endl;
  std::cout << std::endl;
  std::cout << "           Name|  #| Ex| Dy| Bo| Gl| Fi| In|      Value|    Default|        Min|        Max|" << std::endl;
  std::cout << "===============|===|===|===|===|===|===|===|===========|===========|===========|===========|" << std::endl;

  for (u_int loop = 0; loop < enum_cfgNumItems; loop++)
  {
     std::cout << std::setw(15) << cfg[loop].name << "|"
               << std::setw(3)  << loop << "|"
	       << std::setw(3)  << static_cast<int>(cfg[loop].expert) << "|"
	       << std::setw(3)  << static_cast<int>(cfg[loop].dynamic) << "|"
	       << std::setw(3)  << static_cast<int>(cfg[loop].isBool) << "|"
	       << std::setw(3)  << static_cast<int>(cfg[loop].global) << "|"
	       << std::setw(3)  << static_cast<int>(cfg[loop].fixed) << "|"
	       << std::setw(3)  << static_cast<int>(cfg[loop].init) << "|"
    	       << std::setw(11) << cfg[loop].value << "|"  
    	       << std::setw(11) << cfg[loop].defVal << "|" 
    	       << std::setw(11) << cfg[loop].minVal << "|"
    	       << std::setw(11) << cfg[loop].maxVal << "|" << std::endl;


    u_int dw = cfg[loop].defVal;
    u_int cw = cfg[loop].value;
    u_int ex = static_cast<int>(cfg[loop].expert);
    u_int fi = static_cast<int>(cfg[loop].fixed);

    if((loop != enum_cfgTempAlarmValue) && (cw != dw) && ex && !fi)
      somebad = 1;
  }

  if (somebad)
  {
    std::cout << std::endl;
    std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
    std::cout << "Some Expert parameters do not have the default value." << std::endl;
    std::cout << "You may want to check this!" << std::endl;

    for (u_int loop = 0; loop < enum_cfgNumItems; loop++)
    {
      u_int dw = cfg[loop].defVal;
      u_int cw = cfg[loop].value;
      u_int ex = static_cast<int>(cfg[loop].expert);
      u_int fi = static_cast<int>(cfg[loop].fixed);
    
      if ((loop != enum_cfgTempAlarmValue) && !fi && (cw != dw) && ex)
        std::cout << "Parameter " << std::setw(15) << cfg[loop].name << " is " << cfg[loop].value << " instead of " << cfg[loop].defVal << std::endl;
    }
    std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
  }  
  
  return(somebad);
}


/**********************************************************/
void outtext(std::string text, Byte *data, u_int len, u_int mode)
/**********************************************************/
{
  u_int loop;

  std::cout << text;

  if (mode == 0) 
  { 
    for(loop = 0; loop < len; loop++)
      std::cout << data[loop];
  }
  
  if (mode == 1)
  {
    std::cout << static_cast<u_int>(data[0]);
    std::cout << static_cast<u_int>(data[1]);
    std::cout << static_cast<u_int>(data[2]);
    std::cout << static_cast<u_int>(data[3]) << "/";
    std::cout << static_cast<u_int>(data[4]);
    std::cout << static_cast<u_int>(data[5]) << "/";
    std::cout << static_cast<u_int>(data[6]);
    std::cout << static_cast<u_int>(data[7]);
  }
   
  std::cout << std::endl;
}


/*****************************************/
void decodestat(StatisticsBlock *statblock)
/*****************************************/
{
  u_int loop;
  std::string text;
  
  if (statblock->statVersion != enum_statVersion)
  {
    std::cout << "statVersion          = 0x" << HEX(statblock->statVersion) << std::endl;
    std::cout << "Sorry. I was expecting 0x" << HEX(enum_statVersion) << std::endl;
    std::cout << "serNum               = " << statblock->serNum << std::endl;
  }
  else
  {      
    std::cout << "statVersion           = 0x" << HEX(statblock->statVersion) << std::endl;
    std::cout << "endianFlag            = 0x" << HEX(statblock->endianFlag) << std::endl;
    std::cout << "statByteSize          = 0x" << HEX(statblock->statByteSize) << std::endl;
    std::cout << "serNum                = " << statblock->serNum << std::endl;
    std::cout << "swVersion             = 0x" << HEX(statblock->swVersion) << std::endl;
    std::cout << "cfgVersion            = 0x" << HEX(statblock->cfgVersion) << std::endl;
    std::cout << "cfgNumItems           = 0x" << HEX(statblock->cfgNumItems) << std::endl;
    std::cout << "designVersion         = 0x" << HEX(statblock->designVersion) << std::endl;
    std::cout << "fragFormat            = 0x" << HEX(statblock->fragFormat) << std::endl;
    std::cout << "bufByteSize           = 0x" << HEX(statblock->bufByteSize) << std::endl;
    std::cout << "numChannels           = 0x" << HEX(statblock->numChannels) << std::endl;
    std::cout << "cpuSpeed              = " << statblock->cpuSpeed << std::endl;
    std::cout << "robinModel            = 0x" << HEX(statblock->robinModel) << std::endl;
    std::cout << "appType               = 0x" << HEX(statblock->appType) << std::endl;
    std::cout << "switches              = 0x" << HEX(statblock->switches) << std::endl;
    std::cout << "cDate                 = " << statblock->cDate << std::endl;           
    std::cout << "cTime                 = " << statblock->cTime << std::endl; 
    
    // check, eventaully convert endianness
    SecSiInfo newSecSi, *secSiPtr;
    Dword *secSiSrc = (Dword*)&statblock->secSi, *secSiDest = (Dword*)&newSecSi;

    for (loop = 0; loop < sizeof(SecSiInfo) / sizeof(Dword);loop++)	
      secSiDest[loop] = CONVERT_ENDIAN_32(secSiSrc[loop]);
    secSiPtr = &newSecSi;
    std::cout << "SECSI magic word      = 0x" << HEX(CONVERT_ENDIAN_32(secSiPtr->magic)) << std::endl;
    std::cout << "SECSI version         = " << CONVERT_ENDIAN_16(secSiPtr->secSiVersion) << std::endl;
 
    std::cout << "SECSI ATLAS Part ID   = ";
    std::cout << static_cast<u_int>(secSiPtr->id.project[0]); 
    std::cout << static_cast<u_int>(secSiPtr->id.project[1]) << "-"; 
    std::cout << static_cast<u_int>(secSiPtr->id.system[0]) << "-"; 
    std::cout << static_cast<u_int>(secSiPtr->id.subSystem[0]) << "-"; 
    std::cout << static_cast<u_int>(secSiPtr->id.institute[0]); 
    std::cout << static_cast<u_int>(secSiPtr->id.institute[1]); 
    std::cout << static_cast<u_int>(secSiPtr->id.institute[2]) << "-"; 
    std::cout << static_cast<u_int>(secSiPtr->id.prefix[0]) << "-"; 
    std::cout << static_cast<u_int>(secSiPtr->id.sernum[0]); 
    std::cout << static_cast<u_int>(secSiPtr->id.sernum[1]); 
    std::cout << static_cast<u_int>(secSiPtr->id.sernum[2]); 
    std::cout << static_cast<u_int>(secSiPtr->id.sernum[3]); 
    std::cout << static_cast<u_int>(secSiPtr->id.sernum[4]); 
    std::cout << static_cast<u_int>(secSiPtr->id.sernum[5]); 
    std::cout << std::endl;

    text ="SECSI prodDate        = ";
    outtext (text, &secSiPtr->prodDate[0], 8, 1);

    text ="SECSI schemDate       = ";
    outtext (text, &secSiPtr->schemDate[0], 8, 1);

    std::cout << "SECSI schemId         = " << CONVERT_ENDIAN_32(secSiPtr->schemId) << std::endl;

    text ="SECSI pcbName         = ";
    outtext (text, &secSiPtr->pcbName[0], 16,0);

    text ="SECSI pcbId           = ";
    outtext (text, &secSiPtr->pcbId[0], 16,0);

    std::cout << "SECSI gerberId        = " << CONVERT_ENDIAN_32(secSiPtr->gerberId) << std::endl;

    text ="SECSI asmName         = ";
    outtext (text, &secSiPtr->asmName[0], 16, 0);

    std::cout << "SECSI asmId           = " << CONVERT_ENDIAN_32(secSiPtr->asmId) << std::endl;
    std::cout << "SECSI bomVersion      = " << CONVERT_ENDIAN_32(secSiPtr->bomVersion) << std::endl;
    std::cout << "pagesFree             = 0x" << HEX(statblock->pagesFree) << std::endl;		
    std::cout << "pagesInUse            = 0x" << HEX(statblock->pagesInUse) << std::endl;	
    std::cout << "idleCount             = 0x" << HEX(statblock->idleCount) << std::endl;        
//    std::cout << "idleCount (64bit)     = 0x" << HEX(swap64(statblock->idleCount)) << std::endl;        
    std::cout << "errCntSize            = 0x" << HEX(statblock->errCntSize) << std::endl;			
    std::cout << "fragStatSize          = 0x" << HEX(statblock->fragStatSize) << std::endl;		
    std::cout << "pageStatSize          = 0x" << HEX(statblock->pageStatSize) << std::endl;	
    std::cout << "msgStatSize           = 0x" << HEX(statblock->msgStatSize) << std::endl;		
    std::cout << "histoSize             = 0x" << HEX(statblock->histoSize) << std::endl;		
    std::cout << "bistResult            = 0x" << HEX(statblock->bistResult) << std::endl;
    std::cout << "mostRecentId          = 0x" << HEX(statblock->mostRecentId) << std::endl;
    std::cout << "savedRejectedPages    = " << statblock->savedRejectedPages << std::endl;
    std::cout << "eventLog.logEnabled   = 0x" << HEX(statblock->eventLog.logEnabled) << std::endl;
    std::cout << "eventLog.logSize      = 0x" << HEX(statblock->eventLog.logSize) << std::endl;
    std::cout << "eventLog.current      = 0x" << HEX(statblock->eventLog.current) << std::endl;
    std::cout << "eventLog.overflow     = 0x" << HEX(statblock->eventLog.overflow) << std::endl;
    std::cout << "eventLog.eventArray   = 0x" << HEX(statblock->eventLog.eventArray) << std::endl;
    std::cout << "bufferFull            = 0x" << HEX(statblock->bufferFull) << std::endl;    
    std::cout << "rolXoffStat           = 0x" << HEX(statblock->rolXoffStat) << std::endl;    
    std::cout << "rolDownStat           = 0x" << HEX(statblock->rolDownStat) << std::endl;   
    std::cout << "tempOk                = " << statblock->tempOk << std::endl;
    
    
//    std::cout << "The remaining parameters are all 64bit long" << std::endl;

    for(loop = 0; loop < enum_LAST_ERRCNT; loop++)
      std::cout << "errors[" << std::setw(2) << loop << "](" << std::setw(54) << robinErrorStrings[loop] << ") = 0x" << HEX(statblock->errors[loop]) << std::endl;

    for(loop = 0; loop < enum_LAST_FRAGSTAT; loop++)
      std::cout << "fragStat[" << std::setw(2) << loop << "](" << std::setw(52) << robinFragStatStrings[loop] << ") = 0x" << HEX(statblock->fragStat[loop]) << std::endl;
//e.g.      std::cout << "fragStat[" << std::setw(2) << loop << "](" << std::setw(52) << robinFragStatStrings[loop] << ") = 0x" << HEX(swap64(statblock->fragStat[loop])) << std::endl;

    for(loop = 0; loop < enum_LAST_PAGESTAT; loop++)
      std::cout << "pageStat[" << std::setw(2) << loop << "](" << std::setw(52) << robinPageStatStrings[loop] << ") = 0x" << HEX(statblock->pageStat[loop]) << std::endl;

    for(loop = 0; loop < enum_LAST_MSGSTAT; loop++)
      std::cout << "msgStat[" << std::setw(2) << loop << "](" << std::setw(53) << robinMsgStatStrings[loop] << ") = 0x" << HEX(statblock->msgStat[loop]) << std::endl;

    for(loop = 0; loop < enum_histEntries; loop++)
      std::cout << "fragHisto[" << std::setw(2) << loop << "] = 0x" << HEX(statblock->fragHisto[loop]) << std::endl;

    for(loop = 0; loop < enum_histEntries; loop++)
      std::cout << "bufHisto[" << std::setw(2) << loop << "]  = 0x" << HEX(statblock->bufHisto[loop]) << std::endl;
  }
}


/*********************************/
u_int64_t swap64(u_int64_t data_in)
/*********************************/
{
  u_int64_t w1, w2;

  w1 = data_in & 0xffffffff;
  w2 = data_in >> 32;
  return((w1 << 32) | w2);
}


/*****************/
int robinmenu(void)
/*****************/
{
  int fun = 1;
  static u_int slotNumber = 0, timeout = 100, numberOfOutstandingReq = 5, msgInputMemorySize = 1024, dmaMemoryOrder = 5;
  static u_int miscSize = 4092, eventSize = 2048, regdata = 0, regaddr = 0;
  static u_int rol = 0, service = 0, p1 = 0, p2 = 0;
  static u_long ra = 0;
  static u_int msize = 0x100, mhandle;
  static u_long mstart_add;
  Robin *probin = 0;
  
  std::cout << std::endl << "=========================================================================" << std::endl;
  while (fun != 0)  
  {
    std::cout << std::endl;
    std::cout << "Select a test:" << std::endl;
    std::cout << "   " << CREATE              << " constructor" << std::endl;
    std::cout << "   " << OPEN                << " open" << std::endl;
    std::cout << "   " << SETDMARAMPARAMS     << " setDmaRamParams" << std::endl;
    std::cout << "   " << RESERVEMSGRESOURCES << " reserveMsgResources" << std::endl;
    std::cout << "   " << GETVIRTEVENTBASE    << " getVirtEventBase" << std::endl;
    std::cout << "   " << GETPHYSEVENTBASE    << " getPhysEventBase" << std::endl;
    std::cout << "   " << GETVIRTMISCBASE     << " getVirtMiscBase" << std::endl;
    std::cout << "   " << GETPHYSMISCBASE     << " getPhysMiscBase" << std::endl;
    std::cout << "   " << SEND1               << " sendMsg(u_int rolId, u_int service, u_int replyAddress)" << std::endl;    
    std::cout << "   " << SEND2               << " sendMsg(u_int rolId, u_int service, u_int replyAddress, u_int parameter)" << std::endl;    
    std::cout << "   " << SEND3               << " sendMsg(u_int rolId, u_int service, u_int replyAddress, u_int parameter1, u_int parameter2)" << std::endl;    
    std::cout << "   " << SEND4               << " sendMsg(u_int rolId, u_int service, u_int replyAddress, u_int size, u_int *data)" << std::endl;    
    std::cout << "   " << RECEIVE             << " receiveMsg" << std::endl;
    std::cout << "   " << RPLX                << " readPlx" << std::endl;
    std::cout << "   " << WPLX                << " writePlx" << std::endl;
    std::cout << "   " << RFPGA               << " readFpga" << std::endl;
    std::cout << "   " << WFPGA               << " writeFpga" << std::endl;
    std::cout << "   " << RESET               << " reset" << std::endl;
    std::cout << "   " << GETMISC             << " getMiscMem" << std::endl;
    std::cout << "   " << RETMISC             << " retMiscMem" << std::endl;
    std::cout << "   " << FREEMSGRESOURCES    << " freeMsgResources" << std::endl;
    std::cout << "   " << GETPROCINFO         << " getProcInfo" << std::endl;
    std::cout << "   " << IRQWAIT             << " waitForInterrupt" << std::endl;
    std::cout << "   " << CLOSE               << " close"   << std::endl;
    std::cout << "   " << DESTRUCT            << " destructor" << std::endl;
    std::cout << "   0 Exit" << std::endl;
    std::cout << "Your choice" << std::endl;
    fun = getdecd(fun);    

    if (fun == CREATE) 
    {
      std::cout << "Enter the value for slotNumber: " << std::endl;
      slotNumber = getdecd(slotNumber);
      
      std::cout << "Enter the value for timeout: " << std::endl;
      timeout = getdecd(timeout);
            
      probin = new Robin(slotNumber, timeout);
      std::cout << "The Robin object has been created" << std::endl;
    }
    
    else if (probin == 0)
    {
      std::cout << "Seems you did not call the constructor yet...." << std::endl;
    }
    
    else if (fun == OPEN) 
    {
      try
      {
        probin->open();
      }
      catch (ROSRobinExceptions& e)
      {
        std::cout << e << std::endl;
        break;
      }
      std::cout << "The Robin object has been opened" << std::endl;
    }

    else if (fun == SETDMARAMPARAMS) 
    {
      std::cout << "Enter the value for dmaMemoryOrder: " << std::endl;
      dmaMemoryOrder = gethexd(dmaMemoryOrder);
      
      try
      {
        probin->setDmaRamParams(dmaMemoryOrder); 
      }
      catch (ROSRobinExceptions& e)
      {
        std::cout << e << std::endl;
        break;
      }
      std::cout << "The memory for direct DMA has been allocated" << std::endl;
    }

    else if (fun == RESERVEMSGRESOURCES) 
    {
      std::cout << "Enter the value for numberOfOutstandingReq: " << std::endl;
      numberOfOutstandingReq = getdecd(numberOfOutstandingReq);

      std::cout << "Enter the value for msgInputMemorySize: " << std::endl;
      msgInputMemorySize = gethexd(msgInputMemorySize);

      std::cout << "Enter the value for miscSize: " << std::endl;
      miscSize = gethexd(miscSize);

      std::cout << "Enter the value for eventSize: " << std::endl;
      eventSize = gethexd(eventSize);

      try
      {
        probin->reserveMsgResources(numberOfOutstandingReq, msgInputMemorySize, miscSize, eventSize); 
      }
      catch (ROSRobinExceptions& e)
      {
        std::cout << e << std::endl;
        break;
      }
      std::cout << "The message passing resources have been allocated" << std::endl;
    }

    else if (fun == GETVIRTEVENTBASE)
    {
      u_long evt_virt_base = probin->getVirtEventBase();
      std::cout << "The virtual base address of the fragment memory is 0x" << HEX(evt_virt_base) << std::endl;
      ra = evt_virt_base;
    }

    else if (fun == GETPHYSEVENTBASE)
    {
      u_int evt_phys_base = probin->getPhysEventBase();
      std::cout << "The virtual base address of the fragment memory is 0x" << HEX(evt_phys_base) << std::endl;
      ra = evt_phys_base;
    }
    
    else if (fun == GETVIRTMISCBASE)
    {
      u_int msc_virt_base = probin->getVirtMiscBase();
      std::cout << "The virtual base address of the fragment memory is 0x" << HEX(msc_virt_base) << std::endl;
      ra = msc_virt_base;
    }

    else if (fun == GETPHYSMISCBASE)
    {
      u_int msc_phys_base = probin->getPhysMiscBase();
      std::cout << "The virtual base address of the fragment memory is 0x" << HEX(msc_phys_base) << std::endl;
      ra = msc_phys_base;
    }
    
    else if (fun == SEND1)
    {
      std::cout << "Enter the ROL number: " << std::endl;
      rol = getdecd(rol);
      
      std::cout << "Enter the service number: " << std::endl;
      service = getdecd(service);     
      
      std::cout << "Enter the virtual reply address: " << std::endl;
      ra = gethexd(ra);       
      
      probin->sendMsg(rol, service, ra); 
      std::cout << "The message has been sent" << std::endl;
    }

    else if (fun == SEND2)
    {
      std::cout << "Enter the ROL number: " << std::endl;
      rol = getdecd(rol);
      
      std::cout << "Enter the service number: " << std::endl;
      service = getdecd(service);     
      
      std::cout << "Enter the virtual reply address: " << std::endl;
      ra = gethexd(ra);       
      
      std::cout << "Enter the additional word for the message: " << std::endl;
      p1 = getdecd(p1);
      
      probin->sendMsg(rol, service, ra, p1);
      std::cout << "The message has been sent" << std::endl;
    }

    else if (fun == SEND3)
    {
      std::cout << "Enter the ROL number: " << std::endl;
      rol = getdecd(rol);
      
      std::cout << "Enter the service number: " << std::endl;
      service = getdecd(service);     
      
      std::cout << "Enter the virtual reply address: " << std::endl;
      ra = gethexd(ra);       
      
      std::cout << "Enter the first additional word for the message: " << std::endl;
      p1 = getdecd(p1);
      
      std::cout << "Enter the second additional word for the message: " << std::endl;
      p2 = getdecd(p2);     
      
      probin->sendMsg(rol, service, ra, p1, p2);
      std::cout << "The message has been sent" << std::endl;
    }

    else if (fun == SEND4)
    {
      std::cout << "This method has not yet been implemented" << std::endl;
    }

    else if (fun == RECEIVE)
    {
      std::cout << "Enter the virtual reply address: " << std::endl;
      ra = gethexd(ra);  
      
      probin->receiveMsg(ra, 1); 
      std::cout << "The message has been received" << std::endl;
    }

    else if (fun == RPLX)
    {
      std::cout << "Enter the address of the register to read: " << std::endl;
      regaddr = gethexd(regaddr);
      regdata = probin->readPlx(regaddr);
      std::cout << "The register contains 0x" << HEX(regdata) << std::endl;
    }
    
    else if (fun == WPLX)
    {
      std::cout << "Enter the address of the register to write: " << std::endl;
      regaddr = gethexd(regaddr);
      
      std::cout << "Enter the data to write: " << std::endl;
      regdata = gethexd(regdata);
          
      probin->writePlx(regaddr, regdata);
      std::cout << "The data has bee written" << std::endl;
    }

    else if (fun == RFPGA)
    {
      std::cout << "Enter the address of the register to read: " << std::endl;
      regaddr = gethexd(regaddr);
      regdata = probin->readFpga(regaddr);
      std::cout << "The register contains 0x" << HEX(regdata) << std::endl;
    }
    
    else if (fun == WFPGA)
    {
      std::cout << "Enter the address of the register to write: " << std::endl;
      regaddr = gethexd(regaddr);
      
      std::cout << "Enter the data to write: " << std::endl;
      regdata = gethexd(regdata);
          
      probin->writeFpga(regaddr, regdata);
      std::cout << "The data has bee written" << std::endl;
    }
    
    else if (fun == RESET)
    {
      probin->reset(); 
      std::cout << "The Robin has been reset" << std::endl;
    }
    
    else if (fun == FREEMSGRESOURCES) 
    {
      try
      {
        probin->freeMsgResources(); 
      }
      catch (ROSRobinExceptions& e)
      {
        std::cout << e << std::endl;
        break;
      }
      std::cout << "The message passing resources have been deallocated" << std::endl;
    }
    
    else if (fun == GETPROCINFO) 
    {
      try
      {
        probin->getProcInfo(); 
      }
      catch (ROSRobinExceptions& e)
      {
        std::cout << e << std::endl;
        break;
      }
    }
    
    else if (fun == IRQWAIT) 
    {
      T_irq_info irq_info;
      u_int card;
      
      std::cout << "Waiting for an interrupt....." << std::endl;
      try
      {
        probin->waitForInterrupt(&irq_info); 
      }
      catch (ROSRobinExceptions& e)
      {
        std::cout << e << std::endl;
        break;
      }
      std::cout << ".....interrupt received" << std::endl;

      for(card = 0; card < MAXCARDS; card++)
      {
        if (irq_info.ch0_type[card])
  	  std::cout << "Card = " << card << ", channel = 0, type = " << irq_info.ch0_type[card] << ", ID = " << irq_info.ch0_id[card] << std::endl;
        if (irq_info.ch1_type[card])
  	  std::cout << "Card = " << card << ", channel = 1, type = " << irq_info.ch1_type[card] << ", ID = " << irq_info.ch1_id[card] << std::endl;
        if (irq_info.ch2_type[card])
  	  std::cout << "Card = " << card << ", channel = 2, type = " << irq_info.ch2_type[card] << ", ID = " << irq_info.ch2_id[card] << std::endl;
      } 
    }
    
    else if (fun == GETMISC) 
    {
      std::cout << "Enter the size in bytes: " << std::endl;
      msize = gethexd(msize);
      probin->getMiscMem(msize, &mstart_add, &mhandle);
      std::cout << "The start address is 0x" << (mstart_add) << std::endl;
      std::cout << "The handle is " << mhandle << std::endl;
    }

    else if (fun == RETMISC) 
    {
      std::cout << "Enter the handle: " << std::endl;
      mhandle = getdecd(mhandle);
      probin->retMiscMem(mhandle);
      std::cout << "The misc. memory has been returned" << std::endl;
    }  

    else if (fun == CLOSE) 
    {
      try
      {
        probin->close();
      }
      catch (ROSRobinExceptions& e)
      {
        std::cout << e << std::endl;
        break;
      }
      std::cout << "The Robin object has been closed" << std::endl;
    }

    else if (fun == DESTRUCT) 
    {
      delete probin;
      std::cout << "The Robin object has been deleted" << std::endl;
    }
  }
  std::cout << "=========================================================================" << std::endl << std::endl;
  return(0);
}


/****************/
int setdebug(void)
/****************/
{
  std::cout << "-----------------------------" << std::endl;
  std::cout << "Identifier | Description" << std::endl;
  std::cout << "-----------|----------------- " << std::endl;       
  std::cout << "         2 | ROSEventFragment" << std::endl;
  std::cout << "         5 | ROSMemoryPool" << std::endl;
  std::cout << "        14 | ROSRobin" << std::endl;
  std::cout << "       100 | cmem_rcc" << std::endl << std::endl;

  std::cout << "Enter the debug package: " << std::endl;
  dbpackage = getdecd(dbpackage);
  std::cout << "Enter the debug level: " << std::endl;
  dblevel = getdecd(dblevel);

  DF::GlobalDebugSettings::setup(dblevel, dbpackage);
  return(0);
}


/***************/
int errmenu(void)
/***************/
{
  int fun = 1;
  u_int loop, timeout, slotNumber = 0, rolId, l1id, getl1id, numberOfOutstandingReq, msgInputMemorySize, miscSize;
  u_int getev = 0, uploadsize, eventSize, *fptr, ticket = 0, npages, pageSize;
  Rol *prol = 0;
  Robin *rorobin;
  WrapperMemoryPool *m_memoryPool;
  MemoryPage *mem_page = NULL;
  StatisticsBlock statblock;

  //DF::GlobalDebugSettings::setup(20, 14);
  
  slotNumber             = 0;
  timeout                = 100;
  numberOfOutstandingReq = 2;
  msgInputMemorySize     = 0x200;
  miscSize               = 0x4000;
  npages                 = 10;
  pageSize               = 0x2000;
  eventSize              = npages * pageSize; 
  rolId                  = 0;
    
  try
  {
    rorobin = new Robin(slotNumber, timeout);
    rorobin->open();
    rorobin->reserveMsgResources(numberOfOutstandingReq, msgInputMemorySize, miscSize, eventSize); 
    prol = new Rol(*rorobin, rolId);
    prol->setConfig(enum_cfgRolEmu, enum_emuTypeUpload);
    prol->setConfig(enum_cfgPageSize, 256);
    prol->setConfig(enum_cfgMaxRxPages, 1);
    prol->setConfig(enum_cfgRolDataGen, 0);
    prol->setConfig(enum_cfgRolEnabled, 1);

    // Create a memoryPool structure around the event buffer
    u_long eventBase       = rorobin->getVirtEventBase();
    u_int physicalAddress = rorobin->getPhysEventBase();
    m_memoryPool = new WrapperMemoryPool(npages, pageSize, eventBase, physicalAddress);
  }
  catch (ROSRobinExceptions& e)
  {
    std::cout << e << std::endl;
    return(-1);
  }
  
  l1id = 0;
  std::cout << std::endl << "=========================================================================" << std::endl;
  while (1)  
  {
    std::cout << "Select an error type:" << std::endl;
    std::cout << "                                   Errors that are detected by the ROBIN" << std::endl;
    std::cout << "========================================================================" << std::endl;
    std::cout << "    " << ERR_1 << " A ROD fragment without error" << std::endl;
    std::cout << "    " << ERR_2 << " A duplicated fragment" << std::endl;
    std::cout << "    " << ERR_3 << " A missing ROD fragment" << std::endl;
    std::cout << "    " << ERR_4 << " Oversized fragment" << std::endl;
    std::cout << "    " << ERR_5 << " Out of sequence fragment" << std::endl;
    std::cout << "    " << ERR_6 << " Double BOF" << std::endl;
    std::cout << "    " << ERR_7 << " Double EOF" << std::endl;
    std::cout << "    " << ERR_8 << " Error bit 0 in ECW set" << std::endl;
    std::cout << "    " << ERR_9 << " Error bit 1 in ECW set" << std::endl;
    std::cout << "   " << ERR_10 << " Missing BOF" << std::endl;
    std::cout << "   " << ERR_11 << " Missing EOF" << std::endl;
    std::cout << "   " << ERR_12 << " No data between SCW and ECW" << std::endl;
    std::cout << "   " << ERR_13 << " Data but no L1ID between SCW and ECW" << std::endl;
    std::cout << "   " << ERR_14 << " L1ID but no complete ROD fragment between SCW and ECW" << std::endl;
    std::cout << "   " << ERR_15 << " Incorrect header marker" << std::endl;
    std::cout << "   " << ERR_16 << " Incorrect event format" << std::endl;
    std::cout << "   " << ERR_17 << " Size mismatch" << std::endl;
    std::cout << "========================================================================" << std::endl;
    std::cout << "   " << ERR_18 << " Clear statistics" << std::endl;
    std::cout << "   " << ERR_19 << " Reset ROL" << std::endl;
    std::cout << "   " << ERR_20 << " Show the number of special kept pages" << std::endl;
    std::cout << "   " << ERR_21 << " Get a corrupted fragment from the special kept pages" << std::endl;
    std::cout << "   " << ERR_22 << " Clear the special kept pages" << std::endl;
    std::cout << "    0 Exit" << std::endl;
    std::cout << "Your choice" << std::endl;
    fun = getdecd(fun);   
    
    std::cout << std::endl;
    std::cout << "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv" << std::endl;

    if (fun == 0)
    {
      delete prol;
      rorobin->freeMsgResources(); 
      rorobin->close();
      delete rorobin;
      return(0);
    }
    
    if (fun == ERR_1)
    {
      getl1id = l1id;
      emulate_err1(getl1id, prol);
      getev = 1;
    }  
    
    if (fun == ERR_2)
    {
      getl1id = l1id - 1;
      emulate_err1(getl1id, prol);
      getev = 1;
    }  
    
    else if (fun == ERR_3) 
    {
      l1id = l1id - 1;
      uploadsize = 0;
      getl1id = 0xf0000000;
      getev = 1;
    }  
    
    else if (fun == ERR_4) 
    {
      emulate_err7(l1id, prol);
      getl1id = l1id;
      getev = 1;
    } 
    
    else if (fun == ERR_5) 
    {
      emulate_err1(l1id + 10, prol);
      getl1id = l1id + 10;
      getev = 1;
    }
    
    else if (fun == ERR_6) 
    {
      emulate_err4(0, l1id, prol);
      getl1id = l1id;
      getev = 1;
    } 

    else if (fun == ERR_7) 
    {
      emulate_err4(1, l1id, prol);
      getl1id = l1id;
      getev = 1;
    }
    
    else if (fun == ERR_8) 
    {
      emulate_err5(0, l1id, prol);
      getl1id = l1id;
      getev = 1;
    } 
    
    else if (fun == ERR_9) 
    {
      emulate_err5(1, l1id, prol);
      getl1id = l1id;
      getev = 1;
    } 
    
    else if (fun == ERR_10) 
    {
      emulate_err6(l1id, prol);
      getl1id = l1id;
      getev = 1;
    } 
    
    else if (fun == ERR_11) 
    {
      emulate_err8(l1id, prol);
      getl1id = l1id;
      getev = 1;
      l1id++;
    }     
    
    else if (fun == ERR_12) 
    {
      emulate_err2(prol);
      getev = 0;
    }
     
    else if (fun == ERR_13) 
    {
      emulate_err3(0, l1id, prol);
      getev = 0;
    }
    
    else if (fun == ERR_14) 
    {
      emulate_err3(1, l1id, prol);
      getl1id = l1id;
      getev = 1;
    }
    
    else if (fun == ERR_15) 
    {
      emulate_err9(0, l1id, prol);
      getl1id = l1id;
      getev = 1;
    }
  
    else if (fun == ERR_16) 
    {
      emulate_err9(1, l1id, prol);
      getl1id = l1id;
      getev = 1;
    }

    else if (fun == ERR_17) 
    {
      emulate_err9(2, l1id, prol);
      getl1id = l1id;
      getev = 1;
    }
        
    else if (fun == ERR_18) 
    {
      prol->clearStatistics();
      std::cout << "Statistics reset." << std::endl;
      std::cout << std::endl;
      std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
      std::cout << std::endl;
      continue;
    }
    
    else if (fun == ERR_19) 
    {
      prol->reset();
      std::cout << "ROL reset." << std::endl;
      l1id = 0;
      std::cout << std::endl;
      std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
      std::cout << std::endl;
      continue;
    }
 
    else if (fun == ERR_20) 
    {
      statblock = prol->getStatistics();
      std::cout << "There are currently " << statblock.savedRejectedPages << " pages saved." << std::endl;
      std::cout << std::endl;
      std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
      std::cout << std::endl;
      continue;
    }

    else if (fun == ERR_21) 
    {
      dump_inmate(prol);
      continue;
    }

    else if (fun == ERR_22) 
    {
      prol->clearRejectedPages();
      std::cout << "The pecial pages have been erased" << std::endl;
      std::cout << std::endl;
      std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
      std::cout << std::endl;
      continue;
    }  
 
    if (getev)
    {
      try
      {
	mem_page = m_memoryPool->getPage();
      } 
      catch(std::exception& e)
      {
	std::cout << "Exception: " << e.what() << std::endl;
	return(-1);
      } 

      // Check if the allocated page is OK
      if (mem_page == 0) 
      {						
	std::cout << " Failed to get a page from the memory pool" << std::endl;
	return(-1);
      }

      std::cout << "Requesting fragment with L1ID 0x" << HEX(getl1id) << std::endl;
      u_int *fragBufferAddress = static_cast<u_int *> (mem_page->reserve(mem_page->capacity()));
      try
      {
	ticket = prol->requestFragment(getl1id, reinterpret_cast<u_long> (fragBufferAddress));
	fptr = prol->getFragment(ticket);      
      }
      catch (ROSRobinExceptions& e)
      {
	std::cout << e << std::endl;
	return(-1);
      }
      
      //std::cout << "Dumping raw data (first 20 words)....." << std::endl;
      //for(loop = 0; loop < 20; loop++)
      //  std::cout << "word " << loop << " = 0x" << HEX(fptr[loop]) << std::endl;

      std::cout << std::endl;
      std::cout << "Dumping relevant fields of the ROB header....." << std::endl;
      std::cout << "Start of header marker    = 0x" << HEX(fptr[0]) << std::endl;
      std::cout << "Total fragment size       = 0x" << HEX(fptr[1]) << std::endl;
      std::cout << "Number of status elements = 0x" << HEX(fptr[5]) << std::endl;
      std::cout << "Status element 1          = 0x" << HEX(fptr[6]) << std::endl;
      
      stat_decode(fptr[6]);
      
      std::cout << "Status element 2          = 0x" << HEX(fptr[7]) << std::endl;
      std::cout << "CRC type                  = " << fptr[8] << std::endl;
      std::cout << "L1ID                      = 0x" << HEX(fptr[14]) << std::endl;
      std::cout << "ROB trailer (CRC word)    = 0x" << HEX(fptr[fptr[1] - 1]) << std::endl;
      mem_page->free();
      l1id++;
    }

    std::cout << std::endl;
    std::cout << "Dumping new error and fragment statistics" << std::endl;
    statblock = prol->getStatistics();
    for(loop = 0; loop < enum_LAST_ERRCNT; loop++)
    {
      std::cout << "errors[" << std::setw(2) << loop << "](" << std::setw(29) << robinErrorStrings[loop] << ") = 0x" << HEX(statblock.errors[loop]) <<"    ";
      if (loop < enum_LAST_FRAGSTAT)
        std::cout << "fragStat[" << std::setw(2) << loop << "](" << std::setw(27) << robinFragStatStrings[loop] << ") = 0x" << HEX(statblock.fragStat[loop]) << std::endl;
      else
        std::cout << std::endl;
    }
    if (enum_LAST_FRAGSTAT > enum_LAST_ERRCNT)
      for(loop = 0; loop < (enum_LAST_FRAGSTAT - enum_LAST_ERRCNT); loop++)
        std::cout  << std::setw(60) << "fragStat[" << std::setw(2) << (loop + enum_LAST_ERRCNT + 1) << "](" 
        << std::setw(27) << robinFragStatStrings[loop + enum_LAST_ERRCNT + 1] << ") = 0x" << HEX(statblock.fragStat[loop + enum_LAST_ERRCNT + 1]) << std::endl;

    std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
  }

  return(0);
}
      
      
/*************************/      
void dump_inmate(Rol *prol)
/*************************/      
{      
  std::cout << "Enter the page number (0..99): " << std::endl;
  u_int pagenum = getdecd(0);
  Rol::KeptPage keptpage;

  keptpage = prol->getRejectedPage(pagenum);

  std::cout << "Most recent L1ID                     = 0x" << HEX(keptpage.rentry.mri) << std::endl;
  std::cout << "info.upfInfo.status                  = 0x" << HEX(keptpage.rentry.info.upfInfo.status) << std::endl;
  std::cout << "info.upfInfo.eventId                 = 0x" << HEX(keptpage.rentry.info.upfInfo.eventId) << std::endl;
  std::cout << "info.upfInfo.pageNum                 = 0x" << HEX(keptpage.rentry.info.upfInfo.pageNum) << std::endl;
  std::cout << "info.upfInfo.pageLen                 = 0x" << HEX(keptpage.rentry.info.upfInfo.pageLen) << std::endl;
  std::cout << "info.upfInfo.runNum                  = 0x" << HEX(keptpage.rentry.info.upfInfo.runNum) << std::endl;
  std::cout << "Number of words in rejected fragment = " << keptpage.pagedata_size << std::endl;

  if (keptpage.rentry.info.upfInfo.pageNum == 0)
    std::cout << "You have requested an invalid page" << std::endl;
  else
  {
    std::cout << "Now dumping the data of the rejected event" << std::endl;
    for (u_int loop = 0; loop < keptpage.pagedata_size; loop++)
      std::cout << "Word " << loop << " = 0x" << HEX(keptpage.pagedata[loop]) << std::endl;
  }
  std::cout << std::endl;
  std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
  std::cout << std::endl;
}
      
      
/**************************************/
void emulate_err1(u_int l1id, Rol *prol)
/**************************************/
{ 
  u_int word = 0, data[1024];

  data[word++] = 0xb0f00000; //start control word
  data[word++] = 0xee1234ee; //header marker
  data[word++] = 0x00000009; //header size
  data[word++] = 0x03010000; //format version
  data[word++] = 0x00000011; //source ID
  data[word++] = 0x00000022; //run number 
  data[word++] = l1id;       //L1ID
  data[word++] = 0x00000033; //BCID
  data[word++] = 0x00000044; //trigger type
  data[word++] = 0x00000055; //detector event type
  data[word++] = 0x00000000; //status word
  data[word++] = 0x00000000; //data word
  data[word++] = 0x00000001; //data word
  data[word++] = 0x00000002; //data word
  data[word++] = 0x00000003; //data word
  data[word++] = 0x00000004; //data word
  data[word++] = 0x00000005; //data word
  data[word++] = 0x00000006; //data word
  data[word++] = 0x00000007; //data word
  data[word++] = 0x00000008; //data word
  data[word++] = 0x00000009; //data word
  data[word++] = 0x00000001; //number of status elements
  data[word++] = 0x0000000a; //number of data elements
  data[word++] = 0x00000000; //status block position (before data)
  data[word++] = 0xe0f00000; //end control word

  std::cout << "Uploading regular fragment with L1ID 0x" << HEX(l1id) << std::endl;
  try
  {
    prol->loadFragment(word, data, enum_emuEventNoError);
  }
  catch (ROSRobinExceptions& e)
  {
    std::cout << e << std::endl;
    exit(-1);
  }
}

      
/**************************/
void emulate_err2(Rol *prol)
/**************************/
{ 
  u_int word = 0, data[1024];

  data[word++] = 0xb0f00000; //start control word
  data[word++] = 0xe0f00000; //end control word
  
  try
  {
    prol->loadFragment(word, data, enum_emuEventMemoryTag | enum_emuEventBofCtl | enum_emuEventEofCtl);
  }
  catch (ROSRobinExceptions& e)
  {
    std::cout << e << std::endl;
    exit(-1);
  }
}


/**************************************************/
void emulate_err3(u_int mode, u_int l1id, Rol *prol)
/**************************************************/
{ 
  u_int word = 0, data[1024];

  data[word++] = 0xb0f00000; //start control word
  data[word++] = 0xee1234ee; //header marker
  data[word++] = 0x00000009; //header size
  data[word++] = 0x03010000; //format version
  data[word++] = 0x00000011; //source ID
  data[word++] = 0x00000022; //run number 
                             //L1ID missing (header incomplete)
  if (mode)
  {
    data[word++] = l1id;       //L1ID
    data[word++] = 0x00000033; //BCID
    data[word++] = 0x00000044; //trigger type
    data[word++] = 0x00000055; //detector event type
    data[word++] = 0x00000000; //number of status elements
    data[word++] = 0x00000000; //number of data elements
                               //status block position missing (trailer incomplete)
    std::cout << "Uploading incomplete fragment with L1ID 0x" << HEX(l1id) << std::endl;
  }
  else
  {  
    std::cout << "Uploading incomplete fragment without L1ID" << std::endl;
  }
  
  data[word++] = 0xe0f00000; //end control word
  
  try
  {
    prol->loadFragment(word, data, enum_emuEventNoError);
  }
  catch (ROSRobinExceptions& e)
  {
    std::cout << e << std::endl;
    exit(-1);
  }
}


/**************************************************/
void emulate_err4(u_int mode, u_int l1id, Rol *prol)
/**************************************************/
{ 
  u_int word = 0, data[1024];

  if (mode == 0)
  {
    data[word++] = 0xb0f00000; //duplicated start control word

    try
    {
      prol->loadFragment(word, data, enum_emuEventMemoryTag | enum_emuEventBofCtl);
    }
    catch (ROSRobinExceptions& e)
    {
      std::cout << e << std::endl;
      exit(-1);
    }
  }

  word = 0;
  data[word++] = 0xb0f00000; //start control word
  data[word++] = 0xee1234ee; //header marker
  data[word++] = 0x00000009; //header size
  data[word++] = 0x03010000; //format version
  data[word++] = 0x00000011; //source ID
  data[word++] = 0x00000022; //run number 
  data[word++] = l1id; //L1ID
  data[word++] = 0x00000033; //BCID
  data[word++] = 0x00000044; //trigger type
  data[word++] = 0x00000055; //detector event type
  data[word++] = 0x00000000; //status word
  data[word++] = 0x00000000; //data word
  data[word++] = 0x00000001; //data word
  data[word++] = 0x00000002; //data word
  data[word++] = 0x00000003; //data word
  data[word++] = 0x00000004; //data word
  data[word++] = 0x00000005; //data word
  data[word++] = 0x00000006; //data word
  data[word++] = 0x00000007; //data word
  data[word++] = 0x00000008; //data word
  data[word++] = 0x00000009; //data word
  data[word++] = 0x00000001; //number of status elements
  data[word++] = 0x0000000a; //number of data elements
  data[word++] = 0x00000000; //status block position (before data)
  data[word++] = 0xe0f00000; //end control word
  
  std::cout << "Uploading fragment with L1ID 0x" << HEX(l1id) << std::endl;
  try
  {
    prol->loadFragment(word, data, enum_emuEventNoError);
  }
  catch (ROSRobinExceptions& e)
  {
    std::cout << e << std::endl;
    exit(-1);
  }
  
  if (mode == 1)
  {
    word = 0;
    data[word++] = 0xe0f00000; //duplicated end control word

    try
    {
      prol->loadFragment(word, data, enum_emuEventMemoryTag | enum_emuEventEofCtl);
    }
    catch (ROSRobinExceptions& e)
    {
      std::cout << e << std::endl;
      exit(-1);
    }
  }
}


/**************************************************/
void emulate_err5(u_int mode, u_int l1id, Rol *prol)
/**************************************************/
{ 
  u_int word = 0, data[1024];

  data[word++] = 0xb0f00000; //start control word
  data[word++] = 0xee1234ee; //header marker
  data[word++] = 0x00000009; //header size
  data[word++] = 0x03010000; //format version
  data[word++] = 0x00000011; //source ID
  data[word++] = 0x00000022; //run number 
  data[word++] = l1id; //L1ID
  data[word++] = 0x00000033; //BCID
  data[word++] = 0x00000044; //trigger type
  data[word++] = 0x00000055; //detector event type
  data[word++] = 0x00000000; //status word
  data[word++] = 0x00000000; //data word
  data[word++] = 0x00000001; //data word
  data[word++] = 0x00000002; //data word
  data[word++] = 0x00000003; //data word
  data[word++] = 0x00000004; //data word
  data[word++] = 0x00000005; //data word
  data[word++] = 0x00000006; //data word
  data[word++] = 0x00000007; //data word
  data[word++] = 0x00000008; //data word
  data[word++] = 0x00000009; //data word
  data[word++] = 0x00000001; //number of status elements
  data[word++] = 0x0000000a; //number of data elements
  data[word++] = 0x00000000; //status block position (before data)

  if (mode == 0)
    data[word++] = 0xe0f00001; //end control word
  else
    data[word++] = 0xe0f00002; //end control word

  std::cout << "Uploading fragment with L1ID 0x" << HEX(l1id) << std::endl;
  try
  {
    prol->loadFragment(word, data, enum_emuEventNoError);
  }
  catch (ROSRobinExceptions& e)
  {
    std::cout << e << std::endl;
    exit(-1);
  }
}


/**************************************/
void emulate_err6(u_int l1id, Rol *prol)
/**************************************/
{ 
  u_int word = 0, data[1024];

  data[word++] = 0xee1234ee; //header marker
  data[word++] = 0x00000009; //header size
  data[word++] = 0x03010000; //format version
  data[word++] = 0x00000011; //source ID
  data[word++] = 0x00000022; //run number 
  data[word++] = l1id; //L1ID
  data[word++] = 0x00000033; //BCID
  data[word++] = 0x00000044; //trigger type
  data[word++] = 0x00000055; //detector event type
  data[word++] = 0x00000000; //status word
  data[word++] = 0x00000000; //data word
  data[word++] = 0x00000001; //data word
  data[word++] = 0x00000002; //data word
  data[word++] = 0x00000003; //data word
  data[word++] = 0x00000004; //data word
  data[word++] = 0x00000005; //data word
  data[word++] = 0x00000006; //data word
  data[word++] = 0x00000007; //data word
  data[word++] = 0x00000008; //data word
  data[word++] = 0x00000009; //data word
  data[word++] = 0x00000001; //number of status elements
  data[word++] = 0x0000000a; //number of data elements
  data[word++] = 0x00000000; //status block position (before data)
  data[word++] = 0xe0f00000; //end control word

  try
  {
    std::cout << "Uploading fragment with L1ID 0x" << HEX(l1id) << ". BOF missing" << std::endl;
    prol->loadFragment(word, data, enum_emuEventMemoryTag | enum_emuEventHasData | enum_emuEventEofCtl);
  }
  catch (ROSRobinExceptions& e)
  {
    std::cout << e << std::endl;
    exit(-1);
  }
}

/**************************************/
void emulate_err7(u_int l1id, Rol *prol)
/**************************************/
{ 
  u_int loop, word = 0, data[1024];

  data[word++] = 0xb0f00000; //start control word
  data[word++] = 0xee1234ee; //header marker
  data[word++] = 0x00000009; //header size
  data[word++] = 0x03010000; //format version
  data[word++] = 0x00000011; //source ID
  data[word++] = 0x00000022; //run number 
  data[word++] = l1id; //L1ID
  data[word++] = 0x00000033; //BCID
  data[word++] = 0x00000044; //trigger type
  data[word++] = 0x00000055; //detector event type
  data[word++] = 0x00000000; //status word
  for (loop = 0; loop < 256; loop++)
    data[word++] = loop;     //data word
  data[word++] = 0x00000001; //number of status elements
  data[word++] = 256;        //number of data elements
  data[word++] = 0x00000000; //status block position (before data)
  data[word++] = 0xe0f00000; //end control word

  std::cout << "Uploading fragment with L1ID 0x" << HEX(l1id) << std::endl;
  try
  {
    prol->loadFragment(word, data, enum_emuEventNoError);
  }
  catch (ROSRobinExceptions& e)
  {
    std::cout << e << std::endl;
    exit(-1);
  }
}

/**************************************/
void emulate_err8(u_int l1id, Rol *prol)
/**************************************/
{ 
  u_int word = 0, data[1024];

  data[word++] = 0xb0f00000; //start control word
  data[word++] = 0xee1234ee; //header marker
  data[word++] = 0x00000009; //header size
  data[word++] = 0x03010000; //format version
  data[word++] = 0x00000011; //source ID
  data[word++] = 0x00000022; //run number 
  data[word++] = l1id; //L1ID
  data[word++] = 0x00000033; //BCID
  data[word++] = 0x00000044; //trigger type
  data[word++] = 0x00000055; //detector event type
  data[word++] = 0x00000000; //status word
  data[word++] = 0x00000000; //data word
  data[word++] = 0x00000001; //data word
  data[word++] = 0x00000002; //data word
  data[word++] = 0x00000003; //data word
  data[word++] = 0x00000004; //data word
  data[word++] = 0x00000005; //data word
  data[word++] = 0x00000006; //data word
  data[word++] = 0x00000007; //data word
  data[word++] = 0x00000008; //data word
  data[word++] = 0x00000009; //data word
  data[word++] = 0x00000001; //number of status elements
  data[word++] = 0x0000000a; //number of data elements
  data[word++] = 0x00000000; //status block position (before data)
  
  try
  {
    std::cout << "Uploading fragment with L1ID 0x" << HEX(l1id) << ". EOF missing" << std::endl;
    prol->loadFragment(word, data, enum_emuEventMemoryTag | enum_emuEventHasData | enum_emuEventBofCtl);
  }
  catch (ROSRobinExceptions& e)
  {
    std::cout << e << std::endl;
    exit(-1);
  }
  
  word = 0;
  data[word++] = 0xb0f00000; //start control word
  data[word++] = 0xee1234ee; //header marker
  data[word++] = 0x00000009; //header size
  data[word++] = 0x03010000; //format version
  data[word++] = 0x00000011; //source ID
  data[word++] = 0x00000022; //run number 
  data[word++] = l1id + 1  ; //L1ID
  data[word++] = 0x00000033; //BCID
  data[word++] = 0x00000044; //trigger type
  data[word++] = 0x00000055; //detector event type
  data[word++] = 0x00000000; //status word
  data[word++] = 0x00000000; //data word
  data[word++] = 0x00000001; //number of status elements
  data[word++] = 0x00000001; //number of data elements
  data[word++] = 0x00000000; //status block position (before data)
  data[word++] = 0xe0f00000; //end control word
  
  try
  {
    std::cout << "Uploading additional fragment with L1ID 0x" << HEX(l1id + 1) << std::endl;
    prol->loadFragment(word, data, enum_emuEventMemoryTag | enum_emuEventHasData | enum_emuEventEofCtl | enum_emuEventBofCtl);
  }
  catch (ROSRobinExceptions& e)
  {
    std::cout << e << std::endl;
    exit(-1);
  }
}


/**************************************************/
void emulate_err9(u_int mode, u_int l1id, Rol *prol)
/**************************************************/
{ 
  u_int word = 0, data[1024];

  data[word++] = 0xb0f00000; //start control word

  if (mode == 0)
    data[word++] = 0xee1111ee; //header marker
  else
    data[word++] = 0xee1234ee; //header marker

  data[word++] = 0x00000009; //header size
  
  if (mode == 1)
    data[word++] = 0x04010000; //format version
  else
    data[word++] = 0x03010000; //format version

  data[word++] = 0x00000011; //source ID
  data[word++] = 0x00000022; //run number 
  data[word++] = l1id;       //L1ID
  data[word++] = 0x00000033; //BCID
  data[word++] = 0x00000044; //trigger type
  data[word++] = 0x00000055; //detector event type

  if (mode == 3)  
    data[word++] = 0x10001000; //status word
  else
    data[word++] = 0x00000000; //status word

  data[word++] = 0x00000000; //data word
  data[word++] = 0x00000001; //data word
  data[word++] = 0x00000002; //data word
  data[word++] = 0x00000003; //data word
  data[word++] = 0x00000004; //data word
  data[word++] = 0x00000005; //data word
  data[word++] = 0x00000006; //data word
  data[word++] = 0x00000007; //data word
  data[word++] = 0x00000008; //data word
  data[word++] = 0x00000009; //data word
  data[word++] = 0x00000001; //number of status elements
  
  if (mode == 2)  
    data[word++] = 0x0000000b; //number of data elements
  else
    data[word++] = 0x0000000a; //number of data elements

  data[word++] = 0x00000000; //status block position (before data)
  data[word++] = 0xe0f00000; //end control word

  std::cout << "Uploading fragment with L1ID 0x" << HEX(l1id) << std::endl;
  try
  {
    prol->loadFragment(word, data, enum_emuEventNoError);
  }
  catch (ROSRobinExceptions& e)
  {
    std::cout << e << std::endl;
    exit(-1);
  }
}


/**************************/
void stat_decode(u_int data)
/**************************/
{
  if (data & 0xff)
  {
    std::cout << std::endl;
    std::cout << "The generic field of the status word contains these errors:" << std::endl;
    
    if (data & 0x1)
      std::cout << "Bit 0: An internal check of the BCID has failed" << std::endl;
    if (data & 0x2)
      std::cout << "Bit 1: An internal check of the EL1ID has failed" << std::endl;
    if (data & 0x4)
      std::cout << "Bit 2: A timeout in one of the modules has occurred. The fragment may be incomplete" << std::endl;
    if (data & 0x8)
      std::cout << "Bit 3: Data may be incorrect Further explanation in specific field" << std::endl;
    if (data & 0x10)
      std::cout << "Bit 4: On overflow in one of the internal buffers has occurred. The fragment may be incomplete" << std::endl;
    if (data & 0xffe0)
      std::cout << "There are additional generic error bits set that cannot be decoded" << std::endl;
    std::cout << std::endl;
  }
  
  if (data & 0xffff0000)
  {
    std::cout << "The specific field of the status word contains these errors:" << std::endl;

    if (data & 0x00010000)   
      std::cout << "Bit 16 Unused. If you see this you should be worried" << std::endl;
    if (data & 0x00020000)   
      std::cout << "Bit 17 Fragment size error: actual number of words does not match header + trailer + number of data words + number of status words (indicated in trailer)" << std::endl;
    if (data & 0x00040000)   
      std::cout << "Bit 18 Data block error: S-LINK transmission error on data block" << std::endl;
    if (data & 0x00080000)   
      std::cout << "Bit 19 CTL word error: S-LINK transmission error on control word (EOF or BOF)" << std::endl;
    if (data & 0x00100000)   
      std::cout << "Bit 20 Missing BOF: new fragment started without BOF (after preceeding one terminated with EOF)" << std::endl;
    if (data & 0x00200000)   
      std::cout << "Bit 21 Missing EOF: new fragment started with BOF, without the preceeding one terminated by EOF" << std::endl;
    if (data & 0x00400000)   
      std::cout << "Bit 22 Invalid header marker: the header marker was neither 0xee1234ee nor 0xee3412ee" << std::endl;
    if (data & 0x00800000)   
      std::cout << "Bit 23 Format error: the major format version (upper 16 bit) don't match expected format (currently 0x0301)" << std::endl;
    if (data & 0x01000000)   
      std::cout << "Bit 24 Duplicate event: when this fragment was received the ROBIN still had a fragment with the same L1ID in memory. The new fragment has replaced the older one" << std::endl;
    if (data & 0x02000000)   
      std::cout << "Bit 25 Sequence error: the L1ID of this ROD fragment was not in sequence with the L1ID of the fragment previously received (L1ID_new = L1ID_old + 1)" << std::endl;
    if (data & 0x04000000)   
      std::cout << "Bit 26 Tx error: general flag for a S-Link transmission or formatting error. See bits 16 thru 23" << std::endl;
    if (data & 0x08000000)   
      std::cout << "Bit 27 Truncation: the amount of data sent across S-Link for this fragment was larger than the maximum fragment size the ROBIN was configured to handle. Therefore this fragment has been truncated" << std::endl;
    if (data & 0x10000000)   
       std::cout << "Bit 28 Short fragment: the amount of data between the S-Link control words (BOF and EOF) was less than the size of an empty ROD fragment (ROD header + ROD trailer)" << std::endl;
   if (data & 0x20000000)   
      std::cout << "Bit 29 Lost: the ROBIN did not have a fragment for the requested L1ID. It therefore generated an empty fragment" << std::endl;
    if (data & 0x40000000)   
      std::cout << "Bit 30 Pending: the ROBIN did not have a fragment for the requested L1ID but this fragment may still arrive. It therefore generated an empty fragment" << std::endl;
    if (data & 0x80000000)   
      std::cout << "Bit 31 No data: general flag indicating that the ROBIN did not find a fragment with the requested L1ID. Set together with bits 29 or 30" << std::endl;
    std::cout << std::endl;
  }    
}


/****************/
int testmenu(void)
/****************/
{
  int fun = 1;
  u_int timeout, slotNumber = 0, rolId, l1id, numberOfOutstandingReq, msgInputMemorySize, miscSize;
  u_int eventSize, *fptr, ticket = 0, npages, pageSize;
  Rol *prol = 0;
  Robin *rorobin;
  WrapperMemoryPool *m_memoryPool;
  MemoryPage *mem_page = NULL;
  
  slotNumber             = 0;
  timeout                = 100;
  numberOfOutstandingReq = 2;
  msgInputMemorySize     = 0x200;
  miscSize               = 0x4000;
  npages                 = 10;
  pageSize               = 0x2000;
  eventSize              = npages * pageSize; 
  rolId                  = 0;
    
  try
  {
    rorobin = new Robin(slotNumber, timeout);
    rorobin->open();
    rorobin->reserveMsgResources(numberOfOutstandingReq, msgInputMemorySize, miscSize, eventSize); 
    prol = new Rol(*rorobin, rolId);
    prol->setConfig(enum_cfgRolEmu, enum_emuTypeUpload);
    prol->setConfig(enum_cfgPageSize, 256);
    prol->setConfig(enum_cfgMaxRxPages, 1);
    prol->setConfig(enum_cfgRolDataGen, 0);
    prol->setConfig(enum_cfgRolEnabled, 1);

    // Create a memoryPool structure around the event buffer
    u_long eventBase       = rorobin->getVirtEventBase();
    u_int physicalAddress = rorobin->getPhysEventBase();
    m_memoryPool = new WrapperMemoryPool(npages, pageSize, eventBase, physicalAddress);
  }
  catch (ROSRobinExceptions& e)
  {
    std::cout << e << std::endl;
    return(-1);
  }
  
  l1id = 0;
  std::cout << std::endl << "=========================================================================" << std::endl;
  while (1)  
  {
    std::cout << std::endl;
    std::cout << "Select a test:" << std::endl;
    std::cout << " 1  Test PCI interface (PCI bursts from Robin to ROS)" << std::endl;
    std::cout << " 2  Test PCI interface (R/W of PLX MBOX with single cycles)" << std::endl;
    std::cout << " 3  Set debugging parameters" << std::endl;
    std::cout << " 0  Exit" << std::endl;
    std::cout << "Your choice" << std::endl;
    fun = getdecd(fun);   
    
    std::cout << std::endl;
    std::cout << "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv" << std::endl;
    std::cout << std::endl;

    if (fun == 0)
    {
      delete prol;
      rorobin->freeMsgResources(); 
      rorobin->close();
      delete rorobin;
      return(0);
    }
    
    if (fun == 1)
    {
      u_int l1idtoget = 0, size, loop, word, data[1024];

      std::cout << "Enter the number of data words per fragment (max. 200): " << std::endl;
      size = getdecd(100);
      
      word = 0;
      data[word++] = 0xb0f00000; //start control word
      data[word++] = 0xee1234ee; //header marker
      data[word++] = 0x00000009; //header size
      data[word++] = 0x03010000; //format version
      data[word++] = 0x00000011; //source ID
      data[word++] = 0x00000022; //run number 
      data[word++] = 0;          //L1ID
      data[word++] = 0x00000033; //BCID
      data[word++] = 0x00000044; //trigger type
      data[word++] = 0x00000055; //detector event type
      data[word++] = 0x00000000; //status word

      for (loop = 0; loop < size; loop++)
	data[word++] = 0xaaaaaaaa;

      data[word++] = 0x00000001; //number of status elements
      data[word++] = size;       //number of data elements
      data[word++] = 0x00000000; //status block position (before data)
      data[word++] = 0xe0f00000; //end control word

      std::cout << "Uploading fragment with L1ID 0 and data pattern 0xaaaaaaaa" << std::endl;
      try
      {
	prol->loadFragment(word, data, enum_emuEventNoError);
      }
      catch (ROSRobinExceptions& e)
      {
	std::cout << e << std::endl;
	exit(-1);
      }

      word = 0;
      data[word++] = 0xb0f00000; //start control word
      data[word++] = 0xee1234ee; //header marker
      data[word++] = 0x00000009; //header size
      data[word++] = 0x03010000; //format version
      data[word++] = 0x00000011; //source ID
      data[word++] = 0x00000022; //run number 
      data[word++] = 1;          //L1ID
      data[word++] = 0x00000033; //BCID
      data[word++] = 0x00000044; //trigger type
      data[word++] = 0x00000055; //detector event type
      data[word++] = 0x00000000; //status word

      for (loop = 0; loop < size; loop++)
	data[word++] = 0x55555555;

      data[word++] = 0x00000001; //number of status elements
      data[word++] = size;       //number of data elements
      data[word++] = 0x00000000; //status block position (before data)
      data[word++] = 0xe0f00000; //end control word

      std::cout << "Uploading fragment with L1ID 1 and data pattern 0x55555555" << std::endl;
      try
      {
	prol->loadFragment(word, data, enum_emuEventNoError);
      }
      catch (ROSRobinExceptions& e)
      {
	std::cout << e << std::endl;
	exit(-1);
      }  
      
      //Allways receive the fragments in the same page
      try
      {
	mem_page = m_memoryPool->getPage();
      } 
      catch(std::exception& e)
      {
	std::cout << "Exception: " << e.what() << std::endl;
	return(-1);
      } 

      // Check if the allocated page is OK
      if (mem_page == 0) 
      {						
	std::cout << " Failed to get a page from the memory pool" << std::endl;
	return(-1);
      }

      u_int *fragBufferAddress = static_cast<u_int *> (mem_page->reserve(mem_page->capacity()));
      
      cont = 1;
      std::cout << "Press <ctrl>+\\ to terminate the test" << std::endl;
      u_int ftested = 0;
      while (cont)
      {
	try
	{
	  ticket = prol->requestFragment(l1idtoget, reinterpret_cast<u_long> (fragBufferAddress));
	  fptr = prol->getFragment(ticket);      
	}
	catch (ROSRobinExceptions& e)
	{
	  std::cout << e << std::endl;
	  return(-1);
	}      

        for (u_int loop = 0; loop < size; loop++)
	{
	  if (!l1idtoget && (fptr[19 + loop] != 0xaaaaaaaa))	  
	    std::cout << "Data word # " << loop << " is 0x" << HEX(fptr[19 + loop]) << " instead of 0xaaaaaaaa" << std::endl;

	  if (l1idtoget && (fptr[19 + loop] != 0x55555555))	  
	    std::cout << "Data word # " << loop << " is 0x" << HEX(fptr[19 + loop]) << " instead of 0x55555555" << std::endl;

        }
        l1idtoget = 1 - l1idtoget;
	ftested++;
	if (!(ftested & 0x3fff))
	  std::cout << ftested << " fragments tested so far." << std::endl;
      }
      
      mem_page->free();
    }  

    if (fun == 2)
    {  
      std::cout << "Not yet implemented" << std::endl;
      ///u_int data[0x100];
      ///u_int *iptr, vbase, wpattern = 0x55555555, rpattern;
      ///unsigned long long *llptr, lldata;
      ///vbase = rorobin->getDPMVbase();
      ///std::cout << "DPM is at " << HEX(vbase) << std::endl;
      ///llptr = (unsigned long long *)vbase;
      ///iptr = (u_int *)vbase;
      ///iptr[0] = 0x55555555;      
      ///iptr[1] = 0xaaaaaaaa;
      ///lldata = *llptr;
      /////*llptr = 0x1122334455667788;
      ///iptr[2] = 0xbbbbbbbb;      
      ///iptr[3] = 0xcccccccc;
      ///memcpy(data, (void *)vbase, 0x100);
    }
    
    if (fun == 3) 
      setdebug();

    std::cout << std::endl;
    std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
    std::cout << std::endl;
  }

  return(0);
}


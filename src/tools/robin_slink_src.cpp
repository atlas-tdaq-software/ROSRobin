/*****************************************************************************/
/*                                                                           */
/* File Name        : robin_slink_src.cpp                                    */
/*                                                                           */
/* Author           : Markus Joos			                     */
/*                                                                           */
/***** C 2009 - A nickel program worth a dime ********************************/

#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <cmdl/cmdargs.h>
#include <tmgr/tmresult.h>
#include "ROSEventFragment/ROBFragment.h"
#include "ROSRobin/ROSRobinExceptions.h"
#include "ROSRobin/Robin.h"
#include "ROSRobin/Rol.h"
#include "ROSBufferManagement/Buffer.h"
#include "ROSMemoryPool/MemoryPage.h"
#include "ROSMemoryPool/WrapperMemoryPool.h"
#include "ROSGetInput/get_input.h"
#include "DFDebug/DFDebug.h"
#include "ROSRobin/robin.h"

using namespace ROS;
using namespace daq::tmgr;

#ifndef TRUE
  #define TRUE                  0x01
#endif

#ifndef FALSE
  #define FALSE                 0x00
#endif

#define MAX_EV_SIZE 0x10000

// global variables
int verbose = FALSE;
int occurence = 1;
int readfile = FALSE;
u_int page_size = 256, max_rx_pages = 1;
char filename[200] = {0};
int dev;
u_int dblevel = 0, dbpackage = 14;
Rol *prol = 0;
Robin *rorobin;

// Constants
#define MAX_PACKET_SIZE  (1024 * 1024)  // = 1 MB 
#define MAXROLS          18 


/**************/
void usage(void)
/**************/
{
  std::cout << "Valid options are ..." << std::endl;
  std::cout << "-o x: Occurence (= ROL number 1..N)                -> Default: " << occurence << std::endl;
  std::cout << "-p x: Robin PageSize (in words)                    -> Default: " << page_size << std::endl;
  std::cout << "-m x: Robin MaxRxPages                             -> Default: " << max_rx_pages << std::endl;
  std::cout << "-f x: Read data from file. The parameter is the path and the name of the file" << std::endl;
  std::cout << "-v  : Verbose output                               -> Default: FALSE" << std::endl;
  std::cout << "-d  : Debug level                                  -> Default: 0" << std::endl;
  std::cout << "-D  : Debug package                                -> Default: 14" << std::endl;
  std::cout << std::endl;
}


/*********************/
void terminate_it(void) 
/*********************/
{
  delete prol;
  std::cout << "prol deleted" << std::endl;
  rorobin->freeMsgResources(); 
  std::cout << "message resources returned" << std::endl;
  delete rorobin;  
  std::cout << "rorobin deleted" << std::endl;
}


/*****************************/
int main(int argc, char **argv)
/*****************************/
{
  u_int word_number;
  int c, robin_nr, rol_nr;
  u_int numberOfOutstandingReq, msgInputMemorySize, miscSize;
  u_int eventBufferSize;
  u_int fret, dummy, event_id = 0, data_words[MAX_EV_SIZE], event_size, edloop; 

  static struct option long_options[] = {"DVS", no_argument, NULL, '1'}; 
 
  while ((c = getopt_long(argc, argv, "p:m:o:f:vd:D:h", long_options, NULL)) != -1)
    switch (c) 
    {
    case 'h':
      std::cout << "Usage: " << argv[0] << " [options]: "<< std::endl;
      usage();
      exit(-1);
      break;

    case 'o':
      occurence = atoi(optarg);
      if (occurence <= 0 || occurence > MAXROLS)
      { 
	std::cout << "Occurence exceeds allowed bounds" << std::endl;
        exit(-1);
      }
      break;
                  
    case 'f':   
      readfile = TRUE;
      sscanf(optarg,"%s", filename);
      std::cout << "read data from file " << filename << std::endl;
      break;
      
    case 'v': verbose = TRUE;               break;
    case 'd': dblevel = atoi(optarg);       break;
    case 'p': page_size = atoi(optarg);     break;
    case 'm': max_rx_pages = atoi(optarg);  break;
    case 'D': dbpackage = atoi(optarg);     break;                   

    default:
      std::cout << "Invalid option " << c << std::endl;
      std::cout << "Usage: " << argv[0] << " [options]: " << std::endl;
      usage();
      exit (-1);
    }

  // Initialize the debug macro
  DF::GlobalDebugSettings::setup(dblevel, dbpackage);      
      
  robin_nr = (occurence - 1) / 3;
  rol_nr = (occurence - 1) % 3;
  std::cout << "Opening access to ROL " << (rol_nr + 1) << " on ROBIN " << (robin_nr + 1) << std::endl;
   
  try
  {
    rorobin = new Robin(robin_nr, 100.0);
    rorobin->open();
    prol = new Rol(*rorobin, rol_nr);
    
    numberOfOutstandingReq = 2;
    msgInputMemorySize = 0x200;
    miscSize = 0x4000;
    eventBufferSize = 0x20000;
    rorobin->reserveMsgResources(numberOfOutstandingReq, msgInputMemorySize, miscSize, eventBufferSize);
    
    prol->setConfig(enum_cfgRolEmu, enum_emuTypeUpload);
    prol->setConfig(enum_cfgPageSize, page_size);
    prol->setConfig(enum_cfgMaxRxPages, max_rx_pages);
    prol->setConfig(enum_cfgRolDataGen, 0);
    prol->setConfig(enum_cfgRolEnabled, 1);  
  }
  catch (ROSRobinExceptions& e)
  {
    std::cout << e << std::endl;
    exit(-1);
  }

  //Open the input file
  FILE *inf;
  inf = fopen(filename, "r");
  if(inf == 0)
  {
    std::cout << "Can't open input file " << filename << std::endl;
    exit(-1);
  }

  word_number = 0;

  while(!feof(inf))
  {
     //Read fragment size
     fret = fscanf(inf, "%x", &event_size);
     if (fret != 1)
       break;
     word_number++;
     event_size += 3; //Add ROD trailer

     if (verbose)
       std::cout << "Event size = " << event_size << " words." << std::endl;

     if (event_size > MAX_EV_SIZE)
     {
       std::cout << "Event is too big" << std::endl;
       exit(-1);
     }

     //Read the event data
     data_words[0] = 0xb0f00000; //start control word
     for (edloop = 1; edloop <= event_size; edloop++)
     {
       fscanf(inf, "%x", &data_words[edloop]);
       word_number++;
       if (verbose && edloop < 15)
         std::cout << "Data word " << word_number << " is " << HEX(data_words[edloop]) << std::endl;
     }

     data_words[edloop] = 0xe0f00000; //end control word
     fscanf(inf, "%x", &dummy);  //Read ROB CRC
     word_number++;

     //write the ROD fragment to the memory of the Robin
     try
     {
       data_words[6] = event_id;  //Replace the original event ID
       prol->loadFragment(event_size + 2, data_words, enum_emuEventNoError);
      
       if (verbose)
         std::cout << "Event with L1ID 0x" << HEX(data_words[6]) << " loaded. Event size: " << event_size << std::endl;
       event_id++;
     }
     catch (ROSRobinExceptions& e)
     {
       std::cout << e;
       exit (-2);
     }

  } 
  std::cout << "In total the file had " << word_number << " words and " << event_id << " events." << std::endl;

  terminate_it();
  exit(0); 
}

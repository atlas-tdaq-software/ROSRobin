// --------x----------------------------x-----------------------------------x--
/** 
 *  \file   FlashProgrammer.h
 *
 *  \author M.Mueller (mmueller@ti.uni-mannheim.de)
 *  
 *  \date   27.01.2005
 *
 *  
 *
 */
// --------x----------------------------x-----------------------------------x--

/*
 * $Id$
 * $Revision$
 * $Log$
 * Revision 1.3  2008/12/03 10:18:43  joos
 * (some) compiler warnings fixed
 *
 * Revision 1.2  2007/07/18 06:49:43  joos
 * spelling fixed
 *
 * Revision 1.1  2005/06/28 14:41:28  mmueller
 * Added ppcflash functionality.
 *
 *
 *
 */

/** This file implements the class FlashProgrammer 
 *  PowerPC Flash through a special FPGA design. The special FPGA design
 *  has to be programmed in advance and has to match to the version expected by
 *  this class
 */

// --------x----------------------------x-----------------------------------x--

#include <iostream>
#include <new>
#include <exception>
#include <fstream>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "ROSRobin/Robin.h"
#include "ROSRobin/ROSRobinExceptions.h"
#include "rcc_time_stamp/tstamp.h"

#include "FlashProgrammer.h"


// --------x----------------------------x-----------------------------------x--
// Constants

// The design ID constants
const unsigned int ROS::FlashProgrammer::c_flashDesignId_ROBIN1_1500 = 0x12350102;
const unsigned int ROS::FlashProgrammer::c_flashDesignId_ROBIN1_2000 = 0x12350104;
const unsigned int ROS::FlashProgrammer::c_flashDesignId_ROBIN2_2000 = 0x12350205;

// FPGA addresses
const unsigned int ROS::FlashProgrammer::c_designIdRegister = 0;
const unsigned int ROS::FlashProgrammer::c_controlRegister  = 1 << 2;
const unsigned int ROS::FlashProgrammer::c_addressRegister  = 2 << 2;
const unsigned int ROS::FlashProgrammer::c_dataOutRegister  = 3 << 2;
const unsigned int ROS::FlashProgrammer::c_dataInRegister   = 4 << 2;


// Flash dedicated constants
const unsigned int ROS::FlashProgrammer::c_flashOffset= 0xff800000;
const unsigned int ROS::FlashProgrammer::c_sectorSize_robin1 = 0x20000;
const unsigned int ROS::FlashProgrammer::c_numSectors_robin1 = 64;
const unsigned int ROS::FlashProgrammer::c_sectorSize_robin2 = 0x10000;
const unsigned int ROS::FlashProgrammer::c_numSectors_robin2 = 128;

// define control register bits
// use lower 16 bits only
const unsigned int ROS::FlashProgrammer::c_ctlFCS        = 0;
const unsigned int ROS::FlashProgrammer::c_ctlOE         = 1;
const unsigned int ROS::FlashProgrammer::c_ctlBE0        = 2;
const unsigned int ROS::FlashProgrammer::c_ctlBE1        = 3;
const unsigned int ROS::FlashProgrammer::c_ctlLEDBase    = 4;
const unsigned int ROS::FlashProgrammer::c_ctlLastCTLBit = c_ctlLEDBase + 3;	// 4 user leds
const unsigned int ROS::FlashProgrammer::c_statHoldAck = 16;	


// ***************************************************************************
ROS::FlashProgrammerException::FlashProgrammerException(std::string message) :
// ***************************************************************************
ROSRobinExceptions(static_cast<ROS::ROSRobinExceptions::ErrorCode>(0), message), m_message(message)
{
}


// *******************************************************************
ROS::FlashProgrammerException::~FlashProgrammerException(void) throw()
// *******************************************************************
{
}

// **************************************************************************************
std::string ROS::FlashProgrammerException::getErrorString(unsigned int /*errorId*/) const
// ************************************************************************************** 

{
  return "Robin Flash Programmer Exception: " + m_message;
}


/** This constructor creates an FlashProgrammer object with a reference on a
    ROBIN prototype board. First the internal board variable and the board
    type are initialised. The the board is opened if required and a sanity
    check is done.
*/
// **************************************************************
ROS::FlashProgrammer::FlashProgrammer(ROS::Robin &  robinBoard) :
// **************************************************************
  m_robin(robinBoard), m_boardType(FlashProgrammer::UNKNOWN), m_timeout(10000000)
{
  // Get the number of ROLs to determine the board type:
  //       2 -> ROBIN1
  //       3 -> ROBIN2
  //  others -> throw exception   
  if (robinBoard.getNrOfRols() == 2) {
    
    std::cout << "Board is ROBIN Prototype" << std::endl;

    m_boardType = ROBIN1_BOARD;
    
  }
  else if (robinBoard.getNrOfRols() == 3) {

    std::cout << "Board is 3 ROL ROBIN" << std::endl;
    
    m_boardType = ROBIN2_BOARD;

  }
  else {
    throw ROS::FlashProgrammerException("ROBIN board not recognised");
  }

  // check the design ID
  unsigned int id = m_robin.readFpga( c_designIdRegister );

  std::cout << "Design ID: " << std::hex << id << std::dec << std::endl;

  // If the FPGA firmware ID does not match: goodbye!!
  if (m_boardType == ROBIN1_BOARD) {
    if ((id != c_flashDesignId_ROBIN1_1500) && (c_flashDesignId_ROBIN1_2000)) {
      throw ROS::FlashProgrammerException("FPGA firmware version does not match!");
    }
  }

  if (m_boardType == ROBIN2_BOARD) {
    if (id != c_flashDesignId_ROBIN2_2000) {
      throw ROS::FlashProgrammerException("FPGA firmware version does not match!");
    }
  }

  // For the ROBIN2 check if the PPC bus has been locked up.
  if (m_boardType == ROBIN2_BOARD) {
    // If this is a ROBIN 2 we have to wait for an ackn.
    unsigned int stat;
    unsigned int mask = 1 << c_statHoldAck; 
    
    // Read the status from the control register and wait until the
    // achnowledge comes up.
    tstamp startTime, currentTime; 
    ts_clock(&startTime);
    do {
      // Get the status
      ts_clock(&currentTime);
      if (ts_duration(startTime, currentTime) > m_timeout) {
	throw ROS::ROSRobinExceptions(ROS::ROSRobinExceptions::TIMEOUT, 
				      "FlashProgrammer: PPC bus did not lock");
      }
      stat = m_robin.readFpga(c_controlRegister);  

    } while ( ( stat & mask ) == 0 );
  }
  
}

// --------x----------------------------x-----------------------------------x--

// *****************************************  
ROS::FlashProgrammer::~FlashProgrammer(void) 
// *****************************************
{

}

// --------x----------------------------x-----------------------------------x--
/** This method sends a reset command to the flash device which brings it back
    to read mode.
*/
// ***********************************
void ROS::FlashProgrammer::reset(void) 
// ***********************************
{
  unsigned int cmd;
  unsigned int ctl;
  unsigned int ctlMask;
  // reset sequence: f0
  const unsigned short cmdCode = 0xf0;

  // For the ROBIN 1 enable the both flash chips. This is done by asserting
  // both byte enable signals
  if (m_boardType == ROBIN1_BOARD) {              // This is for ROBIN 1
      ctlMask = (1 << c_ctlBE1) | (1 << c_ctlBE0) | (1 << c_ctlFCS);
  }
  else {                                         // And this for ROBIN 2
    ctlMask = (1 << c_ctlBE0) | (1 << c_ctlFCS);
  }
  
  // address is don't care
  // copy data for 2 devices. For one (ROBIN2) it does not hurt.
  cmd = cmdCode | (cmdCode << 8);
  // move to upper word
  cmd <<= 16; 
// write data
  m_robin.writeFpga(c_dataOutRegister, cmd);

  // Get the current content of the control register for modification  
  ctl = m_robin.readFpga(c_controlRegister);

  // assert be0, be1, fcs
  ctl &= ~ctlMask;

  // write ctl value
  m_robin.writeFpga(c_controlRegister, ctl);

  // wait access 
  m_robin.readFpga(c_designIdRegister);

  // de-assert be0, be1, fcs
  ctl |= ctlMask;
  m_robin.writeFpga(c_controlRegister, ctl);

}
// --------x----------------------------x-----------------------------------x--
/** Write a data character to the flash
 */
// ***********************************************************************
void ROS::FlashProgrammer::write(unsigned int address, unsigned char data) 
// ***********************************************************************
{

  // commands for write sequence: aa 55 a0 data
  const unsigned int cmdAndDataSize = 4;
  unsigned short cmdCodeAndData[cmdAndDataSize] = {0xaa, 0x55, 0xa0, 0x0};
  unsigned int i;
  unsigned int cmd, ctl, ctlMask;
  unsigned int physAddress;
  unsigned int cmdAddr;

  // Write the address to the address register
  // In case of ROBIN 1 we have to write a 16 bit word address!
  if (m_boardType == ROBIN1_BOARD) {    
    physAddress = address >> 1;
  }
  else {
    physAddress = address;
  }

  m_robin.writeFpga( c_addressRegister, physAddress);
  
  // The byte enables in the control register determine to which flash
  // device the data will get written. On ROBIN 2 this is clear. On ROBIN 1 the
  // LSBit of the address decides.
  if (m_boardType == ROBIN1_BOARD) {              // This is for ROBIN 1

    if ((address & 0x1) == 1) {
      // Set the mask for the byte enables
      ctlMask = (1 << c_ctlBE1) | (1 << c_ctlFCS);
   }
    else {
      ctlMask = (1 << c_ctlBE0) | (1 << c_ctlFCS);
    }

  }
  else {                                         // And this for ROBIN 2

    ctlMask = (1 << c_ctlBE0) | (1 << c_ctlFCS);
 
  }

  // Add the value to be written to the command and data stream.
  cmdCodeAndData[cmdAndDataSize - 1] = data;

  // Now write the command sequence and then the data to the flash!
  // The byte enables decide which of the two devices are really written
  // in case of the ROBIN 1
  for (i = 0; i < cmdAndDataSize; i++){

    cmd = cmdCodeAndData[i]; 
    // move to upper word
    if ((m_boardType == ROBIN1_BOARD) && ((address & 0x1) == 1)) {  
      cmd <<= 16;
    }
    else {
      cmd <<= 24;
    }

    // write data
    // First command sequence. In the PCI-Express Robin, these have to be written to specific locations
    // In case of old Robin, the location doesn't matter, so the code below should also work in case
    //of the "old" Robin.
    switch(i) {
    case 0: cmdAddr = cmdBaseAddress + 0xaaa; break;
    case 1: cmdAddr = cmdBaseAddress + 0x555; break;
    case 2: cmdAddr = cmdBaseAddress + 0xaaa; break;
    default: cmdAddr = address;
    }
    m_robin.writeFpga( c_addressRegister, cmdAddr);
    m_robin.writeFpga(c_dataOutRegister, cmd);
    
    
    // get ctl value
    ctl = m_robin.readFpga(c_controlRegister);
    // assert be0, be1, fcs
    ctl &= ~ctlMask;
    // write ctl value
    m_robin.writeFpga(c_controlRegister, ctl);

    // Wait a bit. 1 us may be sufficient
    ts_delay(2);      

    // de-assert be0, be1, fcs
    ctl |= ctlMask;
    // write ctl
    m_robin.writeFpga(c_controlRegister, ctl);

  }

  // Wait a bit until the programming algorithm of the device has started
  ts_delay(2);

  // Finally we poll for completion
  tstamp startTime, currentTime; 
  bool programmed = false;
  unsigned int status;

  // Enable OE and disable the BE's that we can read from the device
  ctlMask = (1 << c_ctlOE) | (1 << c_ctlFCS);
  ctl = m_robin.readFpga(c_controlRegister);
  ctl &= ~ctlMask;
  m_robin.writeFpga(c_controlRegister, ctl);

  // Read from the device until D7 equals the data value which has to be programmed
  // If D5 is raised the device signals a timeout! The we break the loop  and check 
  // if the data could be programmed anyway.
  // ( see the AMD flash data book!)
  ts_clock(&startTime);
  do {
    
    // Timeout condition
    ts_clock(&currentTime);
    if (ts_duration(startTime, currentTime) > m_timeout) {
      throw ROS::ROSRobinExceptions(ROS::ROSRobinExceptions::TIMEOUT, 
				    "FlashProgrammer: Write did not finish");
    }

    // Read the device status
    status = m_robin.readFpga(c_dataInRegister);

    // Adjust it in case of the ROBIN Prototype and depending on the address
    if ((m_boardType == ROBIN1_BOARD) && ((address & 0x1) == 1)) {  
      status = (status >> 16) & 0xFF;
    }
    else {
      status = status >> 24;
    }

    // Bit D7 is NOT Data bit 7 until the programming has been finalised
    if ((status & 0x80) == (data & 0x80)) {
      programmed = true;
    }

    // If D5 raises we break the loop and signal an error if the value does
    // not match the data.
    if ((status & 0x10) != 0) {
      programmed = true;
    }
       
  }
  while (!programmed);

  ctl |= ctlMask;
  // write ctl
  m_robin.writeFpga(c_controlRegister, ctl);

  // Finally read the data value from the device to check if programming could be done
  unsigned char val = read(address);
  if (val != data) {    
    throw ROS::FlashProgrammerException("Verification of the programmed char failed");
  }

  
}

// --------x----------------------------x-----------------------------------x--

// ********************************************************************************************************
void ROS::FlashProgrammer::write(unsigned int /*address*/, unsigned char * /*data*/, unsigned int /*size*/) 
// ********************************************************************************************************
{

}

// --------x----------------------------x-----------------------------------x--

// ***********************************************************  
unsigned char ROS::FlashProgrammer::read(unsigned int address) 
// ***********************************************************
{

  unsigned char val;
  unsigned int ctl;
  unsigned int ctlMask;
  unsigned int rawData;
  unsigned int physAddress;

  // Write the address to the address register
  // In case of ROBIN 1 we have to write a 16 bit word address!
  if (m_boardType == ROBIN1_BOARD) {    
    physAddress = address >> 1;
  }
  else {
    physAddress = address;
  }
  m_robin.writeFpga( c_addressRegister, physAddress);

  // The byte enables in the control register determine to which flash
  // device the data will get written. On ROBIN 2 this is clear. On ROBIN 1 the
  // LSBit of the  address decides.
  if (m_boardType == ROBIN1_BOARD) {              // This is for ROBIN 1

    if ((address & 0x1) == 1) {
      // Set the mask for the byte enables
      ctlMask = (1 << c_ctlBE1) | (1 << c_ctlFCS) | (1 << c_ctlOE);
   }
    else {
      ctlMask = (1 << c_ctlBE0) | (1 << c_ctlFCS) | (1 << c_ctlOE);
    }

  }
  else {                                         // And this for ROBIN 2

    ctlMask = (1 << c_ctlOE) | (1 << c_ctlFCS);
 
  }
  
  // Now set the OE and FCS
  // Get the current content of the control register for modification
  ctl = m_robin.readFpga(c_controlRegister);
  // Modify the control register value to assert OE and FCS
  ctl &= ~ctlMask;
  // Write the new value back to the control register
  m_robin.writeFpga(c_controlRegister, ctl);
  
    // Wait a bit. 2 us may be sufficient
  ts_delay(2);   

  rawData = m_robin.readFpga(c_dataInRegister);

  // ROBIN 1 uses a 16 bit access to two flash devices.
  // ROBIN 2 uses only one flash device which is 8 bit.
  // We have to distinguish between both!
  if (m_boardType == ROBIN1_BOARD) {              // This is for ROBIN 1

    if ((address & 0x1) == 1) {
      val = (unsigned char )((rawData >> 16) & 0xFF);
    }
    else {
      val = (unsigned char )((rawData >> 24) & 0xFF);
    }

  }
  else {                                         // And this for ROBIN 2

    val = (unsigned char )((rawData >> 24) & 0xFF);

  }
  
  // Modify the control register value to de-assert OE and FCS again
  ctl |= ctlMask;
  // Write the new value back to the control register
  m_robin.writeFpga(c_controlRegister, ctl);

  return val;

}

// --------x----------------------------x-----------------------------------x--

//********************************************************************************************************
void ROS::FlashProgrammer::read(unsigned int /*address*/, unsigned char * /*data*/, unsigned int /*size*/) 
//********************************************************************************************************
{
}

// --------x----------------------------x-----------------------------------x--

//**********************************************************
void ROS::FlashProgrammer::erase(unsigned int sectorAddress) 
//**********************************************************
{
  unsigned int ctl, cmd, ctlMask;
  unsigned int physAddress;
  const int cmdSize = 6;
  // write sequence: aa 55 80 aa 55 10
  const unsigned short cmdCode[cmdSize] = {0xaa, 0x55, 0x80, 0xaa, 0x55, 0x30};
  unsigned int cmdAddr;
  
  // Always a complete sector is erased! On the ROBIN Prototype two flash memories are present
  // This means a sector has twice the size then on the 3 ROL ROBIN.

  // Check the sector start address!
  if (m_boardType == ROBIN1_BOARD) {

    if ((sectorAddress & 0x1FFFF) != 0) {
      throw ROS::FlashProgrammerException("Erase must start on a sector boundary!");
    }
    ctlMask = (1 << c_ctlBE1) | (1 << c_ctlBE0) | (1 << c_ctlFCS);
    physAddress = sectorAddress >> 1;

  }
  else {

    if ((sectorAddress & 0xFFFF) != 0) {
      throw ROS::FlashProgrammerException("Erase must start on a sector boundary!");
    }
    ctlMask = (1 << c_ctlBE0) | (1 << c_ctlFCS);
    physAddress = sectorAddress;

  }

  // Write the real address to the device(s)
  m_robin.writeFpga(c_addressRegister, physAddress);


  // Program the command sequence
  for (int i=0;i<cmdSize;i++){

    // copy data for 2 devices, don´t care for Robin 2
    cmd = cmdCode[i] | (cmdCode[i] << 8);

    // move to upper word
    cmd <<= 16;
    
    //write data
    // First command sequence. In the PCI-Express Robin, these have to be written to specific locations
    // In case of old Robin, the location doesn't matter, so the code below should also work in case
    // of the PCI-X Robin
    switch(i) {
    case 0: cmdAddr = sectorAddress + 0xaaa; break;
    case 1: cmdAddr = sectorAddress + 0x555; break;
    case 2: cmdAddr = sectorAddress + 0xaaa; break;
    case 3: cmdAddr = sectorAddress + 0xaaa; break;
    case 4: cmdAddr = sectorAddress + 0x555; break;
    default: cmdAddr = sectorAddress;
    }
    m_robin.writeFpga( c_addressRegister, cmdAddr);
    m_robin.writeFpga(c_dataOutRegister, cmd);

    // Get the current content of the control register for modification
    ctl = m_robin.readFpga(c_controlRegister);
    // Modify the control register value to assert OE and FCS
    ctl &= ~ctlMask;
    // Write the new value back to the control register
    m_robin.writeFpga(c_controlRegister, ctl);

    // Wait a bit
    ts_delay(10);

    // de-assert be0, be1, fcs
    ctl |= ctlMask;
    // write ctl
    m_robin.writeFpga(c_controlRegister, ctl);
  }

  // Wait a bit until the erase algorithm of the device has started
  ts_delay(1);

  // Finally we poll for completion
  // Set the physical address to the end of the sector
   physAddress |= 0xFFFF;
   m_robin.writeFpga(c_addressRegister, physAddress);
  
  // Calculate what must be coming out of the chips data signals
  unsigned int mask;
  // move to upper word
  if (m_boardType == ROBIN1_BOARD) {  
    mask = 0x80 << 16 | 0x80 << 24;
  }
  else {
    mask = 0x80 << 24;
  }

  // Enable OE and disable the BE's that we can read from the device
  ctlMask = (1 << c_ctlOE) | (1 << c_ctlFCS);
  ctl = m_robin.readFpga(c_controlRegister);
  ctl &= ~ctlMask;
  m_robin.writeFpga(c_controlRegister, ctl);

  tstamp startTime, currentTime; 
  bool erased = false;
  ts_clock(&startTime);
  do {
    
    // Timeout condition
    ts_clock(&currentTime);
    if (ts_duration(startTime, currentTime) > m_timeout) {
      throw ROS::ROSRobinExceptions(ROS::ROSRobinExceptions::TIMEOUT, 
				    "FlashProgrammer: Erase did not finish");
    }

    //    ts_delay(10);    
    unsigned int status = m_robin.readFpga(c_dataInRegister);

    if ((status & 0x80000000) == mask) {
      erased = true;
    }
    
  }
  while (!erased);

  ctl |= ctlMask;
  // write ctl
  m_robin.writeFpga(c_controlRegister, ctl);
  
  ts_delay(10);


  // Finally read from the device to check if erasing could be done
  // Enable OE that we can read from the device
  ctl = m_robin.readFpga(c_controlRegister);
  ctl &= ~ctlMask;
  m_robin.writeFpga(c_controlRegister, ctl);

  unsigned int val = m_robin.readFpga(c_dataInRegister);

  if (m_boardType == ROBIN1_BOARD) {  

    if ((val & 0xFFFF0000) != 0xFFFF0000) {    
      throw ROS::FlashProgrammerException("Verification of erase failed");
    }

  }
  else {

    if ((val & 0xFF000000) != 0xFF000000) {    
      throw ROS::FlashProgrammerException("Verification of erase failed");
    }

  }
  
  ctl |= ctlMask;
  // write ctl
  m_robin.writeFpga(c_controlRegister, ctl);

  ts_delay(10);
  
}

// --------x----------------------------x-----------------------------------x--

//***************************************
void ROS::FlashProgrammer::eraseAll(void) 
//***************************************
{

}

// --------x----------------------------x-----------------------------------x--

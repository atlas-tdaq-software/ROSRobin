/* --------x----------------------------x-----------------------------------x-- */
/**
 ** \file   robintty.c
 **
 ** \author  Matthias Mueller
 **
 ** \date 2005
 **
 */
/* --------x----------------------------x-----------------------------------x-- */


// Description:
// This driver enables access of the ROBINs PowerPC serial port via
// PCI and the FPGA. The driver provides a plugin into the Linux tty
// subsystem. Thus the generated node can be used by kermit or minicom.

#include <linux/config.h>

#ifdef CONGFIG_SMP  //SMP needed for that machine, else errors
#define __SMP__
#endif

#include <linux/kernel.h>
#include <linux/config.h>
#include <linux/module.h>
#include <linux/errno.h>
#include <linux/signal.h>
#include <linux/sched.h>
#include <linux/interrupt.h>
#include <linux/tty.h>
#include <linux/tty_flip.h>
#include <linux/serial.h>
#include <linux/major.h>
#include <linux/ptrace.h>
#include <linux/ioport.h>
#include <linux/mm.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <linux/console.h>
#include <linux/types.h>
#include <asm/io.h>
#include <asm/irq.h>
#include <asm/uaccess.h>
#include "ROSRobin/robin_vhdl_map_ver4.hxx"


/*****************/
/* Module macros */
/*****************/
#ifdef MODULE_LICENSE
  MODULE_LICENSE("Private: Contact markus.joos@cern.ch");
#endif
MODULE_AUTHOR("Matthias Mueller");
MODULE_DESCRIPTION("Robin tty driver");
MODULE_PARM (debug, "i");
MODULE_PARM_DESC(debug, "1 = enable debugging   0 = disable debugging");

/****************/
/* Debug macros */
/****************/
#ifdef ROBIN_DEBUG
  #define kdebug(fmt, args...) {if (debug) printk(KERN_ERR "robin_tty: " fmt, ## args );}
#else
  #define kdebug(fmt, args...)
#endif

#define kerror(fmt, args...) {if (debug) printk(KERN_ERR "robin_tty: " fmt, ## args );}

/***************/
/* More Macros */
/***************/
#define DEVICE_NAME "tty_robin" /* device name as it apears in /proc/devices */
#define DEVICE_MAJOR 0          /* major number, 0 for autodetecttion */
#define DEVICE_MINOR 0          /* use 0, atm no special purpose for another value */
#define DELAY_TIME (HZ / 20)
#define STATUS_REG ((PCI_REGISTER_RANGE << PCI_RANGE_ADDRESS_SIZE) + PCI_UART_STATUS_REG_ADDRESS)
#define RW_REG ((PCI_REGISTER_RANGE << PCI_RANGE_ADDRESS_SIZE) + PCI_UART_WRITE_DATA_ADDRESS)
#define MAX_CARDS_NR 10 
#define FIFO_LENGTH  16

/*************/
/* Functions */
/*************/
static int  robintty_ioctl(struct tty_struct *tty,struct file * file,u_int cmd,u_long arg);
static int  robintty_open(struct tty_struct *tty, struct file *filp);
static int  robintty_chars_in_buffer(struct tty_struct *tty);
static int  robintty_write_room(struct tty_struct *tty);
static int  robintty_write(struct tty_struct *tty, int from_user, const u_char * buf, int count);
static void robintty_close(struct tty_struct *tty, struct file *filp);
static void robintty_put_char(struct tty_struct *tty, unsigned char ch);
static void robintty_flush_chars(struct tty_struct *tty);
static void robintty_start(struct tty_struct *tty);
static void robintty_stop(struct tty_struct *tty);
static void robintty_timer (u_long data);
static void robintty_flush_buffer(struct tty_struct *tty);
static void robintty_send_xchar(struct tty_struct *tty, char ch);
static void robintty_set_termios(struct tty_struct *tty, struct termios *old);
extern int readFpga(u_int card, u_int addressOffset);
extern int fpgaLoaded(u_int card, u_int *fpgadesign);
extern int getCardNum(void);
extern void writeFpga(u_int card, u_int addressOffset, int data);


/********************/
/* Global variables */
/********************/
static int debug = 0;
static int robintty_refcount;
static int nr_of_cards;
static int *robintty_use_count;
static struct tty_driver robintty_driver;
static struct termios *robintty_termios[MAX_CARDS_NR];
static struct termios *robintty_termios_locked[MAX_CARDS_NR];
static struct tty_struct *robintty_serial_table[MAX_CARDS_NR];
static struct tty_struct *robintty_tty;
static struct timer_list **robintty_timers;


/****************************/
/* Function implementations */
/****************************/

// This function is a callback for a timer. It is called periodically
// and checks if there is data in the receive FIFO on the FPGA. This
// has to be replaced by an interrupt.
/**************************************/
static void robintty_timer (u_long data)
/**************************************/
{
  unsigned int fpgadesign, line;
  
  /* We expect to get a pointer to a tty struct as an argument*/
  struct tty_struct *tty = (struct tty_struct *)data ;

  /* Get the card (line) */
  line = MINOR(tty->device) - tty->driver.minor_start;

  kdebug("timer called \n");

  /* Force pushing the flip buffer if it is full */
  if (tty->flip.count >= TTY_FLIPBUF_SIZE) 
    tty_flip_buffer_push(tty);

  /* Now test if the ROBIN board is running */
  if  (fpgaLoaded(line, &fpgadesign) == 0) 
  {
    int number_of_words = 0, i;

    /* Test if the FIFO has chars to read */
    number_of_words = readFpga(line, STATUS_REG) & 0xFFF;
    
    /* Dont read more data than we can store in the flip buffer */
    if (number_of_words > (TTY_FLIPBUF_SIZE - tty->flip.count)) 
      number_of_words = TTY_FLIPBUF_SIZE - tty->flip.count;
    
    /* Read the data */
    for (i = 0; i < number_of_words; i++) 
    {      
      char ch;
      
      ch = (char )(readFpga(line, RW_REG) & 0xFF);
      tty_insert_flip_char(tty,ch,0);    
    }
    
    /* Force pushing the flip buffer */
    tty_flip_buffer_push(tty);
    tty_schedule_flip (tty);
  }

  /* Program the next timer activation */
  robintty_timers[line]->expires = jiffies + DELAY_TIME;

  if (robintty_timers[line] != NULL)
    add_timer (robintty_timers[line]);
}


// Writes the characters in a buffer to the ROBIN
/*********************************************************************************************/
static int robintty_write(struct tty_struct *tty, int from_user, const u_char * buf, int count)
/*********************************************************************************************/
{
  int i;
  unsigned int line, fpgadesign;
  
  kdebug("Write called \n");

  /* Get the card (line) */
  line = MINOR(tty->device) - tty->driver.minor_start;

  /* Test if the write buffer is in user space and check it then */
  if (from_user && verify_area(VERIFY_READ, buf, count))
    return -EINVAL;
  
  /* Now get the chars from the buffer and write them to the board */
  for (i = 0; i < count; i++) 
  {
    char ch;

    if (from_user) 
      __get_user(ch, buf+i);
    else
      ch = buf[i];

    kdebug("Write: %c \n", ch);
    
    /* Now test if the ROBIN board is running */
    if (fpgaLoaded(line, &fpgadesign) == 0) 
    {
      int number_of_words = 0;

      /* Test if the write FIFO has sufficient entries */
      number_of_words = (readFpga(line, STATUS_REG) & 0xF0000) >> 16;
      kdebug("number_of_words is %d \n", number_of_words);

      /* the write FIFO size is 16, if number_of_words>14, just wait! */
      while (number_of_words > 14 )
      {
        number_of_words = 0;
        number_of_words = (readFpga(line, STATUS_REG) & 0xF0000) >> 16;

      }
 
      writeFpga(line, RW_REG, (u_int )ch);
    }    
  }
  return i;
}


// Called when the tty device is getting opened
/*****************************************************************/
static int robintty_open(struct tty_struct *tty, struct file *filp)
/*****************************************************************/
{
  int line;

  kdebug("Entering Open \n");

  /* Calculate the number of the ROBIN BOARD by the openeded minor */
  line = MINOR(tty->device) - tty->driver.minor_start;

  /* Check if this exists */
  if ((line >= tty->driver.num) || (line < 0)) 
  {
    kerror("Device not found: %x \n", MINOR(tty->device));
    return -ENODEV;
  }

  /* Check if this has been already opened. */
  if (robintty_use_count[line] != 0) 
  {
    /* If so we do not allow to re-open it! */
    kerror("Device is in use: %x \n", MINOR(tty->device));
    return -EBUSY;
  }
  else 
    robintty_use_count[line] = 1;  /* Mark the device as opened */

  /* Set the driver data to NULL */
  tty->driver_data = NULL;

  /* Create the read timer and submit it */
  if (robintty_timers[line] == NULL) 
  {
    robintty_timers[line] = kmalloc (sizeof (*robintty_timers[line]), GFP_KERNEL);
    if (robintty_timers[line] == NULL) 
    {
      kerror("Failed to alocate mem...");
      return -ENOMEM;
    }
    
    /* Init the timer */
    init_timer (robintty_timers[line]);

    /* Program it */
    robintty_timers[line]->data = (u_long)tty;
    robintty_timers[line]->expires = jiffies + DELAY_TIME;
    robintty_timers[line]->function = robintty_timer;
   
    /* and start it */
    add_timer (robintty_timers[line]);
  }
  kdebug("Leaving open \n");
  return 0;
}


/*******************************************************************/
static void robintty_close(struct tty_struct *tty, struct file *filp)
/*******************************************************************/
{  
  int line;
  
  kdebug("Close called \n");

  /* Calculate the number of the ROBIN BOARD by the openeded minor */
  line = MINOR(tty->device) - tty->driver.minor_start;
  
  /* Check if this exists */
  if ((line >= tty->driver.num) || (line < 0)) 
  {
    kerror("Device not found: %x \n", MINOR(tty->device));
    return;
  }
  
  /* Check if it has been already opened */
  if (robintty_use_count[line] != 1) 
  {
    kerror("Device is not in use: %x \n", MINOR(tty->device));
    return;
  }
  else 
    robintty_use_count[line] = 0;  /* Mark it as free */

  /* Flush all buffers */
  if (tty->driver.flush_buffer)
    tty->driver.flush_buffer(tty);
  
  if (tty->ldisc.flush_buffer) 
    tty->ldisc.flush_buffer(tty);
  
  
  /* shut down the timer */
  del_timer (robintty_timers[line]);

  /* free the timer and set it to NULL */
  kfree(robintty_timers[line]);
  robintty_timers[line] = NULL;
}


/*********************************************************************/
static void robintty_put_char(struct tty_struct *tty, unsigned char ch)
/*********************************************************************/
{
  kdebug("robintty_put_char called. This function is empty \n");
}


/****************************************************/
static int robintty_write_room(struct tty_struct *tty)
/****************************************************/
{
  int number_of_words = 0;
  unsigned int fpgadesign, line;

  kdebug("write_room called \n");

  /* Get the card (line) */
  line = MINOR(tty->device) - tty->driver.minor_start;

  /* Now test if the ROBIN board is running */
  if (fpgaLoaded(line, &fpgadesign) == 0) 
    number_of_words = (readFpga(line, STATUS_REG) & 0xF0000) >> 16;  /* Test if the write FIFO has sufficient entries */

  return number_of_words;
}


/******************************************************/
static void robintty_flush_chars(struct tty_struct *tty)
/******************************************************/
{
  kdebug("flush_chars called \n");
}


/************************************************/
static void robintty_start(struct tty_struct *tty)
/************************************************/
{
  kdebug("start called \n");
}


/***********************************************/
static void robintty_stop(struct tty_struct *tty)
/***********************************************/
{
  kdebug("stop called \n");
}


/*********************************************************/
static int robintty_chars_in_buffer(struct tty_struct *tty)
/*********************************************************/
{
  kdebug("chars_in_buffer called \n");
  return 0;
}


/*******************************************************/
static void robintty_flush_buffer(struct tty_struct *tty)
/*******************************************************/
{
  kdebug("flush_in_buffer called \n");
}


/**************************************************************/
static void robintty_send_xchar(struct tty_struct *tty, char ch)
/**************************************************************/
{
  kdebug("send_xchar called \n");
}


/***************************************************************************/
static void robintty_set_termios(struct tty_struct *tty, struct termios *old)
/***************************************************************************/
{
  kdebug("set_termios called \n");
}


/****************************************************************************************/
static int robintty_ioctl(struct tty_struct *tty, struct file * file,u_int cmd,u_long arg) 
/****************************************************************************************/
{
  kdebug("ioctl called ( cmd : %x , arg : %x )\n",cmd,arg);
  return -ENOIOCTLCMD;
}


/***********************************/
static int __init robintty_init(void)
/***********************************/
{
  int error, i;
  kdebug("Entering init  \n");

  /* Get teh number of boards */
  nr_of_cards = getCardNum();

  kdebug("Found %d Robin cards \n", nr_of_cards );
 
  for (i = 0; i < nr_of_cards; i++)
  {
	robintty_termios[i] = 0;
	robintty_termios_locked[i] = 0;
  }
  
  /* Init the tty driver struct */
  robintty_driver.magic                = TTY_DRIVER_MAGIC;
  robintty_driver.driver_name          = "robintty";
  robintty_driver.name                 = DEVICE_NAME;
  robintty_driver.major                = DEVICE_MAJOR;
  robintty_driver.minor_start          = DEVICE_MINOR;
  robintty_driver.num                  = nr_of_cards;
  robintty_driver.type                 = TTY_DRIVER_TYPE_SERIAL;
  robintty_driver.subtype              = SERIAL_TYPE_NORMAL;
  robintty_driver.init_termios         = tty_std_termios;
  robintty_driver.init_termios.c_cflag = B9600 | CS8 | CREAD | HUPCL | CLOCAL;
  robintty_driver.flags                = TTY_DRIVER_REAL_RAW | TTY_DRIVER_NO_DEVFS;
  robintty_driver.refcount             = &robintty_refcount;
  robintty_driver.table                = 0;
  robintty_driver.termios              = 0;
  robintty_driver.termios_locked       = 0;
  robintty_driver.table                = robintty_serial_table;
  robintty_driver.termios              = robintty_termios;
  robintty_driver.termios_locked       = robintty_termios_locked;
  robintty_driver.open                 = robintty_open;
  robintty_driver.close                = robintty_close;
  robintty_driver.write                = robintty_write;
  robintty_driver.put_char             = robintty_put_char;
  robintty_driver.flush_chars          = robintty_flush_chars;
  robintty_driver.write_room           = robintty_write_room;
  robintty_driver.chars_in_buffer      = robintty_chars_in_buffer;
  robintty_driver.flush_buffer         = robintty_flush_buffer;
  robintty_driver.send_xchar           = robintty_send_xchar;
  robintty_driver.set_termios          = robintty_set_termios;
  robintty_driver.ioctl                = robintty_ioctl;
  robintty_driver.stop                 = robintty_stop;
  robintty_driver.start                = robintty_start;

  /* register the tty driver */
  error = tty_register_driver(&robintty_driver);
  kdebug("Registering Major %d\n ", robintty_driver.major);
  if (error < 0) 
  {
    kerror("Couldn`t register robintty serial driver (%d) \n",error);
    return -EFAULT;
  }

  /* Allocated memory for the timer struct array */
  kdebug("Allocating memory for the timer struct\n");
  robintty_timers = (struct timer_list ** ) kmalloc(nr_of_cards * sizeof(struct timer_list *), GFP_KERNEL);
  if (robintty_timers == NULL) 
  {
    kerror("Couldn`t alloc memory for timer array\n");
    tty_unregister_driver(&robintty_driver);
    return -EFAULT;
  }

  kdebug("Allocating memory for the usage array\n");
  /* Allocate memory for the usage count array */
  robintty_use_count = (int *) kmalloc(nr_of_cards * sizeof(int), GFP_KERNEL);
  if (robintty_use_count ==  NULL) 
  {
    kerror("Couldn`t alloc memory for timer array\n");
    kfree(robintty_timers);
    tty_unregister_driver(&robintty_driver);
    return -EFAULT;
  }

  kdebug("Initialising arrays\n");
  /* Init the arrays */
  for (i = 0; i < nr_of_cards; i++) 
  {
    robintty_use_count[i] = 0;
    robintty_timers[i] = NULL;
    kdebug("Robin FPGA Design ID of card %d: 0x%08x\n", i, readFpga(i, STATUS_REG));
  }
 
  kdebug("Leaving init\n ");
  return 0;
}


/************************************/
static void __exit robintty_fini(void)
/************************************/
{
  u_long flags;
  int ret;

  kdebug("\n Entering exit \n ");
  kfree(robintty_timers);
  kfree(robintty_use_count);
  ret = tty_unregister_driver(&robintty_driver);
  if (ret)
    kerror("Unable to unregister robintty driver (%d) \n",ret);
}

module_init(robintty_init);
module_exit(robintty_fini);

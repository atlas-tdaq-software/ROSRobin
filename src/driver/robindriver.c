/****************************************************************/
/* file:   robindriver.c					*/
/* author: Markus Joos, CERN-PH/ESS				*/
/****** C 2005 - The software with that certain something *******/


//  Description:
//  Driver for the ATLAS ROBIN PCI card
//  NOTES:
//  - This driver has been developed with SLC3 

//  Strings like P311 refer to pages (e.g.311) in the third edition of the 
//  Linux Device Drivers book


#include <linux/config.h>

#ifndef EXPORT_SYMTAB   //MJ: For 2.6: symbol seems not to be required
#define EXPORT_SYMTAB
#endif

#if defined(CONFIG_MODVERSIONS) && !defined(MODVERSIONS)
  #define MODVERSIONS
  #include <linux/modversions.h>
#endif

#include <linux/version.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/pci.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/mm.h>
#include <linux/vmalloc.h>
#include <linux/mman.h>
#include <linux/delay.h>
#include <linux/string.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/types.h>
#include <asm/io.h>
#include <asm/uaccess.h>
#include "ROSRobin/robindriver_common.h"
#include "ROSRobin/robindriver.h"

#ifdef MODULE
  MODULE_PARM (debug, "i");  //MJ: For 2.6 see P36
  MODULE_PARM_DESC(debug, "1 = enable debugging   0 = disable debugging"); //MJ: For 2.6: symbol seems not to exist
  MODULE_PARM (errorlog, "i");
  MODULE_PARM_DESC(errorlog, "1 = enable error logging   0 = disable error logging");
  MODULE_PARM (allvariants, "i");
  MODULE_PARM_DESC(allvariants, "1 = support all ROBIN varians   0 = support just the production ROBINs");
  MODULE_DESCRIPTION("ATLAS ROBIN");
  MODULE_AUTHOR("Markus Joos, CERN/PH");
  #ifdef MODULE_LICENSE
    MODULE_LICENSE("Private: Contact markus.joos@cern.ch");
  #endif
#endif


/******************/
/* 2.6 compat     */  //MJ: Can be removed once transition to 2.6 is made
/******************/
#ifndef PCI_DEVICE
  #define PCI_DEVICE(VENDOR_ID, DEVICE_ID) .class = 0,	\
  		.class_mask = 	0, \
		.vendor     =	VENDOR_ID, \
		.device     =	DEVICE_ID, \
		.subvendor  =	PCI_ANY_ID, \
		.subdevice  =	PCI_ANY_ID,
#endif

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,0)
  #define iowrite32 writel
  #define ioread32 readl
#endif


/*********/
/*Globals*/
/*********/
static int debug = 0, errorlog = 1, allvariants = 0, msg_fifo_avail[MAXCARDS];
static int robin_major = 0; // use dynamic allocation
static u_int maxcard;
static T_card robincard[MAXCARDS];
static struct proc_dir_entry *robin_file;
struct robin_proc_data_t robin_proc_data;
static T_blocklet blocklets[MAXCARDS][BLOCK_TYPES][MAX_BLOCKLETS];
char *proc_read_text; 

/*****************/
/*Some prototypes*/
/*****************/
static int robin_write_procmem(struct file *file, const char *buffer, u_long count, void *data);
static int robin_read_procmem(char *buf, char **start, off_t offset, int count, int *eof, void *data);
static int robinProbe(struct pci_dev *dev, const struct pci_device_id *id);
static void robinRemove(struct pci_dev *dev);
module_init(robindriver_init);  //MJ: See P39
module_exit(robindriver_exit);


static struct file_operations fops = 
{
  owner:   THIS_MODULE,
  mmap:    robin_mmap,
  ioctl:   robin_ioctl,
  open:    robin_open,    
  release: robin_release
};


// memory handler functions
static struct vm_operations_struct robin_vm_ops = 
{
  close:  robin_vclose,   // mmap-close
};


/* PCI driver structures */

/* ID tables */  //MJ: See P311
static struct pci_device_id allrobinIds[] = 
{
  { PCI_DEVICE(PCI_VENDOR_ID_ROBIN3, PCI_DEVICE_ID_ROBIN3) },      //ROBIN2, correct VID
  { PCI_DEVICE(PCI_VENDOR_ID_ROBIN3, PCI_DEVICE_ID_ROBIN3_bad) },  //ROBIN2, wrong VID
  { PCI_DEVICE(PCI_VENDOR_ID_ROBIN2, PCI_DEVICE_ID_ROBIN2) },      //ROBIN prototype, correct VID
  { 0, },
};

static struct pci_device_id robinIds[] = 
{
  { PCI_DEVICE(PCI_VENDOR_ID_ROBIN3, PCI_DEVICE_ID_ROBIN3) },      //ROBIN2, correct VID
  { 0, },
};


/* The driver struct */  //MJ: See P311
static struct pci_driver robinPciDriverAll = 
{
  .name     = "robindriver",
  .id_table = allrobinIds,
  .probe    = robinProbe,
  .remove   = robinRemove,
};

static struct pci_driver robinPciDriver = 
{
  .name     = "robindriver",
  .id_table = robinIds,
  .probe    = robinProbe,
  .remove   = robinRemove,
};

/*******************************/
static int robindriver_init(void)
/*******************************/
{
  int result, i;

  kerror(("robin driver version 2.1\n")); 

  // Initialise the robin card map. This is used to assign the slot parameter to a ROBIN board when calling ioctl(link)
  
  for (i = 0; i < MAXCARDS; i++) 
  {
    robincard[i].pciDevice = NULL;
    robincard[i].pci_memaddr_plx = 0;
    robincard[i].pci_memaddr_bus = 0;
    robincard[i].pci_memsize_plx = 0; 
    robincard[i].pci_memsize_bus = 0;
    robincard[i].mem_base = 0;
    robincard[i].mem_size = 0;
    robincard[i].plx_regs = NULL;
    robincard[i].fpga_regs = NULL;
    robincard[i].nrols = 0;
    robincard[i].bar2 = 0;
    robincard[i].fpgaConfigured = 0;  //Will be set in INIT_DMA
  }

  maxcard = 0;

  kdebug(("robin(robindriver_init): registering PCIDriver\n"));
  
  if (allvariants)
    result = pci_register_driver(&robinPciDriverAll);  //MJ: See P312
  else
    result = pci_register_driver(&robinPciDriver);  //MJ: See P312
  
  if (result == 0)
  {
    kerror(("robin(robindriver_init): Warning! no PCI devices found!\n"));
    return(-RD_EIO);
  }
  else 
  {
    kerror(("robin(robindriver_init): Found %d devices!\n", result));
  }

  result = register_chrdev(robin_major, "robin", &fops); //MJ: This is old fashioned. Update for 2.6. See P57
  if (result < 1)
  {
    kerror(("robin(robindriver_init): registering robin driver failed.\n"));
    return(-RD_EIO2);
  }
  robin_major = result;

  proc_read_text = (char *)kmalloc(PROC_MAX_CHARS, GFP_KERNEL);
  if (proc_read_text == NULL)
  {
    kdebug(("robin(robindriver_init): error from kmalloc\n"));
    return(-RD_KMALLOC);
  }

  robin_file = create_proc_entry("robin", 0644, NULL);  //MJ: See P87 (seq_file) for a better 2.6 implementation 
  if (robin_file == NULL)
  {
    kerror(("robin(robindriver_init): error from call to create_proc_entry\n"));
    return(-RD_PROC);
  }

  strcpy(robin_proc_data.name, "robin");
  strcpy(robin_proc_data.value, "robin");
  robin_file->data = &robin_proc_data;
  robin_file->read_proc = robin_read_procmem;
  robin_file->write_proc = robin_write_procmem;
  robin_file->owner = THIS_MODULE;

  kdebug(("robin(robindriver_init): driver loaded; major device number = %d\n", robin_major));
  return(0);
}


/********************************/
static void robindriver_exit(void)
/********************************/
{
  if (unregister_chrdev(robin_major, "robin") != 0) 
  {
    kerror(("robin(cleanup_module): cleanup_module failed\n"));
  }
  
  kfree(proc_read_text);
  remove_proc_entry("robin", NULL);
  if(allvariants)
    pci_unregister_driver(&robinPciDriverAll);
  else
    pci_unregister_driver(&robinPciDriver);

  kdebug(("robin(cleanup_module): driver removed\n"));
}


/*****************************/
/* PCI driver functions      */
/*****************************/

/**********************************************************************************/
static int __devinit robinProbe(struct pci_dev *dev, const struct pci_device_id *id) //MJ: for _devinit see P31
/**********************************************************************************/
{
  int i;
  u_long flags;
  u_int baddr, size, card, bar2Available;

  // Check which type of ROBIN is installed
  kdebug(("robin(robinProbe): called for device: 0x%04x / 0x%04x\n", dev->vendor, dev->device));
  kdebug(("robin(robinProbe): Bus: 0x%04x / Devfn 0x%04x\n", dev->bus->number, dev->devfn));
  
  //MJ: Should we check if the FPGA is loaded and exit if not?
  
  /* Find a new "slot" for the card */
  for (i = 0, card = -1; (i < MAXCARDS) && (card == -1); i++) 
  {
    if (robincard[i].pciDevice == NULL) 
    {
      card = i;
      robincard[card].pciDevice = dev;
    }
  }

  if (card == -1) 
  {
    kerror(("robin(robinProbe): Could not find space in the robincard array for this card."));
    return(-RD_TOOMANYCARDS);
  }

  flags = pci_resource_flags(dev, PLX_BAR);  //MJ: See P317
  if ((flags & IORESOURCE_MEM) != 0) 
  {
    baddr = pci_resource_start(dev, PLX_BAR) & PCI_BASE_ADDRESS_MEM_MASK;  //MJ: See P317
    size = pci_resource_end(dev, PLX_BAR) - pci_resource_start(dev, PLX_BAR);    //MJ: See P317
    kdebug(("robin(robinProbe): PLX: Mapping %d bytes at PCI address 0x%08x\n", size, baddr));
    robincard[card].plx_regs = (u_int *)ioremap(baddr, size);
    if (robincard[card].plx_regs == NULL)
    {
      kerror(("robin(robinProbe): error from ioremap for robincard[%d].plx_regs\n", card));
      robincard[card].pciDevice = NULL;
      return(-RD_REMAP);
    }
    kdebug(("robin(robinProbe): robincard[%d].plx_regs is at 0x%08x\n", card, robincard[card].plx_regs));

    robincard[card].pci_memaddr_plx = baddr;
    robincard[card].pci_memsize_plx = size;
  }
  else 
  {
    kerror(("robin(robinProbe): Bar 0 is not a memory resource"));
    return(-RD_BAR);
  }

  flags = pci_resource_flags(dev, FPGA_BAR);
  bar2Available = 1;
  if ((flags & IORESOURCE_MEM) != 0) 
  {
    baddr = pci_resource_start(dev, FPGA_BAR) & PCI_BASE_ADDRESS_MEM_MASK;
    size = pci_resource_end(dev, FPGA_BAR) - pci_resource_start(dev, FPGA_BAR);
    kdebug(("robin(robinProbe): FPGA: Mapping %d bytes at PCI address 0x%08x\n", size, baddr));
    robincard[card].fpga_regs = (u_int *)ioremap(baddr, size);
    robincard[card].bar2 = baddr;
    if (robincard[card].fpga_regs == NULL)
    {
      kerror(("robin(robinProbe): error from ioremap for robincard[%d].fpga_regs\n", card));
      return(-RD_REMAP);
    }
    kdebug(("robin(robinProbe): robincard[%d].fpga_regs is at 0x%08x\n", card, robincard[card].fpga_regs));
    
    robincard[card].pci_memaddr_bus = baddr;
    robincard[card].pci_memsize_bus = size;
  }
  else 
  {
    kerror(("robin(robinProbe): Warning! Bar 2 is not available"));
    bar2Available = 0;
  }

  //MJ: bar2Available is there for the rare case that a Robin is used un-initialized for production tests
  if ((dev->device == 0x144) || (dev->device == 0x324)) 
  {
    /* This is a ROBIN */
    robincard[card].nrols = 3;
  }
  else if ((dev->device == 0x9656) && (bar2Available = 1)) 
  {
    /* This is a ROBIN Prototype */
    robincard[card].nrols = 2;
  }
  
  maxcard += 1;
  return(0);
}


/******************************************/
static void robinRemove(struct pci_dev *dev) 
/******************************************/
{
  int i;
  u_int card;

  /* Find the "slot" of the card */
  for (i = 0, card = -1; (i < MAXCARDS) && (card == -1); i++) 
  {
    if (robincard[i].pciDevice == dev) 
      card = i;
  }
  
  if (card == -1) 
  {
    kerror(("robin(robinRemove): Could not find device to remove."));
    return;
  }

  iounmap((void *)robincard[card].plx_regs);
  iounmap((void *)robincard[card].fpga_regs);

  robincard[card].pciDevice = NULL;
  robincard[card].pci_memaddr_plx = 0;
  robincard[card].pci_memaddr_bus = 0;
  robincard[card].pci_memsize_plx = 0; 
  robincard[card].pci_memsize_bus = 0;
  robincard[card].mem_base = 0;
  robincard[card].mem_size = 0;
  robincard[card].plx_regs = NULL;
  robincard[card].fpga_regs = NULL;
  robincard[card].nrols = 0;
  robincard[card].bar2 = 0;
  robincard[i].fpgaConfigured = 0;

  maxcard -= 1;
}



/*****************************/
/* Standard driver functions */
/*****************************/


/*********************************************************/
static int robin_open(struct inode *ino, struct file *file)
/*********************************************************/
{
  u_int loop;
  T_private_data *pdata;

  kdebug(("robin(robin_open): called from process %d\n", current->pid))
  pdata = (T_private_data *)kmalloc(sizeof(T_private_data), GFP_KERNEL);
  if (pdata == NULL)
  {
    kerror(("robin(robin_open): error from kmalloc\n"))
    return(-RD_KMALLOC);
  }

  //I guess we have to protect code from being executed by parallel threads on a SMP machine. Therefore:
  sema_init(&pdata->thread_sema, 1);

  for(loop = 0; loop < MAXCARDS; loop++)
  {
    pdata->dpm_handle[loop] = -1;
    pdata->mem_handle[loop] = -1;
    pdata->fifo_handle[loop] = 0;
  }

  pdata->slot = -1;
   
  file->private_data = (char *)pdata;
  return(0);
}


/************************************************************/
static int robin_release(struct inode *ino, struct file *file)
/************************************************************/
{
  T_private_data *pdata;
  u_int ret;
  
  kdebug(("robin(robin_release): called from process %d\n", current->pid))

  pdata = (T_private_data *)file->private_data;
  kdebug(("robin(robin_release): Releasing orphaned resources for slot %d\n", pdata->slot))

  if (pdata->slot != -1) 
  {
    if (pdata->fifo_handle[pdata->slot] != 0)
    {  
      kdebug(("robin(robin_release): Releasing %d FIFO slots for card %d\n", pdata->fifo_handle[pdata->slot], pdata->slot))
      msg_fifo_avail[pdata->slot] += pdata->fifo_handle[pdata->slot];
    }
    
    if (pdata->dpm_handle[pdata->slot] != -1)
    {  
      kdebug(("robin(robin_release): Freeing handle %d of the DPM\n", pdata->dpm_handle[pdata->slot]))
      ret = retmem(pdata->slot, 0, pdata->dpm_handle[pdata->slot]);
      if (ret)
	kerror(("robin(robin_release): retmem for DPM returned %d\n", ret));
    }
    
    if (pdata->mem_handle[pdata->slot] != -1)
    { 
      kdebug(("robin(robin_release): Freeing handle %d of the PC MEM\n", pdata->mem_handle[pdata->slot]))
      ret = retmem(pdata->slot, 1, pdata->mem_handle[pdata->slot]);
      if (ret)
	kerror(("robin(robin_release): retmem for PC MEM returned %d\n", ret));
    }
  }

  kfree(file->private_data);
  kdebug(("robin(robin_release): kfreed file private data @ %x\n", (u_int)file->private_data))
  file->private_data = NULL;	// Required?
  return(0);
}


/***********************************************************************************/
static int robin_ioctl(struct inode *inode, struct file *file, u_int cmd, u_long arg)
/***********************************************************************************/
{
  T_private_data *pdata;
  int ret, slot;
  
  pdata = (T_private_data *)file->private_data;
  kdebug(("robin(ioctl): file at 0x%08x, file private data at 0x%08x\n", file, (u_int)pdata))

  switch (cmd)
  { 
    case LINK:
    {
      kdebug(("robin(ioctl,LINK): called from process %d\n", current->pid))
      
      //As pdata->link_params has to be shared by all threads that are derived from the same
      //open(/dev/robin) call we have to avoid that two threads use it at the same time. Therefore:
      down(&pdata->thread_sema);
      
      if (copy_from_user(&pdata->link_params, (void *)arg, sizeof(T_link_params)) !=0)
      {
	kerror(("robin(ioctl,LINK): error from copy_from_user\n"));
        up(&pdata->thread_sema);
	return(-RD_CFU);
      }      

      if (robincard[pdata->link_params.slot].pciDevice == NULL) 
      {
        up(&pdata->thread_sema);
	return(-RD_NOCARD);
      }
      
      kdebug(("robin(ioctl,LINK): params.slot      = %d\n", pdata->link_params.slot))
      pdata->link_params.plx_base  = robincard[pdata->link_params.slot].pci_memaddr_plx;  
      pdata->link_params.plx_size  = robincard[pdata->link_params.slot].pci_memsize_plx;
      pdata->link_params.fpga_base = robincard[pdata->link_params.slot].pci_memaddr_bus;
      pdata->link_params.fpga_size = robincard[pdata->link_params.slot].pci_memsize_bus;
      pdata->link_params.nrols = robincard[pdata->link_params.slot].nrols;
      kdebug(("robin(ioctl,LINK): params.nrols     = 0x%08x\n", robincard[pdata->link_params.slot].nrols))
      kdebug(("robin(ioctl,LINK): params.plx_base  = 0x%08x\n", pdata->link_params.plx_base))
      kdebug(("robin(ioctl,LINK): params.plx_size  = 0x%08x\n", pdata->link_params.plx_size))
      kdebug(("robin(ioctl,LINK): params.fpga_base = 0x%08x\n", pdata->link_params.fpga_base))
      kdebug(("robin(ioctl,LINK): params.fpga_size = 0x%08x\n", pdata->link_params.fpga_size))

      if (copy_to_user((void *)arg, &pdata->link_params, sizeof(T_link_params)) != 0)
      {
	kerror(("robin(ioctl,LINK): error from copy_to_user\n"));
        up(&pdata->thread_sema);
	return(-RD_CTU);
      }

      up(&pdata->thread_sema);
      kdebug(("robin(ioctl,LINK): OK\n"))
      break;
    }

    case INIT_DMA:
    {
      kdebug(("robin(ioctl,INIT_DMA): called from process %d\n", current->pid))
      //This ioctl will only be called one from "robinconfigure". Therefore SMP and race conditions are not an issue            

      if (copy_from_user(&pdata->init_dma_params, (void *)arg, sizeof(T_init_dma_params)) !=0)
      {
	kerror(("robin(ioctl,INIT_DMA): error from copy_from_user\n"));
	return(-RD_CFU);
      } 

      //MJ: This is a bit of a trick. Before calling INIT_DMA "robinconfigure" checks via Jtag if the FPGA is loaded.
      //Therefore proc_read and the TTY driver will only work once "robinconfigure" has been run
      robincard[pdata->init_dma_params.slot].fpgaConfigured = 1;

      kdebug(("robin(ioctl,INIT_DMA): link_params.slot      = %d\n", pdata->init_dma_params.slot))
      kdebug(("robin(ioctl,INIT_DMA): link_params.mem_base  = 0x%08x\n", pdata->init_dma_params.mem_base))
      kdebug(("robin(ioctl,INIT_DMA): link_params.mem_size  = 0x%08x\n", pdata->init_dma_params.mem_size))
      robincard[pdata->init_dma_params.slot].mem_base = pdata->init_dma_params.mem_base;
      robincard[pdata->init_dma_params.slot].mem_size = pdata->init_dma_params.mem_size;

      //Initialize the memory manager
      initmem(pdata->init_dma_params.slot, pdata->init_dma_params.mem_size, pdata->init_dma_params.mem_base);

      break;
    }

    case MSG_REG:
    {
      kdebug(("robin(ioctl,MSG_REG): called from process %d\n", current->pid));
      
      //As pdata->msg_req_params has to be shared by all threads that are derived from the same
      //open(/dev/robin) call we have to avoid that two threads use it at the same time. Therefore:
      down(&pdata->thread_sema);

      if (copy_from_user(&pdata->msg_req_params, (void *)arg, sizeof(T_msg_req_params)) !=0)
      {
	kerror(("robin(ioctl,LINK): error from copy_from_user\n"));
        up(&pdata->thread_sema);
	return(-RD_CFU);
      }    
      
      kdebug(("robin(ioctl,MSG_REG): msg_req_params.slot     = %d\n", pdata->msg_req_params.slot))
      kdebug(("robin(ioctl,MSG_REG): msg_req_params.n_fifo   = %d\n", pdata->msg_req_params.n_fifo))
      kdebug(("robin(ioctl,MSG_REG): msg_req_params.dpm_size = %d\n", pdata->msg_req_params.dpm_size))
      kdebug(("robin(ioctl,MSG_REG): msg_req_params.mem_size = %d\n", pdata->msg_req_params.mem_size))
      
      //MJ: If pdata->slot already has a value assigned we should return an error
      pdata->slot = pdata->msg_req_params.slot;
      
      if ((pdata->dpm_handle[pdata->msg_req_params.slot] != -1) || (pdata->mem_handle[pdata->msg_req_params.slot] != -1))
      {
        kerror(("robin(ioctl,MSG_REG): This process has already requested message resources for card %d\n", pdata->msg_req_params.slot));
        kerror(("robin(ioctl,MSG_REG): pdata->dpm_handle[%d] = %d\n", pdata->msg_req_params.slot, pdata->dpm_handle[pdata->msg_req_params.slot]));
        kerror(("robin(ioctl,MSG_REG): pdata->mem_handle[%d] = %d\n", pdata->msg_req_params.slot, pdata->mem_handle[pdata->msg_req_params.slot]));
        up(&pdata->thread_sema);
	return(-RD_ALREADYDONE);
      }
      
      //Get a buffer in the DPM
      ret = getmem(pdata->msg_req_params.slot, 0, pdata->msg_req_params.dpm_size, &pdata->msg_req_params.dpm_base, &pdata->dpm_handle[pdata->msg_req_params.slot]);
      if (ret)
      {
        kerror(("robin(ioctl,MSG_REG): getmem for DPM returned %d\n", ret));
        up(&pdata->thread_sema);
	return(ret);
      } 

      //Add the PCI base address of the DPM
      pdata->msg_req_params.dpm_offset = pdata->msg_req_params.dpm_base;
      pdata->msg_req_params.dpm_base += (robincard[pdata->msg_req_params.slot].pci_memaddr_bus + MESSAGE_RAM_OFFSET);
      kdebug(("robin(ioctl,MSG_REG): getmem returns for DPM: base=0x%08x  handle=%d\n", pdata->msg_req_params.dpm_base, pdata->dpm_handle[pdata->msg_req_params.slot]))
      kdebug(("robin(ioctl,MSG_REG): pdata->msg_req_params.dpm_offset = 0x%08x\n", pdata->msg_req_params.dpm_offset))

      //Get a buffer in the PC MEM
      ret = getmem(pdata->msg_req_params.slot, 1, pdata->msg_req_params.mem_size, &pdata->msg_req_params.mem_base, &pdata->mem_handle[pdata->msg_req_params.slot]);
      if (ret)
      {
        kerror(("robin(ioctl,MSG_REG): getmem for PC MEM returned %d\n", ret));
        up(&pdata->thread_sema);
	return(ret);
      } 
      kdebug(("robin(ioctl,MSG_REG): getmem returns for MEM: base=0x%08x  handle=%d\n", pdata->msg_req_params.mem_base, pdata->mem_handle[pdata->msg_req_params.slot]))

      pdata->msg_req_params.mem_offset = pdata->msg_req_params.mem_base - robincard[pdata->msg_req_params.slot].mem_base;
      kdebug(("robin(ioctl,MSG_REG): pdata->msg_req_params.mem_base   = 0x%08x\n", pdata->msg_req_params.mem_base))
      kdebug(("robin(ioctl,MSG_REG): robincard[%d].mem_base           = 0x%08x\n", pdata->msg_req_params.slot, robincard[pdata->msg_req_params.slot].mem_base))
      kdebug(("robin(ioctl,MSG_REG): pdata->msg_req_params.mem_offset = 0x%08x\n", pdata->msg_req_params.mem_offset))

      //Get FIFO slots
      kdebug(("robin(ioctl,MSG_REG): card=%d   FIFO entries requested=%d   FIFO entries available=%d\n", pdata->msg_req_params.slot, msg_fifo_avail[pdata->msg_req_params.slot], pdata->msg_req_params.n_fifo));
      if (msg_fifo_avail[pdata->msg_req_params.slot] < pdata->msg_req_params.n_fifo)
      {
        kerror(("robin(ioctl,MSG_REG): card=%d   FIFO entries requested=%d   FIFO entries available=%d\n", pdata->msg_req_params.slot, msg_fifo_avail[pdata->msg_req_params.slot], pdata->msg_req_params.n_fifo));
        up(&pdata->thread_sema);
	return(-RD_FIFOEMPTY);
      }
      else
      {
	pdata->fifo_handle[pdata->msg_req_params.slot] = pdata->msg_req_params.n_fifo;
	msg_fifo_avail[pdata->msg_req_params.slot] -= pdata->msg_req_params.n_fifo;
	kdebug(("robin(ioctl,MSG_REG): %d entries in MSG FIFO allocated on card %d\n", pdata->msg_req_params.n_fifo, pdata->msg_req_params.slot))
      }

      if (copy_to_user((void *)arg, &pdata->msg_req_params, sizeof(T_msg_req_params)) != 0)
      {
	kerror(("robin(ioctl,MSG_REG): error from copy_to_user\n"));
        up(&pdata->thread_sema);
	return(-RD_CTU);
      }
      
      up(&pdata->thread_sema);
      break;
    }

    case MSG_FREE:
    {
      kdebug(("robin(ioctl,MSG_FREE): called from process %d\n", current->pid))

      if (copy_from_user(&slot, (void *)arg, sizeof(int)) !=0)
      {
	kerror(("robin(ioctl,MSG_FREE): error from copy_from_user\n"));
	return(-RD_CFU);
      } 
            
      kdebug(("robin(ioctl,MSG_FREE): card = %d\n", slot))
      kdebug(("robin(ioctl,MSG_FREE): Freeing %d entries in the FIFO\n", pdata->fifo_handle[slot]))
      msg_fifo_avail[slot] += pdata->fifo_handle[slot];
      pdata->fifo_handle[slot] = 0;
      
      kdebug(("robin(ioctl,MSG_FREE): Freeing handle %d of the DPM\n", pdata->dpm_handle[slot]))
      ret = retmem(slot, 0, pdata->dpm_handle[slot]);
      if (ret)
      {
        kerror(("robin(ioctl,MSG_FREE): retmem for DPM returned %d\n", ret));
	return(ret);
      } 
      
      kdebug(("robin(ioctl,MSG_FREE): Freeing handle %d of the PC MEM\n", pdata->mem_handle[slot]))
      ret = retmem(slot, 1, pdata->mem_handle[slot]);
      if (ret)
      {
        kerror(("robin(ioctl,MSG_FREE): retmem for PC MEM returned %d\n", ret));
	return(ret);
      } 
    
      pdata->dpm_handle[slot] = -1;
      pdata->mem_handle[slot] = -1;
      kdebug(("robin(ioctl,MSG_FREE): pdata->dpm_handle[%d] = %d\n", slot, pdata->dpm_handle[slot]))
      kdebug(("robin(ioctl,MSG_FREE): pdata->mem_handle[%d] = %d\n", slot, pdata->mem_handle[slot]))

      break;
    }

    case LINK_IRQ:
    {
      kdebug(("robin(ioctl,LINK_IRQ): called from process %d\n", current->pid))
      break;
    }

    case GETPROC:
    {
      u_int len;
      kdebug(("robin(ioctl,GETPROC): called from process %d\n", current->pid))
      
      len = fill_proc_read_text();

      if (copy_to_user((void *)arg, proc_read_text, len) != 0)
      {
	kerror(("cmem_rcc(ioctl,GETPROC): error from copy_to_user\n"));
	return(-RD_CTU);
      }

      break;
    }

  }   
  return(0);
}


/*********************************************************************************************/
static int robin_write_procmem(struct file *file, const char *buffer, u_long count, void *data)
/*********************************************************************************************/
{
  int len;
  struct robin_proc_data_t *fb_data = (struct robin_proc_data_t *)data;

  kdebug(("robin(robin_write_procmem): robin_write_procmem called\n"));

  if(count > 99)
    len = 99;
  else
    len = count;

  if (copy_from_user(fb_data->value, buffer, len))
  {
    kerror(("robin(robin_write_procmem): error from copy_from_user\n"));
    return(-RD_CFU);
  }

  kdebug(("robin(robin_write_procmem): len = %d\n", len));
  fb_data->value[len - 1] = '\0';
  kdebug(("robin(robin_write_procmem): text passed = %s\n", fb_data->value));

  if (!strcmp(fb_data->value, "debug"))
  {
    debug = 1;
    kdebug(("robin(robin_write_procmem): debugging enabled\n"));
  }

  if (!strcmp(fb_data->value, "nodebug"))
  {
    kdebug(("robin(robin_write_procmem): debugging disabled\n"));
    debug = 0;
  }

  if (!strcmp(fb_data->value, "elog"))
  {
    kdebug(("robin(robin_write_procmem): Error logging enabled\n"))
    errorlog = 1;
  }

  if (!strcmp(fb_data->value, "noelog"))
  {
    kdebug(("robin(robin_write_procmem): Error logging disabled\n"))
    errorlog = 0;
  }
  
  return(len);
}


/***************************************************************************************************/
static int robin_read_procmem(char *buf, char **start, off_t offset, int count, int *eof, void *data)
/***************************************************************************************************/
{
  int nchars = 0, loop1;
  static len = 0;

  kdebug(("robin(robin_read_procmem): Called with buf    = 0x%08x\n", (unsigned int)buf));
  kdebug(("robin(robin_read_procmem): Called with *start = 0x%08x\n", (unsigned int)*start));
  kdebug(("robin(robin_read_procmem): Called with offset = %d\n", (unsigned int)offset));
  kdebug(("robin(robin_read_procmem): Called with count  = %d\n", count));

  if (offset == 0)
    len = fill_proc_read_text();
  kdebug(("robin(robin_read_procmem): len = %d\n", len));

  if (count < (len - offset))
    nchars = count;
  else
    nchars = len - offset;
  kdebug(("robin(robin_read_procmem): min nchars         = %d\n", nchars));
  kdebug(("robin(robin_read_procmem): position           = %d\n", (offset & (PAGE_SIZE - 1))));
  
  if (nchars > 0)
  {
    for (loop1 = 0; loop1 < nchars; loop1++)
      buf[loop1 + (offset & (PAGE_SIZE - 1))] = proc_read_text[offset + loop1];
    *start = buf + (offset & (PAGE_SIZE - 1));
  }
  else
  {
    nchars = 0;
    *eof = 1;
  }
 
  kdebug(("robin(robin_read_procmem): returning *start   = 0x%08x\n", (unsigned int)*start));
  kdebug(("robin(robin_read_procmem): returning nchars   = %d\n", nchars));
  return(nchars);
}


//------------------
// Service functions
//------------------

/**********************************/
static int fill_proc_read_text(void)
/**********************************/
{  
  //MJ-SMP: protect this function (preferrably with a spinlock)
  u_int perr, ret, len, card, fpgadesign, value, v1, v2, v3, v4, v5, v6, loop1, loop2;
  u_short sdata;
  
  kdebug(("robin(fill_proc_read_text): Creating text....\n"));
  len = 0;
  len += sprintf(proc_read_text + len, "\n\n\nROBIN driver for release %s (based on CVS tag %s)\n", RELEASE_NAME, CVSTAG);

  len += sprintf(proc_read_text + len, "Number of cards detected = %d\n", maxcard);
  len += sprintf(proc_read_text + len, "\nSome PLX registers\n");

  perr = 0;

  len += sprintf(proc_read_text + len, "card |      MBOX2 |       DMRR |     DMLBAM |     DMPBAM |  PCI_STATUS |\n");
  for (card = 0; card < maxcard; card++)
  {
    len += sprintf(proc_read_text + len, "%4d |", card);
    len += sprintf(proc_read_text + len, " 0x%08x |", robincard[card].plx_regs[PLX_MBOX2]);
    len += sprintf(proc_read_text + len, " 0x%08x |", robincard[card].plx_regs[PLX_DMRR]);
    len += sprintf(proc_read_text + len, " 0x%08x |", robincard[card].plx_regs[PLX_DMLBAM]);
    len += sprintf(proc_read_text + len, " 0x%08x |", robincard[card].plx_regs[PLX_DMPBAM]);
    ret = pci_read_config_word(robincard[card].pciDevice, PCI_STATUS, &sdata);
    if (ret)
    {
      kerror(("robin(fill_proc_read_text): ERROR from pci_read_config_word\n"));
      len += sprintf(proc_read_text + len, "UNREADABLE\n");
    }
    else
    {
      len += sprintf(proc_read_text + len, "      0x%04x |\n", sdata);
      if (sdata & 0x8000)
        perr = 1;
    }
  }
  
  if(perr)
    len += sprintf(proc_read_text + len, "ATTENTION: THERE WAS A PARITY ERROR ON ONE ROBIN\n");

/*
  len += sprintf(proc_read_text + len, "card |     LAS0RR |     LAS0BA |       DMRR |     DMLBAM |     DMPBAM |      CNTRL |     INTCSR |\n");
  for (card = 0; card < maxcard; card++)
  {
    len += sprintf(proc_read_text + len, "%4d |", card);
    len += sprintf(proc_read_text + len, " 0x%08x |", robincard[card].plx_regs[PLX_LAS0RR]);
    len += sprintf(proc_read_text + len, " 0x%08x |", robincard[card].plx_regs[PLX_LAS0BA]);
    len += sprintf(proc_read_text + len, " 0x%08x |", robincard[card].plx_regs[PLX_DMRR]);
    len += sprintf(proc_read_text + len, " 0x%08x |", robincard[card].plx_regs[PLX_DMLBAM]);
    len += sprintf(proc_read_text + len, " 0x%08x |", robincard[card].plx_regs[PLX_DMPBAM]);
    len += sprintf(proc_read_text + len, " 0x%08x |", robincard[card].plx_regs[PLX_CNTRL]);
    len += sprintf(proc_read_text + len, " 0x%08x |\n", robincard[card].plx_regs[PLX_INTCSR]);
  }
*/

  len += sprintf(proc_read_text + len, "\nSome other registers\n");
  len += sprintf(proc_read_text + len, "card | FPGA design ID |       BAR2 |\n");
  for (card = 0; card < maxcard; card++)
  {
    len += sprintf(proc_read_text + len, "%4d |", card);
    ret = fpgaLoaded(card, &fpgadesign);
    if(ret == 0)
    {
      len += sprintf(proc_read_text + len, "     0x%08x |", robincard[card].fpga_regs[0]);
      len += sprintf(proc_read_text + len, " 0x%08x |\n", robincard[card].bar2);
    }
    else if (ret == -1)
      len += sprintf(proc_read_text + len, "robinconfig has not yet been run on this Robin\n");
    else
      len += sprintf(proc_read_text + len, "The FPGA of this card (0x%08x) is out of date\n", fpgadesign);
  }

  len += sprintf(proc_read_text + len, "\nS-Link status bits\n");
  len += sprintf(proc_read_text + len, "Card | ROL | Link err. | Link up | Xoff | Link active | Test mode | ROL Emulation |\n");
  for (card = 0; card < maxcard; card++)
  {
    ret = fpgaLoaded(card, &fpgadesign);
    if(ret == 0)
    {
      value = robincard[card].fpga_regs[SLINK_REG_OFFSET >> 2];

      v1 = (value >> 0) & 0x1;
      v2 = (value >> 1) & 0x1;
      v3 = (value >> 2) & 0x1;
      v4 = (value >> 3) & 0x1;
      v5 = (value >> 15) & 0x1;
      v6 = (value >> 12) & 0x1;
      len += sprintf(proc_read_text + len, "   %d |   0 |       %s |     %s |  %s |         %s |       %s |           %s |\n", card, v1?" No":"Yes",  v2?" No":"Yes", v3?" No":"Yes", v4?" No":"Yes", v5?" No":"Yes", v6?"Yes":" No");

      v1 = (value >> 4) & 0x1;
      v2 = (value >> 5) & 0x1;
      v3 = (value >> 6) & 0x1;
      v4 = (value >> 7) & 0x1;
      v5 = (value >> 16) & 0x1;
      v6 = (value >> 13) & 0x1;    
      len += sprintf(proc_read_text + len, "   %d |   1 |       %s |     %s |  %s |         %s |       %s |           %s |\n", card, v1?" No":"Yes",  v2?" No":"Yes", v3?" No":"Yes", v4?" No":"Yes", v5?" No":"Yes", v6?"Yes":" No");

      v1 = (value >> 8) & 0x1;
      v2 = (value >> 9) & 0x1;
      v3 = (value >> 10) & 0x1;
      v4 = (value >> 11) & 0x1;
      v5 = (value >> 17) & 0x1;
      v6 = (value >> 14) & 0x1;
      len += sprintf(proc_read_text + len, "   %d |   2 |       %s |     %s |  %s |         %s |       %s |           %s |\n", card, v1?" No":"Yes",  v2?" No":"Yes", v3?" No":"Yes", v4?" No":"Yes", v5?" No":"Yes", v6?"Yes":" No");
    }
    else if (ret == -1)
      len += sprintf(proc_read_text + len, "robinconfig has not yet been run on this Robin\n");
    else
      len += sprintf(proc_read_text + len, "The FPGA of this card (0x%08x) is out of date\n", fpgadesign);
   }

  len += sprintf(proc_read_text + len, "\nDumping status of Message FIFOs\n");
  for(loop1 = 0; loop1 < maxcard; loop1++)
    len += sprintf(proc_read_text + len,"There are %d FIFO slots available on card %d\n", msg_fifo_avail[loop1], loop1);

  len += sprintf(proc_read_text + len, "\nDumping status of Dual Ported Memory\n");
  len += sprintf(proc_read_text + len,"Index | Status |      Start |       Size\n");
  for(loop1 = 0; loop1 < maxcard; loop1++)
  {
    len += sprintf(proc_read_text + len, "Card %d\n", loop1);
    for(loop2 = 0; loop2 < MAX_BLOCKLETS; loop2++)
    {
      len += sprintf(proc_read_text + len,"%5d |", loop2);
      len += sprintf(proc_read_text + len,"%7d |", blocklets[loop1][0][loop2].used);
      len += sprintf(proc_read_text + len," 0x%08x |", blocklets[loop1][0][loop2].start);
      len += sprintf(proc_read_text + len," 0x%08x\n", blocklets[loop1][0][loop2].size);
    }
  }

  len += sprintf(proc_read_text + len,"\nDumping status of PC direct-DMA Memory\n");
  len += sprintf(proc_read_text + len,"Index | Status |      Start |       Size\n");
  for(loop1 = 0; loop1 < maxcard; loop1++)
  {
    len += sprintf(proc_read_text + len, "Card %d\n", loop1);
    for(loop2 = 0; loop2 < MAX_BLOCKLETS; loop2++)
    {
      len += sprintf(proc_read_text + len,"%5d |", loop2);
      len += sprintf(proc_read_text + len,"%7d |", blocklets[loop1][1][loop2].used);
      len += sprintf(proc_read_text + len," 0x%08x |", blocklets[loop1][1][loop2].start);
      len += sprintf(proc_read_text + len," 0x%08x\n", blocklets[loop1][1][loop2].size);
    }
  }

  len += sprintf(proc_read_text + len, " \n");
  len += sprintf(proc_read_text + len, "The command 'echo <action> > /proc/robin', executed as root,\n");
  len += sprintf(proc_read_text + len, "allows you to interact with the driver. Possible actions are:\n");
  len += sprintf(proc_read_text + len, "debug   -> enable debugging\n");
  len += sprintf(proc_read_text + len, "nodebug -> disable debugging\n");
  len += sprintf(proc_read_text + len, "elog    -> Log errors to /var/log/message\n");
  len += sprintf(proc_read_text + len, "noelog  -> Do not log errors to /var/log/message\n");
  kdebug(("robin(fill_proc_read_text): Number of characters created = %d\n", len));
  return(len);
}  


/******************************************************************/
static int robin_mmap(struct file *file, struct vm_area_struct *vma)
/******************************************************************/
{
  u_long offset, size;

  kdebug(("robin(robin_mmap): function called\n"));
  
  offset = vma->vm_pgoff << PAGE_SHIFT;  
  size = vma->vm_end - vma->vm_start;
  kdebug(("robin(robin_mmap): offset = 0x%08x\n",(u_int)offset));
  kdebug(("robin(robin_mmap): size   = 0x%08x\n",(u_int)size));

  if (offset & ~PAGE_MASK)
  {
    kerror(("robin(robin_mmap): offset not aligned: %ld\n", offset));
    return(-RD_MMAP);
  }

  // we only support shared mappings. "Copy on write" mappings are
  // rejected here. A shared mapping that is writeable must have the
  // shared flag set.
  if ((vma->vm_flags & VM_WRITE) && !(vma->vm_flags & VM_SHARED))
  {
    kerror(("robin(robin_mmap): writeable mappings must be shared, rejecting\n"));
    return(-RD_NOSHARE);
  }

  vma->vm_flags |= VM_RESERVED;
  
  // we do not want to have this area swapped out, lock it
  vma->vm_flags |= VM_LOCKED;

  // we create a mapping between the physical pages and the virtual
  // addresses of the application with remap_page_range.
  // enter pages into mapping of application
  kdebug(("robin(robin_mmap): Parameters of remap_page_range()\n"));
  kdebug(("robin(robin_mmap): Virtual address  = 0x%08x\n",(u_int)vma->vm_start));
  kdebug(("robin(robin_mmap): Physical address = 0x%08x\n",(u_int)offset));
  kdebug(("robin(robin_mmap): Size             = 0x%08x\n",(u_int)size));
  if (remap_page_range(vma, vma->vm_start, offset, size, vma->vm_page_prot))
  {
    kerror(("robin(robin_mmap): remap page range failed\n"));
    return(-RD_RPR);
  }
  
  vma->vm_ops = &robin_vm_ops;  
  
  kdebug(("robin(robin_mmap): function done\n"));
  return(0);
}


/**************************************************/
static void robin_vclose(struct vm_area_struct *vma)
/**************************************************/
{  
  kdebug(("robin(robin_vclose): Virtual address  = 0x%08x\n",(u_int)vma->vm_start));
  kdebug(("robin(robin_vclose): mmap released.\n"));
}


/****************************************************/
static int initmem(u_int card, u_int size, u_int base)
/****************************************************/
{
  int loop1, loop2;

  kdebug(("robin(initmem): Initializing memory management structures for card %d\n", card));

  for(loop1 = 0; loop1 < BLOCK_TYPES; loop1++)
  {
    for(loop2 = 0; loop2 < MAX_BLOCKLETS; loop2++)
    {
      blocklets[card][loop1][loop2].used = BLOCKLET_UNUSED;
      blocklets[card][loop1][loop2].start = 0;
      blocklets[card][loop1][loop2].size = 0;
    }
  }

  //Initialise the DPM buffer
  blocklets[card][0][0].used = BLOCKLET_FREE;
  blocklets[card][0][0].start = 0;
  blocklets[card][0][0].size = MESSAGE_RAM_SIZE;  

  //Initialise the PC MEM buffer
  blocklets[card][1][0].used = BLOCKLET_FREE;
  blocklets[card][1][0].start = base;
  blocklets[card][1][0].size = size;
  
  msg_fifo_avail[card] = MESSAGE_FIFO_DEPTH;
  kdebug(("robin(initmem):  msg_fifo_avail has been initialised to %d\n", MESSAGE_FIFO_DEPTH));

  return(0);
}


/************************************************************************************/
static int getmem(u_int card, u_int type, u_int size, u_int *start_add, u_int *handle)
/************************************************************************************/
{
  int loop, b1 = -1 , b2 = -1;

  kdebug(("robin(getmem): called with card=%d  type=%d  size=%d\n", card, type, size));

  kdebug(("robin(getmem): looking for space...\n"));
  //look for a large enough free blocklet  
  for(loop = 0; loop < MAX_BLOCKLETS; loop++)
  {
    kdebug(("robin(getmem): blocklets[%d][%d][%d].used = %d\n", card, type, loop, blocklets[card][type][loop].used));
    kdebug(("robin(getmem): blocklets[%d][%d][%d].size = %d\n", card, type, loop, blocklets[card][type][loop].size));
    if (blocklets[card][type][loop].used == BLOCKLET_FREE && blocklets[card][type][loop].size >= size)
    {
      b1 = loop;
      break;
    }  
  }
  
  kdebug(("robin(getmem): looking for free slot...\n"));
  //look for an empty slot in the array to hold the new blocklet
  for(loop = 0; loop < MAX_BLOCKLETS; loop++)
  {
    kdebug(("robin(getmem): blocklets[%d][%d][%d].used = %d\n", card, type, loop, blocklets[card][type][loop].used));
    if (blocklets[card][type][loop].used == BLOCKLET_UNUSED)
    {
      b2 = loop;
      break;
    }  
  } 

  if (b1 == -1)
  {
    kerror(("robin(getmem): failed to find free memory\n"))
    return(-RD_NOMEMORY);
  }
    
  if (b2 == -1)
  {
    kerror(("robin(getmem): failed to find an enpty slot\n"))
    return(-RD_NOSLOT);     
  }
 
  blocklets[card][type][b2].used = BLOCKLET_USED;
  blocklets[card][type][b2].size = size;
  blocklets[card][type][b2].start = blocklets[card][type][b1].start;  

  blocklets[card][type][b1].size -= size;
  if (blocklets[card][type][b1].size == 0)
  {
    blocklets[card][type][b1].used = BLOCKLET_UNUSED;
    blocklets[card][type][b1].start = 0;
  }
  else
    blocklets[card][type][b1].start += size;
  
  *start_add = blocklets[card][type][b2].start;
  *handle = b2;

  return(0);
}


/*****************************************************/
static int retmem(u_int card, u_int type, u_int handle)
/*****************************************************/
{
  int loop, bprev = -1,  bnext = -1;
  
  kdebug(("robin(retmem): called with card=%d  type=%d  handle=%d\n", card, type, handle));
  if (blocklets[card][type][handle].used != BLOCKLET_USED)
    return(-RD_ILLHANDLE);
 
  blocklets[card][type][handle].used = BLOCKLET_FREE;
    
  //Check if the blocklet can be merged with a predecessor   
  for(loop = 0; loop < MAX_BLOCKLETS; loop++)
  {
    if (blocklets[card][type][loop].used == BLOCKLET_FREE && (blocklets[card][type][loop].start + blocklets[card][type][loop].size) == blocklets[card][type][handle].start)
    {
      bprev = loop;
      break;
    }
  }
  
  //Check if the blocklet can be merged with a sucessor   
  for(loop = 0; loop < MAX_BLOCKLETS; loop++)
  {
    if (blocklets[card][type][loop].used == BLOCKLET_FREE && (blocklets[card][type][handle].start + blocklets[card][type][handle].size) == blocklets[card][type][loop].start)
    {
      bnext = loop;
      break;
    }
  }
  
  if (bprev != -1)
  {
    kdebug(("robin(retmem): merging %d with predecessor %d\n", handle, bprev)); 
    blocklets[card][type][handle].start = blocklets[card][type][bprev].start;
    blocklets[card][type][handle].size += blocklets[card][type][bprev].size;
    blocklets[card][type][bprev].used = BLOCKLET_UNUSED;
    blocklets[card][type][bprev].start = 0;
    blocklets[card][type][bprev].size = 0;
  }
  
  if (bnext != -1)
  {
    kdebug(("robin(retmem): merging %d with sucessor %d\n", handle, bnext)); 
    blocklets[card][type][handle].size += blocklets[card][type][bnext].size;
    blocklets[card][type][bnext].used = BLOCKLET_UNUSED;
    blocklets[card][type][bnext].start = 0;
    blocklets[card][type][bnext].size = 0;
  }
  return(0);
}



/**************************************************/
static int readFpga(u_int card, u_int addressOffset)
/**************************************************/
{
  u_int fpgadesign;
  int value = 0;
  void *address;
  
  kdebug(("robin(readFpga): function called\n"))

  if ((robincard[card].fpga_regs != NULL) && (addressOffset < robincard[card].pci_memsize_bus)) 
  {
    address = (void *)(robincard[card].fpga_regs + addressOffset);
    if (fpgaLoaded(card, &fpgadesign) == 0) 
    {
      value = ioread32(address);
      kdebug(("robin(readFpga): 0x%08x read from FPGA of card %d at offset 0x%08x\n", value, card, addressOffset))
    }
    return value;
  }
  else 
    return 0;
}


/**************************************************************/
static void writeFpga(u_int card, u_int addressOffset, int data)
/**************************************************************/
{
  u_int fpgadesign;
  void *address;
  
  kdebug(("robin(writeFpga): function called\n"))

  if ((robincard[card].fpga_regs != NULL) && (addressOffset < robincard[card].pci_memsize_bus)) 
  {
    address = (void *)(robincard[card].fpga_regs + addressOffset);
    if (fpgaLoaded(card, &fpgadesign) == 0) 
      iowrite32(data, robincard[card].fpga_regs + addressOffset);
  }
}


/**************************************************/
static int fpgaLoaded(u_int card, u_int *fpgadesign)  
/**************************************************/
{
  int lPlxLBRD0Reg, lPlxLBRD0RegBackup, value, alive, version, majorRevision, minorRevision;
  int eversion, emajorRevision, eminorRevision;

  kdebug(("robin(fpgaLoaded): function called\n"))
  
  eversion       = DESIGN_ID >> 24;
  emajorRevision = (DESIGN_ID >> 16) & 0xff;
  eminorRevision = DESIGN_ID & 0x0fff;
  
  if ((card < maxcard) && (robincard[card].fpgaConfigured == 1)) 
  {
    value = ioread32((void *) (robincard[card].fpga_regs));
    *fpgadesign = value;
    
    version = value >> 24;
    majorRevision = (value >> 16) & 0xff;
    minorRevision = value & 0x0fff;

    kdebug(("robin(fpgaLoaded): Expected =      0x%08x\n", DESIGN_ID));
    kdebug(("robin(fpgaLoaded): eversion:       0x%x\n", eversion));
    kdebug(("robin(fpgaLoaded): emajorRevision: 0x%x\n", emajorRevision));
    kdebug(("robin(fpgaLoaded): eminorRevision: 0x%x\n", eminorRevision));
    kdebug(("robin(fpgaLoaded): Detected =      0x%08x\n", value));
    kdebug(("robin(fpgaLoaded): version:        0x%x\n", version));
    kdebug(("robin(fpgaLoaded): majorRevision:  0x%x\n", majorRevision));
    kdebug(("robin(fpgaLoaded): minorRevision:  0x%x\n", minorRevision));

    if ((eversion != version) || (emajorRevision != majorRevision))
    {
      kerror(("robin(fpgaLoaded): Incompatible FPGA version. Can't continue!\n"));
      return -2;
    } 
    else 
    {
      if (eminorRevision > minorRevision)
      {
        kerror(("robin(fpgaLoaded): Incompatible FPGA revision (too small). Can't continue!\n"));
        return -2;
      }
      if (eminorRevision < minorRevision)
        kdebug(("robin(fpgaLoaded): Actual FPGA revision too high. Not all FPGA features might be exploited\n"));
    }
    return 0;
  }
  else
  { 
    kdebug(("robin(fpgaLoaded): robinconfig has not yet been called on this Robin\n"));
    *fpgadesign = 0;
    return -1;
  }
}


/*************************/
static int getCardNum(void) 
/*************************/
{
  return(maxcard);
}


EXPORT_SYMBOL(readFpga);
EXPORT_SYMBOL(writeFpga);
EXPORT_SYMBOL(fpgaLoaded);
EXPORT_SYMBOL(getCardNum);


// --------x----------------------------x-----------------------------------x--
/**
 ** \file   robindriver.c
 **
 ** \author Markus Joos, CERN-PH/ESS
 **
 ** \date   4. Feb. 05
 **
 */
// --------x----------------------------x-----------------------------------x--
/************ C 2005 - The software with that certain something **************/

/* $Id$
 * $Revision$
 * $Log$
 * Revision 1.1  2005/08/04 16:23:11  mmueller
 * Added robintty driver
 *
 * Revision 1.12  2005/06/28 14:39:28  mmueller
 * Fixed a bug in the release function concerning the pdata->slot variable.
 *
 * Revision 1.11  2005/06/22 16:13:59  joos
 * bug fixes and other changes
 *
 * Revision 1.10  2005/06/10 13:06:55  joos
 * globally: bug fixes and other modifications. The code contains still stuff for debugging and has to be cleaned up
 *
 * Revision 1.9  2005/05/23 12:46:45  joos
 * lots of small changes and bug fixes
 *
 * Revision 1.8  2005/05/13 13:08:47  joos
 * extra debugging text
 *
 * Revision 1.7  2005/05/10 12:54:16  joos
 * modifications and bug fixes
 *
 * Revision 1.6  2005/04/28 15:50:32  joos
 * modifications and bug fixes
 *
 * Revision 1.5  2005/04/27 13:34:58  joos
 * Lots of new code. Rol.cpp does not compile
 *
 * Revision 1.4  2005/04/20 14:16:58  joos
 * lots of new code. Not yet tested
 *
 * Revision 1.3  2005/04/14 15:00:59  joos
 * lotsof modification and new code. Some first tests done
 *
 * Revision 1.2  2005/04/14 07:06:01  joos
 * lots of new untested code
 *
 *
 */

/**
 * Description:
 * Driver for the ATLAS ROBIN PCI card
 * NOTES:
 * - This driver has been developed with SLC3 
 */

// Module Versioning a la Rubini p.316


//MJ: can I merge robin and robincard?

#include <linux/config.h>

#ifndef EXPORT_SYMTAB
#define EXPORT_SYMTAB
#endif

#if defined(CONFIG_MODVERSIONS) && !defined(MODVERSIONS)
  #define MODVERSIONS
#endif

#if defined(MODVERSIONS)
  #include <linux/modversions.h>
#endif

#include <linux/version.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/pci.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/mm.h>
#include <linux/vmalloc.h>
#include <linux/mman.h>
#include <linux/delay.h>
#include <linux/string.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/types.h>
#include <asm/io.h>
#include <asm/uaccess.h>
#include "ROSRobin/robindriver_common.h"
#include "ROSRobin/robindriver_mm.h"

#include "ROSRobin/robin_vhdl_map_ver4.hxx"



#ifdef MODULE
  MODULE_PARM (debug, "i");
  MODULE_PARM_DESC(debug, "1 = enable debugging   0 = disable debugging");
  MODULE_PARM (errorlog, "i");
  MODULE_PARM_DESC(errorlog, "1 = enable error logging   0 = disable error logging");
  MODULE_DESCRIPTION("ATLAS ROBIN");
  MODULE_AUTHOR("Markus Joos, CERN/PH");
  #ifdef MODULE_LICENSE
    MODULE_LICENSE("Private: Contact markus.joos@cern.ch");
  #endif
#endif

/******************/
/* 2.6 compat     */
/******************/

#ifndef PCI_DEVICE
#define PCI_DEVICE(VENDOR_ID, DEVICE_ID) .class = 0,	\
  		.class_mask = 	0, \
		.vendor =	VENDOR_ID, \
		.device =	DEVICE_ID, \
		.subvendor =	PCI_ANY_ID, \
		.subdevice =	PCI_ANY_ID,

#endif

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,0)
#define iowrite32 writel
#define ioread32 readl
#endif



/******************/
/*Type definitions*/
/******************/
struct robin_proc_data_t 
{
  char name[10];
  char value[100];
};

typedef struct
{
  struct semaphore   thread_sema;
  int                dpm_handle[MAXCARDS];
  int                mem_handle[MAXCARDS];
  int                fifo_handle[MAXCARDS]; //MJ: We do not relly need arays here as one pdata has to be generated per card
  int                slot;
  T_link_params      link_params;
  T_init_dma_params  init_dma_params;
  T_msg_req_params   msg_req_params;
} T_private_data;

typedef struct
{
  struct pci_dev *pciDevice;
  u_int pci_memaddr_plx;
  u_int pci_memaddr_bus;
  u_int pci_memsize_plx;
  u_int pci_memsize_bus;
  u_int mem_base;
  u_int mem_size;
  u_int *plx_regs;      //pointer to PLX registers
  u_int *fpga_regs;     //pointer to registers inside the FPGA
  u_int nrols;
  int fpgaConfigured;
  //  u_int *message_ram;   //pointer to the message passing RAM inside the FPGA
  //  u_int *message_fifo;  //pointer to the message passing FIFO inside the FPGA
} T_card;



static int robin_write_procmem(struct file *file, const char *buffer, u_long count, void *data);
static int robin_read_procmem(char *buf, char **start, off_t offset, int count, int *eof, void *data);


/*********/
/*Globals*/
/*********/
static int debug = 1, errorlog = 1, msg_fifo_avail[MAXCARDS];
static int robin_major = 0; // use dynamic allocation
static u_int maxcard, channels[MAXCARDS][MAXCHANNELS];
static u_char pci_revision[MAXCARDS];
static u_int opid;
static T_card robincard[MAXCARDS];
static struct proc_dir_entry *robin_file;
struct robin_proc_data_t robin_proc_data;
static T_blocklet blocklets[MAXCARDS][BLOCK_TYPES][MAX_BLOCKLETS];

static struct file_operations fops = 
{
  owner:   THIS_MODULE,
  mmap:    robin_mmap,
  ioctl:   robin_ioctl,
  open:    robin_open,    
  release: robin_release
};


// memory handler functions
static struct vm_operations_struct robin_vm_ops = 
{
  close:  robin_vclose,   // mmap-close
};


/* PCI driver structures */

/* ID tables */
static struct pci_device_id robinIds[] = {
  { PCI_DEVICE( 0x10dc, 0x144 )  },
  { PCI_DEVICE( 0x10dc, 0x324 )  },
  { PCI_DEVICE( 0x10b5, 0x9656 )  },
  { 0, },
};

int robinProbe(struct pci_dev *dev, const struct pci_device_id *id);
void robinRemove(struct pci_dev *dev);

/* The driver struct */
static struct pci_driver robinPciDriver = {

  .name = "robindriver",
  .id_table = robinIds,
  .probe = robinProbe,
  .remove = robinRemove,

};

/**************************/
static int robindriver_init(void)
/**************************/
{
  int result;
  int i;

  kerror(("robin driver version 1.0\n")); 

  SET_MODULE_OWNER(&fops);

  /* Initialise the robin card map. This is used to assign the slot parameter to a ROBIN board
     when calling ioctl(link)
  */
  for (i = 0; i < MAXCARDS; i++) {
    robincard[i].pciDevice = NULL;
    robincard[i].pci_memaddr_plx = 0;
    robincard[i].pci_memaddr_bus = 0;
    robincard[i].pci_memsize_plx = 0; 
    robincard[i].pci_memsize_bus = 0;
    robincard[i].mem_base = 0;
    robincard[i].mem_size = 0;
    robincard[i].plx_regs = NULL;
    robincard[i].fpga_regs = NULL;
    robincard[i].nrols = 0;
    robincard[i].fpgaConfigured = 1;
    //    robincard[i].message_ram = NULL;
    //    robincard[i].message_fifo = NULL;
  }

  maxcard = 0;

  kdebug(("robin(init_module): registering PCIDriver\n"));
  result = pci_register_driver(&robinPciDriver);
  if (result == 0)
  {
    kerror(("robin(init_module): Warning! no PCI devices found!\n"));
    //    return(result);
  }
  else {
    kerror(("robin(init_module): Found %d devices!\n", result));
  }

  result = register_chrdev(robin_major, "robin", &fops); 
  if (result < 1)
  {
    kerror(("robin(init_module): registering robin driver failed.\n"));
    return(-RD_EIO);
  }
  robin_major = result;

  robin_file = create_proc_entry("robin", 0644, NULL);
  if (robin_file == NULL)
  {
    kerror(("robin(init_module): error from call to create_proc_entry\n"));
    return (-RD_ENOMEM);
  }

  strcpy(robin_proc_data.name, "robin");
  strcpy(robin_proc_data.value, "robin");
  robin_file->data = &robin_proc_data;
  robin_file->read_proc = robin_read_procmem;
  robin_file->write_proc = robin_write_procmem;
  robin_file->owner = THIS_MODULE;

  opid = 0;

  kdebug(("robin(init_module): driver loaded; major device number = %d\n", robin_major));
  return(0);
}


/******************************/
static void robindriver_exit(void)
/******************************/
{

  if (unregister_chrdev(robin_major, "robin") != 0) 
  {
    kerror(("robin(cleanup_module): cleanup_module failed\n"));
  }
  
  remove_proc_entry("robin", NULL);

  pci_unregister_driver(&robinPciDriver);

  kdebug(("robin(cleanup_module): driver removed\n"));
}

module_init(robindriver_init);
module_exit(robindriver_exit);



/*****************************/
/* PCI driver functions      */
/*****************************/

/*************************************************************************************/
static int __devinit robinProbe(struct pci_dev *dev, const struct pci_device_id *id) 
/*************************************************************************************/
{

  int i;
  unsigned long flags;
  u_int baddr, size, card;
  u_int bar2Available;

  // Check which type of ROBIN is installed
  kdebug(("robin(robinProbe): called for device: 0x%04x / 0x%04x\n", dev->vendor, dev->device));
  kdebug(("robin(robinProbe): Bus: 0x%04x / Devfn 0x%04x\n", dev->bus->number, dev->devfn));
  
  
  /* Find a new "slot" for the card */
  for (i = 0, card = -1; (i < MAXCARDS) && (card == -1); i++) {
    if (robincard[i].pciDevice == NULL) {
      card = i;
      robincard[card].pciDevice = dev;
    }
  }

  if (card == -1) {
    kerror(("robin(robinRemove): Could not find space in the slots for this card."));
    return -ENOMEM;
  }

  flags = pci_resource_flags(dev, 0);  
  if ((flags & IORESOURCE_MEM) != 0) {
    
    baddr = pci_resource_start(dev, 0) & PCI_BASE_ADDRESS_MEM_MASK;
    size = pci_resource_end(dev, 0) - pci_resource_start(dev, 0);
    kdebug(("robin(robinProbe): PLX: Mapping %d bytes at PCI address 0x%08x\n", size, baddr));
    robincard[card].plx_regs = (u_int *)ioremap(baddr, size);
    if (robincard[card].plx_regs == NULL)
      {
	kerror(("robin(robinProbe): error from ioremap for robincard[%d].plx_regs\n", card));
	robincard[card].pciDevice = NULL;
	return -ENOMEM;
      }
    kdebug(("robin(robinProbe): robincard[%d].plx_regs is at 0x%08x\n", card, robincard[card].plx_regs));
    
  }
  else {

    kerror(("robin(robinProbe): Bar 0 is not a memory resource"));
    return -ENODEV;

  }

  flags = pci_resource_flags(dev, 2);  

  bar2Available = 1;

  if ((flags & IORESOURCE_MEM) != 0) {
    
    baddr = pci_resource_start(dev, 2) & PCI_BASE_ADDRESS_MEM_MASK;
    size = pci_resource_end(dev, 2) - pci_resource_start(dev, 2);
    kdebug(("robin(robinProbe): FPGA: Mapping %d bytes at PCI address 0x%08x\n", size, baddr));
    robincard[card].fpga_regs = (u_int *)ioremap(baddr, size);
    if (robincard[card].fpga_regs == NULL)
      {
	kerror(("robin(robinProbe): error from ioremap for robincard[%d].fpga_regs\n", card));
	return -ENOMEM;
      }
    kdebug(("robin(robinProbe): robincard[%d].fpga_regs is at 0x%08x\n", card, robincard[card].fpga_regs));
    
    robincard[card].pci_memaddr_bus = pci_resource_start(dev, 2);
    robincard[card].pci_memsize_bus = pci_resource_end(dev, 2) - pci_resource_start(dev, 2);

  }
  else {

    kerror(("robin(robinProbe): Warning! Bar 2 is not available"));
    bar2Available = 0;

  }

  robincard[card].pci_memaddr_plx = pci_resource_start(dev, 0);  
  robincard[card].pci_memsize_plx = pci_resource_end(dev, 0) - pci_resource_start(dev, 0);

  if ((dev->device == 0x144) || (dev->device == 0x324)) {
    /* This is a ROBIN */
    robincard[card].nrols = 3;
  }
  else if ((dev->device == 0x9656) && (bar2Available = 1)) {
    /* This is a ROBIN Prototype */
    robincard[card].nrols = 2;
  }
  
  maxcard += 1;

  if (fpgaLoaded(0) == 0) {
    kerror(("robin(robinProbe): FPGA is NOT loaded\n"));
  }
  else {
    kerror(("robin(robinProbe): FPGA is loaded\n"));
  }

  kerror(("robin(robinProbe): Test: 0x%x\n", readFpga(0, 0)));
  
  return(0);
}


/********************************************/
static void robinRemove(struct pci_dev *dev) 
/********************************************/
{

  int i;
  u_int card;

  /* Find a new "slot" for the card */
  for (i = 0, card = -1; (i < MAXCARDS) && (card == -1); i++) {
    if (robincard[i].pciDevice == dev) {
      card = i;
    }
  }
  
  if (card == -1) {
    kerror(("robin(robinRemove): Could not find device to remove."));
    return;
  }


  iounmap((void *)robincard[card].plx_regs);
  iounmap((void *)robincard[card].fpga_regs);
  //    iounmap((void *)robincard[card].message_ram);
  //    iounmap((void *)robincard[card].message_fifo);

  robincard[card].pciDevice = NULL;
  robincard[card].pci_memaddr_plx = 0;
  robincard[card].pci_memaddr_bus = 0;
  robincard[card].pci_memsize_plx = 0; 
  robincard[card].pci_memsize_bus = 0;
  robincard[card].mem_base = 0;
  robincard[card].mem_size = 0;
  robincard[card].plx_regs = NULL;
  robincard[card].fpga_regs = NULL;
  robincard[card].nrols = 0;
  robincard[i].fpgaConfigured = 0;
  //  robincard[card].message_ram = NULL;
  //  robincard[card].message_fifo = NULL;

  maxcard -= 1;

}



/*****************************/
/* Standard driver functions */
/*****************************/


/*********************************************************/
static int robin_open(struct inode *ino, struct file *file)
/*********************************************************/
{
  u_int loop;
  char* buf;
  T_private_data *pdata;

  kdebug(("robin(robin_open): called from process %d\n", current->pid))
  buf = (char *)kmalloc(sizeof(T_private_data), GFP_KERNEL);
  if (buf == NULL)
  {
    kerror(("robin(robin_open): error from kmalloc\n"))
    return(-RD_KMALLOC);
  }

  pdata = (T_private_data *)buf;
  //I guess we have to protect code from being executed by parallel threads on a SMP machine. Therefore:
  sema_init(&pdata->thread_sema, 1);

  for(loop = 0; loop < MAXCARDS; loop++)
  {
    pdata->dpm_handle[loop] = -1;
    pdata->mem_handle[loop] = -1;
    pdata->fifo_handle[loop] = 0;
  }

  pdata->slot = -1;
   
  file->private_data = buf;
  return(0);
}


/************************************************************/
static int robin_release(struct inode *ino, struct file *file)
/************************************************************/
{
  T_private_data *pdata;
  u_int ret;
  
  kdebug(("robin(robin_release): called from process %d\n", current->pid))

  kdebug(("robin(robin_release): Releasing orphaned resources for slot %d\n", pdata->slot))

  pdata = (T_private_data *)file->private_data;

  if (pdata->slot != -1) {
    if (pdata->fifo_handle[pdata->slot] != 0)
      {  
	kdebug(("robin(robin_release): Releasing %d FIFO slots for card %d\n", pdata->fifo_handle[pdata->slot], pdata->slot))
	  msg_fifo_avail[pdata->slot] += pdata->fifo_handle[pdata->slot];
      }
    
    if (pdata->dpm_handle[pdata->slot] != -1)
      {  
	kdebug(("robin(robin_release): Freeing handle %d of the DPM\n", pdata->dpm_handle[pdata->slot]))
	  ret = retmem(pdata->slot, 0, pdata->dpm_handle[pdata->slot]);
	if (ret)
	  kerror(("robin(robin_release): retmem for DPM returned %d\n", ret));
      }
    
    if (pdata->mem_handle[pdata->slot] != -1)
      { 
	kdebug(("robin(robin_release): Freeing handle %d of the PC MEM\n", pdata->mem_handle[pdata->slot]))
	  ret = retmem(pdata->slot, 1, pdata->mem_handle[pdata->slot]);
	if (ret)
	  kerror(("robin(robin_release): retmem for PC MEM returned %d\n", ret));
      }
  }


  kfree(file->private_data);
  kdebug(("robin(robin_release): kfreed file private data @ %x\n", (u_int)file->private_data))
  file->private_data = NULL;	// Required?

  return(0);
}


/***********************************************************************************/
static int robin_ioctl(struct inode *inode, struct file *file, u_int cmd, u_long arg)
/***********************************************************************************/
{
  T_private_data *pdata;
  int ret, slot;
  
  pdata = (T_private_data *)file->private_data;
  kdebug(("robin(ioctl): file at 0x%08x, file private data at 0x%08x\n", file, (u_int)pdata))

  switch (cmd)
  { 
    case LINK:
    {
      kdebug(("robin(ioctl,LINK): called from process %d\n", current->pid))
      
      //As pdata->link_params has to be shared by all threads that are derived from the same
      //open(/dev/robin) call we have to avoid that two threads use it at the same time. Therefore:
      down(&pdata->thread_sema);
      
      if (copy_from_user(&pdata->link_params, (void *)arg, sizeof(T_link_params)) !=0)
      {
	kerror(("robin(ioctl,LINK): error from copy_from_user\n"));
        up(&pdata->thread_sema);
	return(-RD_CFU);
      }      

      if (robincard[pdata->link_params.slot].pciDevice == NULL) {
        up(&pdata->thread_sema);
	return -ENODEV;
      }
      
      kdebug(("robin(ioctl,LINK): params.slot      = %d\n", pdata->link_params.slot))
      pdata->link_params.plx_base  = robincard[pdata->link_params.slot].pci_memaddr_plx;  
      pdata->link_params.plx_size  = robincard[pdata->link_params.slot].pci_memsize_plx;
      pdata->link_params.fpga_base = robincard[pdata->link_params.slot].pci_memaddr_bus;
      pdata->link_params.fpga_size = robincard[pdata->link_params.slot].pci_memsize_bus;
      pdata->link_params.nrols = robincard[pdata->link_params.slot].nrols;
      kdebug(("robin(ioctl,LINK): params.nrols     = 0x%08x\n", robincard[pdata->link_params.slot].nrols))
      kdebug(("robin(ioctl,LINK): params.plx_base  = 0x%08x\n", pdata->link_params.plx_base))
      kdebug(("robin(ioctl,LINK): params.plx_size  = 0x%08x\n", pdata->link_params.plx_size))
      kdebug(("robin(ioctl,LINK): params.fpga_base = 0x%08x\n", pdata->link_params.fpga_base))
      kdebug(("robin(ioctl,LINK): params.fpga_size = 0x%08x\n", pdata->link_params.fpga_size))

      if (copy_to_user((void *)arg, &pdata->link_params, sizeof(T_link_params)) != 0)
      {
	kerror(("robin(ioctl,LINK): error from copy_to_user\n"));
        up(&pdata->thread_sema);
	return(-RD_CTU);
      }

      up(&pdata->thread_sema);
      kdebug(("robin(ioctl,LINK): OK\n"))
      break;
    }

    case INIT_DMA:
    {
      kdebug(("robin(ioctl,INIT_DMA): called from process %d\n", current->pid))
      //This ioctl will only be called one from "robinconfigure". Therefore SMP and race conditions are not an issue            

      if (copy_from_user(&pdata->init_dma_params, (void *)arg, sizeof(T_init_dma_params)) !=0)
      {
	kerror(("robin(ioctl,INIT_DMA): error from copy_from_user\n"));
	return(-RD_CFU);
      } 

      kdebug(("robin(ioctl,INIT_DMA): link_params.slot      = %d\n", pdata->init_dma_params.slot))
      kdebug(("robin(ioctl,INIT_DMA): link_params.mem_base  = 0x%08x\n", pdata->init_dma_params.mem_base))
      kdebug(("robin(ioctl,INIT_DMA): link_params.mem_size  = 0x%08x\n", pdata->init_dma_params.mem_size))
      robincard[pdata->init_dma_params.slot].mem_base = pdata->init_dma_params.mem_base;
      robincard[pdata->init_dma_params.slot].mem_size = pdata->init_dma_params.mem_size;

      //Initialize the memory manager
      initmem(pdata->init_dma_params.slot, pdata->init_dma_params.mem_size, pdata->init_dma_params.mem_base);

      break;
    }

    case MSG_REG:
    {
      kdebug(("robin(ioctl,MSG_REG): called from process %d\n", current->pid));
      
      //As pdata->msg_req_params has to be shared by all threads that are derived from the same
      //open(/dev/robin) call we have to avoid that two threads use it at the same time. Therefore:
      down(&pdata->thread_sema);

      if (copy_from_user(&pdata->msg_req_params, (void *)arg, sizeof(T_msg_req_params)) !=0)
      {
	kerror(("robin(ioctl,LINK): error from copy_from_user\n"));
        up(&pdata->thread_sema);
	return(-RD_CFU);
      }    
      
      kdebug(("robin(ioctl,MSG_REG): msg_req_params.slot     = %d\n", pdata->msg_req_params.slot))
      kdebug(("robin(ioctl,MSG_REG): msg_req_params.n_fifo   = %d\n", pdata->msg_req_params.n_fifo))
      kdebug(("robin(ioctl,MSG_REG): msg_req_params.dpm_size = %d\n", pdata->msg_req_params.dpm_size))
      kdebug(("robin(ioctl,MSG_REG): msg_req_params.mem_size = %d\n", pdata->msg_req_params.mem_size))
      
      //MJ: If pdata->slot already has a value assigned we should return an error
      pdata->slot = pdata->msg_req_params.slot;
      
      if ((pdata->dpm_handle[pdata->msg_req_params.slot] != -1) || (pdata->mem_handle[pdata->msg_req_params.slot] != -1))
      {
        kerror(("robin(ioctl,MSG_REG): This process has already requested message resources for card %d\n", pdata->msg_req_params.slot));
        kerror(("robin(ioctl,MSG_REG): pdata->dpm_handle[%d] = %d\n", pdata->msg_req_params.slot, pdata->dpm_handle[pdata->msg_req_params.slot]));
        kerror(("robin(ioctl,MSG_REG): pdata->mem_handle[%d] = %d\n", pdata->msg_req_params.slot, pdata->mem_handle[pdata->msg_req_params.slot]));
        up(&pdata->thread_sema);
	return(-RD_ALREADYDONE);
      }
      
      //Get a buffer in the DPM
      ret = getmem(pdata->msg_req_params.slot, 0, pdata->msg_req_params.dpm_size, &pdata->msg_req_params.dpm_base, &pdata->dpm_handle[pdata->msg_req_params.slot]);
      if (ret)
      {
        kerror(("robin(ioctl,MSG_REG): getmem for DPM returned %d\n", ret));
        up(&pdata->thread_sema);
	return(ret);
      } 
      //Add the PCI base address of the DPM
      pdata->msg_req_params.dpm_offset = pdata->msg_req_params.dpm_base;
      pdata->msg_req_params.dpm_base += (robincard[pdata->msg_req_params.slot].pci_memaddr_bus + MESSAGE_RAM_OFFSET);
      kdebug(("robin(ioctl,MSG_REG): getmem returns for DPM: base=0x%08x  handle=%d\n", pdata->msg_req_params.dpm_base, pdata->dpm_handle[pdata->msg_req_params.slot]))
      kdebug(("robin(ioctl,MSG_REG): pdata->msg_req_params.dpm_offset = 0x%08x\n", pdata->msg_req_params.dpm_offset))

      //Get a buffer in the PC MEM
      ret = getmem(pdata->msg_req_params.slot, 1, pdata->msg_req_params.mem_size, &pdata->msg_req_params.mem_base, &pdata->mem_handle[pdata->msg_req_params.slot]);
      if (ret)
      {
        kerror(("robin(ioctl,MSG_REG): getmem for PC MEM returned %d\n", ret));
        up(&pdata->thread_sema);
	return(ret);
      } 
      kdebug(("robin(ioctl,MSG_REG): getmem returns for MEM: base=0x%08x  handle=%d\n", pdata->msg_req_params.mem_base, pdata->mem_handle[pdata->msg_req_params.slot]))

      pdata->msg_req_params.mem_offset = pdata->msg_req_params.mem_base - robincard[pdata->msg_req_params.slot].mem_base;
      kdebug(("robin(ioctl,MSG_REG): pdata->msg_req_params.mem_base   = 0x%08x\n", pdata->msg_req_params.mem_base))
      kdebug(("robin(ioctl,MSG_REG): robincard[%d].mem_base           = 0x%08x\n", pdata->msg_req_params.slot, robincard[pdata->msg_req_params.slot].mem_base))
      kdebug(("robin(ioctl,MSG_REG): pdata->msg_req_params.mem_offset = 0x%08x\n", pdata->msg_req_params.mem_offset))

      //Get FIFO slots
      kdebug(("robin(ioctl,MSG_REG): card=%d   FIFO entries requested=%d   FIFO entries available=%d\n", pdata->msg_req_params.slot, msg_fifo_avail[pdata->msg_req_params.slot], pdata->msg_req_params.n_fifo));
      if (msg_fifo_avail[pdata->msg_req_params.slot] < pdata->msg_req_params.n_fifo)
      {
        kerror(("robin(ioctl,MSG_REG): card=%d   FIFO entries requested=%d   FIFO entries available=%d\n", pdata->msg_req_params.slot, msg_fifo_avail[pdata->msg_req_params.slot], pdata->msg_req_params.n_fifo));
        up(&pdata->thread_sema);
	return(-RD_FIFOEMPTY);
      }
      else
      {
	pdata->fifo_handle[pdata->msg_req_params.slot] = pdata->msg_req_params.n_fifo;
	msg_fifo_avail[pdata->msg_req_params.slot] -= pdata->msg_req_params.n_fifo;
	kdebug(("robin(ioctl,MSG_REG): %d entries in MSG FIFO allocated on card %d\n", pdata->msg_req_params.n_fifo, pdata->msg_req_params.slot))
      }

      if (copy_to_user((void *)arg, &pdata->msg_req_params, sizeof(T_msg_req_params)) != 0)
      {
	kerror(("robin(ioctl,MSG_REG): error from copy_to_user\n"));
        up(&pdata->thread_sema);
	return(-RD_CTU);
      }
      
      up(&pdata->thread_sema);
      break;
    }

    case MSG_FREE:
    {
      kdebug(("robin(ioctl,MSG_FREE): called from process %d\n", current->pid))

      if (copy_from_user(&slot, (void *)arg, sizeof(int)) !=0)
      {
	kerror(("robin(ioctl,MSG_FREE): error from copy_from_user\n"));
	return(-RD_CFU);
      } 
            
      kdebug(("robin(ioctl,MSG_FREE): card = %d\n", slot))
      kdebug(("robin(ioctl,MSG_FREE): Freeing %d entries in the FIFO\n", pdata->fifo_handle[slot]))
      msg_fifo_avail[slot] += pdata->fifo_handle[slot];
      pdata->fifo_handle[slot] = 0;
      
      kdebug(("robin(ioctl,MSG_FREE): Freeing handle %d of the DPM\n", pdata->dpm_handle[slot]))
      ret = retmem(slot, 0, pdata->dpm_handle[slot]);
      if (ret)
      {
        kerror(("robin(ioctl,MSG_FREE): retmem for DPM returned %d\n", ret));
	return(ret);
      } 
      
      kdebug(("robin(ioctl,MSG_FREE): Freeing handle %d of the PC MEM\n", pdata->mem_handle[slot]))
      ret = retmem(slot, 1, pdata->mem_handle[slot]);
      if (ret)
      {
        kerror(("robin(ioctl,MSG_FREE): retmem for PC MEM returned %d\n", ret));
	return(ret);
      } 
    
      pdata->dpm_handle[slot] = -1;
      pdata->mem_handle[slot] = -1;
      kdebug(("robin(ioctl,MSG_FREE): pdata->dpm_handle[%d] = %d\n", slot, pdata->dpm_handle[slot]))
      kdebug(("robin(ioctl,MSG_FREE): pdata->mem_handle[%d] = %d\n", slot, pdata->mem_handle[slot]))

      break;
    }

    case LINK_IRQ:
    {
      kdebug(("robin(ioctl,LINK_IRQ): called from process %d\n", current->pid))
      break;
    }
  }   
  return(0);
}


/*********************************************************************************************/
static int robin_write_procmem(struct file *file, const char *buffer, u_long count, void *data)
/*********************************************************************************************/
{
  int len;
  struct robin_proc_data_t *fb_data = (struct robin_proc_data_t *)data;

  kdebug(("robin(robin_write_procmem): robin_write_procmem called\n"));

  if(count > 99)
    len = 99;
  else
    len = count;

  if (copy_from_user(fb_data->value, buffer, len))
  {
    kerror(("robin(robin_write_procmem): error from copy_from_user\n"));
    return(-EFAULT);
  }

  kdebug(("robin(robin_write_procmem): len = %d\n", len));
  fb_data->value[len - 1] = '\0';
  kdebug(("robin(robin_write_procmem): text passed = %s\n", fb_data->value));

  if (!strcmp(fb_data->value, "debug"))
  {
    debug = 1;
    kdebug(("robin(robin_write_procmem): debugging enabled\n"));
  }

  if (!strcmp(fb_data->value, "nodebug"))
  {
    kdebug(("robin(robin_write_procmem): debugging disabled\n"));
    debug = 0;
  }

  if (!strcmp(fb_data->value, "elog"))
  {
    kdebug(("robin(robin_write_procmem): Error logging enabled\n"))
    errorlog = 1;
  }

  if (!strcmp(fb_data->value, "noelog"))
  {
    kdebug(("robin(robin_write_procmem): Error logging disabled\n"))
    errorlog = 0;
  }
  
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,0)
  if (!strcmp(fb_data->value, "dec"))
  {
    MOD_DEC_USE_COUNT;
    kdebug(("robin(robin_write_procmem): Use count decremented\n")); 
  }
  
  if (!strcmp(fb_data->value, "inc"))
  {
    MOD_INC_USE_COUNT;
    kdebug(("robin(robin_write_procmem): Use count incremented\n")); 
  }
#endif
  
  return len;
}


/***************************************************************************************************/
static int robin_read_procmem(char *buf, char **start, off_t offset, int count, int *eof, void *data)
/***************************************************************************************************/
{
  u_int card, data;
  int len = 0, loop1, loop2;
  
  len += sprintf(buf + len, "\n\n\nROBIN driver for release %s (based on CVS tag %s)\n", RELEASE_NAME, CVSTAG);
  //len += sprintf(buf + len, "\n\n\nROBIN driver for release EXPERIMENTAL2\n\n");
  if  (opid)
    len += sprintf(buf + len, "The drivers is currently used by PID %d\n", opid);


  len += sprintf(buf + len, "card |     LAS0RR |     LAS0BA |       DMRR |     DMLBAM |     DMPBAM |      CNTRL |     INTCSR |\n");
  for (card = 0; card < maxcard; card++)
  {
    len += sprintf(buf + len, "%4d |", card);
    len += sprintf(buf + len, " 0x%08x |", robincard[card].plx_regs[PLX_LAS0RR]);
    len += sprintf(buf + len, " 0x%08x |", robincard[card].plx_regs[PLX_LAS0BA]);
    len += sprintf(buf + len, " 0x%08x |", robincard[card].plx_regs[PLX_DMRR]);
    len += sprintf(buf + len, " 0x%08x |", robincard[card].plx_regs[PLX_DMLBAM]);
    len += sprintf(buf + len, " 0x%08x |", robincard[card].plx_regs[PLX_DMPBAM]);
    len += sprintf(buf + len, " 0x%08x |", robincard[card].plx_regs[PLX_INTCSR]);
    len += sprintf(buf + len, " 0x%08x |\n", robincard[card].plx_regs[PLX_CNTRL]);
  }
  
  len += sprintf(buf + len, "maxcard = %d\n", maxcard);

  data = readFpga(0, 0);
  len += sprintf(buf + len, "FPGA(readFpga) = 0x%08x\n", data);


  len += sprintf(buf + len, "\nDumping status of Message FIFOs\n");
  for(loop1 = 0; loop1 < maxcard; loop1++)
  {
    len += sprintf(buf + len,"There are %d FIFO slots available on card %d\n", msg_fifo_avail[loop1], loop1);
  }

  len += sprintf(buf + len, "\nDumping status of Dual Ported Memory\n");
  len += sprintf(buf + len,"Index | Status |      Start |       Size\n");
  for(loop1 = 0; loop1 < maxcard; loop1++)
  {
    len += sprintf(buf + len, "Card %d\n", loop1);
    for(loop2 = 0; loop2 < MAX_BLOCKLETS; loop2++)
    {
      len += sprintf(buf + len,"%5d |", loop2);
      len += sprintf(buf + len,"%7d |", blocklets[loop1][0][loop2].used);
      len += sprintf(buf + len," 0x%08x |", blocklets[loop1][0][loop2].start);
      len += sprintf(buf + len," 0x%08x\n", blocklets[loop1][0][loop2].size);
    }
  }

  len += sprintf(buf + len,"\nDumping status of PC direct-DMA Memory\n");
  len += sprintf(buf + len,"Index | Status |      Start |       Size\n");
  for(loop1 = 0; loop1 < maxcard; loop1++)
  {
    len += sprintf(buf + len, "Card %d\n", loop1);
    for(loop2 = 0; loop2 < MAX_BLOCKLETS; loop2++)
    {
      len += sprintf(buf + len,"%5d |", loop2);
      len += sprintf(buf + len,"%7d |", blocklets[loop1][1][loop2].used);
      len += sprintf(buf + len," 0x%08x |", blocklets[loop1][1][loop2].start);
      len += sprintf(buf + len," 0x%08x\n", blocklets[loop1][1][loop2].size);
    }
  }

  len += sprintf(buf + len, " \n");
  len += sprintf(buf + len, "The command 'echo <action> > /proc/robin', executed as root,\n");
  len += sprintf(buf + len, "allows you to interact with the driver. Possible actions are:\n");
  len += sprintf(buf + len, "debug   -> enable debugging\n");
  len += sprintf(buf + len, "nodebug -> disable debugging\n");
  len += sprintf(buf + len, "elog    -> Log errors to /var/log/message\n");
  len += sprintf(buf + len, "noelog  -> Do not log errors to /var/log/message\n");
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,0)
  len += sprintf(buf + len, "dec     -> Decrement use count\n");
  len += sprintf(buf + len, "inc     -> Increment use count\n");
#endif

  *eof = 1;
  *start = buf + offset;
  len -= offset;

  if (len > count)
    len = count;
  return (len);
}


//------------------
// Service functions
//------------------


/******************************************************************/
static int robin_mmap(struct file *file, struct vm_area_struct *vma)
/******************************************************************/
{
  u_long offset, size;

  kdebug(("robin(robin_mmap): function called\n"));
  
  offset = vma->vm_pgoff << PAGE_SHIFT;
  
  size = vma->vm_end - vma->vm_start;

  kdebug(("robin(robin_mmap): offset = 0x%08x\n",(u_int)offset));
  kdebug(("robin(robin_mmap): size   = 0x%08x\n",(u_int)size));

  if (offset & ~PAGE_MASK)
  {
    kerror(("robin(robin_mmap): offset not aligned: %ld\n", offset));
    return(-ENXIO);
  }

  // we only support shared mappings. "Copy on write" mappings are
  // rejected here. A shared mapping that is writeable must have the
  // shared flag set.
  if ((vma->vm_flags & VM_WRITE) && !(vma->vm_flags & VM_SHARED))
  {
    kerror(("robin(robin_mmap): writeable mappings must be shared, rejecting\n"));
    return(-EINVAL);
  }

  vma->vm_flags |= VM_RESERVED;
  
  // we do not want to have this area swapped out, lock it
  vma->vm_flags |= VM_LOCKED;

  // we create a mapping between the physical pages and the virtual
  // addresses of the application with remap_page_range.
  // enter pages into mapping of application
  kdebug(("robin(robin_mmap): Parameters of remap_page_range()\n"));
  kdebug(("robin(robin_mmap): Virtual address  = 0x%08x\n",(u_int)vma->vm_start));
  kdebug(("robin(robin_mmap): Physical address = 0x%08x\n",(u_int)offset));
  kdebug(("robin(robin_mmap): Size             = 0x%08x\n",(u_int)size));
  if (remap_page_range(vma, vma->vm_start, offset, size, vma->vm_page_prot))
  {
    kerror(("robin(robin_mmap): remap page range failed\n"));
    return -ENXIO;
  }
  
  vma->vm_ops = &robin_vm_ops;  
  
  kdebug(("robin(robin_mmap): function done\n"));
  return(0);
}


/**************************************************/
static void robin_vclose(struct vm_area_struct *vma)
/**************************************************/
{  
  kdebug(("robin(robin_vclose): Virtual address  = 0x%08x\n",(u_int)vma->vm_start));
  kdebug(("robin(robin_vclose): mmap released\n"));
}


/****************************************************/
static int initmem(u_int card, u_int size, u_int base)
/****************************************************/
{
  int loop1, loop2;

  kdebug(("robin(initmem): Initializing memory management structures for card %d\n", card));

  for(loop1 = 0; loop1 < BLOCK_TYPES; loop1++)
  {
    for(loop2 = 0; loop2 < MAX_BLOCKLETS; loop2++)
    {
      blocklets[card][loop1][loop2].used = BLOCKLET_UNUSED;
      blocklets[card][loop1][loop2].start = 0;
      blocklets[card][loop1][loop2].size = 0;
    }
  }

  //Initialise the DPM buffer
  blocklets[card][0][0].used = BLOCKLET_FREE;
  blocklets[card][0][0].start = 0;
  blocklets[card][0][0].size = MESSAGE_RAM_SIZE;  

  //Initialise the PC MEM buffer
  blocklets[card][1][0].used = BLOCKLET_FREE;
  blocklets[card][1][0].start = base;
  blocklets[card][1][0].size = size;
  
  msg_fifo_avail[card] = MESSAGE_FIFO_DEPTH;
  kdebug(("robin(initmem):  msg_fifo_avail has been initialised to %d\n", MESSAGE_FIFO_DEPTH));

  return(0);
}


/************************************************************************************/
static int getmem(u_int card, u_int type, u_int size, u_int *start_add, u_int *handle)
/************************************************************************************/
{
  int loop, b1 = -1 , b2 = -1;

  kdebug(("robin(getmem): called with card=%d  type=%d  size=%d\n", card, type, size));

  kdebug(("robin(getmem): looking for space...\n"));
  //look for a large enough free blocklet  
  for(loop = 0; loop < MAX_BLOCKLETS; loop++)
  {
    kdebug(("robin(getmem): blocklets[%d][%d][%d].used = %d\n", card, type, loop, blocklets[card][type][loop].used));
    kdebug(("robin(getmem): blocklets[%d][%d][%d].size = %d\n", card, type, loop, blocklets[card][type][loop].size));
    if (blocklets[card][type][loop].used == BLOCKLET_FREE && blocklets[card][type][loop].size >= size)
    {
      b1 = loop;
      break;
    }  
  }
  
  kdebug(("robin(getmem): looking for free slot...\n"));
  //look for an empty slot in the array to hold the new blocklet
  for(loop = 0; loop < MAX_BLOCKLETS; loop++)
  {
    kdebug(("robin(getmem): blocklets[%d][%d][%d].used = %d\n", card, type, loop, blocklets[card][type][loop].used));
    if (blocklets[card][type][loop].used == BLOCKLET_UNUSED)
    {
      b2 = loop;
      break;
    }  
  } 

  if (b1 == -1)
  {
    kerror(("robin(getmem): failed to find free memory\n"))
    return(-RD_NOMEMORY);
  }
    
  if (b2 == -1)
  {
    kerror(("robin(getmem): failed to find an enpty slot\n"))
    return(-RD_NOSLOT);     
  }
 
  blocklets[card][type][b2].used = BLOCKLET_USED;
  blocklets[card][type][b2].size = size;
  blocklets[card][type][b2].start = blocklets[card][type][b1].start;  

  blocklets[card][type][b1].size -= size;
  if (blocklets[card][type][b1].size == 0)
  {
    blocklets[card][type][b1].used = BLOCKLET_UNUSED;
    blocklets[card][type][b1].start = 0;
  }
  else
    blocklets[card][type][b1].start += size;
  
  *start_add = blocklets[card][type][b2].start;
  *handle = b2;

  return(0);
}


/*****************************************************/
static int retmem(u_int card, u_int type, u_int handle)
/*****************************************************/
{
  int loop, bprev = -1,  bnext = -1;
  
  kdebug(("robin(retmem): called with card=%d  type=%d  handle=%d\n", card, type, handle));
  if (blocklets[card][type][handle].used != BLOCKLET_USED)
    return(-RD_ILLHANDLE);
 
  blocklets[card][type][handle].used = BLOCKLET_FREE;
    
  //Check if the blocklet can be merged with a predecessor   
  for(loop = 0; loop < MAX_BLOCKLETS; loop++)
  {
    if (blocklets[card][type][loop].used == BLOCKLET_FREE && (blocklets[card][type][loop].start + blocklets[card][type][loop].size) == blocklets[card][type][handle].start)
    {
      bprev = loop;
      break;
    }
  }
  
  //Check if the blocklet can be merged with a sucessor   
  for(loop = 0; loop < MAX_BLOCKLETS; loop++)
  {
    if (blocklets[card][type][loop].used == BLOCKLET_FREE && (blocklets[card][type][handle].start + blocklets[card][type][handle].size) == blocklets[card][type][loop].start)
    {
      bnext = loop;
      break;
    }
  }
  
  if (bprev != -1)
  {
    kdebug(("robin(retmem): merging %d with predecessor %d\n", handle, bprev)); 
    blocklets[card][type][handle].start = blocklets[card][type][bprev].start;
    blocklets[card][type][handle].size += blocklets[card][type][bprev].size;
    blocklets[card][type][bprev].used = BLOCKLET_UNUSED;
    blocklets[card][type][bprev].start = 0;
    blocklets[card][type][bprev].size = 0;
  }
  
  if (bnext != -1)
  {
    kdebug(("robin(retmem): merging %d with sucessor %d\n", handle, bnext)); 
    blocklets[card][type][handle].size += blocklets[card][type][bnext].size;
    blocklets[card][type][bnext].used = BLOCKLET_UNUSED;
    blocklets[card][type][bnext].start = 0;
    blocklets[card][type][bnext].size = 0;
  }
  return(0);
}


/**********************************************************/
static int readFpga(int card, unsigned int addressOffset)
/**********************************************************/
{
  int value = 0;


  if ((robincard[card].fpga_regs != NULL) && (addressOffset < robincard[card].pci_memsize_bus)) {
    void * address;
    address = (void * )(robincard[card].fpga_regs + addressOffset);
    if (fpgaLoaded(card) != 0) {
      value = ioread32(address);
      kerror(("robin(readFpga): reading now!\n"))
      
    }
    return value;
  }
  else {
    return 0;
  }

}


/********************************************************************/
static void writeFpga(int card, unsigned int addressOffset, int data)
/********************************************************************/
{

  if ((robincard[card].fpga_regs != NULL) && (addressOffset < robincard[card].pci_memsize_bus)) {
    void * address;
    address = (void * )(robincard[card].fpga_regs + addressOffset);
    if (fpgaLoaded(card)) {
      iowrite32(data, robincard[card].fpga_regs + addressOffset);
    }
  }

}


/*******************************/
static int fpgaLoaded(int card)
/*******************************/
{

  
  if ((card < maxcard) && (robincard[card].fpgaConfigured == 1)) {
    
    int lPlxLBRD0Reg, lPlxLBRD0RegBackup;
    int value, alive;

    /*
    lPlxLBRD0Reg = ioread32((void *) (robincard[card].plx_regs + (0x18 / 4)));
    lPlxLBRD0RegBackup = lPlxLBRD0Reg;

    lPlxLBRD0Reg &= ~(0x7C);
    lPlxLBRD0Reg |= 0x04;
    iowrite32(lPlxLBRD0Reg, (void *) (robincard[card].plx_regs + (0x18 / 4)));
    */

    value = ioread32((void *) (robincard[card].fpga_regs));

    //    iowrite32(lPlxLBRD0RegBackup, (void *) (robincard[card].plx_regs + (0x18 / 4)));
    
    if (value != DESIGN_ID) {
      kerror(("robin(fpgaLoaded): Found wrong design in FPGA. Expected design ID: 0x%x, found 0x%x\n", DESIGN_ID, value));
      robincard[card].fpgaConfigured = 0;
      return 0;
    }
    else {
      return 1;
    }
    
  }
  else {
    return 0;
  } 


}

/****************************/
static int getCardNum(void) 
/****************************/
{
  return maxcard;
}

EXPORT_SYMBOL(readFpga);
EXPORT_SYMBOL(writeFpga);
EXPORT_SYMBOL(fpgaLoaded);
EXPORT_SYMBOL(getCardNum);



